<?php

/**
 * This is the model class for table "AuthItem".
 *
 * The followings are the available columns in table 'AuthItem':
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 *
 * The followings are the available model relations:
 * @property AuthAssignment[] $authAssignments
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren1
 */
class AuthItem extends CActiveRecord {

    public $_action;
    public $_assign_type = 'role';
    public $_prev_assign_type = '';
    public $_assign_type_options = array('role' => 'Add/Remove Roles', 'task' => 'Add/Remove Tasks');
    public $_selected_role;
    public $_roles_options = array();
    public $_inherited_roles = array();
    public $_available_roles = array();
    public $_inherited_tasks = array();
    public $_available_tasks = array();
    public $_inherited_item = array();
    public $_inherited_items = array();
    public $_available_item = array();
    public $_available_items = array();

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AuthItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'AuthItem';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('name, type', 'required', 'on' => 'insert, update'),
            array('type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 64),
            array('name', 'unique'),
            array('description, bizrule, data', 'safe'),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('name, type, description, bizrule, data', 'safe', 'on' => 'search'),
            array('name, _inherited_items', 'required', 'on' => 'admin-create'),
            array('type', 'default', 'value' => 2, 'on' => 'admin-create'),
            array('name, _assign_type, _prev_assign_type, _inherited_items, _available_items, _action', 'safe', 'on' => 'admin-create'),
            array('_assign_type, _prev_assign_type, _selected_role, _inherited_items, _available_items, _action', 'safe', 'on' => 'admin-update'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'authAssignments' => array(self::HAS_MANY, 'AuthAssignment', 'itemname'),
            'authItemChildren' => array(self::HAS_MANY, 'AuthItemChild', 'parent'),
            'authItemChildren1' => array(self::HAS_MANY, 'AuthItemChild', 'child'),
                //'users' => array(self::HAS_MANY, 'User', 'user_role')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
            '_roles' => 'Roles',
            '_assign_type' => 'Assign',
            '_inherited_items' => 'Inherited Items',
            '_available_items' => 'Available Items'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('bizrule', $this->bizrule, true);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {

        // set default description, this happend for admin-create scenario only, 
        // because the user does not provide description
        if ($this->scenario == 'admin-create') {
            // set description to the name specified by the user, ucwords capitalize the first letter
            $this->description = ucwords($this->name);
            $this->name = ucwords($this->name);
        }

        return parent::beforeSave();
    }

    public function afterSave() {

        if ($this->scenario == 'admin-create') {
            // set inherited items
            // save inherited roles
            $this->_prev_assign_type = '';
            $this->_assign_type = 'role';
            $this->_setInheritedItems();

            foreach ($this->_inherited_items as $child) {
                $child_role = new AuthItemChild;
                $child_role->parent = $this->name;
                $child_role->child = $child;
                $child_role->save();
            }

            // save inherited task
            $this->_prev_assign_type = '';
            $this->_assign_type = 'task';
            $this->_setInheritedItems();

            foreach ($this->_inherited_items as $child) {
                $child_op = new AuthItemChild;
                $child_op->parent = $this->name;
                $child_op->child = $child;
                $child_op->save();
            }

            // if user has createrole permission
            if (User::model()->isAuthorized('CreateRole')) {
                // set the current role as child role of the top role of the
                $creator_id = Yii::app()->user->id;
                $auth = Yii::app()->authManager;
                $inherited_roles = $auth->getRoles($creator_id);
                
                // get the first role
                foreach($inherited_roles as $item){
                    $top_role = $item->name;
                    break;
                }
                
                $child_op = new AuthItemChild;
                $child_op->parent = $top_role;
                $child_op->child = $this->name;
                $child_op->save();
                
            }
        }

        return parent::afterSave();
    }

    public function scopes() {
        return array(
            'userRoleForList' => array(
                'select' => "name, description",
                'condition' => "type = 2",
                'order' => ''
            ),
            'facAdminRoleForList' => array(
                'select' => "name, description",
                'condition' => "name = 'Cust_Admin' OR name = 'Cust_User'",
                'order' => ''
            ),
            'userOperationForList' => array(
                'select' => "name, description",
                'condition' => "type = 0",
                'order' => ''
            ),
        );
    }

// end function scope

    public function getAllRoles() {

        $results = AuthItem::model()->findAllByAttributes(array('type' => 2));

        if (empty($results)) {
            $roles = array();
        } else {
            $roles = array();
            foreach ($results as $row) {
                $roles[$row->name] = $row->description;
            }
        }

        return $roles;
    }

    public function getAllOperations() {

        //$results = AuthItem::model()->findAllByAttributes(array('type' => 0));
        $results = AuthItem::model()->findAll('type = 0 OR type = 1');

        if (empty($results)) {
            $operations = array();
        } else {
            $operations = array();
            foreach ($results as $row) {
                $operations[$row->name] = $row->description;
            }
        }

        return $operations;
    }

    public function getChildItem($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "'";
        $childs = $cmd->query();

        if (empty($childs)) {
            $childs = array();
        }

        return $childs;
    }

    public function getChildRoles($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "' AND i.type = 2";
        $cmd->order = "i.order asc";
        $roles = $cmd->query();

        if (empty($roles)) {
            $roles = array();
        }

        return $roles;
    }

    public function getChildOperations($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "' AND (i.type = 0 OR i.type = 1)";
        $operations = $cmd->query();

        if (empty($operations)) {
            $operations = array();
        }
        return $operations;
    }

    public function _setAvailableItems() {

        // set the available items for gor or operations
        $available_items = array();

        switch ($this->_assign_type) {
            case 'role':
                // get all available roles from authitem, type = 2
                // _available_roles is needed as master list on _prepareInheritedItems function
                $this->_available_roles = $this->getAllRoles();

                // check of unavailable items, items maybe assigned already
                foreach ($this->_available_roles as $name => $desc) {

                    if (!in_array($name, $this->_inherited_items)) {
                        $available_items[$name] = $desc;
                    }
                }

                break;
            case 'task':
                // get all available operations and task from authitem, type = 0 or type = 1,
                $this->_available_tasks = $this->getAllOperations();
                // _available_tasks is needed as master list on _prepareInheritedItems function
                // check of unavailable items, items maybe assigned already
                foreach ($this->_available_tasks as $name => $desc) {
                    if (!in_array($name, $this->_inherited_items)) {
                        $available_items[$name] = $desc;
                    }
                }

                break;
        }

        $this->_available_items = $available_items;

        if ($this->_assign_type != $this->_prev_assign_type) {
            // prepare inherited items only when the prev assign type is not the same to current assign type
            // this will happen when there is an existing inherited items set before,
            // because only authitme codes are save, it does not incldue the description
            // ex. only the code FacilityAdmin in the _inherited_items array, does not include the description Facility Admin
            $this->_prepareInheritedItems();
        }
    }

    public function _saveInheritedItems() {
        // get existing user assignments from the session
        $inherited_items = Yii::app()->session->itemAt('inherited_items'); // gets value

        if ($inherited_items == null) {
            //set inherited items structure if there ar no existing inherited items on the session
            $inherited_items = array('role' => array(), 'task' => array());
        }

        // use the previous assign operation in assigning the assigned items, 
        // since the assign_op could be change when the user changes the dropdown menu
        // _inherited_items, its values are set from the form
        switch ($this->_prev_assign_type) {
            case 'role':
                $inherited_items['role'] = $this->_inherited_items;
                break;
            case 'task':
                $inherited_items['task'] = $this->_inherited_items;
                break;
        }


        Yii::app()->session->add('inherited_items', $inherited_items);
    }

    public function _setInheritedItems() {

        if ($this->_assign_type != $this->_prev_assign_type) {
            // if current selected assign op is not the same to previous
            // change the current assign item from the session, previosuly assigned if existing
            $inherited_items = Yii::app()->session->itemAt('inherited_items'); // gets value

            switch ($this->_assign_type) {
                case 'role':
                    $this->_inherited_items = $inherited_items['role'];
                    break;
                case 'task':
                    $this->_inherited_items = $inherited_items['task'];
                    break;
            }
        }
    }

    public function _prepareInheritedItems() {
        // modify the item list to contain the description
        // from the master list
        $new_items = array();
        $master_list = array();

        switch ($this->_assign_type) {
            case 'role':
                $master_list = $this->_available_roles;
                break;
            case 'task':
                $master_list = $this->_available_tasks;
                break;
        }

        foreach ($this->_inherited_items as $code) {
            $new_items[$code] = $master_list[$code];
        }

        $this->_inherited_items = $new_items;
    }

    public function setRolesOptions() {
        // get all roles on auth item, type  = 2
        $this->_roles_options = $this->getAllRoles();
    }

    public function setInheritedItems() {

        $inherited_items = array();

        switch ($this->_assign_type) {
            case 'role':
                // get direct child roles
                $results = $this->getChildRoles($this->_selected_role);
                $child_roles = array();
                $inherited_roles = array();

                foreach ($results as $row) {
                    $child_roles[$row['name']] = $row['description'];
                    //get inherited child roles
                    AuthAssignment::model()->setChildRoles($row['name'], $inherited_roles);
                }

                $this->_roles_options = array_merge($child_roles, $inherited_roles);

                $inherited_items = $this->_roles_options;

                break;
            case 'task':
                // get all roles
                $this->_assign_type = 'role';
                $this->setInheritedItems();
                $roles = $this->_roles_options;

                // add own user role    
                $roles[$this->_selected_role] = $this->_selected_role;

                $child_operations = array();
                $inherited_operations = array();

                foreach ($roles as $role => $desc) {

                    $results = $this->getChildOperations($role);

                    foreach ($results as $row) {
                        $child_operations[$row['name']] = $row['description'];
                        //get inherited child roles
                        AuthAssignment::model()->setChildOperations($row['name'], $inherited_operations);
                    }
                }

                $inherited_items = array_merge($child_operations, $inherited_operations);

                // arrange alphabetically the operations and task
                ksort($inherited_items);

                $this->_assign_type = 'task';

                break;
        }

        $this->_inherited_items = $inherited_items;
    }

    public function setAvailableItems() {

        // available items base on selected role
        // do not process available items when there is no selected role
        if ($this->_selected_role == null) {
            return;
        }

        $available_items = array();

        switch ($this->_assign_type) {
            case 'role':
                // get all available roles from authitem, type = 2
                $this->_available_roles = $this->getAllRoles();

                // check of unavailable items, items maybe assigned already
                foreach ($this->_available_roles as $name => $desc) {

                    if (!array_key_exists($name, $this->_inherited_items)) {
                        $available_items[$name] = $desc;
                    }
                }
                // exclude the selected role from the list
                unset($available_items[$this->_selected_role]);

                break;
            case 'task':
                // get all available operations from authitem, type = 0
                $this->_available_tasks = $this->getAllOperations();

                // check of unavailable items, items maybe assigned already
                foreach ($this->_available_tasks as $name => $desc) {
                    if (!array_key_exists($name, $this->_inherited_items)) {
                        $available_items[$name] = $desc;
                    }
                }

                break;
        }

        $this->_available_items = $available_items;
    }

    public function saveUpdatedAssignments() {

        // save inherited roles and oeprations
        // stop process when there is no seleted role
        if ($this->_selected_role == null) {
            return false;
        }

        // delete previous role assignment, child on authitmechild table
        AuthItemChild::model()->deleteAllByAttributes(array('parent' => $this->_selected_role));

        // stop process when there is no selected role and no inherited items selected,
        // in these case, just remove the previously assigned child items, inherited items, such as roles and operations
        if ($this->_selected_role == null || $this->_inherited_items == null) {
            return false;
        }

        // add child or inherited items to authitemchild table    
        foreach ($this->_inherited_items as $child) {
            $child_role = new AuthItemChild;
            $child_role->parent = $this->_selected_role;
            $child_role->child = $child;
            $child_role->save();
        }

        return true;
    }

    function _prepareJsonData($data) {

        $new_data = array();

        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $new_data[] = array('key' => $key, 'val' => $val);
            }
        } else {
            $new_data = $data;
        }

        return $new_data;
    }

}