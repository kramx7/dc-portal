<?php

/**
 * This is the model class for table "sensor".
 *
 * The followings are the available columns in table 'sensor':
 * @property integer $sensor_id
 * @property integer $facility_id
 * @property integer $area_id
 * @property integer $client_id
 * @property string $sensor_description
 * @property string $modified_time
 */
class Sensor extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Sensor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sensor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('modified_time', 'required'),
            array('facility_id, area_id, client_id', 'numerical', 'integerOnly' => true),
            array('sensor_description', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('sensor_id, facility_id, area_id, client_id, sensor_description, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sensor_id' => 'Sensor',
            'facility_id' => 'Facility',
            'area_id' => 'Area',
            'client_id' => 'Client',
            'sensor_description' => 'Sensor Description',
            'modified_time' => 'Modified Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('sensor_id', $this->sensor_id);
        $criteria->compare('facility_id', $this->facility_id);
        $criteria->compare('area_id', $this->area_id);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('sensor_description', $this->sensor_description, true);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSensorsByFacilityId($facility_id) {

        $query = "SELECT sensor.sensor_id, sensor.sensor_description
		FROM sensor join client_sensor ON sensor.sensor_id = client_sensor.sensor_id
		WHERE client_sensor.facility_id = :facility_id";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $result = $cmd->query();

        $sensors = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $sensors[$row['sensor_id']] = $row['sensor_description'];
            }
        }

        return $sensors;
    }

    public function getSensorsByAllFacilities($facilities) {
        
        if(count($facilities) <= 0){
            // if facilities does not contain any element
            return array();
        }
        
        $query = "SELECT sensor.sensor_id, sensor.sensor_description
		FROM sensor join client_sensor ON sensor.sensor_id = client_sensor.sensor_id";

        $cmd = Yii::app()->db->createCommand($query);
        
        $where_string = '';
        foreach($facilities as $code => $desc){
           $where_string .= " client_sensor.facility_id = ".$code." OR ";
        }
        
        $where_string = rtrim($where_string,' OR ');
        $cmd->where = $where_string;
        $result = $cmd->query();

        $sensors = array();

        if (!empty($result)) {
            foreach ($result as $row) {
               $sensors[$row['sensor_id']] = $row['sensor_description'];
            }
        }

        return $sensors;
    }

    public function getSensorsByFacilityIdAndClients($facility_id, $clients) {

        $query = "SELECT sensor.sensor_id, sensor.sensor_description
        FROM sensor join client_sensor ON sensor.sensor_id = client_sensor.sensor_id
        WHERE client_sensor.facility_id = :facility_id AND client_sensor.client_id IN(:clients);";

        $clients_value = implode(',', array_keys($clients));

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $sensors = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $sensors[$row['sensor_id']] = $row['sensor_description'];
            }
        }

        return $sensors;
    }

    public function getSensorsByAllFacilitiesAndClients($facilities, $clients) {
        
        if(count($facilities) <= 0){
            // if facilities does not contain any element
            return array();
        }
        
        $query = "SELECT sensor.sensor_id, sensor.sensor_description
        FROM sensor join client_sensor ON sensor.sensor_id = client_sensor.sensor_id
        WHERE client_sensor.facility_id IN(:facilities) AND client_sensor.client_id IN(:clients); 
        ";

        $facilities_value = implode(',', array_keys($facilities));
        $clients_value = implode(',', array_keys($clients));

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facilities',$facilities_value);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $sensors = array();

        if (!empty($result)) {
            foreach ($result as $row) {
               $sensors[$row['sensor_id']] = $row['sensor_description'];
            }
        }

        return $sensors;
    }

}