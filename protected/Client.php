<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $record_id
 * @property integer $client_id
 * @property string $client_name
 * @property string $modified_time
 */
class Client extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Client the static model class
     */
    public $_facility_id = null;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'client';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('client_name', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('client_name', 'length', 'max' => 50),
            array('modified_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, client_id, client_name, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                //'facility' => array(self::MANY_MANY, 'Client','client_facility(client_id, facility_id)')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'client_name' => 'Client Name',
            'modified_time' => 'Modified Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('client_name', $this->client_name, true);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function scopes() {

        $scopes = array(
            'clientForList' => array(
                'select' => 'id, client_name',
                'order' => 'client_name ASC'
            )
        );

        if ($this->_facility_id != null) {
            $scopes['clientForList'] = array(
                'select' => 'client.id as client_id, client.client_name',
                'condition' => "client_facility.facility_id = " . $this->_facility_id,
                'alias' => 'client',
                'join' => 'LEFT JOIN client_facility on client.id = client_facility.client_id',
                'order' => 'client.client_name ASC',
            );
        }

        return $scopes;
    }

    public function getClients() {

        $criteria = new CDbCriteria;
        //$criteria->with = array('itemname0');
        //$criteria->condition = " t.userid = ".$user_id." AND itemname0.type = 2";
        $clients = Client::model()->findAll();

        if (empty($clients)) {
            $clients = array();
        }

        return $clients;
    }

    public function getClientById($client_id) {

        $client = Client::model()->findByAttributes(array('id' => $client_id));

        if (!empty($client)) {
            return $client;
        } else {
            return null;
        }
    }

}