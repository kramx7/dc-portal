<?php

/**
 * This is the model class for table "power_statistics".
 *
 * The followings are the available columns in table 'power_statistics':
 * @property integer $record_id
 * @property integer $facility_id
 * @property integer $client_id
 * @property integer $target_id
 * @property string $target_description
 * @property string $Timestamp
 * @property string $kWh
 * @property string $kVATotal
 * @property string $kWTotal
 * @property string $Frequency
 * @property string $lc
 * @property string $lb
 * @property string $la
 * @property string $kVllca
 * @property string $kVllbc
 * @property string $kVllab
 */
class PowerStatistics extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PowerStatistics the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'power_statistics';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_id, client_id, target_id', 'required'),
            array('facility_id, client_id, target_id', 'numerical', 'integerOnly' => true),
            array('target_description', 'length', 'max' => 255),
            array('kWh', 'length', 'max' => 20),
            array('kVATotal, kWTotal, lc, lb, la, kVllca, kVllbc, kVllab', 'length', 'max' => 8),
            array('Frequency', 'length', 'max' => 4),
            array('Timestamp', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, facility_id, client_id, target_id, target_description, Timestamp, kWh, kVATotal, kWTotal, Frequency, lc, lb, la, kVllca, kVllbc, kVllab', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'record_id' => 'Record',
            'facility_id' => 'Facility',
            'client_id' => 'Client',
            'target_id' => 'Target',
            'target_description' => 'Target Description',
            'Timestamp' => 'Timestamp',
            'kWh' => 'K Wh',
            'kVATotal' => 'K Vatotal',
            'kWTotal' => 'K Wtotal',
            'Frequency' => 'Frequency',
            'lc' => 'Lc',
            'lb' => 'Lb',
            'la' => 'La',
            'kVllca' => 'K Vllca',
            'kVllbc' => 'K Vllbc',
            'kVllab' => 'K Vllab',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('record_id', $this->record_id);
        $criteria->compare('facility_id', $this->facility_id);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('target_id', $this->target_id);
        $criteria->compare('target_description', $this->target_description, true);
        $criteria->compare('Timestamp', $this->Timestamp, true);
        $criteria->compare('kWh', $this->kWh, true);
        $criteria->compare('kVATotal', $this->kVATotal, true);
        $criteria->compare('kWTotal', $this->kWTotal, true);
        $criteria->compare('Frequency', $this->Frequency, true);
        $criteria->compare('lc', $this->lc, true);
        $criteria->compare('lb', $this->lb, true);
        $criteria->compare('la', $this->la, true);
        $criteria->compare('kVllca', $this->kVllca, true);
        $criteria->compare('kVllbc', $this->kVllbc, true);
        $criteria->compare('kVllab', $this->kVllab, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getMetersByClientId($client_id) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "DISTINCT (p.target_id), p.target_description";
        $cmd->from('power_statistics as p');
        $cmd->where = 'p.client_id =  ' . $client_id;
        $cmd->order = "p.target_id asc";
        $result = $cmd->query();

        $meters = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $meters[$row['target_id']] = $row['target_description'];
            }
        }

        return $meters;
    }

    public function getMetersByAllFacilities($facilities) {

        if (count($facilities) <= 0) {
            // if supplied array facilities is empty, no need to query from the database
            return array();
        }

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "DISTINCT (p.target_id), p.target_description";
        $cmd->from('power_statistics as p');

        $where_string = "";
        foreach ($facilities as $code => $desc) {
            $where_string .= "p.facility_id = " . $code . " OR ";
        }

        // remove last OR operator
        $where_string = rtrim($where_string, ' OR ');

        $cmd->where = $where_string;
        $cmd->order = "p.target_id asc";
        $result = $cmd->query();

        $meters = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $meters[$row['target_id']] = $row['target_description'];
            }
        }

        return $meters;
    }

    public function getMetersByFacilityId($facility_id) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "DISTINCT (p.target_id), p.target_description";
        $cmd->from('power_statistics as p');
        $cmd->where = 'p.facility_id =  ' . $facility_id;
        $cmd->order = "p.target_id asc";
        $result = $cmd->query();

        $meters = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $meters[$row['target_id']] = $row['target_description'];
            }
        }

        return $meters;
    }

    public function getMetersByAllFacilitiesAndClients($facilities, $clients) {

        if (count($facilities) <= 0  && count($clients) >0) {
             $query = "SELECT DISTINCT (p.target_id), p.target_description 
                FROM power_statistics as p
                WHERE p.facility_id IN(:facilities) AND p.client_id IN(:clients)  
                ORDER BY p.target_id ASC 
                ;";
            $facilities_value = implode(',', array_keys($facilities));        
            $clients_value = implode(',', array_keys($clients));
        }else{
             $query = "SELECT DISTINCT (p.target_id), p.target_description 
                FROM power_statistics as p
                WHERE p.facility_id IN(:facilities) 
                ORDER BY p.target_id ASC  
                ;";
            $facilities_value = implode(',', array_keys($facilities));   
        }

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facilities',$facilites_value);
        if (count($clients) > 0 ) {
            $cmd->bindParam(':clients',$clients_value);
        }
        $result = $cmd->query();

        $meters = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $meters[$row['target_id']] = $row['target_description'];
            }
        }

        return $meters;
    }

    public function getMetersByFacilityIdAndClients($facility_id, $clients) {

        if ( count($clients) >0) {
             $query = "SELECT DISTINCT (p.target_id), p.target_description 
                FROM power_statistics as p
                WHERE p.facility_id = :facility_id AND p.client_id IN(:clients)  
                ORDER BY p.target_id ASC 
                ;";    
            $clients_value = implode(',', array_keys($clients));
        }else{
             $query = "SELECT DISTINCT (p.target_id), p.target_description 
                FROM power_statistics as p
                WHERE p.facility_id = :facility_id 
                ORDER BY p.target_id ASC  
                ;"; 
        }

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id',$facility_id);
        if (count($clients) > 0 ) {
            $cmd->bindParam(':clients',$clients_value);
        }
        $result = $cmd->query();

        $meters = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $meters[$row['target_id']] = $row['target_description'];
            }
        }

        return $meters;
    }

    public function getMeterName($target_id) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "DISTINCT (p.target_id), p.target_description";
        $cmd->from('power_statistics as p');
        $cmd->where = 'p.target_id =  ' . $target_id;
        $result = $cmd->query();
        $meter_name = '';

        if (!empty($result)) {
            foreach ($result as $row) {
                $meter_name = $row['target_description'];
            }
        } else {
            $meter_name = 'unknown meter name';
        }

        return $meter_name;
    }

    public function getKwByClientIdAndDatesAndTargetId($client_id, $from, $to, $target_id = 0) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, kWTotal as kw_total
		FROM power_statistics
		WHERE ( (client_id = :client_id) 
		AND (target_id = :target_id )
		AND ( Timestamp BETWEEN :from_date AND :to_date))
		GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
		ORDER BY UNIX_TIMESTAMP( Timestamp ) ASC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':client_id', $client_id);
        $cmd->bindParam(':target_id', $target_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total']);
            }
        }

        return $statistics;
    }

    public function getKwByClientIdAndDates($client_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(power_statistics.Timestamp) as tmp,sum(kWTotal) as kw_total, client_power_limit.kW_max as kw_max 
		FROM power_statistics
		JOIN client_power_limit on power_statistics.client_id=client_power_limit.client_id
		WHERE ((power_statistics.client_id = :client_id)
		AND ( Timestamp BETWEEN :from_date AND :to_date))
		GROUP BY floor(UNIX_TIMESTAMP(power_statistics.Timestamp)/300)
		ORDER BY UNIX_TIMESTAMP( power_statistics.Timestamp ) ASC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':client_id', $client_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total'], $row['kw_max']);
            }
        }

        return $statistics;
    }

    public function getKwByFacilityIdAndDatesAndTargetId($facility_id, $from, $to, $target_id = 0) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, kWTotal as kw_total
		FROM power_statistics
		WHERE ( (facility_id = :facility_id) 
		AND (target_id = :target_id )
		AND ( Timestamp BETWEEN :from_date AND :to_date))
		GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
		ORDER BY UNIX_TIMESTAMP( Timestamp ) ASC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $cmd->bindParam(':target_id', $target_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total']);
            }
        }

        return $statistics;
    }

    public function getKwByFacilityIdAndDates($facility_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(power_statistics.Timestamp) as tmp,sum(kWTotal) as kw_total, client_power_limit.kW_max as kw_max 
		FROM power_statistics
		JOIN client_power_limit on power_statistics.facility_id=client_power_limit.facility_id
		WHERE ((power_statistics.facility_id = :facility_id)
		AND ( Timestamp BETWEEN :from_date AND :to_date))
		GROUP BY floor(UNIX_TIMESTAMP(power_statistics.Timestamp)/300)
		ORDER BY UNIX_TIMESTAMP( power_statistics.Timestamp ) ASC";

        //echo $query;

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total'], $row['kw_max']);
            }
        }

        return $statistics;
    }

    public function getKwByFacilityIdAndDatesAndTargetIdAndClients($facility_id, $from, $to, $target_id = 0, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, kWTotal as kw_total
        FROM power_statistics
        WHERE ( (facility_id = :facility_id) 
        AND (target_id = :target_id )
        AND ( Timestamp BETWEEN :from_date AND :to_date))
        AND power_statistics.client_id IN(:clients)
        GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
        ORDER BY UNIX_TIMESTAMP( Timestamp ) ASC";

        $clients_value = implode(',', array_keys($clients));

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $cmd->bindParam(':target_id', $target_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $cmd->bindParam(':clients',$clients_value);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total']);
            }
        }

        return $statistics;
    }

    public function getKwByFacilityIdAndDatesAndClients($facility_id, $from, $to, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(power_statistics.Timestamp) as tmp,sum(kWTotal) as kw_total, client_power_limit.kW_max as kw_max 
        FROM power_statistics
        JOIN client_power_limit on power_statistics.facility_id=client_power_limit.facility_id
        WHERE ((power_statistics.facility_id = :facility_id)
        AND ( Timestamp BETWEEN :from_date AND :to_date))
        AND power_statistics.client_id IN(:clients)
        GROUP BY floor(UNIX_TIMESTAMP(power_statistics.Timestamp)/300)
        ORDER BY UNIX_TIMESTAMP( power_statistics.Timestamp ) ASC";

        $clients_value = implode(',', array_keys($clients));
        //echo $query;

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $cmd->bindParam(':clients',$clients_value);
        //$cmd->limit(100);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kw_total'], $row['kw_max']);
            }
        }

        return $statistics;
    }

    public function getNGERYearByFacilityId($facility_id) {

        $query = "SELECT DISTINCT (Year(YYYYMMDD)) as year
                FROM power_statistics_summary
                WHERE facility_id = :facility_id
		ORDER BY Year(YYYYMMDD) DESC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $result = $cmd->query();

        $years = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $years[$row['year']] = $row['year'];
            }
        }

        return $years;
    }

    public function getNGERYearByFacilityIdAndClients($facility_id, $clients) {

        $query = "SELECT DISTINCT (Year(YYYYMMDD)) as year
                FROM power_statistics_summary
                WHERE facility_id = :facility_id
                AND client_id IN(:clients)
        ORDER BY Year(YYYYMMDD) DESC";

        $clients_value = implode(',', array_keys($clients));

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $years = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $years[$row['year']] = $row['year'];
            }
        }

        return $years;
    }

    public function getNGERYearByAllFacilities($facilities) {

        $query = "SELECT DISTINCT (Year(YYYYMMDD)) as year
                FROM power_statistics_summary
		ORDER BY Year(YYYYMMDD) DESC";

        $cmd = Yii::app()->db->createCommand($query);
        $where_string = "";
        foreach ($facilities as $code => $desc) {
            $where_string .= "facility_id = " . $code . " OR ";
        }
        $where_string = rtrim($where_string, ' OR ');

        $cmd->where = $where_string;
        $result = $cmd->query();

        $years = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $years[$row['year']] = $row['year'];
            }
        }

        return $years;
    }

}