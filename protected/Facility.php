<?php

/**
 * This is the model class for table "facility".
 *
 * The followings are the available columns in table 'facility':
 * @property integer $record_id
 * @property integer $facility_id
 * @property string $facility_name
 * @property string $facility_tz
 * @property string $state
 * @property string $modified_time
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class Facility extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Facility the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'facility';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_name, facility_tz, state', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('facility_name, facility_tz', 'length', 'max' => 50),
            array('state', 'length', 'max' => 3),
            array('modified_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('facility_name, facility_tz, state, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users' => array(self::HAS_MANY, 'User', 'facility'),
            'noticeboards' => array(self::HAS_MANY, 'Noticeboard', 'facility_id'),
                //'client' => array(self::MANY_MANY, 'Facility','client_facility(facility_id, client_id)')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'facility_name' => 'Facility Name',
            'facility_tz' => 'Facility Tz',
            'state' => 'State',
            'modified_time' => 'Modified Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('facility_name', $this->facility_name, true);
        $criteria->compare('facility_tz', $this->facility_tz, true);
        $criteria->compare('state', $this->state, true);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function scopes() {
        return array(
            'facilityForList' => array(
                'select' => 'id as facility_id, facility_name',
                'order' => 'facility_name ASC'
            )
        );
    }

    public function getFacilityListById($id) {
        return Facility::model()->findAllByAttributes(array('id' => $id));
    }

    public function getFacilities() {

        $criteria = new CDbCriteria;
        //$criteria->with = array('itemname0');
        //$criteria->condition = " t.userid = ".$user_id." AND itemname0.type = 2";
        $facilities = Facility::model()->findAll();

        if (empty($facilities)) {
            $facilities = array();
        }

        return $facilities;
    }

    public function getCarbonByPowerDatesAndFacilityId($powers, $facility_id) {

        $carbons = array();

        foreach ($powers as $p) {

            $query = "SELECT CO2Factors.EFScope2A 
                FROM facility join CO2Factors ON facility.state = CO2Factors.State 
                WHERE :day between date(CO2Factors.starttimestamp) and date(CO2Factors.endtimestamp) 
                AND id = :facility_id"; 

            $cmd = Yii::app()->db->createCommand($query);
            $cmd->bindParam(':day', $p['day']);
            $cmd->bindParam(':facility_id', $facility_id);
            $result = $cmd->query();

            if (!empty($result)) {
                foreach ($result as $row) {
                    $carbons[$p['day']] = $row['EFScope2A'];
                }
            } else {
                $carbons[$p['day']] = 0;
            }
        }

        return $carbons;
    }

}