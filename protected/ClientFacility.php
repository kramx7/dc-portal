<?php

/**
 * This is the model class for table "client_facility".
 *
 * The followings are the available columns in table 'client_facility':
 * @property integer $record_id
 * @property integer $client_id
 * @property integer $Facility_id
 * @property string $modified_time
 */
class ClientFacility extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ClientFacility the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'client_facility';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('client_id, facility_id', 'required'),
            array('client_id, facility_id', 'numerical', 'integerOnly' => true),
            array('modified_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, client_id, facility_id, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'client_id' => 'Client',
            'facility_id' => 'Facility',
            'modified_time' => 'Modified Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('facility_id', $this->facility_id);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getClientIdByFacilityId($facility_id) {
        $client = ClientFacility::model()->findByAttributes(array('facility_id' => $facility_id));

        if (!empty($client)) {
            return $client->client_id;
        } else {
            return 0;
        }
    }

    public function getClientDetailByFacilityId($facility_id) {

        $query = "SELECT c.id as client_id, c.client_name
		FROM client_facility as cf
        LEFT JOIN client as c ON c.id = cf.client_id
		WHERE cf.facility_id = :facility_id";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id);
        $result = $cmd->query();

        $clients = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $clients[$row['client_id']] = $row['client_name'];
            }
        }

        return $clients;
    }
    
    public function getFacilityDetailByClientId($client_id) {

        $query = "SELECT f.id as facility_id, f.facility_name
		FROM client_facility as cf
        LEFT JOIN facility as f ON f.id = cf.facility_id
		WHERE cf.client_id = :client_id";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':client_id', $client_id);
        $result = $cmd->query();

        $facilities = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $facilities[$row['facility_id']] = $row['facility_name'];
            }
        }

        return $facilities;
    }

}