<?php

class RbacCommand extends CConsoleCommand {

    private $_authManager;

    public function getHelp() {
        return "
	USAGE
	rbac
	DESCRIPTION
	This command generates an initial RBAC authorization hierarchy.
	";
    }

    /**
     * Execute the action.
     * @param array command line parameters specific for this command
     */
    public function run($args) {

        //ensure that an authManager is defined as this is mandatory for creating an auth heirarchy
        //if(($this->$_authManager=Yii::app()->authManager)===null)
        if (($auth = Yii::app()->authManager) === null) {

            echo "Error: an authorization manager, named 'authManager'
			must be configured to use this command.\n";
            echo "If you already added 'authManager' component in
			application configuration,\n";
            echo "please quit and re-enter the yiic shell.\n";
            return;
        }
        //provide the oportunity for the use to abort the request
        echo "This command will create nine roles: Super Admin, Application Admin, Facility Admin, Customer Admin, Customer User, 
		Operation Staff, Security Staff, Implementation Staff and Internal Staff and their respective task and operations.\n";
        echo "Would you like to continue? [Yes|No] ";
        //check the input from the user and continue if they indicated yes to the above question
        if (!strncasecmp(trim(fgets(STDIN)), 'y', 1)) {
            // first we need to remove all operations, roles, child relationship and assignments

            $auth->clearAll();


            // set portal login's child operation and task
            //$auth->createOperation('PortalLoginPermission', 'Portal Login', '', 'permission');
            
            $task = $auth->createTask('PortalLoginGroup', 'Portal Login', '', 'permission_group,facility');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("PortalLogin");';
            $auth->createOperation('PortalLogin', 'Yes', $bizrule, 'facility');
            
            $task->addChild('PortalLogin');

            $op = $auth->createOperation('ManagePortalLogin', 'Portal Login', '', 'permission');
            $op->addChild('PortalLogin');

            // set portal configuration's child operation and task
            //$auth->createOperation('PortalConfigurationPermission', 'Portal Configuration', '', 'permission');
            
            $task = $auth->createTask('PortalConfigurationGroup', 'Portal Configuration', '', 'permission_group,facility');
            
            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ReadConfiguration") && $params["user"]->isUserInClient("ReadConfiguration");';
            $auth->createOperation('ReadConfiguration', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("UpdateConfiguration") && $params["user"]->isUserInClient("UpdateConfiguration");';
            $auth->createOperation('UpdateConfiguration', 'Update', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ManageConfiguration") && $params["user"]->isUserInClient("ManageConfiguration");';
            $op = $auth->createOperation('ManageConfiguration', 'Manage', $bizrule, 'facility,client');
            $op->addChild('ReadConfiguration');
            $op->addChild('UpdateConfiguration');

            $task->addChild('ManageConfiguration');
            $task->addChild('ReadConfiguration');


            // set customers child operation and task
            //$auth->createOperation('CustomerPermission', 'Customer', '', 'permission');

            $task = $auth->createTask('CustomerGroup', 'Customer', '', 'permission_group,facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("CreateCustomer");';
            $auth->createOperation('CreateCustomer', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("ReadCustomer");';
            $auth->createOperation('ReadCustomer', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("UpdateCustomer");';
            $auth->createOperation('UpdateCustomer', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("DeleteCustomer");';
            $auth->createOperation('DeleteCustomer', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("ManageCustomer");';
            $op = $auth->createOperation('ManageCustomer', 'Manage', $bizrule, 'facility');
            $op->addChild('CreateCustomer');
            $op->addChild('ReadCustomer');
            $op->addChild('UpdateCustomer');
            $op->addChild('DeleteCustomer');

            $task->addChild('ManageCustomer');
            $task->addChild('ReadCustomer');

            // set facility child operation and task
            //$auth->createOperation('FacilityPermission', 'Facility', '', 'permission');

            $task = $auth->createTask('FacilityGroup', 'Facility', '', 'permission_group,facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("facility");';
            $auth->createOperation('CreateFacility', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("ReadFacility");';
            $auth->createOperation('ReadFacility', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("UpdateFacility");';
            $auth->createOperation('UpdateFacility', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("DeleteFacility");';
            $auth->createOperation('DeleteFacility', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("ManageFacility");';
            $op = $auth->createOperation('ManageFacility', 'Manage', $bizrule, 'facility,client');
            $op->addChild('CreateFacility');
            $op->addChild('ReadFacility');
            $op->addChild('UpdateFacility');
            $op->addChild('DeleteFacility');
            

            $task->addChild('ManageFacility');
            $task->addChild('ReadFacility');
        
            // set role child operation and task
            //$auth->createOperation('RolePermission', 'Role', '', 'permission');

            $task = $auth->createTask('RoleGroup', 'Role', '', 'permission_group');

            $auth->createOperation('CreateRole', 'Create');
            $auth->createOperation('ReadRole', 'Read');
            $auth->createOperation('UpdateRole', 'Update');
            $auth->createOperation('DeleteRole', 'Delete');

            $op = $auth->createOperation('ManageRole', 'Manage');
            $op->addChild('CreateRole');
            $op->addChild('ReadRole');
            $op->addChild('UpdateRole');
            $op->addChild('DeleteRole');
            
            $task->addChild('ManageRole');
            $task->addChild('ReadRole');


            // set user child operations and task
            //$auth->createOperation('UserPermission', 'User', '', 'permission');

            $task = $auth->createTask('UserGroup', 'User', '', 'permission_group,facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("CreateUser") && $params["user"]->isUserInClient("CreateUser");';
            $auth->createOperation('CreateUser', 'Create', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ReadUser") && $params["user"]->isUserInClient("UpdateUser");';
            $auth->createOperation('ReadUser', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("UpdateUser") && $params["user"]->isUserInClient("UpdateUser");';
            $auth->createOperation('UpdateUser', 'Update', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("DeleteUser") && $params["user"]->isUserInClient("DeleteUser");';
            $auth->createOperation('DeleteUser', 'Delete', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isProfileOwner();';
            $auth->createOperation('UpdateProfile', 'Update Profile', $bizrule);


            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ManageUser") && $params["user"]->isUserInClient("ManageUser");';
            $op = $auth->createOperation('ManageUser', 'Manage', $bizrule, 'facility,client');
            $op->addChild('CreateUser');
            $op->addChild('ReadUser');
            $op->addChild('UpdateUser');
            $op->addChild('DeleteUser');

            $task->addChild('ManageUser');
            $task->addChild('ReadUser');

            // set proximity car permission's child operation and task
            //$auth->createOperation('ProximityCardPermission', 'Proximity Card', '', 'permission');

            $task = $auth->createTask('ProximityCardGroup', 'Proximity Card', '', 'permission_group,facility');

            $auth->createOperation('ManageProximityCard', 'ManageProximityCard');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitProximityCard");';
            $auth->createOperation('SubmitProximityCard', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveProximityCard");';
            $op = $auth->createOperation('ApproveProximityCard', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitProximityCard');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateProximityCard");';
            $auth->createOperation('DelegateProximityCard', 'Delegate', $bizrule, 'facility');
            
            $task->addChild('ApproveProximityCard');
            $task->addChild('SubmitProximityCard');
            //$task->addChild('DelegateProximityCard');


            // set emf permission's child operation and task
            //$auth->createOperation('EMFPermission', 'EMF', '', 'permission');

            $task = $auth->createTask('EMFGroup', 'EMF', '', 'permission_group,facility');

            $auth->createOperation('ManageEMF', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitEMF");';
            $auth->createOperation('SubmitEMF', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveEMF");';
            $op = $auth->createOperation('ApproveEMF', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitEMF');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateEMF");';
            $auth->createOperation('DelegateEMF', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveEMF');
            $task->addChild('SubmitEMF');
            //$task->addChild('DelegateEMF');

            // set dc access permission's child operation and task
            //$auth->createOperation('DCAccessPermission', 'DC Access', '', 'permission');

            $task = $auth->createTask('DCAccessGroup', 'DC Access', '', 'permission_group,facility');

            $auth->createOperation('ManageDCAccess', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitDCAccess");';
            $auth->createOperation('SubmitDCAccess', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveDCAccess");';
            $op = $auth->createOperation('ApproveDCAccess', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitDCAccess');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateDCAccess");';
            $auth->createOperation('DelegateDCAccess', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveDCAccess');
            $task->addChild('SubmitDCAccess');
            //$task->addChild('DelegateDCAccess');

            // set telco access permission's child operation and task
            //$auth->createOperation('TelcoAccessPermission', 'Telco/Carrier Access', '', 'permission');

            $task = $auth->createTask('TelcoAccessGroup', 'TelcoAccess Access', '', 'permission_group,facility');

            $auth->createOperation('ManageTelcoAccess', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitTelcoAccess");';
            $auth->createOperation('SubmitTelcoAccess', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveTelcoAccess");';
            $op = $auth->createOperation('ApproveTelcoAccess', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitTelcoAccess');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateTelcoAccess");';
            $auth->createOperation('DelegateTelcoAccess', 'Delegate', $bizrule, 'facility');

           
            $task->addChild('ApproveTelcoAccess');
            $task->addChild('SubmitTelcoAccess');
            //$task->addChild('DelegateTelcoAccess');


            // set remote hands permission's child operation and task
            //$auth->createOperation('RemoteHandsPermission', 'Remote Hands', '', 'permission');

            $task = $auth->createTask('RemoteHandsGroup', 'Remote Hands', '', 'permission_group,facility');

            $auth->createOperation('ManageRemoteHands', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitRemoteHands");';
            $auth->createOperation('SubmitRemoteHands', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveRemoteHands");';
            $op = $auth->createOperation('ApproveRemoteHands', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitRemoteHands');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateRemoteHands");';
            $op = $auth->createOperation('DelegateRemoteHands', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveRemoteHands');
            $task->addChild('SubmitRemoteHands');
            //$task->addChild('DelegateRemoteHands');

            // operations for bulletin/announcement
            //$auth->createOperation('BulletinBoardPermission', 'Bulletin Board', '', 'permission');

            $task = $auth->createTask('BulletinBoardGroup', 'Bulletin Board', '', 'permission_group,facility');

            $auth->createOperation('ManagePost', 'Manage');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("ReadPost");';
            $auth->createOperation('ReadPost', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("SubmitPost");';
            $op = $auth->createOperation('SubmitPost', 'Submit', $bizrule, 'facility');
            $op->addChild('ReadPost');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("ApprovePost");';
            $op = $auth->createOperation('ApprovePost', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitPost');

            $task->addChild('ApprovePost');
            $task->addChild('SubmitPost');
            $task->addChild('ReadPost');

            // set library permission's child operation and task
            //$auth->createOperation('LibraryPermission', 'Library', '', 'permission');

            $task = $auth->createTask('LibraryGroup', 'Library', '', 'permission_group,facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("CreateLibraryContent");';
            $auth->createOperation('CreateLibraryContent', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("ReadLibraryContent");';
            $auth->createOperation('ReadLibraryContent', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("UpdateLibraryContent");';
            $auth->createOperation('UpdateLibraryContent', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("DeleteLibraryContent");';
            $auth->createOperation('DeleteLibraryContent', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("ApproveRemoteHands");';
            $op = $auth->createOperation('ManageLibraryContent', 'Manage', $bizrule, 'facility');
            $op->addChild('CreateLibraryContent');
            $op->addChild('ReadLibraryContent');
            $op->addChild('UpdateLibraryContent');
            $op->addChild('DeleteLibraryContent');

            $task->addChild('ManageLibraryContent');
            $task->addChild('ReadLibraryContent');

            // operations for reports
            //$auth->createOperation('ReportPermission', 'Report', '', 'permission');

            $task = $auth->createTask('ReportGroup', 'Report', '', 'permission_group,facility');

            $auth->createOperation('ManageReport', 'ManageReport');

            $bizrule = 'return isset($params["report"]) && $params["report"]->isUserInFacility("ReadReport") && $params["report"]->isUserInClient("ReadReport");';
            $auth->createOperation('ReadReport', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["report"]) && $params["report"]->isUserInFacility("ReportPermission") && $params["report"]->isUserInClient("ReportPermission");';
           
            $task->addChild('ReadReport');

            $auth->createOperation('ReadAllReports', 'Read All Reports');


            $auth->createOperation('DelegateRole', 'Delegate Role', '');  

            // assign operation dropdown
            $auth->createOperation('AssignRole', 'Assign Role');
            $auth->createOperation('AssignOperation', 'Assign Operation');
            //$auth->createOperation('AssignFacility', 'Assign Facility');
            //$auth->createOperation('AssignCustomer', 'Assign Customer');
            
            // associate permission
            $auth->createOperation('AssociateFacility', 'Associate Facility');
            $auth->createOperation('AssociateCustomer', 'Associate Customer');
            $auth->createOperation('AssociateAllFacility', 'Associate All Facility');
            $auth->createOperation('AssociateAllCustomer', 'Associate All Customer');
        
            // fujitso user
            $role = $auth->createRole('FujitsuUser', 'Fujitsu User');
            //set default permission item
            $role->addChild('UpdateProfile');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('SubmitPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // implementation staff
            $role = $auth->createRole('ImplementationStaff', 'Implementation Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // security staff
            $role = $auth->createRole('SecurityStaff', 'Security Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // operation staff
            $role = $auth->createRole('OperationStaff', 'Operation Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // implementation staff
            $role = $auth->createRole('CustomerUser', 'Customer User');
            // set inherited auth
            $role->addChild('UpdateProfile');
            $role->addChild('PortalLogin');
            $role->addChild('SubmitEMF');
            $role->addChild('SubmitDCAccess');
            $role->addChild('ReadPost');

            // Customer admin
            $role = $auth->createRole('CustomerAdmin', 'Customer Administrator');
            // set inherit user
            $role->addChild('CustomerUser');
            // set defaul auth
            $role->addChild('AssociateFacility');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManageRemoteHands');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApproveRemoteHands');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');


            // facility admin
            $role = $auth->createRole('FacilityAdmin', 'Facility Administrator');
            //set inherited user
            $role->addChild('CustomerAdmin');
            $role->addChild('ManagePortalLogin');
            $role->addChild('ReadConfiguration'); // recently added
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageCustomer');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManageRemoteHands');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApproveRemoteHands');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');

            // application admin
            $role = $auth->createRole('ApplicationAdmin', 'Application Administrator');
            // set inherited user
            $role->addChild('FacilityAdmin');
            $role->addChild('OperationStaff');
            $role->addChild('SecurityStaff');
            $role->addChild('ImplementationStaff');
            // set default auth
            $role->addChild('AssociateCustomer');
            $role->addChild('AssociateAllCustomer');
            $role->addChild('AssociateFacility');
            $role->addChild('AssociateAllFacility');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ReadConfiguration');
            $role->addChild('ManageCustomer');
            $role->addChild('ManageFacility');
            $role->addChild('ManageRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManageRemoteHands');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApproveRemoteHands');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');
           
            // superadmin admin
            $role = $auth->createRole('SuperAdmin', 'Super Admin');
            // set inherited user
            $role->addChild('ApplicationAdmin');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageConfiguration');
            $role->addChild('ManageCustomer');
            $role->addChild('ManageFacility');
            $role->addChild('ManageRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManageRemoteHands');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApproveRemoteHands');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');


            echo "Authorization hierarchy successfully generated.";
        }// end if
    }

// end function run
}

// end class