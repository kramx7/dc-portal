<?php
/* @var $this NoticeboardController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bulletin Board',
);

$this->menu=array(
	array('label'=>'Create Announcement', 'url'=>array('create'),'visible'=>User::model()->isAuthorized('createNotice')),
	array('label'=>'Manage Announcement', 'url'=>array('admin'),'visible'=>User::model()->isAuthorized('adminNotices')),
);
?>

<h1>Bulletin Boards</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
