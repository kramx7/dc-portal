<?php
/* @var $this NoticeboardController */
/* @var $model Noticeboard */

$this->breadcrumbs=array(
	'Bulletin Board'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Announcement', 'url'=>array('index')),
	array('label'=>'Create Announcement', 'url'=>array('create'),'visible'=>User::model()->isAuthorized('createNotice')),
	array('label'=>'Update Announcement', 'url'=>array('update', 'id'=>$model->id),'visible'=>User::model()->isAuthorized('updateNotice',array('owner_id'=>$model->submitted_by))),
	array('label'=>'Delete Announcement', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>User::model()->isAuthorized('deleteNotice',array('owner_id'=>$model->submitted_by))),
	array('label'=>'Manage Announcement', 'url'=>array('admin'),'visible'=>User::model()->isAuthorized('adminNotices')),
);
?>

<h1>View Announcement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('label'=>'sticky','value'=>(empty($model->sticky))?'Not Pin to bulletin':'Pin to bulletin'),
		'title',
		array('label'=>'Full Message','type'=>'raw','value'=>$model->full_message),
		//'full_message',
		//array('label'=>'Facility','value'=>($model->facility!=null)?$model->facility1->facility_name:''),
		array('label'=>'Status','value'=>$model->getStatusDescription() ),
		'priority',
		'publish_from',
		'publish_to',
		array('label'=>'Submitted By','value'=>$model->submittedBy->emailaddress),
		'submitted_time',
		array('label'=>'Approved By','value'=>($model->approved_by!=null)?$model->approver1->emailaddress:''),
		'approved_time',
	),
)); ?>
