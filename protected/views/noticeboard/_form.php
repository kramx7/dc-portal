<?php
/* @var $this NoticeboardController */
/* @var $model Noticeboard */
/* @var $form CActiveForm */
?>
<style>
    .cke_toolbox_collapser{
        display: none;
    }    
</style>    

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'noticeboard-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php

 Yii::app()->clientScript->registerScript('dates', "
 $('#from_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	onClose: function (selectedDate) {
		     
            to_date = $('#to_date').val();
		     	      		
              if(selectedDate != 'immediately'){
                    var d = new Date(selectedDate);
                    d.setDate(d.getDate() + 1);
                    $('#to_date').datepicker('option','minDate', d);
             }
             
             if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val(to_date);
             }       
          }
	});
  
 $('#to_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	beforeShow: function (input, dp) {
		  
            from_date = $('#from_date').val();
            to_date = $(this).val(); 	      
            
            if(from_date == 'immediately'){
                from_date = new Date();
            }        
            
            var d = new Date(from_date);
            d.setDate(d.getDate() + 1);
            
            min_date = $.datepicker.formatDate('yy-mm-dd',d);
            
            $(this).datepicker('option','minDate', min_date);
              
          },
          onClose: function (selectedDate) {
            // used to set the from date when it is cleared
            from_date = $('#from_date').val(); 

            if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val('never');
             }
            
            if($('#to_date').val() != 'never'){
                var d = new Date(selectedDate);
                d.setDate(d.getDate() - 1);
                $('#from_date').datepicker('option','maxDate', d);
             }
             
            if($.trim($('#from_date').val()) == ''){
             	$('#from_date').val(from_date);
             } 
                
           }
	});
	
");	

?>


	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php //echo $form->textField($model,'message_status'); ?>
		<?php 
		/*$message_status_list = CHtml::listData(NoticeboardMessageStatus::model()->messageStatusForList()->findAll(), 'message_status','message_status_description');
		$default_status = (!empty($model->message_status))?$model->message_status:0;
		echo $form->dropDownList($model, 'message_status', $message_status_list, 
		array('options'=>array($default_status=>array('selected'=>'selected'))) );*/
		 ?>
		 <?php //echo $form->dropDownList($model,'status',SWHelper::nextStatuslistData($model)); ?>
		 <?php echo CHtml::textField('display_status',$model->swGetStatus()->getLabel(),array('readonly'=>'readonly','style'=>'background-color:#B7D6E7'));?>
		 <?php //echo $form->hiddenField($model,'status',array('value'=>$model->swGetStatus()->toString())); ?>
		<?php //echo $form->error($model,'status'); ?>
		
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'sticky'); ?>
		<?php echo $form->hiddenField($model,'sticky'); ?>
		<?php /*echo $form->radioButtonList($model, 'sticky', 
		array(1=>'Pin<span id="icons"><span class="ui-icon ui-icon-pin-s"></span></span>',
		0=>'Unpin<span id="icons"><span class="ui-icon ui-icon-pin-w"></span></span>'),
		array('separator'=>'','labelOptions'=>array('style'=>'display:inline;margin-right: 10px;'))); */ ?>
		
		<div id="sticky-icon" style="margin-left:40px;">
			<img id="Noticeboard_sticky_icon" src="<?php echo Yii::app()->request->baseUrl; ?>/images/unpin_btn.png" style="border: #000 solid 1px">
		</div>
		
			<?php echo $form->error($model,'sticky'); ?>
			<p class="hint">
			Hint: You may click on the image to pin and unpin the notice.
		</p>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'facility'); ?>
		<?php //echo $form->textField($model,'message_status'); ?>
		<?php
		
		 
		if(empty(Yii::app()->user->facilityId)): // if empty no facility is specifically assigned, a superadmin user
			$facility_option = CHtml::listData($facility_list, 'facility_id','facility_name');
			echo $form->dropDownList($model, 'facility', $facility_option, 
			array('empty'=>'Select Facility','options'=>array($default_facility=>array('selected'=>'selected',))) ); 
		?>
		<?php else:?>
			<?php echo CHtml::textField('display_facility',$facility_list[0]->facility_name,array('readonly'=>'readonly','style'=>'background-color:#B7D6E7')); ?>
			<?php echo $form->hiddenField($model,'facility',array('value'=>$facility_list[0]->facility_id)); ?>
		<?php endif; ?>
		<?php echo $form->error($model,'facility'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php //echo $form->textField($model,'priority',array('size'=>10,'maxlength'=>10)); ?>
		<?php 
		$priority_list = array(1=>1,2,3,4,5);
		$default_priority = (!empty($model->priority))?$model->priority:1;
		echo $form->dropDownList($model, 'priority', $priority_list, 
		array('options'=>array('1'=>array('selected'=>'selected'))) ); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publish_from'); ?>
		<?php //echo $form->textField($model,'publish_from'); ?>
		<?php
		
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'attribute'=>'publish_from',
		'model' => $model,
		'options'=>array(
			'dateFormat'=>'yy-mm-dd',
			'minDate'=>'new Date()', // Today
			),
		'htmlOptions'=>array('id'=>'from_date','readonly'=>'readonly')	
		));
		
		//echo $form->textField($model, 'publish_from', array('id'=>'from_date','readonly'=>'readonly'));
		?>
		<?php echo $form->error($model,'publish_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publish_to'); ?>
		<?php //echo $form->textField($model,'publish_to'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'attribute'=>'publish_to',
			'model' => $model,
			'options'=>array(
				'dateFormat'=>'yy-mm-dd',
				'minDate'=>'new Date()', // Today
				),
			'htmlOptions'=>array('id'=>'to_date','readonly'=>'readonly')	
			));
			
			//echo $form->textField($model, 'publish_to', array('id'=>'to_date','readonly'=>'readonly'));
			?>
		<?php echo $form->error($model,'publish_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_message'); ?>
		<?php //echo $form->textField($model,'short_message',array('size'=>60,'maxlength'=>256)); ?>
		<?php 
				
		$this->widget('ext.editMe.widgets.ExtEditMe', array(
		'model'=>$model,
		'attribute'=>'short_message',
		'height'=>'100',
		'toolbar'=>array(array()),
		'ckeConfig'=>array('removePlugins'=>'elementspath','enableTabKeyTools'=>true),
		'resizeMode'=>false,
		'advancedTabs'=>false,
		));
		?>
		<?php echo $form->error($model,'short_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'full_message'); ?>
		<?php //echo $form->textField($model,'full_message',array('size'=>60,'maxlength'=>10240)); ?>
		<?php 
				
		$this->widget('ext.editMe.widgets.ExtEditMe', array(
		'model'=>$model,
		'attribute'=>'full_message',
		'height'=>'300',
		'toolbar'=>array(array()),
		'ckeConfig'=>array('removePlugins'=>'elementspath','enableTabKeyTools'=>true),
		'resizeMode'=>false,
		'advancedTabs'=>false,		));
		?>
		<?php echo $form->error($model,'full_message'); ?>
		
	</div>
	
	<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Submit'); ?>
		<?php if($model->isNewRecord):?>
			<?php echo CHtml::submitButton('Save as Draft',array('name'=>'draft')); ?>
		<?php endif;?>
		<?php echo CHtml::submitButton('Save and Published',array('name'=>'publish')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



<script language="javascript">
// JavaScript Document
$(document).ready(function(){

	
	if($('#Noticeboard_sticky').val()==0 || $('#Noticeboard_sticky').val()==''){
		$('#Noticeboard_sticky_icon').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/images/unpin_btn.png');
	}else{
		$('#Noticeboard_sticky_icon').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/images/pin_btn.png');	}
	 
	$("#Noticeboard_sticky_icon").click(function(){
		
		if($('#Noticeboard_sticky').val()==0){			
			$('#Noticeboard_sticky').val(1);
			$('#Noticeboard_sticky_icon').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/images/pin_btn.png');
	   	}else{
	   		$('#Noticeboard_sticky').val(0);
		   $('#Noticeboard_sticky_icon').attr('src','<?php echo Yii::app()->request->baseUrl; ?>/images/unpin_btn.png');
	   }
	   
   });
   
   												
}); // end document ready	
</script>