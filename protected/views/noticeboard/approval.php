<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

$this->breadcrumbs = array(
    'Dashboard' => array('/noticeboard'),
    'Bulletin Post'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-noticeboard', "

$('#noticeboard-form select, #noticeboard-form input').change(function(){
	
	field = $(this).attr('id');
	
	if($('#'+field+'_em_').text() == ''){
		$(this).removeClass('error');
		//$(this).css('border-color','#ccc');
		//$(this).css('color','#000');
	}

});

var today = new Date();
var yesterday = new Date();
var lastmonth = new Date();
lastmonth.setMonth(today.getMonth() - 1);
yesterday.setDate(today.getDate()-1);

jQuery('.sdate').datepicker({
        autoSize: true,
        beforeShow: customRange,
        onClose: customRange,
        constrainInput: true,
        dateFormat: 'dd/mm/yy'
});

jQuery('.edate').datepicker({
        autoSize: true,
        beforeShow: customRange,
        onClose: customRange,
        constrainInput: true,
        dateFormat: 'dd/mm/yy'
});

function customRange(input) {

    var min = new Date(2007, 11 - 1, 1);
    var dateMin = new Date();
    var dateMax = new Date(2035, 12, 31);   

    from_date = $('#from_date').val();
    to_date = $('#to_date').val();

    if (input.id === 'from_date') {
        if($.trim(to_date) != 'never' && $.trim(to_date) != ''){
            dateMax = $('#to_date').datepicker('getDate');
        }

        dateMax = new Date(dateMax.getFullYear(), dateMax.getMonth(), dateMax.getDate()-1);
    }

    if (input.id === 'to_date') {
        if($.trim(from_date) != 'now' && $.trim(from_date) != ''){
            dateMin = $('#from_date').datepicker('getDate');
        }

        dateMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate()+1);   
    }

    return {
            minDate: dateMin,
            maxDate: dateMax
    };
}



");
?>

<style>
    .row-fluid{
        margin-bottom: 20px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Create User</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'noticeboard-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php //echo $form->errorSummary($model); ?>
        <div class="row-fluid">
            <div class="span5">
                <label class="cust-label">Status </label> <span class="alert alert-info"><?php echo $model->swGetStatus()->getLabel();?></span>
            </div>
        </div> 

        <div class="row-fluid">
            <div class="span5">
                <label class="cust-label">Title</label> <?php echo $model->title; ?>
            </div>
            <div class="span2">
                <label class="cust-label" style="width:50px;">Sticky</label> <?php echo ($model->sticky==1)?'Sticky':'Not Set'; ?>
            </div>
        </div> 

        <div class="row-fluid">
            <div class="span5">
                <label class="cust-label">Facility</label> 
                <?php echo $model->getFacilityName();?>
            </div> 
            <div class="span3">
                <label class="cust-label" style="width:50px;">Priority</label> 
                <?php echo $model->priority?>
            </div>    
        </div>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Publish From</label>
                <?php echo date('d/m/Y',strtotime($model->publish_from)); ?>
                
            </div>
            <div class="span4">
                <label class="cust-label">Publish To</label>
                <?php echo $model->publish_to; ?>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span7">
                <label class="cust-label">Short Message</label> <br/> <br/> 
                <?php echo $model->short_message; ?>
            </div>  
        </div>
        <div class="row-fluid">
            <div class="span7">
                <label class="cust-label">Full Message</label> <br/> <br/> 
                <?php echo $model->full_message; ?>
            </div>  
        </div>
        <?php echo $form->hiddenField($model, 'title'); ?>
        <?php echo $form->hiddenField($model, 'sticky'); ?>
        <?php echo $form->hiddenField($model, 'facility_id'); ?>
        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Approve', 'htmlOptions'=>array('name'=>'approve'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reject', 'htmlOptions'=>array('name'=>'reject'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
        </div>
        <?php $this->endWidget(); ?>        
        <input type="hidden" id="post-status" value="<?php echo (isset($_POST['save']))?"true":"false";?>">
    </div><!-- end users-box -->

</div>