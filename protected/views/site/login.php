<?php //$this->beginContent('//layouts/login'); ?>

<div class="form form-signin" align="left">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<h2 class="form-signin-heading"><?php echo Yii::app()->params['site_name']?></h2>
        
	<?php if(Yii::app()->user->hasFlash('verification')): ?>
	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('verification'); ?>
	</div>
	<?php endif; ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'username',array('style'=>'display: inline')); ?>
		<?php echo $form->textField($model,'username',array('style'=>'display: inline')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password',array('style'=>'display: inline')); ?>
		<?php echo $form->passwordField($model,'password',array('style'=>'display: inline; margin-left:27px;')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::link('Reset Password', array('/user/requestreset')); ?>
		<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-large btn-primary','style'=>'float:right; margin-right:50px;')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php //$this->endWidget(); ?>