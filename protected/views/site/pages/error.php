<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'Error',
);
?>
<h1>Authorization Error</h1>

<p>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('auth'); ?>
</div>	
	
</p>
