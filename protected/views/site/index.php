<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

$this->breadcrumbs=array('');

Yii::app()->clientScript->registerCoreScript('jquery.ui');


?>


<style>
.column { width: 800px; float: left; padding-bottom: 100px; }
.portlet { margin: 0 1em 1em 0; max-height: 230px;}
.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; border: none; }
.portlet-header .ui-icon { float: right; }
.portlet-content { padding: 0.4em; max-height: 200px; overflow: scroll;}
.ui-sortable-placeholder { border: 2px dotted #00F; visibility: visible !important;}
.ui-sortable-placeholder * { visibility: hidden; }
.action-button-col{width:190px;}
.action-button{padding:7px}
.ui-icon, .action-button{float:left;}
.work-items{max-height: 160px; overflow: scroll;}
</style>
 <script>
$(function() {
    $( ".column" ).sortable({
        connectWith: ".column",
        placeholder: "ui-state-highlight",
        cursor: "move", 
        handle: "div.portlet-header",
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
    .find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" )
    .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
    .end()
    .find( ".portlet-content" );
    
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    
    $( ".column" ).disableSelection();

    $('.item-title').dblclick(function() {
        var url = $(this).attr('url');
        window.location = "<?php echo Yii::app()->createUrl('/'); ?>"+url;
    });
    
});
</script>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => false, // use transitions?
    'closeText' => false, // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
    ),
));
?>
        
<div class="column">
    
    <div class="portlet rounded">
    <div class="portlet-header">My Work Items</div>    
        
        <?php
        
        $gridDataProvider = new CArrayDataProvider($workitems->items);
 
        // $gridColumns
        $gridColumns = array(
            array('name'=>'item', 'header'=>'Item','value'=>'CHtml::link($data->item, array($data->url));','type'=>'raw'),
            array('name'=>'description', 'header'=>'Description'),
            array('name'=>'status', 'header'=>'Status'),
        );

        $this->widget('bootstrap.widgets.TbGridView', array(
            'dataProvider'=>$gridDataProvider,
            'template'=>"{items}",
            'columns'=>$gridColumns,
            'htmlOptions'=>array('class'=>'work-items grid-view')
        ));
            
           
        ?>
        
    </div>
    
    <div class="portlet rounded">
    <div class="portlet-header">
        <?php echo CHtml::link('Bulletin Board',array('/noticeboard/index'),array('class'=>'pull-left')); ?>
        <?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'label'=>'New Post','url'=>array('/noticeboard/create'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'New Post', 'url' => array('/noticeboard/create'), 'htmlOptions' => array('class' => 'pull-right','style'=>'color:#fff'))); ?>
        <br/>
    </div>
    <?php


    $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'bb-clist',
            'htmlOptions'=>array('class'=>'grid-view portlet-content','style'=>'padding:0px; margin-top:20px;'),
            'dataProvider'=>$notices->search(NoticeboardMessageStatus::STATUS_APPROVED),
            'columns'=>array(
                    array(
                        'header'=>'Message',
                        'value'=>'CHtml::link(trim(strip_tags($data->short_message)), Yii::app()->createUrl("noticeboard/view",array("id"=>$data->id)))',
                        'type'=>'raw'
                        ),
                    array('header'=>'Facility','value'=>'$data->getFacilityName()'),
                    array('header'=>'Date','value'=>'$data->getFormatedSubmittedTime()'),
                    array('header'=>'Posted By','value'=>'$data->submittedBy->emailaddress'),
            ),
    ));
    
    ?>
    </div>

</div>    
