<?php
/* @var $this NoticeboardController */
/* @var $data Noticeboard */
?>

<div class="view bulletin-entry">
	
	<strong><?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?></strong>
	<br>
	<br>
	<?php echo $data->full_message; ?>	<br />
	<br>
	Author: <?php echo CHtml::encode($data->submittedBy->emailaddress); ?>
	<br>
	Published Date: <?php echo CHtml::encode(date('F j, Y - l',strtotime($data->submitted_time))); ?>
	
		<?php echo CHtml::link('<img src="'.Yii::app()->baseUrl.'/images/btn_moreinfo.gif"/>', array('view', 'id'=>$data->id),array('style'=>'float:right;')); ?>
	
</div>