<?php
/* @var $this NoticeboardController */
/* @var $model Noticeboard */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'message_status'); ?>
		<?php //echo $form->textField($model,'message_status'); ?>
		<?php 
		/*$message_status_list = CHtml::listData(NoticeboardMessageStatus::model()->messageStatusForList()->findAll(), 'message_status','message_status_description');
		$default_status = isset($model->message_status)?$model->message_status:0;
		echo $form->dropDownList($model, 'message_status', $message_status_list, 
		array('options'=>array($default_status=>array('selected'=>'selected'))) ); 
		*/
		?>
		<?php echo $form->dropDownList($model,'status',SWHelper::nextStatuslistData($model)); ?>
		<?php echo $form->error($model,'message_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priority'); ?>
		<?php //echo $form->textField($model,'priority',array('size'=>10,'maxlength'=>10)); ?>
		<?php 
		$priority_list = array(1=>1,2,3,4,5);
		$default_priority = isset($model->priority)?$model->priority:1;
		echo $form->dropDownList($model, 'priority', $priority_list, 
		array('options'=>array('1'=>array('selected'=>'selected'))) ); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'publish_from'); ?>
		<?php //echo $form->textField($model,'publish_from'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'attribute'=>'publish_from',
			'model' => $model,
			'options'=>array(
				'dateFormat'=>'yy-mm-dd'
				)
			));
			?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'publish_to'); ?>
		<?php //echo $form->textField($model,'publish_to'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'attribute'=>'publish_to',
			'model' => $model,
			'options'=>array(
				'dateFormat'=>'yy-mm-dd'
				)
			));
			?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'short_message'); ?>
		<?php echo $form->textField($model,'short_message',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'full_message'); ?>
		<?php echo $form->textField($model,'full_message',array('size'=>60,'maxlength'=>10240)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submitted_by'); ?>
		<?php echo $form->textField($model,'submitted_by',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submitted_time'); ?>
		<?php //echo $form->textField($model,'submitted_time'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'attribute'=>'submitted_time',
			'model' => $model,
			'options'=>array(
				'dateFormat'=>'yy-mm-dd'
				)
			));
			?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approver'); ?>
		<?php echo $form->textField($model,'approver',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved_time'); ?>
		<?php //echo $form->textField($model,'approved_time'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'attribute'=>'approved_time',
			'model' => $model,
			'options'=>array(
				'dateFormat'=>'yy-mm-dd'
				)
			));
			?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->