<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';


$this->breadcrumbs = array(
    'Reset Password',
);
?>
<?php
echo CHtml::scriptFile(Yii::app()->request->baseUrl . '/js/code.assembly.password.strength.js');

Yii::app()->clientScript->registerScript('pass-strength', '
$(".password_test").keyup(function(){
    passwordStrength($(this).val());
});
'
);
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/code.assembly.password.strength.css" />
<style>
    .form-horizontal .control-group {
        margin-bottom: 10px;
    }
</style> 

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Reset Password</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'user-form',
            'type' => 'inline',
            'enableAjaxValidation' => true,
        ));
        ?>
        <?php
            $this->widget('bootstrap.widgets.TbAlert', array(
                'block' => true, // display a larger alert block?
                'fade' => false, // use transitions?
                'closeText' => false, // close link text - if set to false, no close link is displayed
                'alerts' => array(// configurations per alert type
                    'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
                ),
            ));
            ?>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $form->labelEx($model, 'emailaddress', array('style'=>"width: 150px;"));?>
                <?php echo $model->emailaddress;?>
            </div>
        </div>
    <div class="control-group">
    </div>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $form->labelEx($model, 'new_password', array('style'=>"width: 150px;"));?>
                <?php echo $form->passwordFieldRow($model, 'new_password', array('class'=>'password_test'));?>
                <?php echo $form->error($model,'new_password'); ?>
            </div>
        </div>
<div class="control-group">
    </div>
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->labelEx($model, 'password_compare', array('style'=>"width: 150px;"));?>
                <?php echo $form->passwordFieldRow($model, 'password_compare');?>
                <?php echo $form->error($model,'password_compare'); ?>
            </div>
            <div class="span6">
                    <label for="User_strength_indicator" class="control-label">Password Strength</label>
                    <div class="controls">
                        <div id="passwordDescription">Password not entered</div>
                        <div id="passwordStrength" class="strength0"></div>
                    </div>
            </div>
        </div>     
        <div class="control-group">
    </div>   
        <div class="row-fluid">
            <div class="span6">
                <div class="alert in alert-block fade alert-info"><strong>Hint</strong>: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols !" ? $ % ^ & )
                </div>

            </div>
        </div> 
        <div class="row-fluid">
            <div class="span12">
                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reset Password')); ?>
                </div>
            </div>    
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- end users-box -->

</div>
