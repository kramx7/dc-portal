<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">
	

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	//'stateful'=>true
)); ?>

<?php

 Yii::app()->clientScript->registerScript('tabs', "
 $( '#tabs' ).tabs({selected:".$tabIndex."});	
");			

?>
<div id="tabs"> 	
	 <ul>
		<li><a href="#tabs-1">Step 1</a></li>
		<li><a href="#tabs-2">Step 2</a></li>
		<li><a href="#tabs-3">Step 3</a></li>
		</ul>
		<div id="tabs-1">

			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>
			
			<div class="row">
				<?php echo $form->labelEx($model,'user_role'); ?>
				<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
				<?php
				if( User::model()->isUserHasRole(Yii::app()->params['FacAdminCode'])){ 
					$user_role_list = CHtml::listData(AuthItem::model()->facAdminRoleForList()->findAll(), 'name','description');
				}else{
					$user_role_list = CHtml::listData(AuthItem::model()->userRoleForList()->findAll(), 'name','description');
				}
				
				$default_role = (!empty($model->user_role))?$model->user_role:'';
				echo $form->dropDownList($model, 'user_role', $user_role_list, array('empty'=>'Select User Role','options'=>array($default_role=>array('selected'=>'selected')) ) );  ?>
				<?php echo $form->error($model,'user_role'); ?>
			</div>
			
			<div class="row buttons">
			<?php 
			echo CHtml::submitButton("Cancel",array('name'=>'cancel'));
			echo CHtml::submitButton("Step 2 >>",array('name'=>'step2')); 
			?>			
			</div>
		</div><!-- end of tab-1 -->
		
		<div id="tabs-2">
		
			<p class="note">Fields with <span class="required">*</span> are required.</p>
			
			<?php echo $form->errorSummary($model); ?>
			
			<?php if($model->user_role == 'Cust_Admin' || $model->user_role == 'Cust_User'){ ?>
			<div class="row">
				<?php echo $form->labelEx($model,'client'); ?>
				<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
				<?php 
				$client_list = CHtml::listData(Client::model()->clientForList()->findAll(), 'client_id','client_name');
				
				$client_list[0] = 'Unlimited';
				
				$default_client = (!empty($model->client))?$model->client:'';
				
				echo $form->dropDownList($model, 'client', $client_list, array('empty'=>'Select Client','options'=>array($default_client=>array('selected'=>'selected'))) );  ?>
				<?php echo $form->error($model,'client'); ?>
				
				<?php echo $form->hiddenField($model,'facility');?>
			</div>
			<?php }else{ ?>
			<div class="row">
				<?php echo $form->labelEx($model,'facility'); ?>
				<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
				<?php 
				$facility_list = CHtml::listData(Facility::model()->facilityForList()->findAll(), 'facility_id','facility_name');
				
				$facility_list[0] = 'Unlimited';
				
				$default_facility = (!empty($model->facility))?$model->facility:'';
				
				echo $form->dropDownList($model, 'facility', $facility_list, array('empty'=>'Select Facility','options'=>array($default_facility=>array('selected'=>'selected'))) );  ?>
				<?php echo $form->error($model,'facility'); ?>
				<?php echo $form->hiddenField($model,'client');?>
				
				
			</div>
			
			<?php } ?>
			
			<div class="row buttons">
			<?php
				echo CHtml::submitButton("<< Step 1",array('name'=>'step1'));
				echo CHtml::submitButton("Step 3 >>",array('name'=>'step3'));
			?>			
			</div>
			
		</div><!-- end tab 2 -->
		
		<div id="tabs-3">
		
			<p class="note">Fields with <span class="required">*</span> are required.</p>
			
			<?php echo $form->errorSummary($model); ?>
			
			<div class="row">
				<?php echo $form->labelEx($model,'emailaddress'); ?>
				<?php echo $form->textField($model,'emailaddress',array('size'=>30,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'emailaddress'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'alternateemailaddress'); ?>
				<?php echo $form->textField($model,'alternateemailaddress',array('size'=>30,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'alternateemailaddress'); ?>
			</div>		
		
			<div class="row">
				<?php echo $form->labelEx($model,'officephone'); ?>
				<?php echo $form->textField($model,'officephone',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'officephone'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'officefax'); ?>
				<?php echo $form->textField($model,'officefax',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'officefax'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'mobilephone'); ?>
				<?php echo $form->textField($model,'mobilephone',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'mobilephone'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>				
				<?php echo $form->dropDownList($model,'status',SWHelper::nextStatuslistData($model)); ?>
				
				<?php echo $form->error($model,'status'); ?>
			</div>
			
			
		
			<div class="row">
				<?php echo $form->labelEx($model,'valid_from'); ?>
				<?php //echo $form->textField($model,'valid_from'); ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'attribute'=>'valid_from',
				'model' => $model,
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'minDate'=>'new Date()', // Today
					)
				));
				?>
				<?php echo $form->error($model,'valid_from'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'valid_to'); ?>
				<?php //echo $form->textField($model,'valid_to'); ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'attribute'=>'valid_to',
				'model' => $model,
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'minDate'=>'new Date()', // Today
					)
				));
				?>
				<?php echo $form->error($model,'valid_to'); ?>
			</div>
		
			<div class="row buttons">
				<?php 
				echo CHtml::submitButton("<< Step 2",array('name'=>'step2'));
				echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('name'=>'finish')); ?>
			</div>
			
		</div><!-- end tab 3 -->
	
</div><!-- end of tabs -->

<?php $this->endWidget(); ?>

</div><!-- form -->