<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id),'visible'=>User::model()->isAuthorized('updateUser',array('facility_id'=>$model->facility))),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>User::model()->isAuthorized('deleteUser',array('facility_id'=>$model->facility))),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'emailaddress',
		'officephone',
		'mobilephone',
		array('label'=>'Status','value'=>$model->swGetStatus()->getLabel()),
		'valid_from',
		'valid_to',
		'last_login',
	),
)); ?>
