<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'emailaddress'); ?>
		<?php echo $form->textField($model,'emailaddress',array('size'=>30,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alternateemailaddress'); ?>
		<?php echo $form->textField($model,'alternateemailaddress',array('size'=>30,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'officephone'); ?>
		<?php echo $form->textField($model,'officephone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'officefax'); ?>
		<?php echo $form->textField($model,'officefax',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mobilephone'); ?>
		<?php echo $form->textField($model,'mobilephone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php //echo $form->textField($model,'user_status',array('size'=>10,'maxlength'=>10)); ?>
		<?php 
		/*$user_status_list = CHtml::listData(UserStatus::model()->userStatusForList()->findAll(), 'status_id','status_description');
		$default_status = isset($model->user_status)?$model->user_status:0;
		echo $form->dropDownList($model, 'user_status', $user_status_list, array('options'=>array($default_status=>array('selected'=>'selected'))) );*/
		
		?>
		<?php echo $form->dropDownList($model,'status',SWHelper::allStatuslistData($model)); ?> 
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'user_role'); ?>
		<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
		<?php 
		$user_role_list = CHtml::listData(AuthItem::model()->userRoleForList()->findAll(), 'name','description');
		$default_role = (!empty($model->user_role))?$model->user_role:'Int_Emp';
		echo $form->dropDownList($model, 'user_role', $user_role_list, 
		array('options'=>array($default_role=>array('selected'=>'selected'))) );  ?>
		<?php echo $form->error($model,'user_role'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valid_from'); ?>
		<?php //echo $form->textField($model,'valid_from'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'attribute'=>'valid_from',
		'model' => $model,
		'options'=>array(
			'dateFormat'=>'yy-mm-dd',
			)
		));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valid_to'); ?>
		<?php //echo $form->textField($model,'valid_to'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'attribute'=>'valid_to',
		'model' => $model,
		'options'=>array(
			'dateFormat'=>'yy-mm-dd',
			)
		));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_login'); ?>
		<?php //echo $form->textField($model,'last_login'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'attribute'=>'last_login',
		'model' => $model,
		'options'=>array(
			'dateFormat'=>'yy-mm-dd',
			)
		));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_login_from'); ?>
		<?php echo $form->textField($model,'last_login_from',array('size'=>48,'maxlength'=>48)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_by'); ?>
		<?php echo $form->textField($model,'modified_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_time'); ?>
		<?php //echo $form->textField($model,'modified_time'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'attribute'=>'modified_time',
		'model' => $model,
		'options'=>array(
			'dateFormat'=>'yy-mm-dd',			)
		));
		?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->