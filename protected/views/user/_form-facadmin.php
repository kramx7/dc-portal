<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">
	

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	//'stateful'=>true
)); ?>

<?php

 Yii::app()->clientScript->registerScript('tabs-dates', "
 $( '#tabs' ).tabs({selected:".$tabIndex."});
 
 $('#from_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	onClose: function (selectedDate) {
		     
            to_date = $('#to_date').val();
		     	      		
              if(selectedDate != 'immediately'){
                    var d = new Date(selectedDate);
                    d.setDate(d.getDate() + 1);
                    $('#to_date').datepicker('option','minDate', d);
             }
             
             if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val(to_date);
             }       
          }
	});
        
$('#to_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	beforeShow: function (input, dp) {
		  
            from_date = $('#from_date').val();
            to_date = $(this).val(); 	      
            
            if(from_date == 'immediately'){
                from_date = new Date();
            }        
            
            var d = new Date(from_date);
            d.setDate(d.getDate() + 1);
            
            min_date = $.datepicker.formatDate('yy-mm-dd',d);
            
            $(this).datepicker('option','minDate', min_date);
              
          },
          onClose: function (selectedDate) {
            // used to set the from date when it is cleared
            from_date = $('#from_date').val(); 

            if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val('never');
             }
            
            if($('#to_date').val() != 'never'){
                var d = new Date(selectedDate);
                d.setDate(d.getDate() - 1);
                $('#from_date').datepicker('option','maxDate', d);
             }
             
            if($.trim($('#from_date').val()) == ''){
             	$('#from_date').val(from_date);
             } 
                
           }
	});
		
");			

?>
<div id="tabs"> 	
	 <ul>
		<li><a href="#tabs-1">Step 1</a></li>
		<li><a href="#tabs-2">Step 2</a></li>
		</ul>
		<div id="tabs-1">

			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>
			
			<div class="row">
				<?php echo $form->labelEx($model,'user_role'); ?>
				<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
				<?php
				
				$user_role_option = CHtml::listData($user_role_list, 'name','description');
				
				$default_role = ($model->user_role == '')?$model->user_role:'';
				echo $form->dropDownList($model, 'user_role', $user_role_option, array('empty'=>'Select User Role','options'=>array($default_role=>array('selected'=>'selected')) ) );  ?>
				<?php echo $form->error($model,'user_role'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->labelEx($model,'client'); ?>
				<?php //echo $form->textField($model,'user_role',array('size'=>10,'maxlength'=>10)); ?>
				<?php 
				
				$client_option = CHtml::listData($client_list, 'client_id','client_name');
				
				$default_client = ($model->client == '')?$model->client:'';
				
				echo $form->dropDownList($model, 'client', $client_option, array('empty'=>'Select Client','options'=>array($default_client=>array('selected'=>'selected'))) );  ?>
				<?php echo $form->error($model,'client'); ?>
				
				<?php echo $form->hiddenField($model,'facility');?>
			</div>
			
			<div class="row buttons">
			<?php 
			echo CHtml::submitButton("Cancel",array('name'=>'cancel'));
			echo CHtml::submitButton("Step 2 >>",array('name'=>'step2')); 
			?>			
			</div>
		</div><!-- end of tab-1 -->
				
		<div id="tabs-2">
		
			<p class="note">Fields with <span class="required">*</span> are required.</p>
			
			<?php echo $form->errorSummary($model); ?>
			
			<div class="row">
				<?php echo $form->labelEx($model,'emailaddress'); ?>
				<?php echo $form->textField($model,'emailaddress',array('size'=>30,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'emailaddress'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'alternateemailaddress'); ?>
				<?php echo $form->textField($model,'alternateemailaddress',array('size'=>30,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'alternateemailaddress'); ?>
			</div>		
		
			<div class="row">
				<?php echo $form->labelEx($model,'officephone'); ?>
				<?php echo $form->textField($model,'officephone',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'officephone'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'officefax'); ?>
				<?php echo $form->textField($model,'officefax',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'officefax'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'mobilephone'); ?>
				<?php echo $form->textField($model,'mobilephone',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'mobilephone'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>				
				<?php //echo $form->dropDownList($model,'status',SWHelper::nextStatuslistData($model)); ?>
				<?php echo CHtml::textField('display_status',$model->swGetStatus()->getLabel(),array('readonly'=>'readonly','style'=>'background-color:#B7D6E7'));?>
				<?php echo $form->hiddenField($model,'status',array('value'=>$model->swGetStatus()->toString())); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
			
			
		
			<div class="row">
				<?php echo $form->labelEx($model,'valid_from'); ?>
				<?php //echo $form->textField($model,'valid_from'); ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'attribute'=>'valid_from',
				'model' => $model,
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'minDate'=>'new Date()', // Today
					),
				'htmlOptions'=>array('id'=>'from_date','readonly'=>'readonly')	
				));
				?>
				<?php echo $form->error($model,'valid_from'); ?>
			</div>
		
			<div class="row">
				<?php echo $form->labelEx($model,'valid_to'); ?>
				<?php //echo $form->textField($model,'valid_to'); ?>
				<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'attribute'=>'valid_to',
				'model' => $model,
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'minDate'=>'new Date()', // Today
					),
				'htmlOptions'=>array('id'=>'to_date','readonly'=>'readonly')	
				));
				?>
				<?php echo $form->error($model,'valid_to'); ?>
			</div>
		
			<div class="row buttons">
				<?php 
				echo CHtml::submitButton("<< Step 1",array('name'=>'step1'));
				echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('name'=>'finish')); ?>
			</div>
			
		</div><!-- end tab 2 -->
	
</div><!-- end of tabs -->

<?php $this->endWidget(); ?>

</div><!-- form -->