<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';


$this->widget('bootstrap.widgets.TbBox', array(
'title' => 'Account Verification',
'headerIcon' => 'icon-user',
'content' => $this->renderPartial('_verification',array('model'=>$model), true)
));

?>
