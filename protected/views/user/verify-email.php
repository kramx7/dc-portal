<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/form.css" />
	<title><?php echo Yii::app()->name?> - Verification Email</title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo Yii::app()->name?></div>
	</div><!-- header -->

	<div id="mainmenu">	</div><!-- mainmenu -->
			<div class="breadcrumbs"></div><!-- breadcrumbs -->
	
	<div class="span-19">
	<div id="content">
		
<h1>Account Verification</h1>
Please verify you account before <?php echo $date = date('F j, Y',strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " +30 days"));?>.<br>
Click on the link below to activate your account and to set your password.
<br>
<a href="<?php echo Yii::app()->params['completeUrl'].'/user/verify/'.$model->verification_code?>">
<?php echo Yii::app()->params['completeUrl'].'/user/verify/'.$model->verification_code?>
</a>

<table class="detail-view" id="yw0"><tr class="odd"><th>ID</th><td><?php echo $model->emailaddress?></td></tr>
<tr class="even"><th>Email Address</th><td><?php echo $model->emailaddress?></td></tr>
<tr class="odd"><th>Office Phone</th><td><?php echo $model->officephone?></td></tr>
<tr class="even"><th>Mobile Phone</th><td><?php echo $model->mobilephone?></td></tr>
<tr class="even"><th>Valid From</th><td><?php echo date('F j,Y',strtotime($model->valid_from));?></td></tr>
<tr class="odd"><th>Valid To</th><td><?php echo date('F j,Y',strtotime($model->valid_to));?></td></tr>
<tr class="even"><th> </th><td></td></tr>
</table>	</div><!-- content -->
</div>
<div class="span-5 last">
</div>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; 2013 by <?php echo Yii::app()->name?>.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::app()->name?>	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
