<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';


$this->breadcrumbs = array(
    'Reset Password',
);
?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Reset Password</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'resetpassword-form',
            'type' => 'inline',
            'enableAjaxValidation' => true,
        ));
        ?>
        <?php
            $this->widget('bootstrap.widgets.TbAlert', array(
                'block' => true, // display a larger alert block?
                'fade' => false, // use transitions?
                'closeText' => false, // close link text - if set to false, no close link is displayed
                'alerts' => array(// configurations per alert type
                    'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
                ),
            ));
            ?>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $form->labelEx($model, 'emailaddress');?>
                <?php echo $form->textFieldRow($model, 'emailaddress');?>
                <?php echo $form->error($model,'emailaddress'); ?>
            </div>
        </div>        
        
        <div class="row-fluid">
            <div class="span12">
                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reset Password')); ?>
                </div>
            </div>    
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- end users-box -->

</div>
