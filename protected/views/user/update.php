<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Update User <?php echo $model->id; ?></h1>

<?php 

 if(User::model()->isUserHasRole(Yii::app()->params['FacAdminCode'])){
	echo $this->renderPartial('_form-facadmin', array('model'=>$model,'tabIndex'=>$tabIndex,'user_role_list'=>$user_role_list,
			'client_list'=>$client_list,'facility_list'=>$facility_list)); 
}else{
	echo $this->renderPartial('_form-general', array('model'=>$model,'tabIndex'=>$tabIndex,'user_role_list'=>$user_role_list,
			'client_list'=>$client_list,'facility_list'=>$facility_list));	
}
?>