<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailaddress')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->emailaddress), array('view', 'id'=>$data->id)); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('user_role')); ?>:</b>
	<?php echo CHtml::encode(substr($data->authRole->description,0,strpos($data->authRole->description,'-')-1)); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('mobilephone')); ?>:</b>
	<?php echo CHtml::encode($data->mobilephone); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('officephone')); ?>:</b>
	<?php echo CHtml::encode($data->officephone); ?>
	<br />

</div>