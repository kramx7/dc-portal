<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->layout = '//layouts/column1';

$this->pageTitle = Yii::app()->name . ' - Power Graph';

//set submenu
$this->submenu = array(
    array('label' => 'Power', 'url' => array('/report/power/kW'), 'active' => true),
    array('label' => 'Environment', 'url' => array('/report/environment/Temperature')),
    array('label' => 'NGER', 'url' => array('/report/nger')),
);

//set submenu2
$this->submenu2 = array(
    array('label' => 'kW', 'url' => array('/report/power/kW'), 'active' => ($model->type == 'kW') ? true : false),
    array('label' => 'kW-h', 'url' => array('/report/power/kWh'), 'active' => ($model->type == 'kWh') ? true : false),
);

$this->breadcrumbs = array(
    'Report' => array('/report'),
    'Power'
);
?>
<?php
Yii::app()->clientScript->registerScript('power', "
 
$('select#facility-id').change(function(){
   
$('#form-status').html('Processing...');
var data = $('#power-form').serialize();
    
    $.ajax({
     type: 'POST',
     url: '" . Yii::app()->createAbsoluteUrl("report/power/" . $model->type) . "',
     data: data,
     success:function(items){
        
        $('#meter').html('');
        $.each(items.meters, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('#meter');
        });
        
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
     },
     error:function(data){
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
    },
     dataType:'json'
    });
    
});

function customRange(input) {

        var min = new Date(2007, 11 - 1, 1);
        var dateMin = new Date(2008, 11 - 1, 1);
        var dateMax = new Date();

        if (input.id === 'from_date') {
                dateMax = jQuery('#to_date').datepicker('getDate');
        }

        if (input.id === 'to_date') {
                dateMin = jQuery('#from_date').datepicker('getDate');
        }

        return {
                minDate: dateMin,
                maxDate: dateMax
        };
}


");
?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><h3>Power</h3></div>
    <div id="roles-box" class="bootstrap-widget-content">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'power-form',
            'type' => 'inline',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                //'validateOnSubmit'=>true,
                'validateOnChange' => true,
            //'validateOnType'=>true,
            ),
        ));
        ?>
        <span id="form-status"></span>
        <div class="row-fluid"> 
            <div class="span3">
                <label>Facility: </label> <?php echo $form->dropDownList($model, 'facility_id', $model->facilities, array('id' => 'facility-id', 'select' => $model->default_facility, 'style' => 'width:200px;')); ?>
            </div>
            <div class="span2">
                <?php echo $form->dropDownList($model, 'meter', $model->meters, array('id' => 'meter', 'select' => $model->default_meter, 'style' => 'width:150px;')); ?>
            </div>
            <div class="span2">
                <label>From Date: </label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'from_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'maxDate' => 'new Date()',
                        'changeMonth' => true,
                        'onClose' => 'js:function(selectedDate) { 
                             $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
                        }',
                        'beforeShow' => 'js:function() { 
                               selectedDate = $("#to_date").val();
                               $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
                        }'
                    ),
                    'htmlOptions' => array('id' => 'from_date', 'readonly' => 'readonly', 'style' => 'width: 80px;
                ')
                ));
                ?>
                <!--<i class="icon-calendar"></i>-->
            </div>
            <div class="span2">
                <label>To Date: </label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'to_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'maxDate' => 'new Date()',
                        'changeMonth' => true,
                        'onClose' => 'js:function(selectedDate) { 
                             $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
                        }',
                        'beforeShow' => 'js:function() { 
                               selectedDate = $("#from_date").val();
                               $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
                        }'
                    ),
                    'htmlOptions' => array('id' => 'to_date', 'readonly' => 'readonly', 'style' => 'width: 80px;
                ')
                ));
                ?>
                <!--<i class="icon-calendar"></i>-->
            </div>
            <div class="span1">
                <?php $this->widget('bootstrap.widgets.TbButton', array('id'=>'submit','buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit')); ?>

            </div>
        </div>
        <?php $this->endWidget(); ?>
        <div class="row-fluid">
            <div class="span12">
                <?php if ($model->status == ''): ?>
                    <img id="power-graph-image" src="<?php echo Yii::app()->request->baseUrl . '/' . $graph->filename; ?>" alt="" class="decoded">
                <?php else: ?>
                    <strong><?php echo $model->status; ?></strong>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-actions"></div>
    </div>
</div>

