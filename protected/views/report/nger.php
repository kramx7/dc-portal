<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->layout = '//layouts/column1';

$this->pageTitle = Yii::app()->name . ' - NGER Graph';

//set submenu
$this->submenu = array(
    array('label' => 'Power', 'url' => array('/report/power/kW'),),
    array('label' => 'Environment', 'url' => array('/report/environment/Temperature')),
    array('label' => 'NGER', 'url' => array('/report/nger'), 'active' => true),
);


$this->breadcrumbs = array(
    'Report' => array('/report'),
    'NGER'
);
?>
<?php
Yii::app()->clientScript->registerScript('nger', "
 
$('select#facility-id, select#year').change(function(){
 
$('#done-loading').remove();
 
$('#form-status').html('Processing...');
var data = $('#nger-form').serialize();
    
    $.ajax({
     type: 'POST',
     url: '" . Yii::app()->createAbsoluteUrl("report/nger") . "',
     data:data,
     success:function(items){
        
        $('#kwh-data').html(items.kwh_data);
        $('#average-pue-data').html(items.average_pue_data);
        $('#total-kwh-data').html(items.total_kwh_data);
        $('#carbon-data').html(items.carbon_data);
        
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
     },
     error:function(data){
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
    },
     dataType:'json'
    });
    
});

");
?>

<style>
    .span6{margin-bottom: 10px;}
</style>  

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><h3>Power</h3></div>
    <div id="roles-box" class="bootstrap-widget-content">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'nger-form',
            'type' => 'inline',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                //'validateOnSubmit'=>true,
                'validateOnChange' => true,
            //'validateOnType'=>true,
            ),
        ));
        ?>

        <div class="row-fluid"> 
            <div class="span6">
                <label>Facility: </label> <?php echo $form->dropDownList($model, 'facility_id', $model->facilities, array('id' => 'facility-id')); ?>
            </div>
        </div>



        <div class="row-fluid"> 
            <div class="span6">
                <label>NGER Data for Year Ending 30th June: </label> <?php echo $form->dropDownList($model, 'year', $model->years, array('id' => 'year')); ?>
            </div>

        </div>

        <?php $this->endWidget(); ?>
        <div class="row-fluid">
            <div class="span12">
                <span id="form-status"></span>
                <table class="table table-condensed" style="width:300px;">
                    <tr>
                        <th>IT kW-h</th>
                        <td id="kwh-data"><?php echo $model->sum_kwh ?></td>
                    </tr>
                    <tr>
                        <th>PUE</th>
                        <td id="average-pue-data"><?php echo $model->average_pue ?></td>
                    </tr>
                    <tr>
                        <th>Total kW-h</th>
                        <td id="total-kwh-data"><?php echo $model->sum_total_kwh ?></td>
                    </tr>
                    <tr>
                        <th>CO<sub>2</sub></th>
                        <td id="carbon-data"><?php echo $model->sum_carbon ?> T</td>
                    </tr>
                    
                </table>
                <div class="form-actions"></div>
            </div>
        </div>
    </div>
</div>

