<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->layout = '//layouts/column1';

$this->pageTitle = Yii::app()->name . ' - Environment Graph';

//set submenu
$this->submenu = array(
    array('label' => 'Power', 'url' => array('/report/power/kW')),
    array('label' => 'Environment', 'url' => array('/report/environment/Temperature'), 'active' => true),
    array('label' => 'NGER', 'url' => array('/report/nger')),
);

//set submenu2
$this->submenu2 = array(
    array('label' => 'Temperature', 'url' => array('/report/environment/Temperature'), 'active' => ($model->type == 'Temperature') ? true : false),
    array('label' => 'Humidity', 'url' => array('/report/environment/Humidity'), 'active' => ($model->type == 'Humidity') ? true : false),
);

$this->breadcrumbs = array(
    'Report' => array('/report'),
    'Environment'
);
?>
<?php
Yii::app()->clientScript->registerScript('environment', "

$('select#facility-id').change(function(){
   
$('#form-status').html('Processing...');

var data = $('#environment-form').serialize();
    
    $.ajax({
     type: 'POST',
     url: '" . Yii::app()->createAbsoluteUrl("report/environment/" . $model->type) . "',
     data: data,
     success:function(items){
        
        $('#sensor').html('');
        $.each(items.sensors, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('#sensor');
        });
        
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
     },
     error:function(data){
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        $('#form-status').html('');
    },
     dataType:'json'
    });
    
});

");
?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><h3>Power</h3></div>
    <div id="roles-box" class="bootstrap-widget-content">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'environment-form',
            'type' => 'inline',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                //'validateOnSubmit'=>true,
                'validateOnChange' => true,
            //'validateOnType'=>true,
            ),
        ));
        ?>
        <span id="form-status"></span>
        <div class="row-fluid"> 
            <div class="span3">
                <label>Facility: </label> <?php echo $form->dropDownList($model, 'facility_id', $model->facilities, array('id' => 'facility-id', 'select' => $model->default_facility, 'style' => 'width:200px;')); ?>
            </div>
            <div class="span2">
                <?php echo $form->dropDownList($model, 'sensor', $model->sensors, array('id' => 'sensor', 'style' => 'width:150px;')); ?>
            </div>
            <div class="span2">
                <label>From Date: </label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'from_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'maxDate' => 'new Date()',
                        'changeMonth' => true,
                        'onClose' => 'js:function(selectedDate) { 
                             $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
                        }',
                        'beforeShow' => 'js:function() { 
                               selectedDate = $("#to_date").val();
                               $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
                        }'
                    ),
                    'htmlOptions' => array('id' => 'from_date', 'readonly' => 'readonly', 'style' => 'width: 80px;')
                ));
                ?>
                <!--<i class="icon-calendar"></i>-->
            </div>
            <div class="span2">
                <label>To Date: </label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'to_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'maxDate' => 'new Date()',
                        'changeMonth' => true,
                        'onClose' => 'js:function(selectedDate) { 
                             $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
                        }',
                        'beforeShow' => 'js:function() { 
                               selectedDate = $("#from_date").val();
                               $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
                        }'
                    ),
                    'htmlOptions' => array('id' => 'to_date', 'readonly' => 'readonly', 'style' => 'width: 80px;')
                ));
                ?>
                <!--<i class="icon-calendar"></i>-->
            </div>
            <div class="span1">
                <?php $this->widget('bootstrap.widgets.TbButton', array('id'=>'submit','buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit')); ?>

            </div>
        </div>
        <?php $this->endWidget(); ?>
        <div class="row-fluid">
            <div class="span12">
                <?php if ($model->status == ''): ?>
                    <img id="environment-graph-image" src="<?php echo Yii::app()->request->baseUrl . '/' . $graph->filename; ?>" alt="" class="decoded">
                <?php else: ?>
                    <strong><?php echo $model->status; ?></strong>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-actions"></div>
    </div>
</div>

