<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <!-- yiibooster CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-box.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-editable.min.css" />
        <style>

            ul.top-right-nav li{
                border-right: #000 solid 1px;
                display: block;
                float:left;
                padding-left:10px;
                padding-right:10px;
            }     
            ul.top-right-nav li:last-child {
                border-right: none;
            }  
            .rounded{
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
                margin-top: 40px; 

            }
            .nav-collapse ul.nav{width:800px;}
            .btn-group a{margin:7px; padding:7px;}
            .btn-group button{margin:7px; padding:7px; width: 90px;}
            div.navbar-inner div.container{width: 1170px;}
            div.navbar-inner div.container ul{clear:both;}
            div.breadcrumbs{margin: 0px; padding:3px 20px;}
        </style>
    </head>

    <body>
        <div class="container-fluid" style="position:fixed; right: 20px; top: 7px; z-index:9999;">
            <?php if (!Yii::app()->user->isGuest): ?>
                <ul class="top-right-nav">
                    <li><?php echo CHtml::link('Hello ' . Yii::app()->user->name, array('admin/profile/' . Yii::app()->user->id)) ?></li>
                    <li><?php echo CHtml::link('Profile ', array('/admin/profile/')) ?></li>
                    <li><?php echo CHtml::link('Logout ', array('site/logout')) ?></li>
                </ul>
            <?php endif; ?>
        </div>  
        <div class="navbar">
            <div class="navbar-inner">

                <div class="container-fluid">
                    <div class="nav-collapse collapse">

                        <?php
                        // to serve as params to auth
                        $user = User::model()->findByPk(Yii::app()->user->id);
                        $facility = new Facility();
                        $client = new Client();
                        $params = array('user'=>$user,'facility'=>$facility,'client'=>$client);
                        
                        $this->widget('bootstrap.widgets.TbNavbar', array(//1
                            'brand' => CHtml::encode(Yii::app()->name),
                            'items' =>
                            array(//2
                                array('class' => 'bootstrap.widgets.TbMenu', //3
                                    'items' =>
                                    array(//4
                                        array('label' => 'Dashboard', 'url' => array('/site/index')),
                                        array('label' => 'Administration', 'url' => array('/admin/profile'), 'visible' => !Yii::app()->user->isGuest,
                                            'items' =>
                                            array(
                                                 array('label'=>'Profile','url'=>array('/admin/profile')),
                                                array('label'=>'System','url'=>array('/admin/system'), 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                                                array('label'=>'Users','url'=>array('/admin/user'), 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                                                array('label'=>'Roles','url'=>array('/admin/role'), 'visible'=>User::model()->isAuthorized('ManageRole') ),
                                                array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                                                array('label'=>'Facilities','url'=>array('/admin/facility'), 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
                                            )
                                        ),
                                        array('label' => 'Requests', 'url' => array('#'), 'visible' => !Yii::app()->user->isGuest,
                                            'items' =>
                                            array(
                                                array('label' => 'Facility Access', 'url' => array('/request/facilityaccess'), 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                                                array('label' => 'Proximity Card', 'url' => array('/request/proximity'), 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard')),
                                                array('label' => 'Equipment Movement', 'url' => array('/request/equipment'),  'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement),'ApproveEMF')),
                                                array('label' => 'Remote Hands', 'url' => array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement),'ApproveEMF')),
                                            )
                                        ),
                                        array('label' => 'Reports', 'url' => array('/report/power/kW'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)),
                                            'items' =>
                                            array(
                                                array('label' => 'Power', 'url' => array('/report/power/kW'),  'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)),
                                                    'items' =>
                                                    array(
                                                        array('label' => 'KW', 'url' => array('/report/power/kW'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ),
                                                        array('label' => 'KW-H', 'url' => array('/report/power/kWh'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ),
                                                    ),
                                                ),
                                                array('label' => 'Environment', 'url' => array('/report/environment/Temperature'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ,
                                                    'items' =>
                                                    array(
                                                        array('label' => 'Temperature', 'url' => array('/report/environment/Temperature'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ),
                                                        array('label' => 'Humidity', 'url' => array('/report/environment/Humidity'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ),
                                                    ),
                                                ),
                                                array('label' => 'NGER', 'url' => array('/report/nger'), 'visible'=>User::model()->isAuthorized('ReadReport', array('report'=>new GraphForm)) ),
                                            )
                                        ),
                                        array('label' => 'Library', 'url' => array('/library'), 'visible' => !Yii::app()->user->isGuest),
                                        array('label' => 'Contact Us', 'url' => array('/site/contact')),
                                        array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                    )//4
                                )//3
                            )// 2
                                )//1
                        );
                        ?>
                    </div><!--/.nav-collapse -->
                </div>

            </div>
        </div>

        <div class="container-fluid" style="margin-top:50px;">
            <?php if (count($this->submenu) > 0): ?>
                <div class="row-fluid">
                    <div class="span12" style="height: 40px;">
                        <?php
                        $this->widget('bootstrap.widgets.TbMenu', array(
                            'type' => 'pills',
                            'items' => $this->submenu
                                )
                        );
                        ?><!-- submenu -->
                    </div>
                </div>
            <?php endif ?>

            <?php if (count($this->submenu2) > 0): ?>
                <div class="row-fluid">
                    <div class="span12" style="height: 40px;">
                        <?php
                        $this->widget('bootstrap.widgets.TbMenu', array(
                            'type' => 'pills',
                            'items' => $this->submenu2
                                )
                        );
                        ?><!-- submenu -->
                    </div>
                </div>
            <?php endif ?>

            <?php if (count($this->breadcrumbs) > 0): ?>
                <div class="row-fluid">
                    <div class="span12">
                        <?php
                        $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links' => $this->breadcrumbs,
                            'homeLink' => CHtml::link('Dashboard', array('site/index')),
                        ));
                        ?><!-- breadcrumbs -->
                    </div>
                </div>
            <?php endif ?>
        </div>    

        <div id="content" class="container-fluid" style="margin-top:0px; padding-top: 0px;">       
            <?php echo $content; ?>
        </div>       

    </body>
</html>
