<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="/dcportal/assets/a20b1672/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/dcportal/assets/a20b1672/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="/dcportal/assets/a20b1672/css/bootstrap-yii.css" />
<link rel="stylesheet" type="text/css" href="/dcportal/assets/a20b1672/css/jquery-ui-bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/dcportal/assets/a20b1672/css/bootstrap-notify.css" />
<script type="text/javascript" src="/dcportal/assets/564a9767/jquery.js"></script>
<script type="text/javascript" src="/dcportal/assets/a20b1672/js/bootstrap.bootbox.min.js"></script>
<script type="text/javascript" src="/dcportal/assets/a20b1672/js/bootstrap.notify.js"></script>
<script type="text/javascript" src="/dcportal/assets/a20b1672/js/bootstrap.js"></script>
<title>Fujitsu Data Center Portal - Role Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- yiibooster CSS framework -->
    <link rel="stylesheet" type="text/css" href="/dcportal/css/bootstrap.min.css" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="/dcportal/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="/dcportal/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/dcportal/css/form.css" />
    <style>
    
      ul.top-right-nav li{
        border-right: #000 solid 1px;
        display: block;
        float:left;
        padding-left:10px;
        padding-right:10px;
      }     
     ul.top-right-nav li:last-child {
        border-right: none;
      }  
      .rounded{
          -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
                margin-top: 40px; 
      
      }
      .nav-collapse ul.nav{width:800px;}
      .btn-group a{margin:7px; padding:7px;}
      .btn-group button{margin:7px; padding:7px; width: 90px;}
      div.navbar-inner div.container{width: 1170px;}
      div.navbar-inner div.container ul{clear:both;}
      div.breadcrumbs{margin: 0px; padding:3px 20px;}
    </style>
  </head>

  <body>
    <div class="container-fluid" style="position:fixed; right: 20px; top: 7px; z-index:9999;">
                   <ul class="top-right-nav">
                <li><a href="/dcportal/user/view/1">Hello Jan Mark Salarda</a></li>
                <li><a href="/dcportal/admin/profile">Profile </a></li>
                <li><a href="/dcportal/site/logout">Logout </a></li>
            </ul>
            </div>  
    <div class="navbar">
      <div class="navbar-inner">
        
        <div class="container-fluid">
          <div class="nav-collapse collapse">
              
             <div class="navbar navbar-fixed-top"><div class="navbar-inner"><div class="container"><a href="/dcportal/" class="brand">Fujitsu Data Center Portal</a><ul id="yw0" class="nav"><li><a href="/dcportal/site/index">Dashboard</a></li><li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/dcportal/admin">Administration <span class="caret"></span></a><ul id="yw1" class="dropdown-menu"><li><a tabindex="-1" href="/dcportal/admin/profile">Profile</a></li><li><a tabindex="-1" href="/dcportal/admin/system">System</a></li><li><a tabindex="-1" href="/dcportal/admin/user">Users</a></li><li class="active"><a tabindex="-1" href="/dcportal/admin/role">Roles</a></li><li><a tabindex="-1" href="/dcportal/admin/customer">Customers</a></li><li><a tabindex="-1" href="/dcportal/fadmin/acility">Facilities</a></li></ul></li><li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/dcportal/request">Requests <span class="caret"></span></a><ul id="yw2" class="dropdown-menu"><li><a tabindex="-1" href="/dcportal/dcaccess">DC Access</a></li><li><a tabindex="-1" href="/dcportal/equipment">Equipment Movement</a></li><li><a tabindex="-1" href="/dcportal/badge">Badge</a></li><li><a tabindex="-1" href="/dcportal/remotehands">Remote Hands</a></li></ul></li><li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/dcportal/report">Reports <span class="caret"></span></a><ul id="yw3" class="dropdown-menu"><li><a tabindex="-1" href="/dcportal/power">Power</a></li><li><a tabindex="-1" href="/dcportal/environment">Environment</a></li><li><a tabindex="-1" href="/dcportal/nger">NGER</a></li></ul></li><li><a href="/dcportal/library">Library</a></li><li><a href="/dcportal/site/contact">Contact Us</a></li></ul></div></div></div>          </div><!--/.nav-collapse -->
        </div>
          
      </div>
    </div>
        
      <div class="container-fluid" style="margin-top:50px;">
                  <div class="row-fluid">
              <div class="span12" style="height: 40px;">
           <ul id="yw4" class="nav nav-pills"><li><a href="/dcportal/admin/profile">Profile</a></li><li><a href="/dcportal/admin/system">System</a></li><li><a href="/dcportal/admin/user">Users</a></li><li class="active"><a href="/dcportal/admin/role">Roles</a></li><li><a href="/dcportal/admin/customer">Customers</a></li><li><a href="/dcportal/facility">Facilities</a></li></ul><!-- submenu -->
              </div>
          </div>
                  
                   <div class="row-fluid">
              <div class="span12">
            <div class="breadcrumbs">
<a href="/dcportal/site/index">Dashboard</a> &raquo; <a href="/dcportal/admin">Administration</a> &raquo; <span>Roles</span></div><!-- breadcrumbs -->
            </div>
          </div>
              </div>    
            
     <div id="content" class="container-fluid" style="margin-top:0px; padding-top: 0px;">       
        <div class="row-fluid">
       <div class="span12"> 
	<div class="bootstrap-widget table"><div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Manage Roles</h3></div><div id="roles-box" class="bootstrap-widget-content"></div></div>       </div>    
</div><!-- content -->
      </div>       

  <script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
jQuery('body').tooltip({'selector':'[rel=tooltip]'});
jQuery('body').popover({'selector':'[rel=popover]'});
});
/*]]>*/
</script>
</body>
</html>
