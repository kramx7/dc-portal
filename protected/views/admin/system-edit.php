<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system'), 'active'=>true, 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                    array('label'=>'Users','url'=>array('/admin/user'), 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                    array('label'=>'Roles','url'=>array('/admin/role'), 'visible'=>User::model()->isAuthorized('ManageRole') ),
                    array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                    array('label'=>'Facilities','url'=>array('/admin/facility'), 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
               );  



$this->breadcrumbs = array(
    'Administration',
    'System',
);
?>

<?php
Yii::app()->clientScript->registerScript('users', "

$('.user-edit').dblclick(function(){
    user_id = $(this).attr('userid');
    window.location = '".$this->createAbsoluteUrl('/admin/updateuser')."/'+user_id;
});

");
?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>System Configurations</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'system-form',
            'enableAjaxValidation'=>true,
            )); 
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <div class="row-fluid">
            <div class="span12" style="margin-bottom:5px;">
                <?php echo $form->labelEx($model,'default_priority'); ?>
                <?php echo $form->dropDownList($model, 'default_priority', $model->priority_list, array('empty'=>'Select'));?>
                <?php echo $form->error($model,'default_priority'); ?>
            </div>
        </div>    

        <div class="row-fluid">
            <div class="span12" style="margin-bottom:5px;">        
                <?php echo $form->labelEx($model,'locked_limit'); ?>
                <?php echo $form->textField($model, 'locked_limit');?>
                <?php echo $form->error($model,'locked_limit'); ?>
            </div>
        </div><!-- end row-fluid --> 

        <div class="row-fluid">
            <div class="span12">
                <div class="form-actions"><?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save')); ?>
                </div>
            </div>    
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- end users-box -->

</div>
