<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system'), 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                    array('label'=>'Users','url'=>array('/admin/user'), 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                    array('label'=>'Roles','url'=>array('/admin/role'), 'active'=>true, 'visible'=>User::model()->isAuthorized('ManageRole') ),
                    array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                    array('label'=>'Facilities','url'=>array('/admin/facility'), 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
               );  



$this->breadcrumbs = array(
    'Administration',
    'Roles' => array('/admin/role'),
    'Create New Role'
);
?>

    <?php 

    if($model->isNewRecord) {
        Yii::app()->clientScript->registerScript('new-authitem', '
            $("#role-save-disable").click(function() {
                //$(".myeditable").editable("submit", { 
                $(this).parent().find(".editable").editable("submit", {
                    url: "'.$this->createUrl('admin/createrole').'",
                    ajaxOptions: { dataType: "json" },
                    success: function(data, config) {

                        if(data && data.id) {
                            $(this).editable("option", "pk", data.id);
                            $(this).removeClass("editable-unsaved");
                            $("#msg").removeClass("alert-error").addClass("alert-success")
                            .html("Role created! Now you can update it.").show();
                            $("#role-save").hide();
                        } else {
                            config.error.call(this, data && data.errors ? data.errors : "Unknown error");
                        }
                    },
                error: function(errors) {
                    var msg = "";
                    
                    if(errors && errors.responseText) {
                        msg = errors.responseText;
                    } else {
                        $.each(errors, function(k, v) { msg += v+"<br>"; });
                    }
                    $("#msg").removeClass("alert-success").addClass("alert-error")
                    .html(msg).show();
                }

                });
            });'
        );// end registerScript
    
    }// end if
    
    ?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Create New Role</h3></div>
    <div id="roles-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'role-form',
            'type' => 'horizontal',
            //'enableAjaxValidation' => true,
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'info' => array('block' => true, 'fade' => false,), // success, info, warning, error or danger
            ),
        ));
        ?>
        <?php //echo $form->errorSummary($model); ?>

        <div class = "row-fluid">
            <div class = "span12">
                <?php echo $form->textFieldRow($model, 'name');?> 
            </div>
        </div>
        <div class = "row-fluid">
            <div class = "span12">
                <?php echo $form->error($model, 'allowed_permissions'); ?>
                <div class="grid-view" style="height:200px; overflow: scroll;">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <?php if (array_key_exists('ManagePortalLogin', $model->assigned_permissions)): ?>
                                <th>Portal Login</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageConfiguration', $model->assigned_permissions)): ?>
                                <th>Portal Configuration</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageCustomer', $model->assigned_permissions)): ?>
                                <th>Customers</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageFacility', $model->assigned_permissions)): ?>
                                <th>Facilities</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRole', $model->assigned_permissions)): ?>
                                <th>Roles</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageUser', $model->assigned_permissions)): ?>
                                <th>Users</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageProximityCard', $model->assigned_permissions)): ?>
                                <th>Proximity Card</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageEMF', $model->assigned_permissions)): ?>
                                <th>EMF</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageDCAccess', $model->assigned_permissions)): ?>
                                <th>DC Access</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageTelcoAccess', $model->assigned_permissions)): ?>
                                <th>Telco/Carrier Access</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRemoteHands', $model->assigned_permissions)): ?>
                                <th>Remote Hands</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManagePost', $model->assigned_permissions)): ?>
                                <th>Bulletin Board</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageLibraryContent', $model->assigned_permissions)): ?>
                                <th>Library</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageReport', $model->assigned_permissions)): ?>
                                <th>Report</th>
                            <?php endif; ?>
                        </tr> 
                         <?php 
                            $params = array('ajax' => 'editable-form'); 
                           ?>
                        <tr>
                            <?php if (array_key_exists('ManagePortalLogin', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $source = $model->getPermissionOptions('PortalLoginGroup');
                                    if(count($source) == 1){
                                        $source[""]="No";
                                    }

                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'portallogin_permission',
                                        'url' => $this->createUrl('admin/createrole'), //url for submit data
                                        'source' => $source,
                                        'mode' => 'inline',
                                        //'emptytext' => $model->portallogin_permission,
                                        'params' => array('ajax' => 'editable-form','permission_type'=>'ManagePortalLogin'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageConfiguration', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'portalconfiguration_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('PortalConfigurationGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->portalconfiguration_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageConfiguration'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageCustomer', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'customers_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('CustomerGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->customers_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageCustomer'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageFacility', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'facilities_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('FacilityGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->facilities_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageFacility'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRole', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'roles_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('RoleGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->roles_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageRole'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageUser', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'users_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('UserGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->users_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageUser'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageProximityCard', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'proximitycard_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('ProximityCardGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->proximitycard_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageProximityCard'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageEMF', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'emf_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('EMFGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->emf_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageEMF'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageDCAccess', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'dcaccess_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('DCAccessGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->dcaccess_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageDCAccess'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageTelcoAccess', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'telcoaccess_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('TelcoAccessGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->telcoaccess_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageTelcoAccess'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRemoteHands', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'remotehands_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('RemoteHandsGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->remotehands_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageRemoteHands'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManagePost', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'bulletinboard_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('BulletinBoardGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->bulletinboard_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManagePost'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageLibraryContent', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'library_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $model->getPermissionOptions('LibraryGroup'),
                                        'mode' => 'inline',
                                        //'emptytext' => $model->library_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageLibraryContent'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageReport', $model->assigned_permissions)): ?>
                                <td>
                                    <?php
                                    $source = $model->getPermissionOptions('ReportGroup');
                                    if(count($source) == 1){
                                        $source[""] = "No Permission";        
                                    }

                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $temp_model,
                                        'attribute' => 'report_permission',
                                        'url' => $this->createUrl('/admin/createrole'), //url for submit data
                                        'source' => $source,
                                        'mode' => 'inline',
                                        //'emptytext' => $model->report_permission,
                                        'params'=>array('ajax' => 'editable-form','permission_type'=>'ManageReport'),
                                    ));
                                    ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    </table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save', 'htmlOptions' => array('id' => 'role-save', 'name' => 'save', 'style' => 'margin-right:10px;'))); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/admin/role'), 'htmlOptions' => array('name' => 'back'))); ?>
                </div>
            </div>    
        </div>

        <?php $this->endWidget(); ?>

    </div>

</div>