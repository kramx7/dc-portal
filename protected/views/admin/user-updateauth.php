<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system')),
                    array('label'=>'Users','url'=>array('/admin/user'),'visible'=>User::model()->isAuthorized('viewUsers')),
                    array('label'=>'Roles','url'=>array('/admin/role')),
                    array('label'=>'Customers','url'=>array('/admin/customer')),
                    array('label'=>'Facilities','url'=>array('/facility')),
               );     
                                        


$this->breadcrumbs=array(
	'Administration'=>array('/admin'),
	'Users',
);

$this->menu=array(
	array('label'=>'Create New User', 'url'=>array('/admin/createuser')),
	array('label'=>'View Selected User', 'url'=>array('/admin/viewuser')),
        array('label'=>'Update Selected', 'url'=>array('/admin/updateuser')),
        array('label'=>'Update Authority Matrix', 'url'=>array('/admin/updateauthuser'))
);

$this->widget('bootstrap.widgets.TbBox', array(
'title' => 'Update Authority Matrix',
'headerIcon' => 'icon-user',
'content' => $this->renderPartial('_user-updateauth',array('model'=>$model), true)
));

?>