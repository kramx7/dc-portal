<?php
echo CHtml::scriptFile(Yii::app()->request->baseUrl . '/js/code.assembly.password.strength.js');

Yii::app()->clientScript->registerScript('pass-strength', '
$(".password_test").keyup(function(){
    passwordStrength($(this).val());
});
'
);
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/code.assembly.password.strength.css" />
<style>
    .form-horizontal .control-group {
        margin-bottom: 3px;
    }
</style>    
<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'profile-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
        ));
?>

<?php
/*$this->widget('bootstrap.widgets.TbLabel', array(
    'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
    'label' => 'Fields with * are required.',
));*/
?>

<div class="control-group"></div>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => false, // use transitions?
    'closeText' => false, // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => false,),// 'closeText' => '×'), // success, info, warning, error or danger
    ),
));
?>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label for="User_emailaddress" class="control-label">Email Address</label>
            <div class="controls">
                <label for="User_emailaddress" class="control-label"><?php echo CHtml::encode($model->emailaddress) ?></label>
                <?php echo CHtml::hiddenField('email_address', CHtml::encode($model->emailaddress), array('id' => 'email_address')); // user for password strength  ?>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="control-group">
            <label for="User_valid_to" class="control-label">Expires</label>
            <div class="controls">
                <label for="User_valid_to" class="control-label" style="text-align:left;"><?php echo CHtml::encode($model->valid_to) ?></label>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span6"><?php echo $form->textFieldRow($model, 'first_name'); ?></div>
    <div class="span6"><?php echo $form->textFieldRow($model, 'last_name'); ?></div>
</div>  

<div class="row-fluid">
    <div class="span6"><?php echo $form->textFieldRow($model, 'mobilephone'); ?></div>
    <div class="span6"><?php echo $form->textFieldRow($model, 'officephone'); ?></div>
</div>

<div class="row-fluid">
    <div class="span6"><?php echo $form->textFieldRow($model, 'officefax'); ?></div>
    <div class="span6"><?php echo $form->textFieldRow($model, 'remote_hands'); ?></div>
</div>

<div class="row-fluid">
    <div class="span6">
        <h5 style="margin:0px">Change Password</h5>
        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'If you would like to change the password, type a new one. Otherwise leave this blank.',
        ));
        ?>

        <div class="control-group"></div>
        
        <div class="control-group">
            <label for="User_password" class="control-label" style="width:200px; margin-right:5px; ">Old Password</label>
            <div class="controls">
                <label for="User_password" class="control-label"><?php echo $form->passwordField($model,'old_password'); ?></label>
                <span style="" id="User_password_em_" class="help-inline error"><?php echo $form->error($model,'old_password'); ?></span>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        
        <div class="control-group">
            <label for="User_new_password" class="control-label" style="width:200px; margin-right:5px; ">New Password</label>
            <div class="controls">
                <label for="User_new_password" class="control-label"><?php echo $form->passwordField($model,'new_password',array('class' => 'password_test')); ?></label>
                <span style="" id="User_new_password_em_" class="help-inline error"><?php echo $form->error($model,'new_password'); ?></span>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label for="User_password_compare" class="control-label" style="width:200px; margin-right:5px; ">Type your new password again</label>
            <div class="controls">
                <label for="User_password_compare" class="control-label"><?php echo $form->passwordField($model,'password_compare'); ?></label>
                <span style="" id="User_password_compare_em_" class="help-inline error"><?php echo $form->error($model,'password_compare'); ?></span>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="control-group">
            <label for="User_strength_indicator" class="control-label">Password Strength</label>
            <div class="controls">
                <div id="passwordDescription">Password not entered</div>
                <div id="passwordStrength" class="strength0"></div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <div class="alert in alert-block fade alert-info"><strong>Hint</strong>: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols !" ? $ % ^ & )
        </div>

    </div>
</div>   

<div class="row-fluid">
    <div class="span12">
        <h5 style="margin:0px">Email Me</h5>

        <?php
        /*echo $form->checkBoxListRow($model, 'notification', array(
            1=>'When a new message is posted to the bulletin board.',
            2=>'When I have a new work item.',
            3=>'When my request status is updated.',
        ));*/
        ?>
        <?php echo $form->checkBoxList($model, 'notify_noticeboard', array(
            1=>'When a new message is posted to the bulletin board.'));
            ?>
        <?php echo $form->checkBoxList($model, 'notify_workitem', array(
            1=>'When I have a new work item.'));
            ?>
        <?php echo $form->checkBoxList($model, 'notify_request_update', array(
            1=>'When my request status is updated.'));
            ?>        
    </div
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Update Profile', 'htmlOptions' => array('name' => 'update'))); ?>

        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
