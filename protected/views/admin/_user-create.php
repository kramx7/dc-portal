<style>
    .cust-label{
        float:left;
        width:100px;
    }
    .row-fluid{
        margin-bottom: 5px;  
    }
</style>

<?php
Yii::app()->clientScript->registerScript('create', "

$('#assign-op').change(function(){
    $('#done-loading').remove();
    $('#user-action').val('assign');
    updateForm();
})

$('#user-create').click(function(){
    $('#user-action').val('');
    //updateForm();
    $('#assigned-item').attr('multiple','multiple');
    $('#assigned-item option').attr('selected', 'selected');
    $('#user-form').submit();
})

$('#user-remove').click(function(){
    //$('#user-action').val('remove');
    
    if($('#assigned-item').val() != null){
       
        var options = $('#assigned-item option:selected').sort().clone();
        $('#unassigned-item').append(options);
        $('#assigned-item option:selected').remove();
        
    } 
});

$('#user-add').click(function(){
    
    //$('#user-action').val('add');
    
    if($('#unassigned-item').val() != null){
       var options = $('#unassigned-item option:selected').sort().clone();
        $('#assigned-item').append(options);
        $('#unassigned-item option:selected').remove();
        
    } 
    
});


function updateForm(){
    
    $('#assigned-item').attr('multiple','multiple');
    $('#assigned-item option').attr('selected', 'selected');
    
    var data = $('#user-form').serialize();
    
    $.ajax({
     type: 'POST',
     url: '" . Yii::app()->createAbsoluteUrl("admin/createuser") . "',
     data:data,
     success:function(items){
        
        ///$('body').html(items);
        
        $('#assigned-item').html('');
        
        $.each(items.assigned_items, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('#assigned-item');
        });
        
        $('#unassigned-item').html('');
        
        $.each(items.unassigned_items, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('#unassigned-item');
        });
        
        $('#user-action').val(items.action);
        $('#prev-assign-op').val(items._prev_assign_op);
        
        $('.form-actions').append('<span id=\"done-loading\"></span>');
        
     },
     error:function(data){
       $('.form-actions').append('<span id=\"done-loading\"></span>');
    },
     dataType:'json'
    });
    
}

$('#from_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	onClose: function (selectedDate) {
		     
            to_date = $('#to_date').val();
		     	      		
              if(selectedDate != 'now'){
                    var d = new Date(selectedDate);
                    d.setDate(d.getDate() + 1);
                    $('#to_date').datepicker('option','minDate', d);
             }
             
             if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val(to_date);
             }       
          }
	});
        
$('#to_date').datepicker(
	{
	dateFormat:'yy-mm-dd',
	minDate: new Date(),
	beforeShow: function (input, dp) {
		  
            from_date = $('#from_date').val();
            to_date = $(this).val(); 	      
            
            if(from_date == 'now'){
                from_date = new Date();
            }        
            
            var d = new Date(from_date);
            d.setDate(d.getDate() + 1);
            
            min_date = $.datepicker.formatDate('yy-mm-dd',d);
            
            $(this).datepicker('option','minDate', min_date);
              
          },
          onClose: function (selectedDate) {
            // used to set the from date when it is cleared
            from_date = $('#from_date').val(); 

            if($.trim($('#to_date').val()) == ''){
             	$('#to_date').val('never');
             }
            
            if($('#to_date').val() != 'never'){
                var d = new Date(selectedDate);
                d.setDate(d.getDate() - 1);
                $('#from_date').datepicker('option','maxDate', d);
             }
             
            if($.trim($('#from_date').val()) == ''){
             	$('#from_date').val(from_date);
             } 
                
           }
	});


");


/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'user-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
        ));
?>

<?php
$this->widget('bootstrap.widgets.TbLabel', array(
    'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
    'label' => 'Fields with * are required.',
));
?>

<div class="control-group"></div>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => false, // use transitions?
    'closeText' => false, // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
    ),
));
?>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
    <div class="span12">
        <label class="cust-label">Select Facility</label> 
        <?php
        if (count($model->_facility_options) > 0) {
            $html_options = array('style' => 'width: 260px;', 'empty' => 'Do not associate with a facility');
        } else {
            $html_options = array('style' => 'width: 260px;');
        }
        echo $form->dropDownList($model, '_facility', $model->_facility_options, $html_options);
        ?>
        <?php echo $form->error($model, '_facility'); ?>
    </div>    
</div>

<div class="row-fluid">
    <div class="span12">
        <label class="cust-label">Email Address</label> <?php echo $form->textField($model, 'emailaddress'); ?>
        <?php echo $form->error($model, 'emailaddress'); ?>
    </div>    
</div>

<div class="row-fluid">
    <div class="span6">
        <label class="cust-label">First Name</label> <?php echo $form->textField($model, 'firstname'); ?>
        <?php echo $form->error($model, 'firstname'); ?>
    </div>
    <div class="span6">
        <label class="cust-label">Last Name</label> <?php echo $form->textField($model, 'lastname'); ?>
        <?php echo $form->error($model, 'lastname'); ?>
    </div>
</div> 

<div class="row-fluid">
    <div class="span6">
        <label class="cust-label">Mobile Number</label> <?php echo $form->textField($model, 'mobilephone'); ?>
        <?php echo $form->error($model, 'mobilephone'); ?>
    </div>
    <div class="span6">
        <label class="cust-label">Office Number</label> <?php echo $form->textField($model, 'officephone'); ?>
        <?php echo $form->error($model, 'officephone'); ?>
    </div>
</div> 

<div class="row-fluid">
    <div class="span6">
        <label class="cust-label">Valid From</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'attribute' => 'valid_from',
            'model' => $model,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'minDate' => 'new Date()',
            ),
            'htmlOptions' => array('id' => 'from_date', 'readonly' => 'readonly', 'style' => 'width: 100px;')
        ));
        ?>
        <i class="icon-calendar"></i>
        <?php echo $form->error($model, 'valid_from'); ?>

    </div>
    <div class="span6">
        <label class="cust-label">Expire</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'attribute' => 'valid_to',
            'model' => $model,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'minDate' => 'new Date()',
            ),
            'htmlOptions' => array('id' => 'to_date', 'readonly' => 'readonly', 'style' => 'width: 100px;')
        ));
        ?>
        <i class="icon-calendar"></i>
        <?php echo $form->error($model, 'valid_to'); ?>

    </div>
</div>


<div class="row-fluid">
    <div class="span12">

        <label class="cust-label" style="width:160px;">Associate with Customer</label> 
        <?php
        if (count($model->_client_options) > 0) {
            $html_options = array('style' => 'width: 260px;', 'empty' => 'Do not associate with a customer');
        } else {
            $html_options = array('style' => 'width: 260px;');
        }
        echo $form->dropDownList($model, '_client', $model->_client_options, $html_options);
        ?>
        <?php echo $form->error($model, '_client'); ?>
    </div>
</div>


<div class="row-fluid">
    <div class="span4 offset4">
        <?php
        echo $form->dropDownList($model, '_assign_op', $model->_assign_options, array('id' => 'assign-op'));
        ?>
    </div>
</div>   


<div class="row-fluid">
    <div class="span4 offset1">
        <?php
        echo $form->dropDownList($model, '_assigned_item', $model->_assigned_items, array('name' => 'User[_assigned_items][]', 'size' => 10, 'id' => 'assigned-item', 'multiple' => 'multiple'));
        ?>
    </div>
    <div class="span2">
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'stacked' => true,
            'buttons' => array(
                array('buttonType' => 'button', 'label' => 'Remove >>', 'htmlOptions' => array('id' => 'user-remove')),
                array('buttonType' => 'button', 'label' => '<< Add', 'htmlOptions' => array('id' => 'user-add')),
            ),
        ));
        ?>
    </div>
    <div class="span4">
        <?php
        echo $form->dropDownList($model, '_unassigned_item', $model->_unassigned_items, array('name' => 'User[_unassigned_items][]', 'size' => 10, 'id' => 'unassigned-item', 'multiple' => 'multiple'));
        ?>
    </div>
</div>
<?php echo $form->hiddenField($model, '_action', array('id' => 'user-action')); ?>
<?php echo $form->hiddenField($model, '_prev_assign_op', array('id' => 'prev-assign-op')); ?>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'button', 'type' => 'primary', 'label' => 'Create User', 'htmlOptions' => array('name' => 'create', 'id' => 'user-create'))); ?>

</div>


<?php $this->endWidget(); ?>
