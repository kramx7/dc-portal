<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system'), 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                    array('label'=>'Users','url'=>array('/admin/user'), 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                    array('label'=>'Roles','url'=>array('/admin/role'), 'visible'=>User::model()->isAuthorized('ManageRole') ),
                    array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                    array('label'=>'Facilities','url'=>array('/admin/facility'), 'active'=>true, 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
               );     



$this->breadcrumbs = array(
    'Administration',
    'Facilities',
);


$gridColumns = array(
    array('name'=>'id', 'header'=>'ID', 'htmlOptions'=>array('style'=>'width: 60px')),
    array('name'=>'facility_name', 'header'=>'Facility Name'),
    /*array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'deleteButtonUrl'=>$this->createUrl('admin/deletefacility',array('id'=>$data->id)),
    )*/
    array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',
            'buttons'=>array( 
                    'delete' => array(
                      'url'=>'Yii::app()->controller->createUrl("admin/deletefacility", array("id"=>$data["id"]))',
                    ),
            ),
    ),     
);
?>
<style type="text/css">
    table.table{width:400px;}
</style>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Create New Role</h3></div>
    <div id="roles-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'facility-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'info' => array('block' => true, 'fade' => false,), // success, info, warning, error or danger
            ),
        ));
        ?>
        <?php //echo $form->errorSummary($model); ?>

        <div class = "row-fluid">
            <div class = "span12">
                <?php

                    $this->widget('bootstrap.widgets.TbGridView', array(
                    'dataProvider'=>$gridDataProvider,
                    'template'=>"{items}",
                    'columns'=>$gridColumns,
                    ));

                ?> 
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Create Facility', 'url' => array('/admin/createfacility'), 'htmlOptions' => array('id' => 'facility-create', 'name' => 'save', 'style' => 'margin-right:10px;'))); ?>
                    <?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/admin/facility'), 'htmlOptions' => array('name' => 'back'))); ?>
                </div>
            </div>    
        </div>

        <?php $this->endWidget(); ?>

    </div>

</div>