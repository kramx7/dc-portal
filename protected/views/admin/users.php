<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system'), 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                    array('label'=>'Users','url'=>array('/admin/user'), 'active'=>true, 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                    array('label'=>'Roles','url'=>array('/admin/role'), 'visible'=>User::model()->isAuthorized('ManageRole') ),
                    array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                    array('label'=>'Facilities','url'=>array('/admin/facility'), 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
               );     


$this->breadcrumbs = array(
    'Administration',
    'Users',
);
?>

<?php
Yii::app()->clientScript->registerScript('users', "

$('.user-edit').dblclick(function(){
    user_id = $(this).attr('userid');
    window.location = '".$this->createAbsoluteUrl('/admin/updateuser')."/'+user_id;
});

");
?>

<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Manage Users</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <div class="row-fluid">
            <div class="span12" style="margin-bottom:5px;">
                <?php
                $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id' => 'user-filter-form',
                    'type' => 'inline',
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        //'validateOnSubmit'=>true,
                        'validateOnChange' => true,
                    //'validateOnType'=>true,
                    ),
                ));
                ?>
                <div class="pull-left"><?php echo $form->dropDownListRow($model, 'facility', $model->facilities, array('id' => 'facility', 'style' => 'width: 260px;')); ?></div>
                <div class="pull-right">Search User 
                    <?php echo $form->textField($model, 'search_user', array('id' => 'search-user', 'class' => 'search-query', 'placeholder' => 'Search')); ?>
                    <button class="btn btn-primary" id="search-submit" type="submit">Submit</button><br>
                    <?php //echo $form->checkBoxRow($model, 'deleted_users',array('class'=>'pull-right')); ?>
                </div>
                
                <?php $this->endWidget(); ?>

            </div>
        </div><!-- end row-fluid --> 

        <div class="row-fluid">
            <div class="span12"  style="margin-top:5px;">
                <?php
                $this->widget('bootstrap.widgets.TbAlert', array(
                    'block' => true, // display a larger alert block?
                    'fade' => false, // use transitions?
                    'closeText' => false, // close link text - if set to false, no close link is displayed
                    'alerts' => array(// configurations per alert type
                        'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
                    ),
                ));
                ?>
                <div class="grid-view" style="height:400px; overflow: scroll;">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <th>Email Address</th>
                            <th>Role</th>
                            <th>Status</th>
                            <?php if (array_key_exists('ManagePortalLogin', $model->permissions)): ?>
                                <th>Portal Login</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageCustomer', $model->permissions)): ?>
                                <th>Customers</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageFacility', $model->permissions)): ?>
                                <th>Facilities</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRole', $model->permissions)): ?>
                                <th>Roles</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageUser', $model->permissions)): ?>
                                <th>Users</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageProximityCard', $model->permissions)): ?>
                                <th>Proximity Card</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageEMF', $model->permissions)): ?>
                                <th>EMF</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageDCAccess', $model->permissions)): ?>
                                <th>DC Access</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageTelcoAccess', $model->permissions)): ?>
                                <th>Telco/Carrier Access</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageRemoteHands', $model->permissions)): ?>
                                <th>Remote Hands</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManagePost', $model->permissions)): ?>
                                <th>Bulletin Board</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageLibraryContent', $model->permissions)): ?>
                                <th>Library</th>
                            <?php endif; ?>
                            <?php if (array_key_exists('ManageReport', $model->permissions)): ?>
                                <th>Report</th>
                            <?php endif; ?>
                        </tr> 
                        <?php
                        foreach ($model->users as $user) {
                        
                            $params = array('ajax' => 'editable-form', 'id' => $user->id, 'facility' => $model->facility);
                            ?>
                            <tr class="user-edit" userid="<?php echo $user->id;?>">
                                <td><?php echo $user->fullname; ?></td>
                                <td><?php echo $user->emailaddress; ?></td>
                                <td>
                                    <?php
                                    if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->role, $model->roles);
                                    }else{
                                    
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $user,
                                        'attribute' => 'role',
                                        'url' => $this->createUrl('admin/user'), //url for submit data
                                        'source' => $model->roles,
                                        'mode' => 'inline',
                                        'emptytext' => $user->role,
                                        'params' => $params,
                                    ));
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->user_status, $model->getStatusList());
                                    }else{
                                    $this->widget('bootstrap.widgets.TbEditableField', array(
                                        'type' => 'select',
                                        'model' => $user,
                                        'attribute' => 'user_status',
                                        'url' => $this->createUrl('admin/user'), //url for submit data
                                        'source' => $model->getStatusList(),
                                        'mode' => 'inline',
                                        'params' => $params,
                                    ));
                                    }
                                    ?>
                                </td>
                                <?php if (array_key_exists('ManagePortalLogin', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
		                                    echo $model->printStringFromArrayValues($user->portallogin_permission,$model->portallogin_permissions);
	                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'portallogin_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => array('PortalLogin' => 'Yes', '' => 'No'),
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageCustomer', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->customers_permission, $model->customers_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'customers_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->customers_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageFacility', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
                                        echo $model->printStringFromArrayValues($user->facility_permission, $model->facility_permissions);
                                    }else{
                                        ;
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'facility_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->facility_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                 <?php endif; ?>
                                 <?php if (array_key_exists('ManageRole', $model->permissions)): ?>
                                <td>
                                    <?php
                                    
                                    if($model->creator_id == $user->id){
                                        echo $model->printStringFromArrayValues($user->role_permission, $model->role_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'role_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->role_permissions,
                                            'mode' => 'inline',
                                            'emptytext' =>'Not Set',//'emptytext' => $model->roles_permission,
                                            'params'=>$params,
                                        ));
                                    }

                                    ?>
                                </td>
                            <?php endif; ?>   
                                <?php if (array_key_exists('ManageUser', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->users_permission, $model->users_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'users_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->users_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageProximityCard', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->proximitycard_permission, $model->proximitycard_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'proximitycard_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->proximitycard_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageEMF', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->emf_permission, $model->emf_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'emf_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->emf_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageDCAccess', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->dcaccess_permission, $model->dcaccess_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'dcaccess_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->dcaccess_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageTelcoAccess', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->telcoaccess_permission, $model->telcoaccess_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'telcoaccess_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->telcoaccess_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageRemoteHands', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->remotehands_permission, $model->remotehands_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'remotehands_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->remotehands_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManagePost', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->bulletinboard_permission, $model->bulletinboard_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'bulletinboard_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->bulletinboard_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageLibraryContent', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->library_permission, $model->library_permissions);
                                    }else{
                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'library_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $model->library_permissions,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                                <?php if (array_key_exists('ManageReport', $model->permissions)): ?>
                                    <td>
                                        <?php
                                        if($model->creator_id == $user->id){
	                                    echo $model->printStringFromArrayValues($user->report_permission, $model->report_permissions);
                                    }else{
                                         $source = $model->report_permissions;
                                        if(count($source) == 1){
                                            $source[""] = "No Permission";        
                                        }

                                        $this->widget('bootstrap.widgets.TbEditableField', array(
                                            'type' => 'select',
                                            'model' => $user,
                                            'attribute' => 'report_permission',
                                            'url' => $this->createUrl('admin/user'), //url for submit data
                                            'source' => $source,
                                            'mode' => 'inline',
                                            'emptytext' => 'Not Set',
                                            'params' => $params,
                                        ));
                                        }
                                        ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php }; ?>
                    </table>

                </div>     
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="form-actions"><?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Create User', 'url' => array('/admin/createuser'), 'htmlOptions' => array('name' => 'back'))); ?>
                    </div>
                </div>    
            </div>
        </div> 
    </div><!-- end users-box -->

</div>
