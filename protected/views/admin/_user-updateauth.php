<style>

.cust-label{
    float:left;
    width:100px;
}
</style>

<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
'id'=>'user-auth-form',
'type'=>'horizontal',
'enableAjaxValidation'=>true,    
));
        
?>

<?php
$this->widget('bootstrap.widgets.TbLabel', array(
'type'=>'info', // 'success', 'warning', 'important', 'info' or 'inverse'
'label'=>'Fields with * are required.',
));

?>

<div class="control-group"></div>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
'block'=>true, // display a larger alert block?
'fade'=>true, // use transitions?
'closeText'=>'×', // close link text - if set to false, no close link is displayed
'alerts'=>array( // configurations per alert type
'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'), // success, info, warning, error or danger
),
));

?>

<?php echo $form->errorSummary($model); ?>

<div class="control-group"></div>

<div class="control-group"></div>

<div class="row-fluid">
    <div class="span3">
        <label class="cust-label">Email Address</label>
    </div>    
    <div class="span3">
        <?php echo CHtml::encode($model->emailaddress); ?>
    </div>
    <div class="span3">
        <label class="cust-label">Full Name</label>
    </div>
    <div class="span3">
        <?php echo CHtml::encode($model->firstname.' '.$model->lastname); ?>
    </div>
</div> 

<div class="control-group"></div>

<div class="row-fluid">
    <div class="span3">
        <label class="cust-label">Company</label>
    </div>    
    <div class="span3">
        <?php echo CHtml::encode($model->client); ?>
    </div>
    <div class="span3">
        <label class="cust-label">Mobile Number</label>
    </div>
    <div class="span3">
        <?php echo CHtml::encode($model->mobilephone); ?>
    </div>
</div>

<div class="control-group"></div>

<div class="row-fluid">
    <div class="span2">
        <label class="cust-label">Select Facility</label>
    </div>
    <div class="span3">
        <?php
        $assign_options = array('Roles'=>'Roles','Operations'=>'Operations','Customers'=>'Customers','Facilities'=>'Facilities');
        echo CHtml::dropDownList('facility','',$assign_options, array('id'=>'assign'));
        ?>
    </div>
</div>   

<div class="control-group"></div>

<div class="bootstrap-widget-header"><h3>Data Centre Access</h3></div>

<div class="control-group"></div>

<?php

echo $form->checkBoxListRow($model, 'notification', array(
'Allow to access the Data Centre 24/7',
'Proximity Card Assigned'    
)); 
 
?>

<div class="control-group"></div>

<div class="bootstrap-widget-header"><h3>Notification and Reports</h3></div>

<div class="control-group"></div>

<?php

echo $form->checkBoxListRow($model, 'notification', array(
'Receive Severity 1 incident notices via SMS 24/7',
'Receive Data Notices via Email',
'Montly Report Required'    
)); 
 
?>

<div class="control-group"></div>

<div class="bootstrap-widget-header"><h3>Authorization</h3></div>

<div class="control-group"></div>

<?php

echo $form->checkBoxListRow($model, 'notification', array(
'Authority Matrix Updates',
'Proximity Card Request Forms',   
'Authorize consumable Expenditure',
'EMF Requests',
'Authorize Telco/Carrier Work',
'Data Centre Access Forms'   
)); 
 
?>

<div class="control-group"></div>

<div class="bootstrap-widget-header"><h3>Remote Hands</h3></div>

<div class="control-group"></div>

<?php

echo $form->checkBoxListRow($model, 'notification', array(
'Allowed to contact DC Support to lodge remote hands requests',
)); 
 
?>

<div class="control-group"></div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Update Authority Matrix','htmlOptions'=>array('name'=>'update'))); ?>
    
</div>

<?php $this->endWidget(); ?>
