<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Profile','url'=>array('/admin/profile')),
                    array('label'=>'System','url'=>array('/admin/system'), 'visible'=>User::model()->isAuthorized('ManageConfiguration', array('user'=>new User)) ),
                    array('label'=>'Users','url'=>array('/admin/user'), 'active'=>true, 'visible'=>User::model()->isAuthorized('ManageUser', array('user'=>new User)) ),
                    array('label'=>'Roles','url'=>array('/admin/role'), 'visible'=>User::model()->isAuthorized('ManageRole') ),
                    array('label'=>'Customers','url'=>array('/admin/customer'), 'visible'=>User::model()->isAuthorized('ManageCustomer', array('client'=>new Client)) ),
                    array('label'=>'Facilities','url'=>array('/admin/facility'), 'visible'=>User::model()->isAuthorized('ManageFacility', array('facility'=>new Facility)) ),
               );     



$this->breadcrumbs = array(
    'Administration',
    'Users' => array('/admin/user'),
    'Create User'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-user', "

$('select#User_facility, select#User_client').change(function(){
 
    var data = $('#user-form').serialize();
    data += '&ajax_t=select-update';
    
    facility = $('select#User_facility').val(); 
    client = $('select#User_client').val();
    
    $.ajax({
     type: 'POST',
     url: '" . Yii::app()->createAbsoluteUrl("admin/createuser") . "',
     data: data,
     success:function(items){
        
        $('select#User_facility').html('');
        var option1 = new Option('Select', '');   
        $(option1).html('Select');
        $('select#User_facility').append(option1);
        
        $.each(items.facilities, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('select#User_facility');
        });
        
        $('select#User_facility [value='+facility+']').attr('selected','selected');

        $('select#User_client').html('');
        var option2 = new Option('Select', '');   
        $(option2).html('Select');
        $('select#User_client').append(option2);
        
        $.each(items.clients, function(key, value){
            $('<option />', {
                value: key,
                text: value
            }).appendTo('select#User_client');
        });
        
        $('select#User_client [value='+client+']').attr('selected','selected');
        
        $('.form-actions').append('<span id=\"done-loading\"></span>');
     },
     error:function(data){
        $('.form-actions').append('<span id=\"done-loading\"></span>');
    },
     dataType:'json'
    });

});

$('select#User_role').change(function(){

    role = $(this).val();
    client_not_required = [".Yii::app()->params['ClientNotRequired']."];

    if(jQuery.inArray(role, client_not_required) == -1 ){
        $('#client-option').show();
    }else{
        $('#client-option').hide();
    }

});

// set default user role option display property
role = $('select#User_role').val();
client_not_required = [".Yii::app()->params['ClientNotRequired']."];

if(jQuery.inArray(role, client_not_required) == -1 && role != ''){
    $('#client-option').show();
}else{
    $('#client-option').hide();
}    


$('#user-form select, #user-form input').change(function(){
	
	field = $(this).attr('id');
	
	if($('#'+field+'_em_').text() == ''){
		$(this).removeClass('error');
		//$(this).css('border-color','#ccc');
		//$(this).css('color','#000');
	}

});

var today = new Date();
var yesterday = new Date();
var lastmonth = new Date();
lastmonth.setMonth(today.getMonth() - 1);
yesterday.setDate(today.getDate()-1);

jQuery('.sdate').datepicker({
        autoSize: true,
        beforeShow: customRange,
        onClose: customRange,
        constrainInput: true,
        dateFormat: 'dd/mm/yy'
});

jQuery('.edate').datepicker({
        autoSize: true,
        beforeShow: customRange,
        onClose: customRange,
        constrainInput: true,
        dateFormat: 'dd/mm/yy'
});

function customRange(input) {

    var min = new Date(2007, 11 - 1, 1);
    var dateMin = new Date();
    var dateMax = new Date(2035, 12, 31);   

    from_date = $('#from_date').val();
    to_date = $('#to_date').val();

    if (input.id === 'from_date') {
        if($.trim(to_date) != 'never' && $.trim(to_date) != ''){
            dateMax = $('#to_date').datepicker('getDate');
        }

        dateMax = new Date(dateMax.getFullYear(), dateMax.getMonth(), dateMax.getDate()-1);
    }

    if (input.id === 'to_date') {
        if($.trim(from_date) != 'now' && $.trim(from_date) != ''){
            dateMin = $('#from_date').datepicker('getDate');
        }

        dateMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate()+1);   
    }

    return {
            minDate: dateMin,
            maxDate: dateMax
    };
}



");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Create User</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'user-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php //echo $form->errorSummary($model); ?>

        <div class="row-fluid">
            <div class="span12">
                <label class="cust-label">Facility</label> 
                <?php
                echo $form->dropDownList($model, 'facility', $model->facilities, array('style' => 'width: 260px;', 'empty' => 'Select',''));
                ?>
                <?php echo $form->error($model, 'facility'); ?>
            </div>    
        </div>
        
        <div class="row-fluid">
            <div class="span12">
                <label class="cust-label">Role</label> 
                <?php
                echo $form->dropDownList($model, 'role', $model->roles, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'role'); ?>
            </div>    
        </div> 

        <div class="row-fluid" id="client-option">
            <div class="span12">
                <label class="cust-label">Client</label> 
                <?php
                echo $form->dropDownList($model, 'client', $model->clients, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'client'); ?>
            </div>    
        </div>

        <div class="row-fluid">
            <div class="span5">
                <label class="cust-label">Email Address</label> <?php echo $form->textField($model, 'emailaddress'); ?>
                <?php echo $form->error($model, 'emailaddress'); ?>
            </div>  
            <div class="span3">
                <label class="cust-label">Valid From</label>
                <?php echo $form->textField($model, 'valid_from',array('id'=>'from_date','class'=>'sdate','style'=>'width:90px;','readonly'=>'readonly')); ?>
                
                <i class="icon-calendar"></i>
                <?php echo $form->error($model, 'valid_from'); ?>
            </div>
            <div class="span4">
                <label class="cust-label" style="width: 50px;">Expires</label>
                <?php echo $form->textField($model, 'valid_to',array('id'=>'to_date','class'=>'edate','style'=>'width:90px;','readonly'=>'readonly')); ?>
                
                <i class="icon-calendar"></i>
                <?php echo $form->error($model, 'valid_to'); ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">First Name</label> <?php echo $form->textField($model, 'first_name'); ?>
                <?php echo $form->error($model, 'first_name'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Last Name</label> <?php echo $form->textField($model, 'last_name'); ?>
                <?php echo $form->error($model, 'last_name'); ?>
            </div>
        </div> 

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Mobile Phone</label> <?php echo $form->textField($model, 'mobilephone'); ?>
                <?php echo $form->error($model, 'mobilephone'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Office Phone</label> <?php echo $form->textField($model, 'officephone'); ?>
                <?php echo $form->error($model, 'officephone'); ?>
            </div>
        </div> 

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Fax</label> <?php echo $form->textField($model, 'officefax'); ?>
                <?php echo $form->error($model, 'officefax'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Remote Hands</label> <?php echo $form->textField($model, 'remote_hands'); ?>
                <?php echo $form->error($model, 'remote_hands'); ?>
            </div>
        </div> 

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save', 'htmlOptions'=>array('name'=>'save'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/admin/user'))); ?>
        </div>
        <?php $this->endWidget(); ?>        
        <input type="hidden" id="post-status" value="<?php echo (isset($_POST['save']))?"true":"false";?>">
    </div><!-- end users-box -->

</div>