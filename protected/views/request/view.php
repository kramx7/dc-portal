<?php
/* @var $this RequestController */
/* @var $model AccessRequest */

$this->breadcrumbs=array(
	'Access Requests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AccessRequest', 'url'=>array('index')),
	array('label'=>'Create AccessRequest', 'url'=>array('create')),
	array('label'=>'Update AccessRequest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AccessRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AccessRequest', 'url'=>array('admin')),
);
?>

<h1>View AccessRequest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'facility_id',
		'client_id',
		'status',
		'submitted',
		'submitted_by',
		'approved',
		'approved_by',
		'workarea',
		'clientspecificarea',
		'supervisionrequired',
		'supervised_by',
		'reasonforaccess',
		'ordernumber',
		'serviceid',
		'carrier',
		'cclocation',
		'servicetype',
		'imps_approved',
		'imps_approved_by',
		'notes',
	),
)); ?>
