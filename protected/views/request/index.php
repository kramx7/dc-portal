<?php
/* @var $this RequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Access Requests',
);

$this->menu=array(
	array('label'=>'Create AccessRequest', 'url'=>array('create')),
	array('label'=>'Manage AccessRequest', 'url'=>array('admin')),
);
?>

<h1>Access Requests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
