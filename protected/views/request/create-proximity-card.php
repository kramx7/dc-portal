<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Facility Access','url'=>array('/request/facilityaccess'), 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                    array('label'=>'Proximity Card','url'=>array('/request/proximity'), 'active'=>true, 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard') ),
                    array('label'=>'Equipment Movement','url'=>array('/request/equipment'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
                    array('label'=>'Remote Hands','url'=>array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
               );   



$this->breadcrumbs = array(
    'Administration',
    'Request',
    'Proximity Card'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('proximity-card', "

");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Proximity Card Request</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'proximity-card-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Facility</label> 
                <?php
                echo $form->dropDownList($model, 'facility_id', $model->facilities, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'facility_id'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Validity</label> 
                <?php
                echo $form->dropDownList($model, 'validity', $model->validity_months, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'validity'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Type</label> 
                <?php
                echo $form->dropDownList($model, 'type', $model->types, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'type'); ?>
            </div>     
        </div>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Company</label> 
                <?php echo $form->textField($model, 'company'); ?>
                <?php echo $form->error($model, 'company'); ?>
            </div>   
        </div>
        <div class="row-fluid">
            <div class="span7">
                <label class="cust-label" style="width:180px;">Data Hall/Cabinet/Cage #</label> 
                <?php echo $form->textField($model, 'area'); ?>
                <?php echo $form->error($model, 'area'); ?>
            </div>  
        </div>
        
        <div class="row-fluid">
            <div class="span10">
                Comments <br>
                <?php echo $form->textArea($model, 'comments',array('style'=>'width: 800px;')); ?> 
                <?php echo $form->error($model, 'comments'); ?>
            </div>     
        </div>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit', 'htmlOptions'=>array('name'=>'submit'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save Draft', 'htmlOptions'=>array('name'=>'draft'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
        </div>

        <?php $this->endWidget(); ?>        
        
    </div><!-- end users-box -->

</div>