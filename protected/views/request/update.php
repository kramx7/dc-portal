<?php
/* @var $this RequestController */
/* @var $model AccessRequest */

$this->breadcrumbs=array(
	'Access Requests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AccessRequest', 'url'=>array('index')),
	array('label'=>'Create AccessRequest', 'url'=>array('create')),
	array('label'=>'View AccessRequest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AccessRequest', 'url'=>array('admin')),
);
?>

<h1>Update AccessRequest <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>