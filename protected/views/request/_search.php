<?php
/* @var $this RequestController */
/* @var $model AccessRequest */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'facility_id'); ?>
		<?php echo $form->textField($model,'facility_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submitted'); ?>
		<?php echo $form->textField($model,'submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'submitted_by'); ?>
		<?php echo $form->textField($model,'submitted_by',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved'); ?>
		<?php echo $form->textField($model,'approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved_by'); ?>
		<?php echo $form->textField($model,'approved_by',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'workarea'); ?>
		<?php echo $form->textField($model,'workarea'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'clientspecificarea'); ?>
		<?php echo $form->textField($model,'clientspecificarea',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supervisionrequired'); ?>
		<?php echo $form->textField($model,'supervisionrequired',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supervised_by'); ?>
		<?php echo $form->textField($model,'supervised_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reasonforaccess'); ?>
		<?php echo $form->textField($model,'reasonforaccess',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ordernumber'); ?>
		<?php echo $form->textField($model,'ordernumber',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serviceid'); ?>
		<?php echo $form->textField($model,'serviceid',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'carrier'); ?>
		<?php echo $form->textField($model,'carrier',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cclocation'); ?>
		<?php echo $form->textField($model,'cclocation',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'servicetype'); ?>
		<?php echo $form->textField($model,'servicetype',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imps_approved'); ?>
		<?php echo $form->textField($model,'imps_approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imps_approved_by'); ?>
		<?php echo $form->textField($model,'imps_approved_by',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>4096)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->