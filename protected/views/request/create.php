<?php
/* @var $this RequestController */
/* @var $model AccessRequest */

$this->breadcrumbs=array(
	'Access Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AccessRequest', 'url'=>array('index')),
	array('label'=>'Manage AccessRequest', 'url'=>array('admin')),
);
?>

<h1>Create AccessRequest</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>