<?php
/* @var $this RequestController */
/* @var $model AccessRequest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'access-request-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'facility_id'); ?>
		<?php echo $form->textField($model,'facility_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'facility_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'submitted'); ?>
		<?php echo $form->textField($model,'submitted'); ?>
		<?php echo $form->error($model,'submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'submitted_by'); ?>
		<?php echo $form->textField($model,'submitted_by',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'submitted_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved'); ?>
		<?php echo $form->textField($model,'approved'); ?>
		<?php echo $form->error($model,'approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved_by'); ?>
		<?php echo $form->textField($model,'approved_by',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'approved_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workarea'); ?>
		<?php echo $form->textField($model,'workarea'); ?>
		<?php echo $form->error($model,'workarea'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clientspecificarea'); ?>
		<?php echo $form->textField($model,'clientspecificarea',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'clientspecificarea'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'supervisionrequired'); ?>
		<?php echo $form->textField($model,'supervisionrequired',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'supervisionrequired'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'supervised_by'); ?>
		<?php echo $form->textField($model,'supervised_by',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'supervised_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reasonforaccess'); ?>
		<?php echo $form->textField($model,'reasonforaccess',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'reasonforaccess'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ordernumber'); ?>
		<?php echo $form->textField($model,'ordernumber',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ordernumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serviceid'); ?>
		<?php echo $form->textField($model,'serviceid',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'serviceid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'carrier'); ?>
		<?php echo $form->textField($model,'carrier',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'carrier'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cclocation'); ?>
		<?php echo $form->textField($model,'cclocation',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cclocation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'servicetype'); ?>
		<?php echo $form->textField($model,'servicetype',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'servicetype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imps_approved'); ?>
		<?php echo $form->textField($model,'imps_approved'); ?>
		<?php echo $form->error($model,'imps_approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imps_approved_by'); ?>
		<?php echo $form->textField($model,'imps_approved_by',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'imps_approved_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>4096)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->