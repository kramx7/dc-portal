<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Facility Access','url'=>array('/request/facilityaccess'),'active'=>true, 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                    array('label'=>'Proximity Card','url'=>array('/request/proximity'), 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard') ),
                    array('label'=>'Equipment Movement','url'=>array('/request/equipment'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
                    array('label'=>'Remote Hands','url'=>array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
               );      



$this->breadcrumbs = array(
    'Administration',
    'Request',
    'Facility Access'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-request', "

");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Facility Access Request</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'request-access-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->hiddenField($model, 'facility_id');?>
        <?php echo $form->hiddenField($model, 'company');?>
        <?php echo $form->hiddenField($model, 'workarea');?>
        <?php echo $form->hiddenField($model, 'supervised_by');?>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Facility:</label> 
                <?php echo $model->facilities[$model->facility_id]; ?>
            </div>
            <div class="span3">
                <label style="float: right;">Expected Work Date & Time</label>
            </div>
            <div class="span5">
                <span>Start:</span>     
                <?php echo $model->expected_start_date; ?>
                <?php echo $model->expected_start_time; ?>    

            </div>     
        </div>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Company:</label> 
                <?php echo $model->company; ?>
            </div>
            <div class="span3">
                
            </div>
            <div class="span5">
                <span>End:</span>     
                <?php echo $model->expected_end_date; ?>
                
                <?php echo $model->expected_end_time; ?>
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Area of Work:</label> 
                <?php echo $model->workareas[$model->workarea]; ?>
            </div>
            <div class="span6">
                <span>Client Speficic Area</span> 
                <?php //echo $model->workareas[$model->clientspecificarea]; ?>
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span4">
                <?php ?>
                <span>Require Supervision:</span> 
                <?php echo ($model->supervisionrequired==1)?'Yes':'No'; ?>
            </div>
            <div class="span6">
                <span>Supervised By (Name):</span>     
                <?php echo $model->supervised_by; ?>
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span10">
                Description of Work, Requirements, Comments: <br>
                <?php echo $model->reasonforaccess; ?>
            </div>     
        </div>
        <div class="row-fluid">
            <br>
            <div class="span6">
                <h5 style="margin:0px">Carrier/Telco Information and Cross Connect</h5>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <span>Carrier Telco:</span> 
                <?php echo $model->carrier;?>
            </div>
            <div class="span6">
                <span>Service Type:</span>     
                <?php echo $model->service_types[$model->servicetype]; ?>
            </div>     
        </div>

        <div class="row-fluid">
            <div class="span4">
                <span>Order Number:</span> 
                <?php echo $model->ordernumber; ?>
            </div>
            <div class="span4">
                <span>Cross Connect:</span> 
                <?php echo $model->cclocation; ?>
            </div>    
        </div>

        <div class="row-fluid">
            <div class="span4">
                <span>Service ID/FNN:</span> 
                <?php echo $model->serviceid; ?>
            </div>   
        </div>

        <div class="form-actions">
             <?php if($model->status == AccessRequestStatus::STATUS_SUBMITTED && $model->approve_permission && $model->client_user){ ?>    
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Approve', 'htmlOptions'=>array('name'=>'customer_approve'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reject', 'htmlOptions'=>array('name'=>'customer_reject'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
            <?php }else if($model->status == AccessRequestStatus::STATUS_APPROVAL && $model->approve_permission){ ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Approve', 'htmlOptions'=>array('name'=>'fujitsu_approve'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reject', 'htmlOptions'=>array('name'=>'fujitsu_reject'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
            <?php } ?>
        </div>

        <?php $this->endWidget(); ?>        
        
    </div><!-- end users-box -->

</div>