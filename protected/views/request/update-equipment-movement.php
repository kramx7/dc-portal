<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Facility Access','url'=>array('/request/facilityaccess'), 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                    array('label'=>'Proximity Card','url'=>array('/request/proximity'), 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard') ),
                    array('label'=>'Equipment Movement','url'=>array('/request/equipment'), 'active'=>true, 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
                    array('label'=>'Remote Hands','url'=>array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
               );      



$this->breadcrumbs = array(
    'Administration',
    'Request',
    'Facility Access'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-request', "

var itemnumber = 1;

$('#item-add').click(function(){

    error_count = 0;

    type = $('select#item-type').val();
    description = $('input#item-desc').val();
    makemodel = $('input#item-model').val();
    area = $('select#item-area').val();
    area_desc = $('select#item-area option[value=\"'+area+'\"]').text();
    serialnumber = $('input#item-serial').val();
    hostname = $('input#item-hostname').val();

    $('span#item-type').html('');

    if(type == ''){
        error_count++;
        $('span#item-type').append('<br>Please specify in or out item.');
    }

    $('span#item-serial').html('');

    if(serialnumber == ''){
        error_count++;
        $('span#item-serial').append('<br>Serial Number is required.');
    }


    if(error_count == 0){

        data = 'ajax=item-add&item_number='+itemnumber+'&description='+description
        +'&makemodel='+makemodel+'&location='+area+'&hostname='+hostname+'&serialnumber='+serialnumber+'&type='+type;


        $.ajax({
         type: 'POST',
         url: '" . Yii::app()->createAbsoluteUrl("request/updateequipment/".$model->id) . "',
         data: data,
         success:function(items){
            
         },
         error:function(data){
            
        },
         dataType:'html'
        });

        var new_item = '<tr id=\"items-'+itemnumber+'\">'
        + '<td> <input type=\"checkbox\" name=\"items[]\" value=\"'+itemnumber+'\" id=\"item-check\"> </td>'
        + '<td> '+ type +' </td>'
        + '<td> '+ description +' </td>'
        + '<td> '+ makemodel +' </td>'
        + '<td> '+ area_desc +' </td>'
        + '<td> '+ serialnumber +' </td>'
        + '<td> '+ hostname +' </td>'
        + '</tr>';

        $('#items-table').append(new_item);

        itemnumber++;

        $('select#item-type').val('');
        $('input#item-desc').val('');
        $('input#item-model').val('');
        $('select#item-area').val('');
        $('input#item-serial').val('');
        $('input#item-hostname').val('');

    }

});

$('#item-remove').click(function(){

    $('input#item-check:checked').each(function() {
       id = $(this).val();
       $('#items-'+id).remove();
    });

});

");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }

    #items-table tr td span{color:red;}
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Equipment Movement Form</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'equipment-movement-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>
        <?php echo $form->hiddenField($model,'client_id');?>
        <?php echo $form->hiddenField($model,'facility_id');?>
        <?php echo $form->hiddenField($model,'receivedfrom');?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Client:</label> 
                <?php echo $model->client_name;?>
            </div>
            <div class="span4">
                <label class="cust-label">Facility:</label> 
                <?php echo $model->facilities[$model->facility_id]; ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
            <label class="cust-label">From:</label> 
            <?php echo $model->locations[$model->receivedfrom]; ?>
            </div>
         </div>  

         <div class="row-fluid">   
            <div class="span4">
                <label class="cust-label">Delivery Date:</label>
                <?php echo $model->delivery_date;?>
            </div>
            <div class="span4">
                <label class="cust-label">Time:</label>
                <?php echo $model->delivery_time; ?>

            </div>     
        </div>

        <div class="row-fluid">
            <div class="span12">
                <label class="cust-label" style="width:150px">Number of Boxes:</label> 
                <?php echo $model->numberofboxes; ?>
            </div>
        </div>

        <div class="row-fluid">   
            <div class="span3">
                <label class="cust-label" >Staging Date:</label>
                <?php echo $model->requestedstagingtime; ?>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span10">Comments <br>
                <?php echo $model->comments; ?> 
            </div>     
        </div>

        <div class="row-fluid">
            <div class="span12">

                <table width="100%" id="items-table">
                    <tr>
                        <td></td>
                        <td>In or Out</td>
                        <td>Description</td>
                        <td>Make/Model</td>
                        <td>Area</td>
                        <td>Serial Number</td>
                        <td>Hostname</td>
                    </tr> 
                    <?php foreach($model->entries as $i => $row){ ?>
                       <tr>
                            <td> </td>
                            <td> <?php echo $row['type']?> </td>
                            <td> <?php echo $row['description']?> </td>
                            <td> <?php echo $row['makemodel']?> </td>
                            <td> <?php echo $row['location']?> </td>
                            <td> <?php echo $row['serialnumber']?> </td>
                            <td> <?php echo $row['hostname']?> </td>
                        </tr>
                    <?php } ?> 
                </table>
                
            </div>   
        </div>

        <div class="form-actions">
             <?php if($model->status == EquipmentMovementStatus::STATUS_SUBMITTED && $model->approve_permission == true && $model->client_user){ ?>    
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Approve', 'htmlOptions'=>array('name'=>'approve'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Reject', 'htmlOptions'=>array('name'=>'reject'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
            <?php } ?>
        </div>

        <?php $this->endWidget(); ?>        
        
    </div><!-- end users-box -->

</div>