<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Facility Access','url'=>array('/request/facilityaccess'),'active'=>true, 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                    array('label'=>'Proximity Card','url'=>array('/request/proximity'), 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard') ),
                    array('label'=>'Equipment Movement','url'=>array('/request/equipment'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
                    array('label'=>'Remote Hands','url'=>array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
               );     



$this->breadcrumbs = array(
    'Administration',
    'Request',
    'Facility Access'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-request', "

");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Facility Access Request</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'request-access-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php //echo $form->errorSummary($model); ?>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Facility</label> 
                <?php
                echo $form->dropDownList($model, 'facility_id', $model->facilities, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'facility_id'); ?>
            </div>
            <div class="span3">
                <label style="float: right;">Expected Work Date & Time</label>
            </div>
            <div class="span5">
                <span>Start</span>     
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'expected_start_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'minDate'=>'new Date()'
                        ),
                    'htmlOptions' => array('readonly' => 'readonly','style'=>'width:100px;')
                ));
                ?>
                <?php

                $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
                    'model'=>$model,
                     'attribute'=>'expected_start_time',
                     // additional javascript options for the date picker plugin
                     'options'=>array(
                         'showPeriod'=>true,
                         ),
                     'htmlOptions'=>array('size'=>8,'maxlength'=>8, 'readonly' => 'readonly','style'=>'width:80px;'),
                 ));

                ?>    
                <?php echo $form->error($model, 'expected_start_date'); ?>

            </div>     
        </div>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Company</label> 
                <?php echo $form->textField($model, 'company'); ?>
                <?php echo $form->error($model, 'company'); ?>
            </div>
            <div class="span3">
                
            </div>
            <div class="span5">
                <span>End</span>     
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'expected_end_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        'minDate'=>'new Date()'
                        ),
                    'htmlOptions' => array('readonly' => 'readonly','style'=>'width:100px;')
                ));
                ?>
                <?php

                $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
                    'model'=>$model,
                     'attribute'=>'expected_end_time',
                     // additional javascript options for the date picker plugin
                     'options'=>array(
                         'showPeriod'=>true,
                         ),
                     'htmlOptions'=>array('size'=>8,'maxlength'=>8, 'readonly' => 'readonly','style'=>'width:80px;'),
                 ));

                    ?>    
                <?php echo $form->error($model, 'expected_end_date'); ?>
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Area of Work</label> 
                <?php
                echo $form->dropDownList($model, 'workarea', $model->workareas, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'workarea'); ?>
            </div>
            <div class="span6">
                <span>Client Speficic Area</span>     
                <?php
                echo $form->dropDownList($model, 'clientspecificarea', $model->workareas, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>    
               <?php echo $form->error($model, 'clientspecificarea'); ?>     
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span4">
                <?php echo $form->checkBox($model, 'supervisionrequired'); ?>
                <span>Require Supervision</span> 
                <?php echo $form->error($model, 'supervisionrequired'); ?>
            </div>
            <div class="span6">
                <span>Supervised By (Name)</span>     
                <?php echo $form->textField($model, 'supervised_by'); ?>    
                <?php echo $form->error($model, 'supervised_by'); ?>
            </div>     
        </div>
        <div class="row-fluid">
            <div class="span10">
                Description of Work, Requirements, Comments <br>
                <?php echo $form->textArea($model, 'reasonforaccess',array('style'=>'width: 800px;')); ?> 
                <?php echo $form->error($model, 'reasonforaccess'); ?>
            </div>     
        </div>
        <div class="row-fluid">
            <br>
            <div class="span6">
                <h5 style="margin:0px">Carrier/Telco Information and Cross Connect</h5>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <span>Carrier Telco</span> 
                <?php echo $form->textField($model, 'carrier'); ?>
                <?php echo $form->error($model, 'carrier'); ?>
            </div>
            <div class="span6">
                <span>Service Type</span>     
                <?php echo $form->dropDownList($model, 'servicetype', $model->service_types, array('empty'=>'Select') ); ?>    
                <?php echo $form->error($model, 'servicetype'); ?>
            </div>     
        </div>

        <div class="row-fluid">
            <div class="span4">
                <span>Order Number</span> 
                <?php echo $form->textField($model, 'ordernumber'); ?>
                <?php echo $form->error($model, 'ordernumber'); ?>
            </div>
            <div class="span4">
                <span>Cross Connect</span> 
                <?php echo $form->textField($model, 'cclocation'); ?>
                <?php echo $form->error($model, 'cclocation'); ?>
            </div>    
        </div>

        <div class="row-fluid">
            <div class="span4">
                <span>Service ID/FNN</span> 
                <?php echo $form->textField($model, 'serviceid'); ?>
                <?php echo $form->error($model, 'serviceid'); ?>
            </div>   
        </div>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit', 'htmlOptions'=>array('name'=>'submit'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save Draft', 'htmlOptions'=>array('name'=>'draft'))); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
        </div>

        <?php $this->endWidget(); ?>        
        
    </div><!-- end users-box -->

</div>