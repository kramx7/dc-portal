<?php
/* @var $this RequestController */
/* @var $model AccessRequest */

$this->breadcrumbs=array(
	'Access Requests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AccessRequest', 'url'=>array('index')),
	array('label'=>'Create AccessRequest', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#access-request-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Access Requests</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'access-request-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'facility_id',
		'client_id',
		'status',
		'submitted',
		'submitted_by',
		/*
		'approved',
		'approved_by',
		'workarea',
		'clientspecificarea',
		'supervisionrequired',
		'supervised_by',
		'reasonforaccess',
		'ordernumber',
		'serviceid',
		'carrier',
		'cclocation',
		'servicetype',
		'imps_approved',
		'imps_approved_by',
		'notes',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
