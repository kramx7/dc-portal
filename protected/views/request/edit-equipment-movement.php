<?php
/* @var $this UserController */
/* @var $model User */

$this->layout = '//layouts/column1';

//set submenu
$this->submenu = array(
                    array('label'=>'Facility Access','url'=>array('/request/facilityaccess'), 'visible'=>User::model()->isAuthorized('SubmitDCAccess', array('request'=>new AccessRequest), 'ApproveDCAccess')),
                    array('label'=>'Proximity Card','url'=>array('/request/proximity'), 'visible'=>User::model()->isAuthorized('SubmitProximityCard', array('request'=>new ProximityCardRequest), 'ApproveProximityCard') ),
                    array('label'=>'Equipment Movement','url'=>array('/request/equipment'), 'active'=>true, 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
                    array('label'=>'Remote Hands','url'=>array('/request/remotehands'), 'visible'=>User::model()->isAuthorized('SubmitEMF', array('request'=>new EquipmentMovement), 'ApproveEMF') ),
               );     



$this->breadcrumbs = array(
    'Administration',
    'Request',
    'Facility Access'
);

?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php
Yii::app()->clientScript->registerScript('create-request', "

var itemnumber = ".$model->last_item_number."+1;

$('#item-add').click(function(){

    error_count = 0;

    in_or_out = $('select#item-in_or_out').val();
    in_or_out_desc = $('select#item-in_or_out option[value=\"'+in_or_out+'\"]').text();
    description = $('input#item-desc').val();
    makemodel = $('input#item-model').val();
    area = $('select#item-area').val();
    area_desc = $('select#item-area option[value=\"'+area+'\"]').text();
    serialnumber = $('input#item-serial').val();
    hostname = $('input#item-hostname').val();

    $('span#item-in_or_out').html('');

    if(in_or_out == ''){
        error_count++;
        $('span#item-in_or_out').append('<br>Please specify in or out item.');
    }

    $('span#item-serial').html('');

    if(serialnumber == ''){
        error_count++;
        $('span#item-serial').append('<br>Serial Number is required.');
    }


    if(error_count == 0){

        data = 'ajax=item-add&item_number='+itemnumber+'&description='+description
        +'&makemodel='+makemodel+'&location='+area+'&hostname='+hostname+'&serialnumber='+serialnumber+'&in_or_out='+in_or_out;


        $.ajax({
         type: 'POST',
         url: '" . Yii::app()->createAbsoluteUrl("request/updateequipment/".$model->id) . "',
         data: data,
         success:function(items){
            
         },
         error:function(data){
            
        },
         dataType:'html'
        });

        var new_item = '<tr id=\"items-'+itemnumber+'\">'
        + '<td> <input type=\"checkbox\" name=\"items[]\" value=\"'+itemnumber+'\" id=\"item-check\" item_number=\"'+itemnumber+'\"> </td>'
        + '<td> '+ in_or_out_desc +' </td>'
        + '<td> '+ description +' </td>'
        + '<td> '+ makemodel +' </td>'
        + '<td> '+ area_desc +' </td>'
        + '<td> '+ serialnumber +' </td>'
        + '<td> '+ hostname +' </td>'
        + '</tr>';

        $('#items-table').append(new_item);

        itemnumber++;

        $('select#item-in_or_out').val('');
        $('input#item-desc').val('');
        $('input#item-model').val('');
        $('select#item-area').val('');
        $('input#item-serial').val('');
        $('input#item-hostname').val('');

    }

});

$('#item-remove').click(function(){

    $('input#item-check:checked').each(function() {
        
       id = $(this).val();
       item_number = $(this).attr('item_number');
       $('#items-'+id).remove();

       data = 'ajax=item-remove&item_number='+item_number;

        $.ajax({
         type: 'POST',
         url: '" . Yii::app()->createAbsoluteUrl("request/updateequipment/".$model->id) . "',
         data: data,
         success:function(items){
            
         },
         error:function(data){
            
        },
         dataType:'html'
        });

    });

});

");
?>

<style>
    .row-fluid{
        margin-bottom: 5px;  
    }
    .cust-label{
        float:left;
        width: 100px;
    }

    #items-table tr td span{color:red;}
</style>


<div class="bootstrap-widget table">
    <div class="bootstrap-widget-header"><i class="icon-user"></i><h3>Equipment Movement Form</h3></div>
    <div id="users-box" class="bootstrap-widget-content">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'equipment-movement-form',
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            //'enableClientValidation'=>true
        ));
        ?>

        <?php
        $this->widget('bootstrap.widgets.TbLabel', array(
            'type' => 'info', // 'success', 'warning', 'important', 'info' or 'inverse'
            'label' => 'Fields with * are required.',
        ));
        ?>

        <div class="control-group"></div>

        <?php
        $this->widget('bootstrap.widgets.TbAlert', array(
            'block' => true, // display a larger alert block?
            'fade' => false, // use transitions?
            'closeText' => false, // close link text - if set to false, no close link is displayed
            'alerts' => array(// configurations per alert type
                'success' => array('block' => true, 'fade' => false,), // 'closeText' => '×'), // success, info, warning, error or danger
            ),
        ));
        ?>

        <?php //echo $form->errorSummary($model); ?>

        <div class="row-fluid">
            <div class="span4">
                <label class="cust-label">Client</label> 
                <?php echo $model->client_name;?>
            </div>
            <div class="span4">
                <label class="cust-label">Facility</label> 
                <?php
                echo $form->dropDownList($model, 'facility_id', $model->facilities, array('style' => 'width: 260px;', 'empty' => 'Select'));
                ?>
                <?php echo $form->error($model, 'facility_id'); ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
            <label class="cust-label">From</label> 
            <?php
            echo $form->dropDownList($model, 'receivedfrom', $model->locations, array('style' => 'width: 260px;', 'empty' => 'Select'));
            ?>
            <?php echo $form->error($model, 'receivedfrom'); ?>
            </div>
         </div>  

         <div class="row-fluid">   
            <div class="span4">
                <label class="cust-label">Delivery Date</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'delivery_date',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        ),
                    'htmlOptions' => array('readonly' => 'readonly','style'=>'width:100px;')
                ));
                ?>
                <?php echo $form->error($model, 'delivery_date'); ?>
            </div>
            <div class="span4">
                <label class="cust-label">Time</label>
                <?php

                $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
                    'model'=>$model,
                     'attribute'=>'delivery_time',
                     // additional javascript options for the date picker plugin
                     'options'=>array(
                         'showPeriod'=>true,
                         ),
                     'htmlOptions'=>array('size'=>8,'maxlength'=>8, 'readonly' => 'readonly','style'=>'width:80px;'),
                 ));

                ?>    
                <?php echo $form->error($model, 'delivery_time'); ?>

            </div>     
        </div>

        <div class="row-fluid">
            <div class="span12">
                <label class="cust-label" style="width:150px">Number of Boxes</label> 
                <?php echo $form->textField($model, 'numberofboxes'); ?>
                <?php echo $form->error($model, 'numberofboxes'); ?>
            </div>
        </div>

        <div class="row-fluid">   
            <div class="span3">
                <label class="cust-label" >Staging Date</label>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'attribute' => 'requestedstagingtime',
                    'model' => $model,
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                        ),
                    'htmlOptions' => array('readonly' => 'readonly','style'=>'width:100px;')
                ));
                ?>
                <?php echo $form->error($model, 'requestedstagingtime'); ?>
            </div>
        </div>
        
        <div class="row-fluid">
            <div class="span10">Comments <br>
                <?php echo $form->textArea($model, 'comments',array('style'=>'width: 800px;')); ?> 
                <?php echo $form->error($model, 'comments'); ?>
            </div>     
        </div>

        <div class="row-fluid">
            <div class="span12">

                <table width="100%" id="items-table">
                    <tr>
                        <td><?php echo CHtml::checkBox('name', false, array('name'=>'checkall')); ?></td>
                        <td>In or Out</td>
                        <td>Description</td>
                        <td>Make/Model</td>
                        <td>Area</td>
                        <td>Serial Number</td>
                        <td>Hostname</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <?php echo CHtml::dropDownList('type', 'select', $model->in_or_out_list, array('id'=>'item-in_or_out','style'=>'width:80px;','empty'=>'Select')); ?>
                            <span id="item-in_or_out"></span>
                        </td>
                        <td>
                            <?php echo CHtml::textField('desc', '', array('id'=>'item-desc','style'=>'width:200px;')); ?>
                            <span id="item-desc"></span>
                        </td>
                        <td>
                            <?php echo CHtml::textField('model', '', array('id'=>'item-model','style'=>'width:150px;')); ?>
                            <span id="item-model"></span>
                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('area', 'select', $model->areas, array('id'=>'item-area','style'=>'width:100px;','empty'=>'Select')); ?>
                            <span id="item-area"></span>
                        </td>
                        <td>
                            <?php echo CHtml::textField('serial', '', array('id'=>'item-serial','style'=>'width:100px;')); ?>
                            <span id="item-serial"></span>
                        </td>
                        <td>
                            <?php echo CHtml::textField('hostname', '', array('id'=>'item-hostname','style'=>'width:150px;')); ?>
                            <span id="item-hostname"></span>
                            <br>
                            <br>
                            <div class="pull-right">
                                <?php echo CHtml::button('Add', array('id'=>'item-add')); ?>
                                <?php echo CHtml::button('Remove', array('id'=>'item-remove')); ?>
                            </div>
                        </td>
                    </tr>   
                    <?php foreach($model->entries as $i => $row){ ?>
                       <tr id="items-<?php echo $row['item_number'];?>">
                            <td> <input type="checkbox" name="items[]" value="<?php echo $row['item_number'];?>" id="item-check" item_number="<?php echo $row['item_number'];?>"> </td>
                            <td> <?php echo $model->in_or_out_list[$row['in_or_out']]?> </td>
                            <td> <?php echo $row['description']?> </td>
                            <td> <?php echo $row['makemodel']?> </td>
                            <td> <?php echo $model->areas[$row['location']];?> </td>
                            <td> <?php echo $row['serialnumber']?> </td>
                            <td> <?php echo $row['hostname']?> </td>
                        </tr>
                    <?php } ?> 
                </table>
                
            </div>   
        </div>

        <div class="form-actions">
            <?php if($model->status == EquipmentMovementStatus::STATUS_DRAFT){ ?> 
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Submit', 'htmlOptions'=>array('name'=>'submit'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save Draft', 'htmlOptions'=>array('name'=>'draft'))); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => 'primary', 'label' => 'Cancel', 'url' => array('/site/index'))); ?>
            <?php } ?>
        </div>

        <?php $this->endWidget(); ?>        
        
    </div><!-- end users-box -->

</div>