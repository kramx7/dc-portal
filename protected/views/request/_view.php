<?php
/* @var $this RequestController */
/* @var $data AccessRequest */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('facility_id')); ?>:</b>
	<?php echo CHtml::encode($data->facility_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_id')); ?>:</b>
	<?php echo CHtml::encode($data->client_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submitted')); ?>:</b>
	<?php echo CHtml::encode($data->submitted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('submitted_by')); ?>:</b>
	<?php echo CHtml::encode($data->submitted_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved')); ?>:</b>
	<?php echo CHtml::encode($data->approved); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workarea')); ?>:</b>
	<?php echo CHtml::encode($data->workarea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clientspecificarea')); ?>:</b>
	<?php echo CHtml::encode($data->clientspecificarea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supervisionrequired')); ?>:</b>
	<?php echo CHtml::encode($data->supervisionrequired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supervised_by')); ?>:</b>
	<?php echo CHtml::encode($data->supervised_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reasonforaccess')); ?>:</b>
	<?php echo CHtml::encode($data->reasonforaccess); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordernumber')); ?>:</b>
	<?php echo CHtml::encode($data->ordernumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serviceid')); ?>:</b>
	<?php echo CHtml::encode($data->serviceid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carrier')); ?>:</b>
	<?php echo CHtml::encode($data->carrier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cclocation')); ?>:</b>
	<?php echo CHtml::encode($data->cclocation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('servicetype')); ?>:</b>
	<?php echo CHtml::encode($data->servicetype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imps_approved')); ?>:</b>
	<?php echo CHtml::encode($data->imps_approved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imps_approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->imps_approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	*/ ?>

</div>