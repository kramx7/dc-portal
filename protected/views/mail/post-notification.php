<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['completeUrl']; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<title><?php echo Yii::app()->name?> - Verification Email</title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo Yii::app()->name?></div>
	</div><!-- header -->

	<div id="mainmenu">	</div><!-- mainmenu -->
			<div class="breadcrumbs"></div><!-- breadcrumbs -->
	
	<div class="span-19">
	<div id="content">
<?php //Yii::app()->params['completeUrl']?>		
<h1>Post Notification</h1>
You have a post that needs <?php echo $model->swGetStatus->getLabel()?>.
Click on the link below to take an action on the post.
<br>
<a href="<?php echo $this->createAbsoluteURL('/noticeboard/update/'.$model->id);?>">
<?php echo $this->createAbsoluteURL('/noticeboard/update/'.$model->id)?>
</a>

<table class="detail-view" id="yw0"><tr class="odd"><th>ID</th><td><?php echo $model->title?></td></tr>
<tr class="even"><th>Status</th><td><?php echo $model->swGetStatus->getLabel()?></td></tr>
<tr class="even"><th>Facility</th><td><?php echo $model->getFacilityName?></td></tr>
<tr class="even"><th>Facility</th><td><?php echo ($model->sticky==1)?'Sticky':'Not Set'; ?></td></tr>
<tr class="even"><th>Publish From</th><td><?php echo date('d/m/Y',strtotime($model->publish_from));?></td></tr>
<tr class="odd"><th>Publish To</th><td><?php echo date('d/m/Y',strtotime($model->publish_to));?></td></tr>
<tr class="even"><th>Short Message</th><td><?php echo $model->short_message;?></td></tr>
<tr class="odd"><th>Full Message</th><td><?php echo $model->full_message;?></td></tr>
<tr class="even"><th> </th><td></td></tr>
</table>	</div><!-- content -->
</div>
<div class="span-5 last">
</div>

	<div class="clear"></div>

	

</body>
</html>
