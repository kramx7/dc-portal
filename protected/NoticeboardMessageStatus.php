<?php

/**
 * This is the model class for table "noticeboard_message_status".
 *
 * The followings are the available columns in table 'noticeboard_message_status':
 * @property integer $message_status
 * @property string $message_status_description
 *
 * The followings are the available model relations:
 * @property Noticeboard[] $noticeboards
 */
class NoticeboardMessageStatus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NoticeboardMessageStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'noticeboard_message_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message_status, message_status_description', 'required'),
			array('message_status', 'numerical', 'integerOnly'=>true),
			array('message_status_description', 'length', 'max'=>48),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('message_status, message_status_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noticeboards' => array(self::HAS_MANY, 'Noticeboard', 'message_status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message_status' => 'Message Status',
			'message_status_description' => 'Message Status Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message_status',$this->message_status);
		$criteria->compare('message_status_description',$this->message_status_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function scopes() {
		return array(
				'messageStatusForList' => array(
					'select' => 'message_status, message_status_description',
					'order' => 'message_status ASC'
				)
		);
	}
}