<?php

/**
 * This is the model class for table "noticeboard".
 *
 * The followings are the available columns in table 'noticeboard':
 * @property string $id
 * @property string $facility
 * @property integer $message_status
 * @property string $thread_id
 * @property string $priority
 * @property string $publish_from
 * @property string $publish_to
 * @property integer $sticky
 * @property string $title
 * @property string $short_message
 * @property string $full_message
 * @property string $submitted_by
 * @property string $submitted_time
 * @property string $approver
 * @property string $approved_time
 *
 * The followings are the available model relations:
 * @property User $approver0
 * @property NoticeboardMessageStatus $messageStatus
 * @property User $submittedBy
 */
class Noticeboard extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Noticeboard the static model class
     */
    private $_publish_from = ''; // temporary holder of the string word immediately, used on beforeValidate and afterValidate
    private $_publish_to = ''; // temporary holder of the string word never, used on beforeValidate and afterValidate
    public $_draft_status = 'swNoticeboard/Draft';
    public $_approved_status = 'swNoticeboard/Approve';
    public $_published_status = 'swNoticeboard/Published'; // status that will be used to filter the notice on bulletin, used on Noticeboard/actionAdmin
    public $_search_user_role = ''; // used to check user role to limit search results, used on Noticeboard/actionAdmin
    public $_search_user_facility = 0; // used to limit search result to assigned facility of the user, used on Noticeboard/actionAdmin
    public $_submitted_by = 0;

    const STATUS_DRAFT = 'swNoticeboard/Draft';
    const STATUS_APPROVED = 'swNoticeboard/Approve';
    const STATUS_PUBLISHED = 'swNoticeboard/Published';

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'noticeboard';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, facility_id, priority, publish_from, publish_to, title, short_message, full_message', 'required'),
            array('facility_id, priority, submitted_by, approved_by', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 80),
            array('title', 'filter', 'filter' => 'strip_tags'),
            array('short_message', 'length', 'max' => 256),
            array('short_message', 'filter', 'filter' => 'strip_tags'),
            array('full_message', 'length', 'max' => 10240),
            //array('full_message','filter', 'filter' => 'strip_tags'),
            array('publish_from, publish_to', 'date', 'format' => 'yyyy-MM-dd'),
            array('submitted, approved', 'date', 'format' => 'yyyy-MM-dd H:m:s'),
            array('publish_from,  publish_to', 'checkPublishDateRange'),
            array('submitted', 'default', 'value' => new CDbExpression('NOW()'), 'on' => 'insert'),
            array('sticky', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            /* array('id, facility, message_status, thread_id, priority, publish_from, publish_to, sticky, title, short_message, full_message, submitted_by, submitted_time, approver, approved_time', 'safe', 'on'=>'search'), */
            array('id, status, priority, publish_from, publish_to, title, short_message, full_message, submitted_by, submitted, approved_by, approved', 'safe', 'on' => 'search')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'approver1' => array(self::BELONGS_TO, 'User', 'approved_by'), // it has the number 1 append because there is already a field approver on the table noticeboard
            'submittedBy' => array(self::BELONGS_TO, 'User', 'submitted_by'),
            'facility1' => array(self::BELONGS_TO, 'Facility', 'facility_id'), // it has the number 1 append because there is already a field facility on table noticeboard
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'facility_id' => 'Facility',
            'status' => 'Message Status',
            'priority' => 'Priority',
            'publish_from' => 'Publish From',
            'publish_to' => 'Publish To',
            'sticky' => 'Sticky',
            'title' => 'Title',
            'short_message' => 'Short Message',
            'full_message' => 'Full Message',
            'submitted_by' => 'Submitted By',
            'submitted' => 'Submitted Time',
            'approved_by' => 'Approver',
            'approved' => 'Approved Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('facility_id', $this->facility_id, true);
        $criteria->compare('status', $this->status);
        //$criteria->compare('thread_id',$this->thread_id,true);
        $criteria->compare('priority', $this->priority, true);
        $criteria->compare('publish_from', $this->publish_from, true);
        $criteria->compare('publish_to', $this->publish_to, true);
        $criteria->compare('sticky', $this->sticky);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('short_message', $this->short_message, true);
        $criteria->compare('full_message', $this->full_message, true);
        $criteria->compare('submitted_by', $this->submitted_by, true);
        $criteria->compare('submitted', $this->submitted, true);
        $criteria->compare('approved_by', $this->approved_by, true);
        $criteria->compare('approved', $this->approved, true);


        // limit result to assigned facility of the login user, this is set on 
        // Controller:Noticebard,Action:Search
        // if the user is not assigned to a super admin
        if (!User::model()->isUserHasRole(Yii::app()->params['SuperAdminCode'])) {
            //filter resutls to assign facility
            foreach ($this->_search_user_facility as $id => $name) {
                $criteria->compare('facility', $id, false, 'OR');
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function behaviors() {
        // behavior to auto attach the simpleworkflow extension to manage the status field
        return array(
            'swBehavior' => array(
                'class' => 'application.extensions.simpleWorkflow.SWActiveRecordBehavior'
            ),
        );
    }

    function afterConstruct() {

        // set the default publish from and to dates to string immediately and never as requested
        if ($this->publish_from == null) {
            $this->publish_from = 'immediately';
        }

        if ($this->publish_to == null) {
            $this->publish_to = 'never';
        }

        return parent::afterConstruct();
    }

    function afterFind() {

        // format publish from and to dates to since data from database is formated in y-m-d m:i
        // but the views only need the y-m-d format
        if ($this->publish_from != null) {
            $this->publish_from = date('Y-m-d', strtotime($this->publish_from));
        }

        if ($this->publish_to != null) {
            $this->publish_to = date('Y-m-d', strtotime($this->publish_to));
        }

        // show the never string for to publish to date if it matches the never data on config main file
        if ($this->publish_to == Yii::app()->params['never_date']) {
            $this->publish_to = 'never';
        }

        return parent::afterFind();
    }

    function beforeValidate() {

        // need to change the value of publish dates from and to valid format to avoid 
        // validation errors, it will stored temporarily to its corresponding to temp variable
        if ($this->publish_from == 'immediately') {
            $this->_publish_from = $this->publish_from;
            $this->publish_from = date('Y-m-d');
        }

        if ($this->publish_to == 'never') {
            $this->_publish_to = $this->publish_to;
            $this->publish_to = Yii::app()->params['never_date'];
        }

        return parent::beforeValidate();
    }

    function afterValidate() {

        // set the approver to the user id of login user, not yet used
        if ($this->status == $this->_approved_status) {// if notice is approved
            $this->approved_by = $this->_search_user_role;
            $this->approved = new CDbExpression('NOW()');
        }

        if (!$this->hasErrors() && ($this->scenario == 'insert' || $this->scenario == 'update')) {
            //if there are no errors no need to present the string, immediately and never
            //since the date will be save to the database next
            return parent::afterValidate();
            ;
        }

        // if there publish dates from and to have values like dates or string immediately and never 
        // return the original value from temp var
        // this is executed when there are errors on the validation process
        if ($this->_publish_from != '') {
            $this->publish_from = $this->_publish_from;
        }

        if ($this->_publish_to != '') {
            $this->publish_to = $this->_publish_to;
        }

        return parent::afterValidate();
    }

    public function beforeSave() {

        if ($this->isNewRecord) {
            $this->submitted_time = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }

    public function checkPublishDateRange() {

        // check publish dates; check whether publish from/start date is less than the publish to/end date
        if (strtotime($this->publish_from) > strtotime($this->publish_to)) {
            $this->addError('publish_from', 'Publish From date should be earlier than the Publish To date.');
        }
    }

    public function published() {

        $this->updateByPk($this->id, array('status' => self::STATUS_PUBLISHED));
        $this->status = self::STATUS_PUBLISHED;
    }

    public function getFormatedSubmittedTime($format = 'd/m/Y') {


        return date($format, strtotime($this->submitted_time));
    }

    public function getFacilityName() {

        $facility = Facility::model()->findByAttributes(array('facility_id' => $this->facility));

        if ($facility != null) {
            return $facility->facility_name;
        } else {
            return '';
        }
    }

    public function isUserInFacility($task) {

        if (isset(Yii::app()->user->assigned_roles)) {
            if (array_key_exists(Yii::app()->params['SuperAdminCode'], Yii::app()->user->assigned_roles)) {
                return true;
            }
        }

        $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
        $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
        $command->bindValue(":task", $task, PDO::PARAM_STR);
        return $command->execute() == 1 ? true : false;
    }

}