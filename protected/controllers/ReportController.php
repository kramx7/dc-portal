<?php

class ReportController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow athenticated users to perform 'index' and 'view' actions
                'actions' => array('power', 'environment', 'nger'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionPower($code) {

        // set graph form 
        $model = new GraphForm;

        //check user authorization
        $params["report"] = $model;
        User::model()->checkUserAccess('ReadReport', $params);
        //User::model()->isAuthorized('ReadReport', $params);
        // set type
        $model->type = $code;

        // get the id of current login user
        $id = Yii::app()->user->id;
        
        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReport', 'ReadAllReports');

        if (isset($_POST['GraphForm'])) {
            // if form is submmited
            $model->attributes = $_POST['GraphForm'];
            $model->default_meter = $model->meter;
            $model->target_id = $model->meter;
        } else {
            reset($model->facilities);
            $model->facility_id = key($model->facilities);
        }

        //$model->facility_id = 5;
        // set default facility
        $model->default_facility = $model->facility_id;

        $meters = array();
        // get power meters
        if ($model->facility_id == 0) {
            // get all meters to all the facilities
            //$meters = PowerStatistics::model()->getMetersByClientId($client_id); 
            $meters = PowerStatistics::model()->getMetersByAllFacilitiesAndClients($model->facilities, $model->clients);
        } else {
            // get all meters to for specific facility
            $meters = PowerStatistics::model()->getMetersByFacilityIdAndClients($model->facility_id, $model->clients);
            $params["report"] = $model;
            User::model()->checkUserAccess('ReadReport', $params);
        }

        // set total option for meter
        if (count($meters) > 0) {
            $meters[0] = 'Total';
        } else {
            $meters = array(0 => 'Total');
        }


        // arrange meter by key, starting with total
        ksort($meters);

        // set meters to model
        $model->meters = $meters;

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
        if (Yii::app()->request->isAjaxRequest) {
            $data = array('meters' => $model->meters);
            echo CJSON::encode($data);
            Yii::app()->end();
        }

        // set dates to timestamp for db comparison
        $model->setDatesToTimestamp();

        // get power statistics
        $power_statistics = array();



        if ($model->target_id != 0) {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            }
        } else {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            }
        }

        // set xdata and ydata
        $graph = new GraphComponent;

        // set graph data if there statistics found
        if (count($power_statistics) > 0) {

            $graph->setData($power_statistics, $model->from_date, $model->to_date, $model->type, $model->target_id);

            // set title
            $meter_name = PowerStatistics::model()->getMeterName($model->target_id);
            $graph->setTitle($meter_name);

            // get client id and name
            $client_id = ClientFacility::model()->getClientIdByFacilityId($model->facility_id);
            $client = Client::model()->getClientById($client_id);

            // get facility name
            $facility_name = $model->facilities[$model->facility_id];

            // set subtitle
            $graph->setSubTitle($client->client_name, $facility_name);

            // plot/draw graph
            $graph->plotGraph();
        } else {
            $model->status = 'There are no results for the specified input.';
        }

        // format dates for date picker
        $model->setDatesToFormat('d/m/Y');

        $this->render('power', array('model' => $model, 'graph' => $graph));
    }

    public function actionEnvironment($code) {

        // set graph form 
        $model = new GraphForm;

        //check user authorization
        $params["report"] = $model;
        User::model()->checkUserAccess('ReadReport', $params);

        // set type
        $model->type = $code;

        // get the id of current login user
        $id = Yii::app()->user->id;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReport', 'ReadAllReports');

        if (isset($_POST['GraphForm'])) {
            // if form is submmited
            $model->attributes = $_POST['GraphForm'];
        } else {
            // set first facility as default facility
            reset($model->facilities);
            $model->facility_id = key($model->facilities);
        }

        // set default facility
        $model->default_facility = $model->facility_id;

        // get sensors
        if ($model->facility_id == 0) {
            // get all meters to all the facilities 
            $sensors = Sensor::model()->getSensorsByAllFacilitiesAndClients($model->facilities, $model->clients);
        } else {
            // get all meters to for specific facility
            $sensors = Sensor::model()->getSensorsByFacilityIdAndClients($model->facility_id, $model->clients);
            $params["report"] = $model;
            User::model()->checkUserAccess('ReadReport', $params);
        }

        // set sensor options
        $model->sensors = $sensors;

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
        if (Yii::app()->request->isAjaxRequest) {
            $data = array('sensors' => $model->sensors);
            echo CJSON::encode($data);
            Yii::app()->end();
        }


        if ($model->sensor == 0) {
            // set choosen sensor, first sensor
            reset($model->sensors);
            $model->sensor = key($model->sensors);
        }

        // set dates to timestamp for db comparison
        $model->setDatesToTimestamp();

        // get power statistics
        $statistics = array();

        if ($model->sensor != 0) {
            if ($model->type == 'Temperature') {
                $statistics = EnvironmentStatistics::model()->getTemperatureBySensorIdAndDatesAndClients($model->sensor, $model->from_date, $model->to_date,$model->clients);
            } else {
                $statistics = EnvironmentStatistics::model()->getHumidityBySensorIdAndDatesAndClients($model->sensor, $model->from_date, $model->to_date, $model->clients);
            }
        }

        // set xdata and ydata
        $graph = new GraphComponent;

        if (count($statistics) > 0) {
            $graph->setData($statistics, $model->from_date, $model->to_date, $model->type, $model->target_id);

            // set title

            if ($graph->type != 'Temperature') {
                $graph->type = 'Relative Humidity';
            }

            $sensor_name = $model->sensors[$model->sensor];
            $graph->setTitle($sensor_name);

            // get client id and name
            $client_id = ClientFacility::model()->getClientIdByFacilityId($model->facility_id);
            $client = Client::model()->getClientById($client_id);

            // get facility name
            $facility_name = $model->facilities[$model->facility_id];

            // set subtitle
            $graph->setSubTitle($client->client_name, $facility_name);

            // plot/draw graph
            $graph->plotGraph();
        } else {
            $model->status = 'There are no results for the specified input.';
        }

        // format dates for date picker
        $model->setDatesToFormat('d/m/Y');

        $this->render('environment', array('model' => $model, 'graph' => $graph));
    }

    public function actionNGER() {

        // set graph form 
        $model = new GraphForm;

        //check user authorization
        $params["report"] = $model;
        User::model()->checkUserAccess('ReadReport', $params);

        // get the id of current login user
        $id = Yii::app()->user->id;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReport', 'ReadAllReports');

        if (isset($_POST['GraphForm'])) {
            // if form is submmited
            $model->attributes = $_POST['GraphForm'];
        } else {
            // set first facility as default facility
            reset($model->facilities);
            $model->facility_id = key($model->facilities);
            // set default year
            $model->year = date('Y');
        }

        //$model->year = 2011;
        // set default facility
        $model->default_facility = $model->facility_id;
        
        $params["report"] = $model;
        User::model()->checkUserAccess('ReadReport', $params);
        
        // get years
        $model->years = PowerStatistics::model()->getNGERYearByFacilityIdAndClients($model->facility_id, $model->clients);

        $selected_year = $model->year; //choosen year
        $prev_year = $selected_year - 1;

        // set start date as july 1 of previous year
        $str_sdate = "1 July " . $prev_year;
        $start = strtotime($str_sdate);
        //set end date
        $end = $start + 365 * 24 * 60 * 60; // add one year of timestamp
        //get kw for facility
        $powers = PowerStatisticsSummary::model()->getPowerByFacilityIdAndDatesAndClients($model->facility_id, $start, $end, $model->clients);

        // get pue per day
        $pues = FacilityPue::model()->getPueByPowerDatesAndFacilityId($powers, $model->facility_id);

        // get carbob per due
        $carbons = Facility::model()->getCarbonByPowerDatesAndFacilityId($powers, $model->facility_id);

        // compute kwh total, average pue and total carbon
        $model->computeNGER($powers, $pues, $carbons);

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
        if (Yii::app()->request->isAjaxRequest) {

            $data = array('kwh_data' => $model->sum_kwh,
                'average_pue_data' => $model->average_pue,
                'total_kwh_data' => $model->sum_total_kwh,
                'carbon_data' => $model->sum_carbon);

            echo CJSON::encode($data);
            Yii::app()->end();
        }

        $this->render('nger', array('model' => $model));
    }

}
