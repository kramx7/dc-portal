<?php

class AdminController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('profile','system', 
                    'user', 'createuser', 'updateuser', 'updateauthuser', 
                    'role', 'createrole',
                    'facility','createfacility','deletefacility',
                    'customer','createcustomer','deletecustomer'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadUserModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model, $form_id) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form_id) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionProfile() {
        // get the id of current login user
        $id = Yii::app()->user->id;

        $model = $this->loadUserModel($id);

        //check user authorization
        $params["user"] = $model;
        User::model()->checkUserAccess('UpdateProfile', $params);

        //set scenario; to set required fields
        $model->scenario = 'profile';

        // if it is ajax validation request
        $this->performAjaxValidation($model, 'profile-form');

        if (isset($_POST['User'])) {
            // if form is submmited

            $model->attributes = $_POST['User'];

            // set validation to check on password if there password field is not empty
            if ($model->new_password != '') {
                $model->scenario = 'profile-pass';
            } else {
                $model->scenario = 'profile';
            }

            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Successfully updated the profile.');
                $this->redirect(array('/admin/profile'));
            }
        }

        if ($model->valid_from != '') {
            $model->valid_from = date('d/m/Y', strtotime($model->valid_from));
        }

        if ($model->valid_to != 'never') {
            $model->valid_to = date('d/m/Y', strtotime($model->valid_to));
        }


        $this->render('profile', array(
            'model' => $model
        ));
    }

    public function actionSystem() {
        // get the id of current login user
        $id = Yii::app()->user->id;

        $model = new SystemConfigurationForm;

        //check user authorization
        $params["user"] = new User;
        $manage_permission = User::model()->isAuthorized('ManageConfiguration', $params);
        $read_permission = User::model()->isAuthorized('ReadConfiguration', $params);

        if(!$manage_permission){
            if(!$read_permission){
                User::model()->checkUserAccess('ReadConfiguration', $params);                
            }
        }

        // if it is ajax validation request
        $this->performAjaxValidation($model, 'system-form');

        if (isset($_POST['SystemConfigurationForm'])) {
            // if form is submmited

            $model->attributes = $_POST['SystemConfigurationForm'];

            if ($model->validate()) {

                $model->write();

                Yii::app()->user->setFlash('success', 'Successfully updated the configuration.');
                $this->redirect(array('/admin/system'));
            }
        }else{
            $model->read();
        }

        if($manage_permission){
            $view = 'system-edit';
        }else{
            $view = 'system-view';
        }


        $this->render($view, array(
            'model' => $model
        ));
    }

    public function actionUser() {

        // instantiate a new user model
        $model = new User();

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;
        $model->creator_id = $creator_id;
        $model->id = $creator_id;

        //check user authorization
        $params = array('user' => $model);
        User::model()->checkUserAccess('ManageUser', $params);
        //return isset($params["user"]) && $params["user"]->isUserInFacility("ManageUser") && $params["user"]->isUserInClient("ManageUser");

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'editable-form') {

            $user_id = Yii::app()->request->getParam('id');
            $model = User::model()->findByPk($user_id);

            $model->attributes = $_POST;

            $field = Yii::app()->request->getParam('name');
            $permissions = Yii::app()->request->getParam('value');

            $model->updateUserPermissions($field, $permissions);

            Yii::app()->end();
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer','AssociateAllCustomer');
        
        // set the user's assigned clients
        $model->clients = $clients;

        // get the login user's assigned facilites on user_task_facility table
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'ReadFacility', 'AssociateAllFacility');
        // check if user has the permission to associate all facility assigned
        if (User::model()->isAuthorized('AssociateAllFacility')) {
            //$model->facilities[0] = 'Associate All Facility';
        }

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];
        } else {
            if (count($model->facilities) > 0) {
                // set first facility as default facility
                reset($model->facilities);
                $model->facility = key($model->facilities);
            } else {
                $model->facility = 0;
            }
        }

        // set roles to manage
        $model->primary_role = $model->getPrimaryRole($creator_id);
        $model->setRolesToManage($model->primary_role);


        // set users for facility and search filter
        $model->searchUsers();

        // set users roles and inherited roles
        $model->setRoles($creator_id);

        // set permissions
        $model->setInheritedPermissions($creator_id);

        

        $this->render('users', array('model' => $model
        ));
    }

    public function actionCreateUser() {

        //check user authorization
        $params = array('user' => new User());
        User::model()->checkUserAccess('ManageUser', $params);

        // instantiate a new user model
        $model = new User('admin-create');
        
        // if it is ajax validation request
        $this->performAjaxValidation($model, 'user-form');

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;
        $model->creator_id = $creator_id;

        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'ReadFacility', 'AssociateAllFacility');

        // check if user has the permission to associate all facility assigned
        if (User::model()->isAuthorized('AssociateAllFacility')) {
            $facilities[0] = 'Associate All Facility';
        }
        // set the user's assigned facilities as the list of facility to assigned
        $model->facilities = $facilities;

        // set inherited roles
        //$model->setInheritedPermissions($creator_id);
        $model->setRoles($creator_id);

        // get the login user's assigned facilites on create user task
        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer','AssociateAllCustomer');
        if (User::model()->isAuthorized('AssociateAllCustomer')) {
            $clients[0] = 'Associate All Customer';
        }
        // set the user's assigned facilities as the list of facility to assigned
        $model->clients = $clients;


        // if form is submitted
        if (isset($_POST['User'])) {

            $model->scenario = 'admin-create';    
            // assign posted form data
            $model->attributes = $_POST['User'];

            if ($model->save()) {

                $model->sendVerficationEmail($model);
                // set status unconfirmed when user is created
                User::model()->updateByPk($model->id, array('user_status'=>User::STATUS_UNCONFIRMED));

                Yii::app()->user->setFlash('success', 'Successfully created a new user.');
                //$this->redirect(array('admin/createuser'));
                $this->redirect(array('admin/user'));
            }
        }

        // limit client base on selected facility
        if (!empty($model->facility)) {
            $model->clients = ClientFacility::model()->getClientDetailByFacilityId($model->facility);
        }

        // limit facility base on selected client
        if (!empty($model->client)) {
            $model->facilities = ClientFacility::model()->getFacilityDetailByClientId($model->client);
        }

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
        if (isset($_POST['ajax']) && $_POST['ajax'] == 'select-update') {

            $data = array('facilities' => $model->facilities,
                'clients' => $model->clients,);

            echo CJSON::encode($data);
            Yii::app()->end();
        }


        $this->render('create-user', array('model' => $model
        ));
    }

    public function actionUpdateUser($id) {

        //check user authorization
        $params = array('user' => new User());
        User::model()->checkUserAccess('ManageUser', $params);

        // instantiate a new user model
        //$model = new User('admin-create');
        $model = User::model()->findByPk($id);

        // if it is ajax validation request
        $this->performAjaxValidation($model, 'user-form');

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;
        $model->creator_id = $creator_id;

        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'ReadFacility', 'AssociateAllFacility');

        // check if user has the permission to associate all facility assigned
        if (User::model()->isAuthorized('AssociateAllFacility')) {
            $facilities[0] = 'Associate All Facility';
        }
        // set the user's assigned facilities as the list of facility to assigned
        $model->facilities = $facilities;

        // set inherited roles
        //$model->setInheritedPermissions($creator_id);
        $model->setRoles($creator_id);

        // get the login user's assigned facilites on create user task
        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer','AssociateAllCustomer');
        if (User::model()->isAuthorized('AssociateAllCustomer')) {
            $clients[0] = 'Associate All Customer';
        }
        // set the user's assigned facilities as the list of facility to assigned
        $model->clients = $clients;


        // if form is submitted
        if (isset($_POST['User'])) {

            $model->scenario = 'admin-update';    
            // assign posted form data
            $model->attributes = $_POST['User'];

            if(Yii::app()->request->getQuery('activate')!= false){
                $model->user_status = User::STATUS_NEW;
            }

            if ($model->save()) {

                $model->sendVerficationEmail($model);

                Yii::app()->user->setFlash('success', 'Successfully updated the user.');
                //$this->redirect(array('admin/createuser'));
                $this->redirect(array('admin/user'));
            }
        }

        // limit client base on selected facility
        if (!empty($model->facility)) {
            $model->clients = ClientFacility::model()->getClientDetailByFacilityId($model->facility);
        }

        // limit facility base on selected client
        if (!empty($model->client)) {
            $model->facilities = ClientFacility::model()->getFacilityDetailByClientId($model->client);
        }

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
        if (isset($_POST['ajax']) && $_POST['ajax'] == 'select-update') {

            $data = array('facilities' => $model->facilities,
                'clients' => $model->clients,);

            echo CJSON::encode($data);
            Yii::app()->end();
        }


        $this->render('update-user', array('model' => $model
        ));
    }

    public function actionUpdateAuthUser() {


        $model = new User;

        $this->render('user-updateauth', array('model' => $model
        ));
    }

    public function actionRole() {

        //check user authorization
        User::model()->checkUserAccess('ManageRole');

        // create authitem model, with admin-update scenario, scenario is useful to determine validation rules on the model
        $model = new AuthItem('admin-update');

        // determine the view to render, special case for ajax request that only needs json data to be encoded
        // for faster loading of ajax request
       if (isset($_POST['ajax']) && $_POST['ajax'] === 'editable-form') {

            $model->attributes = $_POST;

            $role = Yii::app()->request->getParam('pk');
            $permission_type = Yii::app()->request->getParam('permission_type');
            $new_permission = Yii::app()->request->getParam('value');   

            $model->updatePermissions($role, $permission_type, $new_permission); 

            Yii::app()->end();
        }

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // get user primary role
        $primary_role = User::model()->getPrimaryRole($creator_id);

         // set list of per permissions base on user's role
        $model->setAssignedPermissions($primary_role);

        $model->setAllRoles();

        $this->render('roles', array('model' => $model
        ));
    }

    function actionCreateRole() {

        //check user authorization
        User::model()->checkUserAccess('ManageRole');

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // get user primary role
        $primary_role = User::model()->getPrimaryRole($creator_id);

        // set temp model, fix for inline edit problem on new record
        $temp_model = AuthItem::model()->findByPk($primary_role);

        // initilize model to use for roles, set admin-create scenario for validation rules
        $model = new AuthItem('admin-create');

         // set list of per permissions base on user's role
        $model->setAssignedPermissions($primary_role);
        $temp_model->setAssignedPermissions($primary_role);

        // save inline edit data temporarily to session
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'editable-form') {

            $model->attributes = $_POST;

            $permission_type = Yii::app()->request->getParam('permission_type');
            $new_permission = Yii::app()->request->getParam('value');
            $model->saveTempPermission($permission_type, $new_permission);

            Yii::app()->end();
        }

        // perform ajax validation
        $this->performAjaxValidation($model, 'role-form');

        if (isset($_POST['AuthItem'])) {
            
            $model->attributes = $_POST['AuthItem'];

            if($model->save()){
                
                Yii::app()->session->remove('temp_permissions');

                Yii::app()->user->setFlash('info', 'Successfully created a new role.');
                $this->redirect(array('admin/createrole'));
            }

            // set temporary permissions from session
            // use the fix model for new record soluton on inline edit
            $temp_model->setTemporaryPermissions();

        }else{   

            // remove temp permissions session if there is an existing session
            $temp_permissions = Yii::app()->session->itemAt('temp_permissions'); // gets value
            if ($temp_permissions != null) {
                Yii::app()->session->remove('temp_permissions');
            }   
        }

       

        $this->render('create-role', array('model' => $model,'temp_model'=>$temp_model
        ));
    }

    function actionCreateFacility() {

        // initilize model to use for roles, set admin-create scenario for validation rules
        $model = new Facility('admin-create');

        //check user authorization
        $params["facility"] = $model;
        User::model()->checkUserAccess('ManageFacility',$params);

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // perform ajax validation
        $this->performAjaxValidation($model, 'facility-form');

        if (isset($_POST['Facility'])) {
            
            $model->attributes = $_POST['Facility'];

            if($model->save()){
                
                Yii::app()->user->setFlash('info', 'Successfully created a new facility.');
                $this->redirect(array('admin/createfacility'));
            }

        }
       

        $this->render('create-facility', array('model' => $model
        ));
    }

    function actionFacility() {

        // initilize model to use for roles, set admin-create scenario for validation rules
        $model = new Facility('admin-create');

        //check user authorization
        $params["facility"] = $model;
        User::model()->checkUserAccess('ManageFacility',$params);

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // perform ajax validation
        $this->performAjaxValidation($model, 'facility-form');

        if (isset($_POST['Facility'])) {
            
            $model->attributes = $_POST['Facility'];

            if($model->save()){
                
                Yii::app()->user->setFlash('info', 'Successfully created a remove facility.');
                $this->redirect(array('admin/facility'));
            }

        }
       
        // get all facilities
        $facilities = $model->getFacilities();
 
        $gridDataProvider = new CArrayDataProvider($facilities);

        $this->render('facilities', array('model' => $model, 'gridDataProvider'=>$gridDataProvider
        ));
    }

    public function actionDeleteFacility($id)
    {
        $model = Facility::model()->findByPk($id);
        
        // check user authorization
        $params["facility"] = $model;
        User::model()->checkUserAccess('ManageFacility', $params);
        
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin/facility'));
    }

    function actionCreateCustomer() {

        // initilize model to use for roles, set admin-create scenario for validation rules
        $model = new Client('admin-create');

        //check user authorization
        $params["client"] = $model;
        User::model()->checkUserAccess('ManageCustomer',$params);

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // perform ajax validation
        $this->performAjaxValidation($model, 'client-form');

        if (isset($_POST['Client'])) {
            
            $model->attributes = $_POST['Client'];

            if($model->save()){
                
                Yii::app()->user->setFlash('info', 'Successfully created a new customer.');
                $this->redirect(array('admin/createcustomer'));
            }

        }
       
        $model->setFacilities($creator_id);
        // set temp facilities, array structure id=> name
        $model->setTempFacilities();

        $this->render('create-customer', array('model' => $model
        ));
    }

    function actionCustomer() {

        // initilize model to use for roles, set admin-create scenario for validation rules
        $model = new Client('admin-create');

        //check user authorization
        $params["client"] = $model;
        User::model()->checkUserAccess('ManageCustomer',$params);

        // get login' user id
        $creator_id = Yii::app()->user->id;

        // perform ajax validation
        $this->performAjaxValidation($model, 'client-form');

        if (isset($_POST['Client'])) {
            
            $model->attributes = $_POST['Client'];
        }
       
        // get all facilities
        $model->setFacilities($creator_id);

        // set temp facilities, array structure id=> name
        $model->setTempFacilities();

        // set default facility
        if(empty($model->facility_id)){
            reset($model->temp_facilities);
            $model->facility_id = key($model->temp_facilities);
        }

        $model->clients = $model->getClients();
 
        $gridDataProvider = new CArrayDataProvider($model->clients);

        $this->render('customers', array('model' => $model, 'gridDataProvider'=>$gridDataProvider
        ));
    }

    public function actionDeleteCustomer($id)
    {
        $client = Client::model()->findByPk($id);

        $client_facility = ClientFacility::model()->findAllByAttributes(array('client_id'=>$id));

        //check user authorization
        $params["client"] = $client;
        User::model()->checkUserAccess('ManageCustomer',$params);
        
        $client->delete();
        $client_facility->deleteAll();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin/customer'));
    }

}

