<?php

class SiteController extends Controller {

    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', //allow anonymous user to perform login
                'actions' => array('login'),
                'users' => array('?')
            ),
            array('allow', // allow all users to perform error action
                'actions' => array('error', 'phpinfo'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'page', 'contact', 'setup', 'auth', 'logout', 'samplegraph', 'simpleline1graph', 'simpleline2graph', 'dateline1graph'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

        // set creator id
        $creator_id = Yii::app()->user->id;
        
        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility','ReadFacility', 'AssociateAllFacility');

        $params["post"] = new Noticeboard;
        $submit_permission = User::model()->isAuthorized('SubmitPost',$params);
        $approve_permission = User::model()->isAuthorized('ApprovePost',$params);

        $workitems = new WorkItem;
        $workitems->user_id = $creator_id;
        $workitems->facilities = $facilities;
        $workitems->setWorkItems();

        // check user authorization
        if(!$submit_permission){
            if(!$approve_permission){
                User::model()->checkUserAccess('ReadPost',$params);
            }
        }


        $notices = new Noticeboard();
        $notices->unsetAttributes();  // clear any default values

        $notices->creator_id = $creator_id;
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $notices->facilities = $facilities;


        $this->render('index', array(
            'workitems' => $workitems,
            'notices' => $notices,
        ));

    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            }
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout = '//layouts/login';

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }

        //$model->username = '';
        $model->password = '';
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    //set up the role base authorization, only once run 
    public function actionSetup() {

        $auth = Yii::app()->authManager;

        $auth->clearAll();

        $task = $auth->createTask('PortalLoginGroup', 'Portal Login', '', 'permission_group,facility');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("PortalLogin");';
            $auth->createOperation('PortalLogin', 'Yes', $bizrule, 'facility');
            
            $task->addChild('PortalLogin');

            $op = $auth->createOperation('ManagePortalLogin', 'Portal Login', '', 'permission');
            $op->addChild('PortalLogin');

            // set portal configuration's child operation and task
            //$auth->createOperation('PortalConfigurationPermission', 'Portal Configuration', '', 'permission');
            
            $task = $auth->createTask('PortalConfigurationGroup', 'Portal Configuration', '', 'permission_group,facility');
            
            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ReadConfiguration") && $params["user"]->isUserInClient("ReadConfiguration");';
            $auth->createOperation('ReadConfiguration', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("UpdateConfiguration") && $params["user"]->isUserInClient("UpdateConfiguration");';
            $auth->createOperation('UpdateConfiguration', 'Update', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ManageConfiguration") && $params["user"]->isUserInClient("ManageConfiguration");';
            $op = $auth->createOperation('ManageConfiguration', 'Manage', $bizrule, 'facility,client');
            $op->addChild('ReadConfiguration');
            $op->addChild('UpdateConfiguration');

            $task->addChild('ManageConfiguration');
            $task->addChild('ReadConfiguration');


            // set customers child operation and task
            //$auth->createOperation('CustomerPermission', 'Customer', '', 'permission');

            $task = $auth->createTask('CustomerGroup', 'Customer', '', 'permission_group,facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("CreateCustomer");';
            $auth->createOperation('CreateCustomer', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("ReadCustomer");';
            $auth->createOperation('ReadCustomer', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("UpdateCustomer");';
            $auth->createOperation('UpdateCustomer', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("DeleteCustomer");';
            $auth->createOperation('DeleteCustomer', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["client"]) && $params["client"]->isUserInFacility("ManageCustomer");';
            $op = $auth->createOperation('ManageCustomer', 'Manage', $bizrule, 'facility');
            $op->addChild('CreateCustomer');
            $op->addChild('ReadCustomer');
            $op->addChild('UpdateCustomer');
            $op->addChild('DeleteCustomer');

            $task->addChild('ManageCustomer');
            $task->addChild('ReadCustomer');

            // set facility child operation and task
            //$auth->createOperation('FacilityPermission', 'Facility', '', 'permission');

            $task = $auth->createTask('FacilityGroup', 'Facility', '', 'permission_group,facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("facility");';
            $auth->createOperation('CreateFacility', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("ReadFacility");';
            $auth->createOperation('ReadFacility', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("UpdateFacility");';
            $auth->createOperation('UpdateFacility', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("DeleteFacility");';
            $auth->createOperation('DeleteFacility', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("ManageFacility");';
            $op = $auth->createOperation('ManageFacility', 'Manage', $bizrule, 'facility,client');
            $op->addChild('CreateFacility');
            $op->addChild('ReadFacility');
            $op->addChild('UpdateFacility');
            $op->addChild('DeleteFacility');
            

            $task->addChild('ManageFacility');
            $task->addChild('ReadFacility');
        
            // set role child operation and task
            //$auth->createOperation('RolePermission', 'Role', '', 'permission');

            $task = $auth->createTask('RoleGroup', 'Role', '', 'permission_group');

            $auth->createOperation('CreateRole', 'Create');
            $auth->createOperation('ReadRole', 'Read');
            $auth->createOperation('UpdateRole', 'Update');
            $auth->createOperation('DeleteRole', 'Delete');

            $op = $auth->createOperation('ManageRole', 'Manage');
            $op->addChild('CreateRole');
            $op->addChild('ReadRole');
            $op->addChild('UpdateRole');
            $op->addChild('DeleteRole');
            
            $task->addChild('ManageRole');
            $task->addChild('ReadRole');


            // set user child operations and task
            //$auth->createOperation('UserPermission', 'User', '', 'permission');

            $task = $auth->createTask('UserGroup', 'User', '', 'permission_group,facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("CreateUser") && $params["user"]->isUserInClient("CreateUser");';
            $auth->createOperation('CreateUser', 'Create', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ReadUser") && $params["user"]->isUserInClient("UpdateUser");';
            $auth->createOperation('ReadUser', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("UpdateUser") && $params["user"]->isUserInClient("UpdateUser");';
            $auth->createOperation('UpdateUser', 'Update', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("DeleteUser") && $params["user"]->isUserInClient("DeleteUser");';
            $auth->createOperation('DeleteUser', 'Delete', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["user"]) && $params["user"]->isProfileOwner();';
            $auth->createOperation('UpdateProfile', 'Update Profile', $bizrule);


            $bizrule = 'return isset($params["user"]) && $params["user"]->isUserInFacility("ManageUser") && $params["user"]->isUserInClient("ManageUser");';
            $op = $auth->createOperation('ManageUser', 'Manage', $bizrule, 'facility,client');
            $op->addChild('CreateUser');
            $op->addChild('ReadUser');
            $op->addChild('UpdateUser');
            $op->addChild('DeleteUser');

            $task->addChild('ManageUser');
            $task->addChild('ReadUser');

            // set proximity car permission's child operation and task
            //$auth->createOperation('ProximityCardPermission', 'Proximity Card', '', 'permission');

            $task = $auth->createTask('ProximityCardGroup', 'Proximity Card', '', 'permission_group,facility');

            $auth->createOperation('ManageProximityCard', 'ManageProximityCard');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitProximityCard");';
            $auth->createOperation('SubmitProximityCard', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveProximityCard");';
            $op = $auth->createOperation('ApproveProximityCard', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitProximityCard');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateProximityCard");';
            $auth->createOperation('DelegateProximityCard', 'Delegate', $bizrule, 'facility');
            
            $task->addChild('ApproveProximityCard');
            $task->addChild('SubmitProximityCard');
            //$task->addChild('DelegateProximityCard');


            // set emf permission's child operation and task
            //$auth->createOperation('EMFPermission', 'EMF', '', 'permission');

            $task = $auth->createTask('EMFGroup', 'EMF', '', 'permission_group,facility');

            $auth->createOperation('ManageEMF', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitEMF");';
            $auth->createOperation('SubmitEMF', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveEMF");';
            $op = $auth->createOperation('ApproveEMF', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitEMF');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateEMF");';
            $auth->createOperation('DelegateEMF', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveEMF');
            $task->addChild('SubmitEMF');
            //$task->addChild('DelegateEMF');

            // set dc access permission's child operation and task
            //$auth->createOperation('DCAccessPermission', 'DC Access', '', 'permission');

            $task = $auth->createTask('DCAccessGroup', 'DC Access', '', 'permission_group,facility');

            $auth->createOperation('ManageDCAccess', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitDCAccess");';
            $auth->createOperation('SubmitDCAccess', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveDCAccess");';
            $op = $auth->createOperation('ApproveDCAccess', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitDCAccess');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateDCAccess");';
            $auth->createOperation('DelegateDCAccess', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveDCAccess');
            $task->addChild('SubmitDCAccess');
            //$task->addChild('DelegateDCAccess');

            // set telco access permission's child operation and task
            //$auth->createOperation('TelcoAccessPermission', 'Telco/Carrier Access', '', 'permission');

            $task = $auth->createTask('TelcoAccessGroup', 'TelcoAccess Access', '', 'permission_group,facility');

            $auth->createOperation('ManageTelcoAccess', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitTelcoAccess");';
            $auth->createOperation('SubmitTelcoAccess', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveTelcoAccess");';
            $op = $auth->createOperation('ApproveTelcoAccess', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitTelcoAccess');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateTelcoAccess");';
            $auth->createOperation('DelegateTelcoAccess', 'Delegate', $bizrule, 'facility');

           
            $task->addChild('ApproveTelcoAccess');
            $task->addChild('SubmitTelcoAccess');
            //$task->addChild('DelegateTelcoAccess');


            // set remote hands permission's child operation and task
            //$auth->createOperation('RemoteHandsPermission', 'Remote Hands', '', 'permission');

            $task = $auth->createTask('RemoteHandsGroup', 'Remote Hands', '', 'permission_group,facility');

            $auth->createOperation('ManageRemoteHands', 'Manage');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("SubmitRemoteHands");';
            $auth->createOperation('SubmitRemoteHands', 'Submit', $bizrule, 'facility');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("ApproveRemoteHands");';
            $op = $auth->createOperation('ApproveRemoteHands', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitRemoteHands');

            $bizrule = 'return isset($params["request"]) && $params["request"]->isUserInFacility("DelegateRemoteHands");';
            $op = $auth->createOperation('DelegateRemoteHands', 'Delegate', $bizrule, 'facility');

            $task->addChild('ApproveRemoteHands');
            $task->addChild('SubmitRemoteHands');
            //$task->addChild('DelegateRemoteHands');

            // operations for bulletin/announcement
            //$auth->createOperation('BulletinBoardPermission', 'Bulletin Board', '', 'permission');

            $task = $auth->createTask('BulletinBoardGroup', 'Bulletin Board', '', 'permission_group,facility');

            $auth->createOperation('ManagePost', 'Manage');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("ReadPost");';
            $auth->createOperation('ReadPost', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("SubmitPost");';
            $op = $auth->createOperation('SubmitPost', 'Submit', $bizrule, 'facility');
            $op->addChild('ReadPost');

            $bizrule = 'return isset($params["post"]) && $params["post"]->isUserInFacility("ApprovePost");';
            $op = $auth->createOperation('ApprovePost', 'Approve', $bizrule, 'facility');
            $op->addChild('SubmitPost');

            $task->addChild('ApprovePost');
            $task->addChild('SubmitPost');
            $task->addChild('ReadPost');

            // set library permission's child operation and task
            //$auth->createOperation('LibraryPermission', 'Library', '', 'permission');

            $task = $auth->createTask('LibraryGroup', 'Library', '', 'permission_group,facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("CreateLibraryContent");';
            $auth->createOperation('CreateLibraryContent', 'Create', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("ReadLibraryContent");';
            $auth->createOperation('ReadLibraryContent', 'Read', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("UpdateLibraryContent");';
            $auth->createOperation('UpdateLibraryContent', 'Update', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("DeleteLibraryContent");';
            $auth->createOperation('DeleteLibraryContent', 'Delete', $bizrule, 'facility');

            $bizrule = 'return isset($params["content"]) && $params["content"]->isUserInFacility("ApproveRemoteHands");';
            $op = $auth->createOperation('ManageLibraryContent', 'Manage', $bizrule, 'facility');
            $op->addChild('CreateLibraryContent');
            $op->addChild('ReadLibraryContent');
            $op->addChild('UpdateLibraryContent');
            $op->addChild('DeleteLibraryContent');

            $task->addChild('ManageLibraryContent');
            $task->addChild('ReadLibraryContent');

            // operations for reports
            //$auth->createOperation('ReportPermission', 'Report', '', 'permission');

            $task = $auth->createTask('ReportGroup', 'Report', '', 'permission_group,facility');

            $auth->createOperation('ManageReport', 'ManageReport');

            $bizrule = 'return isset($params["report"]) && $params["report"]->isUserInFacility("ReadReport") && $params["report"]->isUserInClient("ReadReport");';
            $auth->createOperation('ReadReport', 'Read', $bizrule, 'facility,client');

            $bizrule = 'return isset($params["report"]) && $params["report"]->isUserInFacility("ReportPermission") && $params["report"]->isUserInClient("ReportPermission");';
           
            $task->addChild('ReadReport');

            $auth->createOperation('ReadAllReports', 'Read All Reports');


            $auth->createOperation('DelegateRole', 'Delegate Role', '');  

            // assign operation dropdown
            $auth->createOperation('AssignRole', 'Assign Role');
            $auth->createOperation('AssignOperation', 'Assign Operation');
            //$auth->createOperation('AssignFacility', 'Assign Facility');
            //$auth->createOperation('AssignCustomer', 'Assign Customer');
            
            // associate permission
            $auth->createOperation('AssociateFacility', 'Associate Facility');
            $auth->createOperation('AssociateCustomer', 'Associate Customer');
            $auth->createOperation('AssociateAllFacility', 'Associate All Facility');
            $auth->createOperation('AssociateAllCustomer', 'Associate All Customer');
        
            // fujitso user
            $role = $auth->createRole('FujitsuUser', 'Fujitsu User');
            //set default permission item
            $role->addChild('UpdateProfile');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('SubmitPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // implementation staff
            $role = $auth->createRole('ImplementationStaff', 'Implementation Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // security staff
            $role = $auth->createRole('SecurityStaff', 'Security Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // operation staff
            $role = $auth->createRole('OperationStaff', 'Operation Staff');
            // set inherited user
            $role->addChild('FujitsuUser');
            // set default permission item
            $role->addChild('PortalLogin');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');

            // implementation staff
            $role = $auth->createRole('CustomerUser', 'Customer User');
            // set inherited auth
            $role->addChild('UpdateProfile');
            $role->addChild('PortalLogin');
            $role->addChild('SubmitEMF');
            $role->addChild('SubmitDCAccess');
            $role->addChild('ReadPost');

            // Customer admin
            $role = $auth->createRole('CustomerAdmin', 'Customer Administrator');
            // set inherit user
            $role->addChild('CustomerUser');
            // set defaul auth
            $role->addChild('AssociateFacility');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ReadPost');
            $role->addChild('ReadReport');
            $role->addChild('ReadLibraryContent');


            // facility admin
            $role = $auth->createRole('FacilityAdmin', 'Facility Administrator');
            //set inherited user
            $role->addChild('CustomerAdmin');
            $role->addChild('ManagePortalLogin');
            $role->addChild('ReadConfiguration'); // recently added
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageCustomer');
            $role->addChild('ReadFacility');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');

            // application admin
            $role = $auth->createRole('ApplicationAdmin', 'Application Administrator');
            // set inherited user
            $role->addChild('FacilityAdmin');
            $role->addChild('OperationStaff');
            $role->addChild('SecurityStaff');
            $role->addChild('ImplementationStaff');
            // set default auth
            $role->addChild('AssociateCustomer');
            $role->addChild('AssociateAllCustomer');
            $role->addChild('AssociateFacility');
            $role->addChild('AssociateAllFacility');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ReadConfiguration');
            $role->addChild('ManageCustomer');
            $role->addChild('ManageFacility');
            $role->addChild('ManageRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');
           
            // superadmin admin
            $role = $auth->createRole('SuperAdmin', 'Super Admin');
            // set inherited user
            $role->addChild('ApplicationAdmin');
            $role->addChild('ManagePortalLogin');
            $role->addChild('PortalLogin');
            $role->addChild('DelegateRole');
            $role->addChild('ManageConfiguration');
            $role->addChild('ManageCustomer');
            $role->addChild('ManageFacility');
            $role->addChild('ManageRole');
            $role->addChild('ManageUser');
            $role->addChild('ManageProximityCard');
            $role->addChild('ManageEMF');
            $role->addChild('ManageDCAccess');
            $role->addChild('ManageTelcoAccess');
            $role->addChild('ManagePost');
            $role->addChild('ManageReport');
            $role->addChild('ApproveProximityCard');
            $role->addChild('ApproveEMF');
            $role->addChild('ApproveDCAccess');
            $role->addChild('ApproveTelcoAccess');
            $role->addChild('ApprovePost');
            $role->addChild('ReadReport');
            $role->addChild('ManageLibraryContent');

        echo "Authorization hierarchy successfully generated.";

        //print '<br>Password: '.User::model()->encrypt('sa@123');
    }

    public function actionAuth() {

        // set authitems init values for fixtures
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, i.bizrule, i.data, i.precedence,";
        $cmd->from('AuthItem as i');
        $cmd->order = 'type desc, name asc';
        $items = $cmd->query();

        if (!empty($items)) {
            echo 'return array(';
            foreach ($items as $row) {
                echo "<br>array("
                . "'name'=>'" . $row['name'] . "',"
                . "'type'=>" . $row['type'] . ","
                . "'description'=>'" . $row['description'] . "',"
                . "'bizrule'=>'" . $row['bizrule'] . "',"
                . "'data'=>'" . $row['data'] . "',"
                . "'precedence'=>" . $row['precedence'] . ","
                . "),";
            }
            echo ');';
        }

        echo '<br><br><br>';

        // set authitemchild init values for fixtures
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.parent, i.child";
        $cmd->from('AuthItemChild as i');
        $cmd->order = "parent asc, child asc";
        $items = $cmd->query();

        if (!empty($items)) {
            echo 'return array(';
            foreach ($items as $row) {
                echo "<br>array("
                . "'parent'=>'" . $row['parent'] . "',"
                . "'child'=>'" . $row['child'] . "'"
                . "),";
            }
            echo ');';
        }
    }

    public function actionPhpInfo() {

        $this->render('phpinfo');
    }

    public function actionDateLine1Graph() {

        // create graph and display the graph directly browser

        Yii::import('application.vendors.jpgraph.*');

        require_once('jpgraph.php');
        require_once('jpgraph_line.php');
        require_once('jpgraph_date.php');

        // Create a data set in range (50,70) and X-positions
        DEFINE('NDATAPOINTS', 360);
        DEFINE('SAMPLERATE', 240);
        $start = time();
        $end = $start + NDATAPOINTS * SAMPLERATE;
        $data = array();
        $xdata = array();
        for ($i = 0; $i < NDATAPOINTS; ++$i) {
            $data[$i] = rand(50, 70);
            $xdata[$i] = $start + $i * SAMPLERATE;
        }


        // Create the new graph
        $graph = new Graph(540, 300);

        // Slightly larger than normal margins at the bottom to have room for
        // the x-axis labels
        $graph->SetMargin(40, 40, 30, 130);

        // Fix the Y-scale to go between [0,100] and use date for the x-axis
        $graph->SetScale('datlin', 0, 100);
        $graph->title->Set("Example on Date scale");

        // Set the angle for the labels to 90 degrees
        $graph->xaxis->SetLabelAngle(90);

        $line = new LinePlot($data, $xdata);
        $line->SetLegend('Year 2005');
        $line->SetFillColor('lightblue@0.5');
        $graph->Add($line);

        $graph->Stroke(); // draw / print the image
    }

    public function setSimpleLine1Graph() {

        //create graph and save to file on given filename

        Yii::import('application.vendors.jpgraph.*');

        require_once ('jpgraph.php');
        require_once ('jpgraph_line.php');

// The callback that converts timestamp to minutes and seconds
        function TimeCallback($aVal) {
            return Date('H:i:s', $aVal);
        }

// Fake some suitable random data
        $now = time();
        $datax = array($now);
        for ($i = 0; $i < 360; $i += 10) {
            $datax[] = $now + $i;
        }
        $n = count($datax);
        $datay = array();
        for ($i = 0; $i < $n; ++$i) {
            $datay[] = rand(30, 150);
        }

        // Setup the basic graph
        $graph = new Graph(324, 250);
        $graph->SetMargin(40, 40, 30, 70);
        $graph->title->Set('Date: ' . date('Y-m-d', $now));
        $graph->SetAlphaBlending();

        // Setup a manual x-scale (We leave the sentinels for the
        // Y-axis at 0 which will then autoscale the Y-axis.)
        // We could also use autoscaling for the x-axis but then it
        // probably will start a little bit earlier than the first value
        // to make the first value an even number as it sees the timestamp
        // as an normal integer value.
        $graph->SetScale("intlin", 0, 200, $now, $datax[$n - 1]);

        // Setup the x-axis with a format callback to convert the timestamp
        // to a user readable time
        $graph->xaxis->SetLabelFormatCallback('TimeCallback');
        $graph->xaxis->SetLabelAngle(90);

        // Create the line
        $p1 = new LinePlot($datay, $datax);
        $p1->SetColor("blue");

        // Set the fill color partly transparent
        $p1->SetFillColor("blue@0.4");

        // Add lineplot to the graph
        $graph->Add($p1);

        // Send the graph back to the client
        // Display the graph
        $graph->Stroke('images/simpleline1.png');
    }

    public function actionSampleGraph() {

        // create graph and save to file using an action on controller
        //$this->setSimpleLine1Graph();
        // create graph in a component
        $graph = new GraphComponent;
        $graph->title = 'KW Total University of Melbourne';
        // create graph, do not display, but save as file on given filename
        $graph->setDateLine2Graph(false, 'images/dateline2.png');

        $this->render('graph', array('graph' => $graph,));
    }

}