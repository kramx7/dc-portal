<?php

class NoticeboardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow authenticated users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = Noticeboard::model()->findByPk($id);
                
        // set authorization argument
        $params["post"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitPost',$params);
        $approve_permission = User::model()->isAuthorized('ApprovePost',$params);

        // check user authorization
        if(!$submit_permission){
            if(!$approve_permission){
               User::model()->checkUserAccess('ReadPost',$params);
            }
        }
                
		$this->render('view',array('model'=>$model));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
        $model = new Noticeboard;
        
        // check user authorization
        //$params["post"] = $model;
		//User::model()->checkUserAccess('SubmitPost',$params);

        $params["post"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitPost',$params);
        $approve_permission = User::model()->isAuthorized('ApprovePost',$params);

        // check user authorization
        if(!$submit_permission){
            if(!$approve_permission){
                User::model()->checkUserAccess('SubmitPost',$params);
            }
        }

		// set the creator id to the login user
        $creator_id = Yii::app()->user->id;

        $model->creator_id = $creator_id;
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		if(isset($_POST['Noticeboard']))
		{
			$model->attributes=$_POST['Noticeboard'];
			
			// set submitted by to user id of the log in user
			$model->submitted_by = Yii::app()->user->id;

			if(isset($_POST['submit'])){
				$model->message_status = NoticeboardMessageStatus::STATUS_SUBMITTED;
			}else if(isset($_POST['draft'])){
				$model->message_status = NoticeboardMessageStatus::STATUS_DRAFT;
			}
			
			if($model->save()){
				Yii::app()->user->setFlash('success', 'Successfully created a new post.');
				$this->redirect(array('site/index'));
			}	
		}

		if($model->priority == ''){
			$model->priority = $model->default_priority;
		}

        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'AssociateAllFacility');

        $model->facilities = $facilities;
			
		$this->render('create',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{	
		$model=$this->loadModel($id);
		
		// check user authorization
        $params["post"] = $model;
        $approve_permission = User::model()->isAuthorized('ApprovePost',$params);
        $submit_permission = User::model()->isAuthorized('SubmitPost',$params);

		if(!$approve_permission){
			if(!$submit_permission){
				User::model()->checkAccess('SubmitPost',$params);
			}
		}
		
		// set the creator id to the login user
        $creator_id = Yii::app()->user->id;
        $model->creator_id = $creator_id;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		

		if(isset($_POST['Noticeboard']))
		{	
			if(isset($_POST['submit'])){
				$model->attributes=$_POST['Noticeboard'];
				$model->scenario = 'update';
				$model->message_status = NoticeboardMessageStatus::STATUS_SUBMITTED;
			}else if(isset($_POST['draft'])){
				$model->attributes=$_POST['Noticeboard'];
				$model->scenario = 'update';
				$model->message_status = NoticeboardMessageStatus::STATUS_DRAFT;
			}else if(isset($_POST['approve'])){
				$model->scenario = 'approval';
				$model->message_status = NoticeboardMessageStatus::STATUS_APPROVED;
			}else if(isset($_POST['reject'])){
				$model->scenario = 'approval';
				$model->message_status = NoticeboardMessageStatus::STATUS_CORRECTION;
			}
                        
			if($model->save()){ 
				Yii::app()->user->setFlash('success', 'Successfully updated the post.');
				$this->redirect(array('site/index'));	
	             //$this->redirect(array('view','id'=>$model->id));
			}
		}

		// get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'AssociateAllFacility');

        $model->facilities = $facilities;

        if($model->priority == ''){
			$model->priority = $model->default_priority;
		}
		
		if($creator_id == $model->submitted_by){
			$view = 'update';
		}else{
			$view = 'approval';
		}

		$this->render($view,array(
			'model'=>$model
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model = $this->loadModel($id);
		
		// check user authorization
                $params["post"] = $model;
		User::model()->checkUserAccess('DeletePost', $params);
		
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// check user authorization
		//User::model()->checkUserAccess('viewNotices');
		
		//$dataProvider=new CActiveDataProvider('Noticeboard');
		// set user facility from login user facility id, set in Components:UserIdentity:authenticate
		// use for condition filter, to check if user is limited for certain facility only
		$user_facility_id = 0;// Yii::app()->user->facilityId;
		
		if(!empty($user_facility_id)){
		// facility adminsitrator, customer admin and customer user are only allowed to see the notice on their assigned facility
		// need to create a query, and condition to filter the message to publish message and publish dates and belongs to their facility	
		
			//Noticeboard::model()->_published_status is the status flag for published notices
			$conditions = "status = '".Noticeboard::model()->_published_status."' AND (DATE_FORMAT(publish_from,'%Y%m%d') <=  DATE_FORMAT(NOW(),'%Y%m%d') AND DATE_FORMAT(publish_to,'%Y%m%d') >= DATE_FORMAT(NOW(),'%Y%m%d') ) AND facility = ".$user_facility_id;
		}else{
			// get only the notice that has a publish status and within range
			$conditions = "status = '".Noticeboard::model()->_published_status."' AND (DATE_FORMAT(publish_from,'%Y%m%d') <=  DATE_FORMAT(NOW(),'%Y%m%d') AND DATE_FORMAT(publish_to,'%Y%m%d') >= DATE_FORMAT(NOW(),'%Y%m%d') ) ";
		}
		
		$dataProvider=new CActiveDataProvider('Noticeboard', array(
		    'criteria'=>array(
		        'condition'=>$conditions,
		        'order'=>'sticky DESC, publish_from DESC' // display first records with sticky regardless of dates, then arrange by dates
		    ),
		    'pagination'=>array(
		        'pageSize'=>20,
		    ),
		));
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		// check user authorization
                //$params["post"] = $model;
		//User::model()->checkUserAccess('adminNotices');
		
		$model=new Noticeboard('search');
		$model->unsetAttributes();  // clear any default values
		
		// set search user to filter results to facility for facility administrator, customer admin and customer user
        //get user assigned facilities, set upon user login
        $assigned_facilities = Yii::app()->user->assigned_facilities;

        //get user assigned roles, set upon user login
        $assigned_roles = Yii::app()->user->assigned_roles;
		$model->_search_user_role = $assigned_roles;
		$model->_search_user_facility = $assigned_facilities;
		
		if(isset($_GET['Noticeboard']))
			$model->attributes=$_GET['Noticeboard'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Noticeboard the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Noticeboard::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Noticeboard $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='noticeboard-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
