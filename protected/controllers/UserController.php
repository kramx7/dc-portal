<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{	
		return array(
	        array('allow',
	            'actions'=>array('login','verify','requestreset','resetpassword'), //allow anonymous users to view login action
	            'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		
		// check user authorization
		User::model()->checkUserAccess('viewUser',array('facility_id'=>$model->facility));
		
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		// check user authorization
		User::model()->checkUserAccess('createUser');
		
		
		$model = new User;
		
		// this variable will be used to set default tab view, using the jqueryui tab
		$tabIndex = 0;
		$user_data = array();
		
		if(isset($_POST['User'])){
		  // if form is submmited
		  
		  // assigned data to variable		
		  $user_data = $_POST['User'];
		  
		  // if there is an existing session for newuser data
		  // merge the new data to the previous data
		  // this is because the form has 3 steps, data on step1 will be lost on the step 3
		  // this is limitation of post data
		  // so those data must be save on the session
		  if(is_array(Yii::app()->session->itemAt('NewUser'))){
		  	$user_data = User::model()->mergeFormFieldData(Yii::app()->session->itemAt('NewUser'),$user_data);
		  }
		  
		  // add data or the merge data to session
		  Yii::app()->session->add('NewUser',$user_data);
		  
		}  
		
		if (isset($_POST['cancel'])) {
			// if button cancel is click redirect to list of users
		  	$this->redirect(array('user'));
		
		} elseif (isset($_POST['step2'])) {
		
		  
		  if(User::model()->isUserHasRole(Yii::app()->params['SuperAdminCode'])){
		  	// set separate validation scenario for step1 for superadmin
		  	// only require the user role field for step 1
		  	$model->scenario = 'step1'; 
		  }else{
		  	// for other user, like facility admin, set validation scenarion step1f to allow two required fields on step1
		  	// facilty and client
			$model->scenario = 'step1f'; 
		  }
		  
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);
		  
		  if($model->validate())
		    $tabIndex = 1;
		  else {
		    $tabIndex = 0;
		  }
		  
		}elseif (isset($_POST['step3'])) {
		  
		  $model->scenario = 'step2'; // set 
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);
		  
		  if($model->user_role == 'Cust_Admin' || $model->user_role == 'Cust_User'){
		  	// set validation to require the client field
			  $model->scenario = 'step2f';
		  }else{
		  	  // set validation to require the facility field
			  $model->scenario = 'step2';
		  }
		  
		  if($model->validate())
		    $tabIndex = 2;
		  else {
		    $tabIndex = 1;
		  }
		  
		} elseif (isset($_POST['finish'])) {
		
		  $model=new User; // set validation scenario to default insert to check all required fields
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);	
		  	  
		  if ($model->save()){
		  
		  	$model->sendVerficationEmail($model);
		  	
		    unset(Yii::app()->session['NewUser']);
		    
		    $this->redirect(array('view','id'=>$model->id));
		  
		  }else {
		    $tabIndex = 2;
		   }
		    
		} else { // this is the default, first time (step1)
		  
		  $tabIndex = 0;
		  
		  if(Yii::app()->session->itemAt('NewUser') != null){
		  	unset(Yii::app()->session['NewUser']);
		  }
		  
		} 
		
                // get necessary data needed for the form dropdown option
		$user_role_list = AuthItem::model()->userRoleForList()->findAll();
		$client_list = Client::model()->clientForList()->findAll();
		$facility_list = Facility::model()->facilityForList()->findAll();
				
		$client = new Client;
		
		if(User::model()->isUserHasRole(Yii::app()->params['FacAdminCode'])){
			// for facility administrator, limit the client list to its assigned facility only
			//$client->_facility_id = Yii::app()->user->facilityId;
                        //AuthItem::model()->userRoleForList()->findAll();
                        $user_role_list = AuthItem::model()->facAdminRoleForList()->findAll();
		}		
		
		
		if($model->user_role == 'SuperAdmin'){
			// if user to create is SuperAdmin, set unlimited as default facility, represented by 0
			// but when save to db it is change to empty or null, because of table relations to facility
			$model->facility = 0;
		}
				
		
		$this->render('create',array(
			'model'=>$model,'tabIndex'=>$tabIndex,'user_role_list'=>$user_role_list,
			'client_list'=>$client_list,'facility_list'=>$facility_list));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$model=$this->loadModel($id);
		
		//check user authorization
		User::model()->checkUserAccess('updateUser',array('facility_id'=>$model->facility));
		
		
		$tabIndex = 0;
		$user_data = array();
		
		if(isset($_POST['User'])){
		  // if form is submmited
		  	
		  // this variable will be used to set default tab view, using the jqueryui tab	
		  $user_data = $_POST['User'];
		  
		  // if there is an existing session for newuser data
		  // merge the new data to the previous data
		  // this is because the form has 3 steps, data on step1 will be lost on the step 3
		  // this is limitation of post data
		  // so those data must be save on the session
		  if(is_array(Yii::app()->session->itemAt('UpdateUser'))){
		  	$user_data = User::model()->mergeFormFieldData(Yii::app()->session->itemAt('UpdateUser'),$user_data);
		  }
		  
		  // add data or the merge data to session
		  Yii::app()->session->add('UpdateUser',$user_data);
		  
		}  
		
		if (isset($_POST['cancel'])) {
		  	// if button cancel is click redirect to list of users
		  	$this->redirect(array('user'));
		
		} elseif (isset($_POST['step2'])) {
		
		  if(User::model()->isUserHasRole(Yii::app()->params['SuperAdminCode'])){
		  	// set separate validation scenario for step1 for superadmin
		  	// only require the user role field for step 1
		  	$model->scenario = 'step1'; 
		  }else{
		  	// for other user, like facility admin, set validation scenarion step1f to allow two required fields on step1
		  	// facilty and client
			$model->scenario = 'step1f'; 
		  }
		  
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);
		  
		  if($model->validate())
		    $tabIndex = 1;
		  else {
		    $tabIndex = 0;
		  }
		  
		}elseif (isset($_POST['step3'])) {
		
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);
		  
		  if($model->user_role == 'Cust_Admin' || $model->user_role == 'Cust_User'){
		  	// set validation to require the client field
			  $model->scenario = 'step2f';
		  }else{
		  	  // set validation to require the facility field
			  $model->scenario = 'step2';
		  }
		  
		  // map from data to model fields/attributes; used a customized mapping
		  $model->setFormFieldData($user_data);
		  
		  if($model->validate())
		    $tabIndex = 2;
		  else {
		    $tabIndex = 1;
		  }
		  
		} elseif (isset($_POST['finish'])) {
			
		  	// map from data to model fields/attributes; used a customized mapping
			$model->setFormFieldData($user_data);	
		  		  
		  if ($model->save()){
		  	
		  	$model->sendVerficationEmail($model); 
		  	unset(Yii::app()->session['UpdateUser']);
		  	$this->redirect(array('view','id'=>$model->id));
		  
		  }else {
		    $tabIndex = 2;
		   }
		    
		} else { // this is the default, first time (step1)
		  
		  $tabIndex = 0;
		  
		  if(Yii::app()->session->itemAt('UpdateUser') != null){
		  	unset(Yii::app()->session['UpdateUser']);
		  }
		  
		}
		
		
		// get necessary data needed for the form dropdown option
		$user_role_list = AuthItem::model()->userRoleForList()->findAll();
		$client_list = Client::model()->clientForList()->findAll();
		$facility_list = Facility::model()->facilityForList()->findAll();
                
		if($model->user_role == 'SuperAdmin'){
			// if user to create is SuperAdmin, set unlimited as default facility, represented by 0
			// but when save to db it is change to empty or null, because of table relations to facility
			$model->facility = 0;
		}
                
                $client = new Client;
                
		if(User::model()->isUserHasRole(Yii::app()->params['FacAdminCode'])){
			// for facility administrator, limit the client list to its assigned facility only
			$client->_facility_id = Yii::app()->user->facilityId;
                        //AuthItem::model()->userRoleForList()->findAll();
                        $user_role_list = AuthItem::model()->facAdminRoleForList()->findAll();
		}
                
		$this->render('update',array(
			'model'=>$model,'tabIndex','tabIndex'=>$tabIndex,'user_role_list'=>$user_role_list,
			'client_list'=>$client_list,'facility_list'=>$facility_list
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		
		//check user authorization
		User::model()->checkUserAccess('deleteUser',array('facility_id'=>$model->facility));
		
		//$model->delete();
		// when deleting a user, it means only to flag the status to  deleted and disable login thru clearing the password field
        // not to delete it totally from the table,
        $model->deleteUser($model);
		
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// check user authorization
		User::model()->checkUserAccess('viewUsers');
		
		//$dataProvider=new CActiveDataProvider('User');
		
		if(User::model()->isUserHasRole(Yii::app()->params['FacAdminCode'])){
			$user_facility_id = Yii::app()->user->facilityId;
			$conditions = "facility = ".$user_facility_id;
		}else{
			$conditions = "";
		}
		
		$dataProvider=new CActiveDataProvider('User', array(
		    'criteria'=>array(
		        'condition'=>$conditions
		    ),
		    'pagination'=>array(
		        'pageSize'=>20,
		    ),
		));
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		// check user authorization
		User::model()->checkUserAccess('adminUsers');
		
		$model=new User('search');
		
		//set the user role and assigned facility to be used to limit/filter the results
		$model->_search_user_role = Yii::app()->user->assigned_roles;
		$model->_search_user_facility = Yii::app()->user->assinged_facilities;
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionLogin()
	{
		$model=new User('login');

		if(isset($_POST['User'])) {
			$model->attributes=$_POST['User'];
			// validate user input and redirect
			// to the previous page if valid:
			if($model->validate() && $model->login())
			$this->redirect(Yii::app()->user->returnUrl);
		}
		
		// display the login form
		$this->render('login',array('model'=>$model));	
	}
	
	
	
        
	/* action that will be open from the link on the email verification
         * it requires a code
         * this action has special url configuration on the main config file
         */
        
	public function actionVerify($code){
		// the actionVerify method has a specific route on main config file
                
        if(!Yii::app()->user->isGuest){
          // if there is a login user, logout the account first
           Yii::app()->user->logout();
        }
            
		//get the user data base on verification code
		$model = User::model()->findByAttributes(array('verification_code'=>$code));

		$this->performAjaxValidation($model);
				
		// if form is submitted for verification		
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User']; // map the data from the user to model attributes/fields
			$model->scenario = 'verification'; // set scenario, usefull to set the validation rules to use
			$model->user_status = User::STATUS_ACTIVE; 
			
			if($model->save()){
				// set success verification message and redirect to login
				Yii::app()->user->setFlash('verification','You have successfully verified and activated your account.');
				$this->redirect(Yii::app()->createUrl('site/login'));
			}else{
				$model->user_status = User::STATUS_NEW;
			}
			// else present the verification form again with the validation error messages
		}
		
		
		if($model == null){
			//verification code did not match to any record from the database, table: user, field: verification_code
			Yii::app()->user->setFlash('success','Verification Code is invalid.');
		
		}else if($model->user_status == User::STATUS_ACTIVE ){
			//user account is already verified
			Yii::app()->user->setFlash('success','You have already verified your account.');
			$this->redirect(Yii::app()->createUrl('site/login'));
				
		}else if(strtotime(date('Y-m-d')) >=  strtotime($model->modified_time) + (30*24*60*60)){
			// user verify the accounts after 30 days
				Yii::app()->user->setFlash('success','Verification Code has expired.<br> Please request to the admin for another verification code.');
		}else{
			// clear the password and passCompare so as to avoid other user to look into the password
			// in cases the uses a public or unsecure computer
			$model->password = '';
			$model->password_compare = '';			
		}
		
		$this->render('verification',array('model'=>$model));
		
	} 

	public function actionRequestReset(){
		// the actionVerify method has a specific route on main config file
        
        $model = new User;

        if(isset($_POST['User']))
		{
			$model->scenario = 'request-reset';
			$model->attributes=$_POST['User'];
			
			//if($model->checkActiveStatus($model->emailaddress) ){
			if($model->validate() ){

				$user = User::model()->findByAttributes(array('emailaddress'=>$model->emailaddress));

				if($user != null){
					$user->sendRequestResetNotification($user);
					Yii::app()->user->setFlash('success','Reset password link is sent to email.');
				}else{
					Yii::app()->user->setFlash('success','Email is not registered to any account.');
				}

				$this->redirect(Yii::app()->createUrl('/user/requestreset'));
			}
		}	
		
		$this->render('request-reset',array('model'=>$model));
		
	}

	public function actionResetPassword($code){
		// the actionVerify method has a specific route on main config file
                
        if(!Yii::app()->user->isGuest){
          // if there is a login user, logout the account first
           Yii::app()->user->logout();
        }

        $model = User::model()->findByAttributes(array('verification_code'=>$code));

		$this->performAjaxValidation($model);

        if(isset($_POST['User']))
		{
			$model->scenario = 'reset-password-request';
			$model->attributes=$_POST['User'];
			
			//if($model->checkActiveStatus($model->emailaddress) ){
			if($model->validate() ){

				$user = User::model()->findByAttributes(array('emailaddress'=>$model->emailaddress));
				$user->scenario = 'reset-password-request';
				$user->attributes=$_POST['User'];
				$user->save();

				Yii::app()->user->setFlash('verification','You have successfully changed your password.');

				$user->sendResetPasswordNotification($user);

				$this->redirect(Yii::app()->createUrl('/site/login'));
			}
		}	
		
		$this->render('reset-password',array('model'=>$model));
		
	}
        
	
}
