<?php

class RequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('facilityaccess','updatefacilityaccess',
					'proximity','updateproximity',
					'equipment', 'updateequipment',
					'remotehands'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Performs the AJAX validation.
	 * @param AccessRequest $model the model to be validated
	 */
	protected function performAjaxValidation($model, $form_id)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==$form_id)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionFacilityAccess()
	{
        $model = new AccessRequest();
        $user_id = Yii::app()->user->id;

        //set submit by
        $model->submitted_by = $user_id;

        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitDCAccess',$params);
        $approve_permission = User::model()->isAuthorized('ApproveDCAccess',$params);
        $model->submit_permission = $submit_permission;
        $model->approve_permission = $approve_permission;

        // check user authentication
        User::model()->checkUserAccess('SubmitDCAccess',$params, 'ApproveDCAccess');

        $this->performAjaxValidation($model, 'request-access-form');

        // set facility options
        //$model->facilities = UserTaskFacility::model()->getUserFacilities($user_id);  
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($user_id, 'SubmitDCAccess','ApproveDCAccess');
        $model->workareas = AccessWorkArea::model()->getWorkAreas();

        if(isset($_POST['AccessRequest'])){

            $model->attributes = $_POST['AccessRequest'];
            
            if(isset($_POST['submit'])){
            	$model->status = AccessRequestStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = AccessRequestStatus::STATUS_DRAFT;
            }

            if($model->save()){
            	Yii::app()->user->setFlash('success', 'Successfully created the facility access request.');
                $this->redirect(array('site/index'));	
            }

        }  

		$this->render('create-facility-access',array(
			'model'=>$model,
		));
	}

	public function actionUpdateFacilityAccess($id)
	{
        $model = AccessRequest::model()->findByPk($id);
        $user_id = Yii::app()->user->id;

        //set submit by
        $model->creator_id = $user_id;

        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitDCAccess',$params);
        $approve_permission = User::model()->isAuthorized('ApproveDCAccess',$params);
        $model->submit_permission = $submit_permission;
        $model->approve_permission = $approve_permission;

        User::model()->checkUserAccess('SubmitDCAccess',$params, 'ApproveDCAccess');

        $this->performAjaxValidation($model, 'request-access-form');

        // set user is client
        $user = User::model()->findByPk($user_id);
        $user_role = $user->getPrimaryRole($user_id);

        if($user_role == 'CustomerAdmin' || $user_role == 'CustomerUser'){
            $model->client_user = true;
        }

        // set facility options
        //$model->facilities = UserTaskFacility::model()->getuserFacilities($user_id);
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($user_id, 'SubmitDCAccess','ApproveDCAccess');  
        $model->workareas = AccessWorkArea::model()->getWorkAreas();

        if(isset($_POST['AccessRequest'])){

            $model->attributes = $_POST['AccessRequest'];
            $model->prev_status = $model->status;

            if(isset($_POST['submit'])){
            	$model->status = AccessRequestStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = AccessRequestStatus::STATUS_DRAFT;
            }else if(isset($_POST['customer_approve'])){
            	//$model->status = AccessRequestStatus::STATUS_CUSTOMER_APPROVED;
            	$model->status = AccessRequestStatus::STATUS_APPROVAL;
            }else if(isset($_POST['fujitsu_approve'])){
            	$model->status = AccessRequestStatus::STATUS_APPROVED;
            }else if(isset($_POST['customer_reject'])){
            	$model->status = AccessRequestStatus::STATUS_CORRECTION;
            }else if(isset($_POST['fujitsu_reject'])){
            	$model->status = AccessRequestStatus::STATUS_REJECTED;
            }

            if($model->save()){
            	Yii::app()->user->setFlash('success', 'Successfully created the facility access request.');
                $this->redirect(array('site/index'));	
            }else{
            	$model->status = $model->prev_status;
            }

        }  

        if($model->submitted_by == $user_id && ($model->status == AccessRequestStatus::STATUS_DRAFT || $model->status == AccessRequestStatus::STATUS_CORRECTION) ){
            
    		$this->render('edit-facility-access',array(
    			'model'=>$model,
    		));

        }else{
            
            $this->render('update-facility-access',array(
                'model'=>$model,
            ));
        }
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionProximity()
	{
        $model = new ProximityCardRequest;
        $user_id = Yii::app()->user->id;

        //set submit by
        $model->user_id = $user_id;
        $model->submitted_by = $user_id;


        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitProximityCard',$params);
        $approve_permission = User::model()->isAuthorized('ApproveProximityCard',$params);
        $model->submit_permission = $submit_permission;
        $model->approve_permission = $approve_permission;

        User::model()->checkUserAccess('SubmitProximityCard',$params, 'ApproveProximityCard');

        $this->performAjaxValidation($model, 'proximity-card-form');

        // set facility options
        $model->facilities = UserTaskFacility::model()->getUserFacilities($user_id);

        if(isset($_POST['ProximityCardRequest'])){

            $model->attributes = $_POST['ProximityCardRequest'];

            // set primary client
            $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);

            if(isset($_POST['submit'])){
            	$model->status = ProximityCardRequestStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = ProximityCardRequestStatus::STATUS_DRAFT;
            }

            if($model->save()){
            	Yii::app()->user->setFlash('success', 'Successfully created the proximity card request.');
                $this->redirect(array('site/index'));	
            }

        }  

		$this->render('create-proximity-card',array(
			'model'=>$model,
		));
	}

	public function actionUpdateProximity($id)
	{
        $model = ProximityCardRequest::model()->findByPk($id);

        $user_id = Yii::app()->user->id;

        //set submit by
        $model->user_id = $user_id;
        $model->submitted_by = $user_id;

        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitProximityCard',$params);
        $approve_permission = User::model()->isAuthorized('ApproveProximityCard',$params);
        $model->submit_permission = $submit_permission;
        $model->approve_permission = $approve_permission;

        User::model()->checkUserAccess('SubmitProximityCard',$params, 'ApproveProximityCard');

        $this->performAjaxValidation($model, 'proximity-card-form');

        // set facility options
        $model->facilities = UserTaskFacility::model()->getUserFacilities($user_id);

        if(isset($_POST['ProximityCardRequest'])){

            $model->attributes = $_POST['ProximityCardRequest'];

            // set primary client
            $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);
           
            if(isset($_POST['submit'])){
            	$model->status = ProximityCardRequestStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = ProximityCardRequestStatus::STATUS_DRAFT;
            }else if(isset($_POST['approve'])){
            	$model->status = ProximityCardRequestStatus::STATUS_APPROVED;
            }else if(isset($_POST['reject'])){
            	$model->status = ProximityCardRequestStatus::STATUS_CORRECTION;
            }

            if($model->save()){
            	Yii::app()->user->setFlash('success', 'Successfully updated the proximity card request.');
                $this->redirect(array('site/index'));	
            }

        }  

        if($model->submitted_by == $user_id && ($model->status == ProximityCardRequestStatus::STATUS_DRAFT || $model->status == ProximityCardRequestStatus::STATUS_CORRECTION) ){

    		$this->render('edit-proximity-card',array(
    			'model'=>$model,
    		));
        }else{
           $this->render('update-proximity-card',array(
                'model'=>$model,
            )); 
        }
	}

	public function actionEquipment()
	{
        $model = new EquipmentMovement();
        $user_id = Yii::app()->user->id;

        //set submit by
        $model->submitted_by = $user_id;

        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitEMF',$params);
        $approve_permission = User::model()->isAuthorized('ApproveEMF',$params);
        $model->submit_permission = $submit_permission;
        $model->approve_permission = $approve_permission;

        User::model()->checkUserAccess('SubmitEMF',$params, 'ApproveEMF');

        $this->performAjaxValidation($model, 'equipment-movement-form');

        if(isset($_POST['ajax']) && $_POST['ajax']=='item-add'){
        	$model->saveTempEntries($_POST);
        	Yii::app()->end();
        }else if(isset($_POST['ajax']) && $_POST['ajax']=='item-remove'){
            $model->deleteTempEntries($_POST['item_number']);
            Yii::app()->end();
        }

        // set client name
        $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);
        $client = Client::model()->getClientById($model->client_id);
        $model->client_name = $client->client_name;

        // set temp entries
        $model->setTempEntries();

        $entry = new EquipmentMovementEntry;

        // set facility options
        //$model->facilities = UserTaskFacility::model()->getUserFacilities($user_id);
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($user_id, 'SubmitEMF','ApproveEMF');

        if(isset($_POST['EquipmentMovement'])){

            $model->attributes = $_POST['EquipmentMovement'];

            // set primary client
            $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);

            if(isset($_POST['submit'])){
            	$model->status = EquipmentMovementStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = EquipmentMovementStatus::STATUS_DRAFT;
            }

            if($model->save()){
                Yii::app()->session->remove('temp_entries');
            	Yii::app()->user->setFlash('success', 'Successfully created the equipment movement request.');
                $this->redirect(array('site/index'));	
            }

        }  

		$this->render('create-equipment-movement',array(
			'model'=>$model, 'entry'=>$entry
		));
	}

	public function actionUpdateEquipment($id)
	{

        $model = EquipmentMovement::model()->findByPk($id);
        $user_id = Yii::app()->user->id;

        //set creator id
        $model->creator_id = $user_id;

        $params["request"] = $model;
        $submit_permission = User::model()->isAuthorized('SubmitEMF',$params);
        $approve_permission = User::model()->isAuthorized('ApproveEMF',$params);
        $model->approve_permission = $approve_permission;
        $model->submit_permission = $submit_permission;

        User::model()->checkUserAccess('SubmitEMF',$params, 'ApproveEMF');

        $this->performAjaxValidation($model, 'equipment-movement-form');

        if(isset($_POST['ajax']) && $_POST['ajax']=='item-add'){
        	$model->saveEntries($_POST);
        	Yii::app()->end();
        }else if(isset($_POST['ajax']) && $_POST['ajax']=='item-remove'){
            $model->deleteEntry($_POST['item_number']);
            Yii::app()->end();
        }

        // set client name
        $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);
        $client = Client::model()->getClientById($model->client_id);
        $model->client_name = $client->client_name;

        // set temp entries
        $model->setEntries();

        // set user is client
        $user = User::model()->findByPk($user_id);
        $user_role = $user->getPrimaryRole($user_id);

        if($user_role == 'CustomerAdmin' || $user_role == 'CustomerUser'){
            $model->client_user = true;
        }

        $entry = new EquipmentMovementEntry;

        // set facility options
        $model->facilities = UserTaskFacility::model()->getUserFacilities($user_id);

        if(isset($_POST['EquipmentMovement'])){

            $model->attributes = $_POST['EquipmentMovement'];

            // set primary client
            $model->client_id = UserTaskClient::model()->getPrimaryClient($user_id);

            if(isset($_POST['submit'])){
            	$model->status = EquipmentMovementStatus::STATUS_SUBMITTED;
            }else if(isset($_POST['draft'])){
            	$model->status = EquipmentMovementStatus::STATUS_DRAFT;
            }else if(isset($_POST['approve'])){
                $model->status = EquipmentMovementStatus::STATUS_APPROVED;
            }else if(isset($_POST['reject'])){
                $model->status = EquipmentMovementStatus::STATUS_CORRECTION;
            }

            if($model->save()){
            	Yii::app()->user->setFlash('success', 'Successfully updated the equipment movement request.');
                $this->redirect(array('site/index'));	
            }

        }  

        if($model->submitted_by == $user_id && ($model->status == EquipmentMovementStatus::STATUS_DRAFT || $model->status == EquipmentMovementStatus::STATUS_CORRECTION) ){

    		$this->render('edit-equipment-movement',array(
    			'model'=>$model, 'entry'=>$entry
    		));
        }else{
            $this->render('update-equipment-movement',array(
                'model'=>$model, 'entry'=>$entry
            ));
        }
	}
	
}
