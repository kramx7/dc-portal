<?php

/**
 * This is the model class for table "AuthAssignment".
 *
 * The followings are the available columns in table 'AuthAssignment':
 * @property string $itemname
 * @property string $userid
 * @property string $bizrule
 * @property string $data
 *
 * The followings are the available model relations:
 * @property AuthItem $itemname0
 */
class AuthAssignment extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AuthAssignment the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'AuthAssignment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('itemname, userid', 'required'),
            array('itemname, userid', 'length', 'max' => 64),
            array('bizrule, data', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('itemname, userid, bizrule, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'itemname0' => array(self::BELONGS_TO, 'AuthItem', 'itemname'),
            'itemname1' => array(self::BELONGS_TO, 'AuthItem', 'itemname'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'itemname' => 'Itemname',
            'userid' => 'Userid',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('itemname', $this->itemname, true);
        $criteria->compare('userid', $this->userid, true);
        $criteria->compare('bizrule', $this->bizrule, true);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function deleteUserAssignment($user_id) {
        
        AuthAssignment::model()->deleteAll('userid = ?',$user_id);
    }

    function addUserAssignment($user_id, $assignments) {

        if ($assignments == null || count($assignments) <= 0) {
            return;
        }

        foreach ($assignments as $code) {
            $auth = new AuthAssignment;
            $auth->userid = $user_id;
            $auth->itemname = $code;
            $auth->save();
        }
    }

    public function getRoles($user_id) {

        $criteria = new CDbCriteria;
        $criteria->with = array('itemname0');
        $criteria->condition = " t.userid = " . $user_id . " AND itemname0.type = 2";
        $criteria->order = "itemname0.order asc";
        $roles = AuthAssignment::model()->findAll($criteria);

        if (empty($roles)) {
            $roles = array(); //to avoid errors on foreach
        }

        return $roles;
    }

    public function getRolesInArray($user_id) {

        $criteria = new CDbCriteria;
        $criteria->with = array('itemname0');
        $criteria->condition = " t.userid = " . $user_id . " AND itemname0.type = 2";
        $criteria->order = "itemname0.order asc";
        $roles = AuthAssignment::model()->findAll($criteria);
        $froles = array();

        if (empty($roles)) {
            $froles = array(); //to avoid errors on foreach
        } else {
            foreach ($roles as $role) {
                $froles[$role->itemname0->name] = $role->itemname0->description;
            }
        }

        return $froles;
    }

    public function getOperations($user_id) {

        // get inherited operations base on the user roles
        $roles = $this->getRoles($user_id);

        $operations = array();

        foreach ($roles as $role) {
            $parent = $role->itemname0->name;
            $this->getChildItem($parent, $operations);
        }


        return $operations;
    }

    function getChildItem($parent, &$operations) {
        // recursive check on child item to get the operations

        $childs = AuthItem::model()->getChildItem($parent);

        if (empty($childs)) {
            return;
        }

        foreach ($childs as $child) {

            if ($child['type'] == 1) {
                $operations[$child['name']] = $child['description'];
            } else {
                $this->getChildItem($child['name'], $operations);
            }
        }
    }

    function setChildRoles($parent, &$roles) {
        // recursive check on child item to get the operations

        $childs = AuthItem::model()->getChildRoles($parent);

        if (empty($childs)) {
            return;
        }

        foreach ($childs as $child) {
            $roles[$child['name']] = $child['description'];
            $this->setChildRoles($child['name'], $roles);
        }
    }

    function setChildOperations($parent, &$operations) {
        // recursive check on child item to get the operations

        $childs = AuthItem::model()->getChildOperations($parent);

        if (empty($childs)) {
            return;
        }

        foreach ($childs as $child) {
            $operations[$child['name']] = $child['description'];
            $this->setChildOperations($child['name'], $roles);
        }
    }

    public function getInheritedRoles($user_id) {

        // get the assigned roles on the authassignment table
        $assigned_roles = $this->getRolesInArray($user_id);
        $inherited_roles = array();

        foreach ($assigned_roles as $role => $desc) {
            $temp_inherited_roles = array();
            // get the inherited roles for every roles assigned directly
            $this->setChildRoles($role, $temp_inherited_roles);

            $inherited_roles = array_merge($inherited_roles, $temp_inherited_roles);
        }


        // if user has permission to delegate role, add the directly assigned roles
        if (User::model()->isAuthorized('DelegateRole')) {
            $inherited_roles = array_merge($inherited_roles, $assigned_roles);
        }

        return $inherited_roles;
    }

    public function getInheritedOperations($user_id) {
        // get all operations inherited of the user creator
        // first get assigned roles
        $inherited_roles = $this->getInheritedRoles($user_id);
        $inherited_operations = array();

        // loop thru the inherited roles to get the inherited operations
        // for all the roles
        foreach ($inherited_roles as $role => $desc) {

            $temp_inherited_operations = array();
            // get the child operations from the given role
            AuthAssignment::model()->setChildOperations($role, $temp_inherited_operations);
            // merge previous operations to the new operations    
            $inherited_operations = array_merge($inherited_operations, $temp_inherited_operations);
        }

        return $inherited_operations;
    }

    public function getDirectlyAssignOperations($user_id) {

        // operations assigned directly to user on authassignment , type = 0
        $criteria = new CDbCriteria;
        $criteria->with = array('itemname0');
        $criteria->condition = " t.userid = " . $user_id . " AND itemname0.type = 0";
        $operations = AuthAssignment::model()->findAll($criteria);
        $foperations = array();

        if (empty($operations)) {
            $foperations = array(); //to avoid errors on foreach
        } else {
            foreach ($operations as $operation) {
                $foperations[$operation->itemname0->name] = $operation->itemname0->description;
            }
        }

        return $foperations;
    }

    public function getInheritedAndDirectlyAssignedOperations($user_id) {

        // get user inherited operations base on user role and inherited roles
        $inherited_operations = $this->getInheritedOperations($user_id);
        // get user directly assigned roles on authassignment table
        $directly_assigned_operations = $this->getDirectlyAssignOperations($user_id);

        $operations = array_merge($inherited_operations, $directly_assigned_operations);

        return $operations;
    }

    public function addUserRole($user_id, $role) {

        // delete previous role asssingments
        $this->deleteUserAssignment($user_id);

        $auth = new AuthAssignment;
        $auth->userid = $user_id;
        $auth->itemname = $role;
        $auth->save();
    }

}