<?php

/**
 * This is the model class for table "power_statistics_summary".
 *
 * The followings are the available columns in table 'power_statistics_summary':
 * @property integer $record_id
 * @property integer $facility_id
 * @property integer $client_id
 * @property integer $target_id
 * @property string $target_description
 * @property string $YYYYMMDD
 * @property string $kVllab
 * @property string $kVllbc
 * @property string $kVllca
 * @property string $la
 * @property string $maxla
 * @property string $lb
 * @property string $maxlb
 * @property string $lc
 * @property string $maxlc
 * @property string $Frequency
 * @property string $kWTotal
 * @property string $maxkWTotal
 * @property string $kVATotal
 * @property string $maxkVATotal
 * @property string $kWh
 */
class PowerStatisticsSummary extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PowerStatisticsSummary the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'power_statistics_summary';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_id, client_id, target_id, YYYYMMDD', 'required'),
            array('facility_id, client_id, target_id', 'numerical', 'integerOnly' => true),
            array('target_description', 'length', 'max' => 255),
            array('kVllab, kVllbc, kVllca, la, maxla, lb, maxlb, lc, maxlc, Frequency, kWTotal, maxkWTotal, kVATotal, maxkVATotal', 'length', 'max' => 8),
            array('kWh', 'length', 'max' => 20),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, facility_id, client_id, target_id, target_description, YYYYMMDD, kVllab, kVllbc, kVllca, la, maxla, lb, maxlb, lc, maxlc, Frequency, kWTotal, maxkWTotal, kVATotal, maxkVATotal, kWh', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'record_id' => 'Record',
            'facility_id' => 'Facility',
            'client_id' => 'Client',
            'target_id' => 'Target',
            'target_description' => 'Target Description',
            'YYYYMMDD' => 'Yyyymmdd',
            'kVllab' => 'K Vllab',
            'kVllbc' => 'K Vllbc',
            'kVllca' => 'K Vllca',
            'la' => 'La',
            'maxla' => 'Maxla',
            'lb' => 'Lb',
            'maxlb' => 'Maxlb',
            'lc' => 'Lc',
            'maxlc' => 'Maxlc',
            'Frequency' => 'Frequency',
            'kWTotal' => 'K Wtotal',
            'maxkWTotal' => 'Maxk Wtotal',
            'kVATotal' => 'K Vatotal',
            'maxkVATotal' => 'Maxk Vatotal',
            'kWh' => 'K Wh',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('record_id', $this->record_id);
        $criteria->compare('facility_id', $this->facility_id);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('target_id', $this->target_id);
        $criteria->compare('target_description', $this->target_description, true);
        $criteria->compare('YYYYMMDD', $this->YYYYMMDD, true);
        $criteria->compare('kVllab', $this->kVllab, true);
        $criteria->compare('kVllbc', $this->kVllbc, true);
        $criteria->compare('kVllca', $this->kVllca, true);
        $criteria->compare('la', $this->la, true);
        $criteria->compare('maxla', $this->maxla, true);
        $criteria->compare('lb', $this->lb, true);
        $criteria->compare('maxlb', $this->maxlb, true);
        $criteria->compare('lc', $this->lc, true);
        $criteria->compare('maxlc', $this->maxlc, true);
        $criteria->compare('Frequency', $this->Frequency, true);
        $criteria->compare('kWTotal', $this->kWTotal, true);
        $criteria->compare('maxkWTotal', $this->maxkWTotal, true);
        $criteria->compare('kVATotal', $this->kVATotal, true);
        $criteria->compare('maxkVATotal', $this->maxkVATotal, true);
        $criteria->compare('kWh', $this->kWh, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getKwhByClientIdAndDatesAndTargetId($client_id, $from, $to, $target_id = 0) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, kWh as kwh
          FROM power_statistics_summary
          WHERE ( (client_id = :client_id)
          AND (target_id = :target_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':client_id', $client_id, PDO::PARAM_INT);
        $cmd->bindParam(':target_id', $target_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);

        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getKwhByClientIdAndDates($client_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, sum(kWh) as kwh
          FROM power_statistics_summary
          WHERE ( (client_id = :client_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )
          GROUP BY YYYYMMDD";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':client_id', $client_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);

        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getKwhByFacilityIdAndDatesAndTargetId($facility_id, $from, $to, $target_id = 0) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, kWh as kwh
          FROM power_statistics_summary
          WHERE ( (facility_id = :facility_id)
          AND (target_id = :target_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':target_id', $target_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);

        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getKwhByFacilityIdAndDates($facility_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, sum(kWh) as kwh
          FROM power_statistics_summary
          WHERE ( (facility_id = :facility_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )
          GROUP BY YYYYMMDD";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);

        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getKwhByFacilityIdAndDatesAndTargetIdAndClients($facility_id, $from, $to, $target_id = 0, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, kWh as kwh
          FROM power_statistics_summary
          WHERE ( (facility_id = :facility_id)
          AND (target_id = :target_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )
          AND client_id IN(:clients)      
        ";

        $clients_value = implode(',', array_keys($clients));

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':target_id', $target_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getKwhByFacilityIdAndDatesAndClients($facility_id, $from, $to, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(YYYYMMDD) as tmp, sum(kWh) as kwh
          FROM power_statistics_summary
          WHERE ( (facility_id = :facility_id)
          AND ( DATE(YYYYMMDD) BETWEEN :from_date AND :to_date)
          AND (dayofmonth(YYYYMMDD) >0 ) )
          AND client_id IN(:clients)   
          GROUP BY YYYYMMDD";

         $clients_value = implode(',', array_keys($clients)); 

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['kwh']);
            }
        }

        return $statistics;
    }

    public function getPowerByFacilityIdAndDates($facility_id, $from, $to) {

        $from_date = $from;
        $to_date = $to;

        $query = "SELECT YYYYMMDD as day, sum(kWh) as kwh 
            FROM power_statistics_summary 
            WHERE facility_id = :facility_id 
            AND UNIX_TIMESTAMP(YYYYMMDD) BETWEEN :from_date AND :to_date 
            AND dayofmonth(YYYYMMDD) > 0 
            GROUP BY YYYYMMDD";
        
        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);

        $result = $cmd->query();

        $powers = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $powers[] = array('day' => $row['day'], 'kwh'=>$row['kwh']);
            }
        }

        return $powers;
    }

    public function getPowerByFacilityIdAndDatesAndClients($facility_id, $from, $to, $clients) {

        $from_date = $from;
        $to_date = $to;

        $query = "SELECT YYYYMMDD as day, sum(kWh) as kwh 
            FROM power_statistics_summary 
            WHERE facility_id = :facility_id 
            AND UNIX_TIMESTAMP(YYYYMMDD) BETWEEN :from_date AND :to_date 
            AND dayofmonth(YYYYMMDD) > 0
            AND client_id IN(:clients) 
            GROUP BY YYYYMMDD";

        $clients_value = implode(',', array_keys($clients));    
        
        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':facility_id', $facility_id, PDO::PARAM_INT);
        $cmd->bindParam(':from_date', $from_date, PDO::PARAM_STR);
        $cmd->bindParam(':to_date', $to_date, PDO::PARAM_STR);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $powers = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $powers[] = array('day' => $row['day'], 'kwh'=>$row['kwh']);
            }
        }

        return $powers;
    }

}