<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SiteTest
 *
 * @author kramx7
 */
//Yii::import('application.controllers.SiteController');

class SiteTest  extends CTestCase{
    //put your code here
    
    public function testLoginFormValidation(){
        
        // provide the username and password
        // username can be change example to check on validation rule for email
        // password can be change example to check the length
        // user account can be change to test invalid, verified and unverified account 
        
        $model=new LoginForm;
        
        $model->username = 'kramx7yahoo.com';
        $model->password = 's@123';
        
        
        // test 1: test code on Models:LoginForm:rules - validation rules for login
        // validate user input 
        $model->validate();
        
        // show validation errors
        if($model->hasErrors()){
            print_r($model->getErrors());
        }
        
        // if the username and password does not meet the validation rule
        // the following error will be shown on the terminal
        // SiteTest::testLoginForm
        // Failed asserting that false matches expected true.
        $this->assertEquals($model->hasErrors(),false);
        
    }
    
    public function testUserIdentity(){
        
        // test 2: test code on Components:UserIdentity:authenticate
        // this method is called by Models:LoginForm:authenticate
        // second parameter options:
        // 0 -> no errors
        // 1 -> username is not found on the user table
        // 2 -> password is invalid
        // 3 -> user is inactive, account not verified
        $identity = new UserIdentity('kramx7@yahoo.com','sa@123');
        $this->assertEquals($identity->errorCode, !0);
        
    }
    
    public function testLoginFormAuthenticate(){
        
        $model=new LoginForm;
        
        $model->username = 'kramx7@yahoo.com';
        $model->password = 'sa@123';
        
        //test 3: test code on Models:LoginForm:authenticate
        // check authentication, valid password, activated/verified
        $model->authenticate('password',$model->password);
        
        // show validation errors
        if($model->hasErrors()){
            print_r($model->getErrors());
        }
        
        // if the username and password is expected to pass,
        // valid username, password and verified,
        // change the second argument to false
        // otherwise change it to true, true mean the account is invalid or not verified 
        $this->assertEquals($model->hasErrors(),false);
        
    }
    
    public function testLoginFormLogin(){
        
        $model=new LoginForm;
        
        $model->username = 'kramx7@yahoo.com';
        $model->password = 'sa@123';
        
        // test 4: test code on  Models:LoginForm:login
        // authenticate user before logging the user to the system
        // true -> user is expected to successfully login
        // false -> user is expected not to successfully login 
        $this->assertEquals($model->login(),true);
        
    }
    
    
}

?>
