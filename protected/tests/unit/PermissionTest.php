<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */
class PermissionTest extends CDbTestCase {

    //put your code here

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds'=>'AuthItemChild',
        'authitems'=>'AuthItem',
    );


    public function testUserPermission() {
	    
	    $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;
        
        $creator_id = Yii::app()->user->id;
        
        $model = new User;
        
        // check authorization
        $params = array('user' => $model);
        $auth = User::model()->isAuthorized('ManageUser', $params);
        
        $this->assertTrue($auth);

        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer','AssociateAllCustomer');
        
        // set the user's assigned clients
        $model->clients = $clients;
        
        // get the login user's assigned facilites on user_task_facility table
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'AssociateAllFacility');
        // check if user has the permission to associate all facility assigned
        if (User::model()->isAuthorized('AssociateAllFacility')) {
            $model->facilities[0] = 'Associate All Facility';
        }
       
        $this->assertTrue(count($model->facilities)> 0);
       
        if (count($model->facilities) > 0) {
            // set first facility as default facility
            reset($model->facilities);
            $model->facility = key($model->facilities);
        }else{
            $model->facility = 0;
        }
        
        
        $this->assertTrue($model->facility != 0);
        
        // set users for facility and search filter
        $model->searchUsers();
        
        $this->assertTrue(count($model->users) > 0);
        
        // set users roles and inherited roles
        $model->setRoles($creator_id);
        
        $this->assertTrue(count($model->roles)>0);
        
        // set permissions
        $model->setInheritedPermissions($creator_id);
        
        $this->assertTrue(count($model->permissions)>0);
        
        $child_items = $model->getAuthItemChildren('PortalLoginPermission');
        
        $this->assertTrue(count($child_items) >0 ) ;
        $auth = Yii::app()->authManager;
        // get previous role asssingments
        $previous_assignments = $auth->getAuthAssignments($creator_id);

        foreach($previous_assignments as $row) {
            $auth->revoke($row->itemname, $creator_id);
        }
    }

    public function testUserTask()
    {
        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "task";
        $cmd->from('user_task_facility');
        $tasks = $cmd->query();

        if (!empty($tasks)) {
            foreach($tasks as $row){
                var_dump($row);
                /*    
                $cmd2 = Yii::app()->db->createCommand();
                $cmd2->select = "name";
                $cmd2->from('AuthItem');
                $cmd2->where = "name = '".$row['task']."'";
                $auth = $cmd2->query();

                if(empty($auth)){
                    echo '*'.$row['task'].'->';
                }else{
                    foreach($auth as $r){
                        echo $r['name'].'->';
                    }
                }*/

            }
        }
    }

}

?>
