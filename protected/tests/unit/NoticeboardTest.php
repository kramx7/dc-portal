<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */

//Yii::import('application.controllers.NoticeboardController');
//Yii::import('application.models.User');

class NoticeboardTest extends CTestCase{
    //put your code here
    
    public function testCreate(){
        
        // insert a notice in new status
        $notice =new Noticeboard;

        $notice->setAttributes(array(
            'facility'=>5,// 5 -> malaga, 8 -> noble park
            'priority'=>1,
            'publish_from'=>date('Y-m-d'),
            'publish_to'=>date('Y-m-d', strtotime('+1 month')),
            'sticky'=>'me',
            'title'=>'title '.time(),
            'short_message'=>'short message',
            'full_message'=>'full message',
            'submitted_by'=>1
        ),false);

        $this->assertTrue($notice->save(false));

        // verify the notice is in draft status
        $notice=Noticeboard::model()->findByPk($notice->id);
        $this->assertTrue($notice instanceof Noticeboard);
        $this->assertEquals(Noticeboard::STATUS_DRAFT,$notice->status);

        // call published() and verify the notice is in published status
        // in case the user click the save and published button
        $notice->published();
        // test if status is changed
        $this->assertEquals(Noticeboard::STATUS_PUBLISHED,$notice->status);
        // requery from database and check again the status if it is saved
        $notice=Noticeboard::model()->findByPk($notice->id);
        $this->assertEquals(Noticeboard::STATUS_PUBLISHED,$notice->status);
    }
    
    public function testUpdate(){
        
        //specify one notice, a draft notice to update
        $id = 2;
        // verify the notice is in draft status
        $notice=Noticeboard::model()->findByPk($id);
        $this->assertTrue($notice instanceof Noticeboard);
        
        //set new info for update
        $notice->setAttributes(array(
            'facility'=>5,// 5 -> malaga, 8 -> noble park
            'priority'=>1,
            'publish_from'=>date('Y-m-d'),
            'publish_to'=>date('Y-m-d', strtotime('+1 month')),
            'sticky'=>'me',
            'title'=>'title '.time(),
            'short_message'=>'short message - update',
            'full_message'=>'full message -update'
        ),false);
        
        //save changes
        $this->assertTrue($notice->save(false));
        
        // call published() and verify the notice is in published status
        // in case the user click the save and published button
        $notice->published();
        // test if status is changed
        $this->assertEquals(Noticeboard::STATUS_PUBLISHED,$notice->status);
        // requery from database and check again the status if it is saved
        $notice=Noticeboard::model()->findByPk($notice->id);
        $this->assertEquals(Noticeboard::STATUS_PUBLISHED,$notice->status);
        
    }
            
            
}

?>
