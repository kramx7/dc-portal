<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */
class RoleTest extends CDbTestCase {

    //put your code here

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds'=>'AuthItemChild',
        'authitems'=>'AuthItem',
    );


    public function testCreateRole() {
	    
        // test scenario: create new role, assigne permissions to role
        // 
	    $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;
        
        $creator_id = Yii::app()->user->id;

        $params = array('user' => $user);
        User::model()->isAuthorized('ManageRole',$params);

        // get user primary role
        $primary_role = $user->getPrimaryRole($user->id);

        $this->assertTrue($primary_role != false);

        $model = new AuthItem;

        // set list of per permissions base on user's role
        $model->setPermissions($primary_role);

        $this->assertTrue(count($model->allowed_permissions) > 0);
        $this->assertTrue(count($model->permission_options) > 0);
            
        // set new role
        $model->name = 'New Role';
        $model->description = 'Custom Role - '.$model->name;
        $model->type = 2;

        // assign permissions   
        $permission_type = 'PortalLoginPermission';
        $new_permission = array('PortalLoginStatus');
        $model->saveTempPermission($permission_type, $new_permission);
        $this->assertTrue(count($model->temp_permissions['PortalLoginPermission']) > 0);

        $permission_type = 'CustomerPermission';
        $new_permission = array('ManageCustomer','ReadCustomer');
        $model->saveTempPermission($permission_type, $new_permission);
        $this->assertTrue(count($model->temp_permissions['CustomerPermission']) > 0);

        $permission_type = 'UserPermission';
        $new_permission = array('ManageUser','ReadUser');
        $model->saveTempPermission($permission_type, $new_permission);
        $this->assertTrue(count($model->temp_permissions['UserPermission']) > 0);
        
        $permission_type = 'ProximityCardPermission';
        $new_permission = array('ApproveProximityCard','DelegateProximityCard','SubmitProximityCard');
        $model->saveTempPermission($permission_type, $new_permission);
        $this->assertTrue(count($model->temp_permissions['ProximityCardPermission']) > 0);

        $model->setTemporaryPermissions();

        $this->assertTrue(strlen($model->portallogin_permission) >0);
        $this->assertTrue(strlen($model->customers_permission) >0);
        $this->assertTrue(strlen($model->users_permission) >0);
        $this->assertTrue(strlen($model->proximitycard_permission) >0);

        // save new role
        $model->save();

        // save permission to database, authchilditem table
        $model->assignTemporaryPermissions();

        // check permissions are save, set the new role
        $model->setPermissions($model->name);

        $this->assertTrue(count($model->allowed_permissions) > 0);

        $this->assertTrue(count($model->permission_options) > 0);
        
        // check if assign main permission type/group is present
        $this->assertArrayHasKey("PortalLoginPermission", $model->allowed_permissions);
        $this->assertArrayHasKey("CustomerPermission", $model->allowed_permissions);
        $this->assertArrayHasKey("UserPermission", $model->allowed_permissions);
        $this->assertArrayHasKey("ProximityCardPermission", $model->allowed_permissions);

        $this->assertArrayHasKey('PortalLoginStatus', $model->permission_options['PortalLoginPermission']);
        $this->assertArrayHasKey('ManageCustomer', $model->permission_options['CustomerPermission']);
        $this->assertArrayHasKey('ManageUser', $model->permission_options['UserPermission']);
        $this->assertArrayHasKey('ApproveProximityCard', $model->permission_options['ProximityCardPermission']);

    }

}

?>
