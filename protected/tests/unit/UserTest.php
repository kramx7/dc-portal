<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */

//Yii::import('application.controllers.NoticeboardController');
//Yii::import('application.models.User');

class UserTest extends CTestCase{
    //put your code here
    
    public function testCreate(){
        
        // insert a notice in new status
        $user =new User;

        $user->setAttributes(array(
            'emailaddress'=>'kramx7a@yahoo.com',
            'user_role'=>'SuperAdmin',
            'client'=>null,
            'facility'=>null,
            'valid_from'=>date('Y-m-d'),
            'valid_to'=>date('Y-m-d', strtotime('+1 month')),
        ),false);

        $this->assertTrue($user->save(false)); // by pass validation

        // verify the notice is in new status
        $user=User::model()->findByPk($user->id);
        $this->assertTrue($user instanceof User);
        $this->assertEquals(User::STATUS_NEW,$user->status);
        
        // check verification code is set, not empty
        $this->assertEquals(false,  empty($user->verification_code));

    }
    
    public function testVerify(){
        
        //specify one notice, a draft notice to update
        $id = 2;
        // verify the notice is in draft status
        $user=User::model()->findByPk($id);
        $this->assertTrue($user instanceof User);
        
        //set new info for update
        $user->setAttributes(array(
            'emailaddress'=>'kramx7a@yahoo.com',
            'user_role'=>'SuperAdmin',
            'client'=>null,
            'facility'=>null,
            'valid_from'=>date('Y-m-d'),
            'valid_to'=>date('Y-m-d', strtotime('+1 month')),
        ),false);
        
        $user->status = User::STATUS_ACTIVE;
        
        //save changes
        $this->assertTrue($user->save(false));
        
        // test if status is changed
        $this->assertEquals(User::STATUS_ACTIVE,$user->status);
        // requery from database and check again the status if it is saved
        $user=User::model()->findByPk($user->id);
        $this->assertEquals(User::STATUS_ACTIVE,$user->status);
        
    }
            
            
}

?>
