<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */
class ReportTest extends CDbTestCase {

    //put your code here

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds' => 'AuthItemChild',
        'authitems' => 'AuthItem',
    );

    public function testPowerGraph() {

        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // get the id of current login user
        $id = Yii::app()->user->id;

        // set graph form 
        $model = new GraphForm;

        // test for kw, total    
        $code = 'kW';

        // set type
        $model->type = $code;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReports','ReadAllReports');

        $this->assertTrue(count($model->facilities) > 0);

        reset($model->facilities);
        $model->facility_id = key($model->facilities);

        // set default facility
        $model->default_facility = $model->facility_id;


        // get power meters
        if ($model->facility_id == 0) {
            // get all meters to all the facilities
            //$meters = PowerStatistics::model()->getMetersByClientId($client_id); 
            $meters = PowerStatistics::model()->getMetersByAllFacilitiesAndClients($model->facilities, $model->clients);
        } else {
            // get all meters to for specific facility
            $meters = PowerStatistics::model()->getMetersByFacilityIdAndClients($model->facility_id, $model->clients);
        }

        $this->assertTrue(count($meters) > 0);

        $model->meters = $meters;

        // set dates
        $model->from_date = '16/04/2013';
        $model->to_date = '17/04/2013';

        // set dates to timestamp for db comparison
        $model->setDatesToTimestamp();

        //check dates are properly converted to timestamp;
        $this->assertTrue($model->from_date > strtotime('2013-01-01'));
        $this->assertTrue($model->to_date > strtotime('2013-01-01'));


        // get power statistics
        $power_statistics = array();

        if ($model->target_id != 0) {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            }
        } else {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            }
        }

        $this->assertTrue(count($power_statistics) > 0);

        //var_dump($power_statistics);
        // set xdata and ydata
        $graph = new GraphComponent;

        $graph->setData($power_statistics, $model->from_date, $model->to_date, $model->type, $model->target_id);

        // set title
        $meter_name = PowerStatistics::model()->getMeterName($model->target_id);

        $graph->setTitle($meter_name);

        $this->assertTrue($graph->title != '');

        // get client id and name
        $client_id = ClientFacility::model()->getClientIdByFacilityId($model->facility_id);
        $client = Client::model()->getClientById($client_id);

        // get facility name
        $facility_name = $model->facilities[$model->facility_id];

        // set subtitle
        $graph->setSubTitle($client->client_name, $facility_name);

        $this->assertTrue($graph->subtitle != '');

        // plot/draw graph
        $graph->plotGraph();

        //echo '<br>',$graph->directory;

        $this->assertTrue($graph->directory != '');


        // test for kwh , total
        $code = 'kWh';
        // set type
        $model->type = $code;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReports','ReadAllReports');

        $this->assertTrue(count($model->facilities) > 0);

        reset($model->facilities);
        $model->facility_id = key($model->facilities);

        // set default facility
        $model->default_facility = $model->facility_id;


        // get power meters
        if ($model->facility_id == 0) {
            // get all meters to all the facilities
            //$meters = PowerStatistics::model()->getMetersByClientId($client_id); 
            $meters = PowerStatistics::model()->getMetersByAllFacilitiesAndClients($model->facilities, $model->clients);
        } else {
            // get all meters to for specific facility
            $meters = PowerStatistics::model()->getMetersByFacilityIdAndClients($model->facility_id, $model->clients);
        }

        $this->assertTrue(count($meters) > 0);

        $model->meters = $meters;

        // set dates
        $model->from_date = '16/03/2013';
        $model->to_date = '17/04/2013';

        // set dates to timestamp for db comparison
        $model->setDatesToTimestamp();

        //check dates are properly converted to timestamp;
        $this->assertTrue($model->from_date > strtotime('2013-01-01'));
        $this->assertTrue($model->to_date > strtotime('2013-01-01'));


        // get power statistics
        $power_statistics = array();

        if ($model->target_id != 0) {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndTargetIdAndClients($model->facility_id, $model->from_date, $model->to_date, $model->target_id, $model->clients);
            }
        } else {
            if ($model->type == 'kW') {
                $power_statistics = PowerStatistics::model()->getKwByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            } else {
                $power_statistics = PowerStatisticsSummary::model()->getKwhByFacilityIdAndDatesAndClients($model->facility_id, $model->from_date, $model->to_date, $model->clients);
            }
        }

        $this->assertTrue(count($power_statistics) > 0);

        //var_dump($power_statistics);
        // set xdata and ydata
        $graph = new GraphComponent;

        $graph->setData($power_statistics, $model->from_date, $model->to_date, $model->type, $model->target_id);

        // set title
        $meter_name = PowerStatistics::model()->getMeterName($model->target_id);

        $graph->setTitle($meter_name);

        $this->assertTrue($graph->title != '');

        // get client id and name
        $client_id = ClientFacility::model()->getClientIdByFacilityId($model->facility_id);
        $client = Client::model()->getClientById($client_id);

        // get facility name
        $facility_name = $model->facilities[$model->facility_id];

        // set subtitle
        $graph->setSubTitle($client->client_name, $facility_name);

        $this->assertTrue($graph->subtitle != '');

        // plot/draw graph
        $graph->plotGraph();

        //echo '<br>',$graph->directory;

        $this->assertTrue($graph->directory != '');
    }

    public function TestEnvironmentGraph() {

        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // get the id of current login user
        $id = Yii::app()->user->id;

        // set graph form 
        $model = new GraphForm;

        // test for temperature type or graph
        $code = 'Temperature';
        // set type
        $model->type = $code;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        //  get facilities
        $model->facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($id, 'ReadReports','ReadAllReports');

        $this->assertTrue(count($model->facilities) > 0);

        reset($model->facilities);
        $model->facility_id = key($model->facilities);

        // set default facility
        $model->default_facility = $model->facility_id;


        // get sensors
        if ($model->facility_id == 0) {
            // get all meters to all the facilities 
            $sensors = Sensor::model()->getSensorsByAllFacilitiesAndClients($model->facilities, $model->clients);
        } else {
            // get all meters to for specific facility
            $sensors = Sensor::model()->getSensorsByFacilityIdAndClients($model->facility_id, $model->clients);
        }

        $this->assertTrue(count($sensors) > 0);

        $model->sensors = $sensors;


        // set choosen sensor, first sensor
        reset($model->sensors);
        $model->sensor = key($model->sensors);

        // set dates
        $model->from_date = '16/04/2013';
        $model->to_date = '17/04/2013';

        // set dates to timestamp for db comparison
        $model->setDatesToTimestamp();

        //check dates are properly converted to timestamp;
        $this->assertTrue($model->from_date > strtotime('2013-01-01'));
        $this->assertTrue($model->to_date > strtotime('2013-01-01'));


        // get power statistics
        $statistics = array();

        if ($model->sensor != 0) {
            if ($model->type == 'Temperature') {
                $statistics = EnvironmentStatistics::model()->getTemperatureBySensorIdAndDatesAndClients($model->sensor, $model->from_date, $model->to_date, $model->clients);
            } else {
                $statistics = EnvironmentStatistics::model()->getHumidityBySensorIdAndDatesAndClients($model->sensor, $model->from_date, $model->to_date, $model->clients);
            }
        }

        $this->assertTrue(count($statistics) > 0);

        //var_dump($power_statistics);
        // set xdata and ydata
        $graph = new GraphComponent;

        $graph->setData($statistics, $model->from_date, $model->to_date, $model->type, $model->target_id);

        // set title
        $sensor_name = $model->sensors[$model->sensor];
        $graph->setTitle($sensor_name);

        $this->assertTrue($graph->title != '');

        // get client id and name
        $client_id = ClientFacility::model()->getClientIdByFacilityId($model->facility_id);
        $client = Client::model()->getClientById($client_id);

        // get facility name
        $facility_name = $model->facilities[$model->facility_id];

        // set subtitle
        $graph->setSubTitle($client->client_name, $facility_name);

        $this->assertTrue($graph->subtitle != '');

        // plot/draw graph
        $graph->plotGraph();

        //echo '<br>',$graph->directory;

        $this->assertTrue($graph->directory != '');
    }

    public function testNger() {
    
        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // get the id of current login user
        $id = Yii::app()->user->id;
         // set graph form 
        $model = new GraphForm;
        
        $facility_id = 5;
        $fiscal_year = 2012;

        // get user clients
        $model->clients = User::model()->getUserClients($id);

        $years = PowerStatistics::model()->getNGERYearByFacilityIdAndClients($facility_id, $model->clients);
        $this->assertTrue(count($years) > 0);

        $today = getdate();
        $current_year = $today['year']; // current year
        $year = $current_year - 1;

        $str_sdate = "1 July " . $year;
        $start = strtotime($str_sdate);

        if ($year == $current_year) {
            $end = $today[0];
        } else {
            $end = $start + 365 * 24 * 60 * 60;
        }

        //get kw for facility
        $powers = PowerStatisticsSummary::model()->getPowerByFacilityIdAndDatesAndClients($facility_id, $start, $end, $model->clients);
        $this->assertTrue(count($powers) > 0);

        $pues = FacilityPue::model()->getPueByPowerDatesAndFacilityId($powers, $facility_id);
        $this->assertTrue(count($pues) > 0);

        $carbons = Facility::model()->getCarbonByPowerDatesAndFacilityId($powers, $facility_id);
        $this->assertTrue(count($carbons) > 0);
        
        $model->computeNGER($powers, $pues, $carbons);
        
        $this->assertTrue($model->sum_kwh > 0 );
        
        $this->assertTrue($model->average_pue > 0 );
        
        $this->assertTrue($model->sum_total_kwh > 0 );
        
        $this->assertTrue($model->sum_carbon > 0 );
        
    }

}

?>
