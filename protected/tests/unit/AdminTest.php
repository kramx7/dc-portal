<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticeboardTest
 *
 * @author kramx7
 */
class AdminTest extends CDbTestCase {

    //put your code here

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds'=>'AuthItemChild',
        'authitems'=>'AuthItem',
    );


    public function testProfile() {

        // get the first AR instance from fixture
        $user = $this->users('row1');

        // check that there is a record found with that id
        $this->assertTrue($user instanceof User);

        // check email address is correct for the choosen account
        $this->assertEquals('kramx7@yahoo.com', $user->emailaddress);

        // check valid to date is set to never for this account, 
        // it is expected to never, this could be comment out for other user
        $this->assertEquals('never', $user->valid_to);

        // enable scenario to encrypt password
        $user->scenario = 'profile-pass';

        //set new info for update
        $user->setAttributes(array(
            'firstname' => 'Jan Mark1',
            'lastname' => 'Salarda2',
            'alternateemailaddress' => 'kramx7z@gmail.com3',
            'officephone' => '12345674',
            'officefax' => '12345675',
            'mobilephone' => '12345676',
            'password' => $user->encrypt('sa@12345'),
                ), false);


        //save changes
        // do not include validation, validation will be checked on functional test
        $this->assertTrue($user->save(false));

        // requery from database and check again if the info are saved
        $user1 = User::model()->findByPk($user->id);

        // change the values to assert to check to equal the new update info above
        // check if first name is save
        $this->assertEquals('Jan Mark1', $user1->firstname);

        // check if last name is save
        $this->assertEquals('Salarda2', $user1->lastname);

        // check if email address is save
        $this->assertEquals('kramx7z@gmail.com3', $user1->alternateemailaddress);

        // check if office number is save
        $this->assertEquals('12345674', $user1->officephone);

        // check if faxx number is save
        $this->assertEquals('12345675', $user1->officefax);

        // check if mobile number is save
        $this->assertEquals('12345676', $user1->mobilephone);

        // check if password is change
        $this->assertEquals($user1->encrypt('sa@12345'), $user1->password);

        $this->assertEquals(128, strlen($user1->password));
    }

    public function testBulletinBoard() {

        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        //get user assigned facilities, set upon user login
        $assigned_facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($user->id, 'AssociateFacility','ReadFacility','AssociateAllFacility');

        //get user assigned roles, set upon user login
        $assigned_roles = AuthAssignment::model()->getRolesInArray($user->id);


        $requests = new Noticeboard('search');
        $requests->unsetAttributes();  // clear any default values
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $requests->_search_user_role = $assigned_roles;
        $requests->_search_user_facility = $assigned_facilities;


        $notices = new Noticeboard('search');
        $notices->unsetAttributes();  // clear any default values
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $notices->_search_user_role = $assigned_roles;
        $notices->_search_user_facility = $assigned_facilities;

        foreach ($requests->search() as $row) {
            if (!array_key_exists('SuperAdmin', $assigned_roles)) {
                $this->assertTrue(array_key_exists($row->facility, $assigned_facilities));
            }
        }

        foreach ($notices->search() as $row) {
            if (!array_key_exists('SuperAdmin', $assigned_roles)) {
                $this->assertTrue(array_key_exists($row->facility, $assigned_facilities));
            }
        }
    }

    public function testCreateUser() {

        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;

        // instantiate a new user model
        $model = new User;

        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility', 'ReadFacility','AssociateAllFacility');

        // test that there are facilities to associate for a superadmin user 
        $this->assertTrue(count($facilities) > 0);

        // set the user's assigned facilities as the list of facility to assigned
        $model->_facility_options = $facilities;

        // get the login user's assigned facilites on create user task
        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer', false);

        // test that there are customers to associate for a superadmin user
        $this->assertTrue(count($clients) > 0);

        // set the user's assigned facilities as the list of facility to assigned
        $model->_client_options = $clients;

        // set assign options
        $model->_setAssignOptions(false);
        // test that there are options to assign like role and operation
        $this->assertTrue(count($model->_assign_options) > 0);

        // set un assigned roles
        $model->_assign_op = 'Roles';
        $model->_setUnassignedItems($user->id);

        // test that there are at least 3 items to assigned
        $this->assertTrue(count($model->_unassigned_items) >= 3);

        // save assignments to session
        $roles_to_assigned = array_keys(array_slice($model->_unassigned_items, 0, 3)); // assign first 3 roles 
        $model->_assigned_items = $roles_to_assigned;


        $model->_prev_assign_op = 'Roles';
        $model->_saveAssignments(); // save to session
        // set un assigned operations
        $model->_assign_op = 'Operations';
        $model->_setUnassignedItems($creator_id);

        // test that there are at least 3 items to assigned
        $this->assertTrue(count($model->_unassigned_items) >= 3);

        // save assignments to session
        $roles_to_assigned = array_keys(array_slice($model->_unassigned_items, 0, 3)); // assign first 3 operations 
        $model->_assigned_items = $roles_to_assigned;
        $model->_prev_assign_op = 'Operations';
        $model->_saveAssignments(); // save to session
        //set assigned items
        $model->_assign_op = 'Operations';
        $model->_prev_assign_op = '';
        $model->_setAssignedItems();
        // test that there are assigned operation items
        $this->assertTrue(count($model->_assigned_items) > 0);

        //set assigned items
        $model->_assign_op = 'Roles';
        $model->_prev_assign_op = '';
        $model->_setAssignedItems();
        // test that there are assigned roles items
        $this->assertTrue(count($model->_assigned_items) > 0);

        // assign a scenario, useful in user model validation and attribute mapping
        $model->scenario = 'admin-create';

        //set new info for update
        $model->setAttributes(array(
            'emailaddress' => 'kramx71@yahoo.com',
            'firstname' => 'Jan Mark',
            'lastname' => 'Salarda',
            'alternateemailaddress' => 'kramx7z@gmail.com',
            'officephone' => '1234567',
            'officefax' => '1234567',
            'mobilephone' => '1234567',
            '_facility' => 5,
            '_client' => 215,
                ), false);

        $this->assertTrue($model->save(false));

        $assignments = Yii::app()->session->itemAt('user_assignments');

        // save roles and operations to the database
        $this->assertTrue($model->saveAssignments($model->id, $assignments));
    }

    public function testManageUser() {
        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;

        // instantiate a new user model
        $model = new UserUpdateForm;

        // get the login user's assigned facilites on user_task_facility table
        $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($creator_id, 'AssociateFacility','ReadFacility','AssociateAllFacility');
        // test that there are facilities to associate for a superadmin user 
        $this->assertTrue(count($facilities) > 0);

        // set the user's assigned facilities as the list of facility to assigned
        $model->_facility_options = $facilities;


        // get the login user's assigned facilites on create user task
        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($creator_id, 'AssociateCustomer', false);
        // test that there are facilities to associate for a superadmin user 
        $this->assertTrue(count($clients) > 0);

        // set the user's assigned facilities as the list of facility to assigned
        $model->_client_options = $clients;

        // set assign options
        $model->assignment_list = User::model()->_setAssignOptions();

        $this->assertTrue(count($model->assignment_list) > 0);
        
        // search user
        $model->search_user = 'kramx72';
        $model->searchUsers();
        // test that there is one user return
        $this->assertTrue(count($model->users) == 1);
        
        // test second user is existing
        $user2 = User::model()->findByPk(2);
        
        $this->assertTrue($user2 instanceof User);
        $model->selected_user = 2;
        $model->assignment_operation = 'Roles';
        
        $model->setUnassignedItems($creator_id);
        
        // test there are unassigned items
        $this->assertTrue(count($model->unassigned_items) > 3);
        
        $model->setAssignedItems();
        
        $roles_to_assigned = array_keys(array_slice($model->unassigned_items, 0, 3)); // assign first 3 roles 
        $model->assigned_items = $roles_to_assigned;
        
        $model->scenario = 'update';
        $model->updateUser();
        
        $model->setAssignedItems();
        
        // check if all items are saved
        foreach($roles_to_assigned as $val){
            $this->assertTrue(array_key_exists($val, $model->assigned_items));
        }
        
        
    }

    public function testCreateRole() {
        
        // get the first AR instance from fixture
        $user = User::model()->findByPk(1);

        Yii::app()->user->id = $user->id;

        // set the creator id to the login user
        $creator_id = Yii::app()->user->id;
        
        // get user assigned and inherited roles
        $user_previous_inherited_roles = AuthAssignment::model()->getInheritedRoles($creator_id);
        
        // initilize model to use for roles, set admin-create scenario for validation rules
        $role = new AuthItem('admin-create');

        //set new info for new record
        $role->setAttributes(array(
            'name' => 'Role1',
            'type' => 2,
            'description' => 'Role1',
                ), false);


        // set available items
        $role->_assign_type = 'role';
        $role->_prev_assign_type = 'role';
        // check available items for role
        $role->_setAvailableItems();
        // test that there should be available items to choose
        $this->assertTrue(count($role->_available_items) > 3); // make sure to have at least 3 items to assign below
        //set inherited items, roles
        $role->_prev_assign_type = 'role';
        // assign the first 3 role, only get their array key, key is the actual name, value is description
        $roles_to_assigned = array_keys(array_slice($role->_available_items, 0, 3)); // assign first 3 roles 
        $role->_inherited_items = $roles_to_assigned;

        // save inherited items, save in session only, temporary while role is not save
        $role->_saveInheritedItems();

        // get inherited items
        $inherited_items = Yii::app()->session->itemAt('inherited_items');

        // get inherited roles
        $inherited_roles = $inherited_items['role'];


        // test that all roles to assigned is save on the session
        foreach ($roles_to_assigned as $val) {
            $this->assertTrue(in_array($val, $inherited_roles));
        }

        // set available items
        $role->_assign_type = 'task';
        $role->_prev_assign_type = 'task';
        // check available items for task
        $role->_setAvailableItems();
        // test that there should be available items to choose
        $this->assertTrue(count($role->_available_items) > 3); // make sure to have at least 3 items to assign below
        //set inherited items, task
        $role->_prev_assign_type = 'task';
        // assign the first 3 task, only get their array key, key is the actual name, value is description
        $task_to_assigned = array_keys(array_slice($role->_available_items, 0, 3));
        $role->_inherited_items = $task_to_assigned;

        // save inherited items, save in session only, temporary while role is not save
        $role->_saveInheritedItems();

        // get inherited items
        $inherited_items = Yii::app()->session->itemAt('inherited_items');

        // get inherited trask
        $inherited_task = $inherited_items['task'];

        // test that all roles to assigned is save on the session
        foreach ($task_to_assigned as $val) {
            $this->assertTrue(in_array($val, $inherited_task));
        }

        // set inherited items when user clicks the asssign op
        $role->_assign_type = 'role';
        $role->_setInheritedItems();


        // test that all set inherited roles is in the current available items
        foreach ($inherited_roles as $val) {
            $this->assertTrue(in_array($val, $role->_inherited_items));
        }

        $role->scenario = 'admin-create';
        // save new role, without validation, validation will be checked on functional test
        $this->assertTrue($role->save(false));
        
        // test that the previous inherited roles does not equal to new role
        $user_new_inherited_roles = AuthAssignment::model()->getInheritedRoles($creator_id);
        
        $this->assertTrue(count($user_previous_inherited_roles) != count($user_new_inherited_roles));
        
        // test that the new role is now inherited by the user
        $this->assertTrue(array_key_exists($role->name, $user_new_inherited_roles));
       
        
    }

    public function testManageRole() {
        
        $this->testCreateRole();
        
        // create authitem model, with admin-update scenario, scenario is useful to determine validation rules on the model
        $role = new AuthItem('admin-update');

        // set roles list
        $role->setRolesOptions();

        // test that there are roles to choose for update
        $this->assertTrue(count($role->_roles_options) > 0);

        // set role to select first
        $role->_selected_role = 'Role1';

        // set inherited items, from authitemchild inherited items
        $role->_assign_type = 'role';
        $role->setInheritedItems();

        // set available items
        $role->setAvailableItems();

        // test that there are roles to choose for a superadmin user, newly created roles, role1
        $this->assertTrue(count($role->_available_items) > 0);

        $role->_inherited_items = array_keys($role->_inherited_items);

        // if user click the update button,
        // and not the assign operation add/remove
        $role->saveUpdatedAssignments();

        // test that the changes are save
        $role = AuthItem::model()->findbyAttributes(array('name' => $role->_selected_role));

        $this->assertTrue($role instanceof AuthItem);

    }

}

?>
