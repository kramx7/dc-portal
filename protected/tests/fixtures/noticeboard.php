<?php

return array(
    'row1' => array(
        'facility' => 5,
        'title' => 'Post1',
        'short_message' => 'Short Message 1',
        'full_message' => 'Full Message 1',
        'status' => 'swNoticeboard/Draft',
        'submitted_by' => 1,
        ''
    ),
    'row2' => array(
        'facility' => 8,
        'title' => 'Post2',
        'short_message' => 'Short Message 2',
        'full_message' => 'Full Message 2',
        'status' => 'swNoticeboard/Published',
        'submitted_by' => 1,
    ),
);
?>
