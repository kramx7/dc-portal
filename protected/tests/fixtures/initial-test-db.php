-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2013 at 01:24 AM
-- Server version: 5.5.25-log
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dcportal_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `AuthAssignment`
--

CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AuthAssignment`
--


-- --------------------------------------------------------

--
-- Table structure for table `AuthItem`
--

CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  `temp` int(11) DEFAULT NULL,
  `order` int(3) DEFAULT 0,
  `facility` int(3) DEFAULT 0,
  `client` int(3) DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AuthItem`
--

INSERT INTO `AuthItem` (`name`, `type`, `description`, `bizrule`, `data`, `temp`) VALUES
('AccessFacility', 0, 'Access Facility', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AccessFacility");', 'N;', NULL),
('ApplicationAdmin', 2, 'Application Administrator', NULL, 'N;', NULL),
('AssignOperation', 0, 'Assign Operation', NULL, 'N;', NULL),
('AssignRole', 0, 'Assign Role', NULL, 'N;', NULL),
('AssociateAllCustomer', 0, 'Associate All Customer', NULL, 'N;', NULL),
('AssociateAllFacility', 0, 'Associate All Facility', NULL, 'N;', NULL),
('AssociateCustomer', 0, 'Associate Customer', NULL, 'N;', NULL),
('AssociateFacility', 0, 'Associate Facility', NULL, 'N;', NULL),
('AuthManagePost', 1, 'Manage Authorize Post', NULL, 'N;', NULL),
('AuthorizeDcAccessRequest', 0, 'Authorize DC Access Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AuthorizeDcAccessRequest");', 'N;', NULL),
('AuthorizeDcTelcoAccessRequest', 0, 'Authorize DC Telco Access Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AuthorizeDcTelcoAccessRequest");', 'N;', NULL),
('AuthorizeEmfRequest', 0, 'Authorize EMF Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AuthorizeEmfRequest");', 'N;', NULL),
('AuthorizeProximityRequest', 0, 'Authorize Proximity Card Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AuthorizeProximityRequest");', 'N;', NULL),
('AuthorizeRemoteHandsRequest', 0, 'Authorize Remote Hands Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("AuthorizeRemoteHandsRequest");', 'N;', NULL),
('AuthPost', 0, 'Authorize BB Post', 'return isset($params["post"]) && $params["post"]->isUserInFacility("AuthPost");', 'N;', NULL),
('CreateCustomer', 0, 'Create Customer', NULL, 'N;', NULL),
('CreateLibraryContent', 0, 'Create Library Content', 'return isset($params["content"]) && $params["content"]->isUserInFacility("CreateLibraryContent") && $params["content"]->isUserInClient("CreateLibraryContent");', 'N;', NULL),
('CreateNewFacility', 0, 'Create New Facility', NULL, 'N;', NULL),
('CreatePost', 0, 'Create BB Post', 'return isset($params["post"]) && $params["post"]->isUserInFacility("CreatePost");', 'N;', NULL),
('CreateReport', 0, 'Create Report', 'return isset($params["report"]) && $params["report"]->isUserInFacility("CreateReport") && $params["report"]->isUserInClient("CreateReport");', 'N;', NULL),
('CreateRole', 0, 'Create Role', NULL, 'N;', NULL),
('CreateUser', 0, 'Create User', NULL, 'N;', NULL),
('CustomerAdmin', 2, 'Customer Administrator', NULL, 'N;', NULL),
('CustomerUser', 2, 'Customer User', NULL, 'N;', NULL),
('DelegateRole', 0, 'Delegate Role', '', 'N;', NULL),
('DeleteCustomer', 0, 'Delete Customer', NULL, 'N;', NULL),
('DeleteFacility', 0, 'Delete Facility', 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("DeleteFacility");', 'N;', NULL),
('DeletePost', 0, 'Delete BB Post', 'return isset($params["post"]) && $params["post"]->isUserInFacility("DeletePost");', 'N;', NULL),
('DeleteReport', 0, 'Delete Report', 'return isset($params["report"]) && $params["report"]->isUserInFacility("DeleteReport") && $params["report"]->isUserInClient("DeleteReport");', 'N;', NULL),
('DeleteRole', 0, 'Delete Role', NULL, 'N;', NULL),
('DeleteUser', 0, 'Delete User', NULL, 'N;', NULL),
('FacilityAdmin', 2, 'Facility Administrator', NULL, 'N;', NULL),
('FujitsoUser', 2, 'Fujitso User', NULL, 'N;', NULL),
('ImplementationStaff', 2, 'Implementation Staff', NULL, 'N;', NULL),
('ManageContent', 1, 'Manage Content', NULL, 'N;', NULL),
('ManageCustomer', 1, 'Manage Customer', NULL, 'N;', NULL),
('ManageFacility', 1, 'Manage Facility', NULL, 'N;', NULL),
('ManagePost', 1, 'Manage Post', NULL, 'N;', NULL),
('ManageReport', 1, 'Manage Reports', NULL, 'N;', NULL),
('ManageRequest', 1, 'Manage Request', NULL, 'N;', NULL),
('ManageRole', 1, 'Manage Role', NULL, 'N;', NULL),
('ManageSystemConfig', 1, 'Manage System Configuration', NULL, 'N;', NULL),
('ManageUser', 1, 'Manage User', NULL, 'N;', NULL),
('ModifySystemConfiguration', 0, 'Modify System Configuration', NULL, 'N;', NULL),
('OperationStaff', 2, 'Operation Staff', NULL, 'N;', NULL),
('ReadCustomer', 0, 'Read Customer', NULL, 'N;', NULL),
('ReadFacility', 0, 'Read Facility', 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("ReadFacility");', 'N;', NULL),
('ReadLibraryContent', 0, 'Read Library Content', 'return isset($params["content"]) && $params["content"]->isUserInFacility("ReadLibraryContent") && $params["content"]->isUserInClient("ReadLibraryContent");', 'N;', NULL),
('ReadPost', 0, 'Read BB Post', 'return isset($params["post"]) && $params["post"]->isUserInFacility("ReadPost");', 'N;', NULL),
('ReadReports', 0, 'Read Reports', 'return isset($params["report"]) && $params["report"]->isUserInFacility("ReadReports") && $params["report"]->isUserInClient("ReadReports");', 'N;', NULL),
('ReadRole', 0, 'Read Role', NULL, 'N;', NULL),
('ReadSystemConfiguration', 0, 'Read System Configuration', NULL, 'N;', NULL),
('ReadUser', 0, 'Read User', NULL, 'N;', NULL),
('SecurityStaff', 2, 'Security Staff', NULL, 'N;', NULL),
('SubmitDcAccessRequest', 0, 'Submit DC Access Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("SubmitDcAccessRequest");', 'N;', NULL),
('SubmitDcTelcoAccessRequest', 0, 'Submit DC Telco Access Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("SubmitDcTelcoAccessRequest");', 'N;', NULL),
('SubmitEmfRequest', 0, 'Submit EMF Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("SubmitEmfRequest");', 'N;', NULL),
('SubmitProximityRequest', 0, 'Submit Proximity Card Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("SubmitProximityRequest");', 'N;', NULL),
('SubmitRemoteHandsRequest', 0, 'Submit Remote Hands Request', 'return isset($params["proximity"]) && $params["proximity"]->isUserInFacility("SubmitRemoteHandsRequest");', 'N;', NULL),
('SuperAdmin', 2, 'Super Admin', NULL, 'N;', NULL),
('UpdateCustomer', 0, 'Update Customer', NULL, 'N;', NULL),
('UpdateFacility', 0, 'Update Facility', 'return isset($params["facility"]) && $params["facility"]->isUserInFacility("UpdateFacility");', 'N;', NULL),
('UpdatePost', 0, 'Update BB Post', 'return isset($params["post"]) && $params["post"]->isUserInFacility("UpdatePost");', 'N;', NULL),
('UpdateProfile', 0, 'Update Profile', NULL, 'N;', NULL),
('UpdateReport', 0, 'Update Report', 'return isset($params["report"]) && $params["report"]->isUserInFacility("UpdateReport") && $params["report"]->isUserInClient("UpdateReport");', 'N;', NULL),
('UpdateRole', 0, 'Update Role', NULL, 'N;', NULL),
('UpdateUser', 0, 'Update User', NULL, 'N;', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AuthItemChild`
--

CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AuthItemChild`
--

INSERT INTO `AuthItemChild` (`parent`, `child`) VALUES
('CustomerAdmin', 'AccessFacility'),
('ManageRequest', 'AccessFacility'),
('SuperAdmin', 'ApplicationAdmin'),
('FacilityAdmin', 'AssignOperation'),
('CustomerAdmin', 'AssignRole'),
('ApplicationAdmin', 'AssociateAllCustomer'),
('ApplicationAdmin', 'AssociateAllFacility'),
('ApplicationAdmin', 'AssociateCustomer'),
('ApplicationAdmin', 'AssociateFacility'),
('CustomerAdmin', 'AssociateFacility'),
('FacilityAdmin', 'AuthManagePost'),
('CustomerAdmin', 'AuthorizeDCAccessRequest'),
('ManageRequest', 'AuthorizeDcAccessRequest'),
('CustomerAdmin', 'AuthorizeDCTelcoAccessRequest'),
('ManageRequest', 'AuthorizeDcTelcoAccessRequest'),
('CustomerAdmin', 'AuthorizeEMFRequest'),
('ManageRequest', 'AuthorizeEmfRequest'),
('CustomerAdmin', 'AuthorizeProximityRequest'),
('ManageRequest', 'AuthorizeProximityRequest'),
('CustomerAdmin', 'AuthorizeRemoteHandsRequest'),
('ManageRequest', 'AuthorizeRemoteHandsRequest'),
('AuthManagePost', 'AuthPost'),
('ManageCustomer', 'CreateCustomer'),
('FacilityAdmin', 'CreateLibraryContent'),
('FujitsoUser', 'CreateLibraryContent'),
('ManageContent', 'CreateLibraryContent'),
('ManageFacility', 'CreateNewFacility'),
('AuthPost', 'CreatePost'),
('FujitsoUser', 'CreatePost'),
('ManagePost', 'CreatePost'),
('ManageReport', 'CreateReport'),
('ManageRole', 'CreateRole'),
('ManageUser', 'CreateUser'),
('ApplicationAdmin', 'CustomerAdmin'),
('FacilityAdmin', 'CustomerAdmin'),
('SuperAdmin', 'CustomerAdmin'),
('ApplicationAdmin', 'CustomerUser'),
('CustomerAdmin', 'CustomerUser'),
('FacilityAdmin', 'CustomerUser'),
('SuperAdmin', 'CustomerUser'),
('ApplicationAdmin', 'DelegateRole'),
('CustomerAdmin', 'DelegateRole'),
('FacilityAdmin', 'DelegateRole'),
('SuperAdmin', 'DelegateRole'),
('ManageCustomer', 'DeleteCustomer'),
('ManageFacility', 'DeleteFacility'),
('AuthPost', 'DeletePost'),
('ManagePost', 'DeletePost'),
('ManageReport', 'DeleteReport'),
('ManageRole', 'DeleteRole'),
('ManageUser', 'DeleteUser'),
('ApplicationAdmin', 'FacilityAdmin'),
('SuperAdmin', 'FacilityAdmin'),
('ImplementationStaff', 'FujitsoUser'),
('OperationStaff', 'FujitsoUser'),
('SecurityStaff', 'FujitsoUser'),
('ApplicationAdmin', 'ImplementationStaff'),
('FacilityAdmin', 'ManageCustomer'),
('ApplicationAdmin', 'ManageFacility'),
('ApplicationAdmin', 'ManageRole'),
('SuperAdmin', 'ManageSystemConfig'),
('CustomerAdmin', 'ManageUser'),
('ManageSystemConfig', 'ModifySystemConfiguration'),
('ApplicationAdmin', 'OperationStaff'),
('ManageCustomer', 'ReadCustomer'),
('FacilityAdmin', 'ReadFacility'),
('ManageFacility', 'ReadFacility'),
('CustomerUser', 'ReadLibraryContent'),
('FujitsoUser', 'ReadLibraryContent'),
('ManageContent', 'ReadLibraryContent'),
('AuthPost', 'ReadPost'),
('CustomerUser', 'ReadPost'),
('FujitsoUser', 'ReadPost'),
('ManagePost', 'ReadPost'),
('CustomerAdmin', 'ReadReports'),
('ManageReport', 'ReadReports'),
('ManageRole', 'ReadRole'),
('ManageSystemConfig', 'ReadSystemConfiguration'),
('ModifySystemConfiguration', 'ReadSystemConfiguration'),
('ManageUser', 'ReadUser'),
('ApplicationAdmin', 'SecurityStaff'),
('AuthorizeDcAccessRequest', 'SubmitDcAccessRequest'),
('CustomerUser', 'SubmitDCAccessRequest'),
('ManageRequest', 'SubmitDcAccessRequest'),
('AuthorizeDcTelcoAccessRequest', 'SubmitDcTelcoAccessRequest'),
('ManageRequest', 'SubmitDcTelcoAccessRequest'),
('AuthorizeEmfRequest', 'SubmitEmfRequest'),
('CustomerUser', 'SubmitEMFRequest'),
('ManageRequest', 'SubmitEmfRequest'),
('AuthorizeProximityRequest', 'SubmitProximityRequest'),
('CustomerUser', 'SubmitProximityRequest'),
('ManageRequest', 'SubmitProximityRequest'),
('AuthorizeRemoteHandsRequest', 'SubmitRemoteHandsRequest'),
('ManageRequest', 'SubmitRemoteHandsRequest'),
('ManageCustomer', 'UpdateCustomer'),
('FacilityAdmin', 'UpdateFacility'),
('ManageFacility', 'UpdateFacility'),
('AuthPost', 'UpdatePost'),
('ManagePost', 'UpdatePost'),
('CustomerUser', 'UpdateProfile'),
('ManageUser', 'UpdateProfile'),
('ManageReport', 'UpdateReport'),
('ManageRole', 'UpdateRole'),
('ManageUser', 'UpdateUser');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `client_name` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`record_id`, `client_id`, `client_name`, `modified_time`) VALUES
(1, 215, 'University of Melbourne', NULL),
(2, 207, 'Monash University', NULL),
(3, 211, 'RMIT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_facility`
--

CREATE TABLE `client_facility` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `Facility_id` int(11) NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `client_facility`
--

INSERT INTO `client_facility` (`record_id`, `client_id`, `Facility_id`, `modified_time`) VALUES
(1, 215, 5, NULL),
(2, 207, 5, NULL),
(3, 211, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_sensor`
--

CREATE TABLE `client_sensor` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `modified_time` datetime NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `sensor_id` (`sensor_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `environment_statistics`
--

CREATE TABLE `environment_statistics` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `abshumidity` decimal(8,2) NOT NULL DEFAULT '0.00',
  `dewpoint` decimal(8,2) NOT NULL DEFAULT '0.00',
  `humidity` decimal(8,2) NOT NULL DEFAULT '0.00',
  `temperature` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`record_id`),
  KEY `sensor_id` (`sensor_id`),
  KEY `sensor_id_2` (`sensor_id`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) NOT NULL,
  `facility_name` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `facility_tz` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `facility_id` (`facility_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`record_id`, `facility_id`, `facility_name`, `facility_tz`, `state`, `modified_time`) VALUES
(1, 5, 'Noble Park', '', 'VIC', NULL),
(2, 8, 'Malaga', '', 'WA', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `noticeboard`
--

CREATE TABLE `noticeboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facility` int(11) unsigned NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '',
  `thread_id` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned DEFAULT NULL,
  `publish_from` datetime NOT NULL,
  `publish_to` datetime NOT NULL,
  `sticky` tinyint(3) unsigned DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `short_message` varchar(256) DEFAULT NULL,
  `full_message` varchar(10240) DEFAULT NULL,
  `submitted_by` int(10) unsigned DEFAULT NULL,
  `submitted_time` datetime DEFAULT NULL,
  `approver` int(10) unsigned DEFAULT NULL,
  `approved_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_noticeboard_1` (`facility`),
  KEY `ix_noticeboard_2` (`publish_from`),
  KEY `ix_noticeboard_3` (`publish_to`),
  KEY `ix_noticeboard_4` (`thread_id`),
  KEY `ix_noticeboard_5` (`status`),
  KEY `submitted_by` (`submitted_by`),
  KEY `approver` (`approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `power_statistics`
--

CREATE TABLE `power_statistics` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Timestamp` datetime DEFAULT NULL,
  `kWh` decimal(20,1) NOT NULL DEFAULT '0.0',
  `kVATotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kWTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Frequency` decimal(4,2) NOT NULL DEFAULT '0.00',
  `lc` decimal(8,2) NOT NULL DEFAULT '0.00',
  `lb` decimal(8,2) NOT NULL DEFAULT '0.00',
  `la` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVllca` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVllbc` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVllab` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `power_statistics_summary`
--

CREATE TABLE `power_statistics_summary` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YYYYMMDD` date NOT NULL,
  `kVllab` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVllbc` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVllca` decimal(8,2) NOT NULL DEFAULT '0.00',
  `la` decimal(8,2) NOT NULL DEFAULT '0.00',
  `maxla` decimal(8,2) NOT NULL DEFAULT '0.00',
  `lb` decimal(8,2) NOT NULL DEFAULT '0.00',
  `maxlb` decimal(8,2) NOT NULL DEFAULT '0.00',
  `lc` decimal(8,2) NOT NULL DEFAULT '0.00',
  `maxlc` decimal(8,2) NOT NULL DEFAULT '0.00',
  `Frequency` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kWTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `maxkWTotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kVATotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `maxkVATotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `kWh` decimal(20,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`),
  KEY `YYYYMMDD` (`YYYYMMDD`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Rights`
--

CREATE TABLE `Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sensor`
--

CREATE TABLE `sensor` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `sensor_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_time` datetime NOT NULL,
  PRIMARY KEY (`sensor_id`),
  KEY `sensor_id` (`sensor_id`,`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `password` char(128) NOT NULL,
  `emailaddress` char(128) NOT NULL DEFAULT '',
  `firstname` char(128) DEFAULT NULL,
  `lastname` char(128) DEFAULT NULL,
  `alternateemailaddress` char(128) DEFAULT NULL,
  `officephone` char(20) DEFAULT NULL,
  `officefax` char(20) DEFAULT NULL,
  `mobilephone` char(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_from` char(48) DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  `verification_code` varchar(128) NOT NULL,
  `verified` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailaddress` (`emailaddress`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `password`, `emailaddress`, `firstname`, `lastname`, `alternateemailaddress`, `officephone`, `officefax`, `mobilephone`, `status`, `valid_from`, `valid_to`, `last_login`, `last_login_from`, `modified_by`, `modified_time`, `verification_code`, `verified`) VALUES
(1, '704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106', 'kramx7@yahoo.com', 'Jan Mark', 'Salarda', 'kramx7zzz@yahoo.com', '1234567', '1234567', '1234567', 'swUser/Active', '2013-05-02 00:00:00', '2035-12-31 00:00:00', '2013-05-21 07:21:08', '127.0.0.1', 0, '2013-05-21 07:15:39', 'c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0', 1),
(2, '704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106', 'kramx71@yahoo.com', 'Jan Mark1', 'Salarda', '', '', '', '', 'swUser/Active', '2013-05-02 00:00:00', '0000-00-00 00:00:00', '2013-05-14 13:45:37', '127.0.0.1', 0, '2013-05-02 14:35:15', 'c903029ad5f7a3330b81834a0c01e1f2dfe0a3fa856d10718d61d09c54cf5c4416eb300025dc98f23ea7315e92eaf1dd7eed680eaea5242b06a05be0044117a8', 1),
(3, '704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106', 'kramx72@yahoo.com', 'Jan Mark2', 'Salarda', '', '', '', '', 'swUser/Active', '2013-05-02 00:00:00', '0000-00-00 00:00:00', '2013-05-02 15:49:57', '127.0.0.1', 0, '2013-05-02 14:53:22', 'a2ad9f9759e9730fe947732544d72e75edb1fcd3c073999a6a8b42ff921244d9f0513f6281101abcb3619aedf049fae31ee026efc2721efd2f319d161e3c5992', 1),
(4, '704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106', 'kramx73@yahoo.com', 'Jan Mark3', 'Salarda', '', '', '', '', 'swUser/Active', '2013-05-02 00:00:00', '0000-00-00 00:00:00', '2013-05-02 15:31:41', '127.0.0.1', 0, '2013-05-02 14:54:59', '68df3fe8174ef3557b67bef2fc5871ed43bf56a490020bde9f3bc483cbe0a033191864a6da9581a9a217321c91bc01b657bdc6f3cd7990a9f8c28cbf08589900', 1),
(49, '704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106', 'kramx724@yahoo.com', 'Jan Mark4a', 'Salarda4a', '', '', '', '', 'swUser/Active', '2013-05-17 00:00:00', '2035-12-31 00:00:00', '2013-05-17 12:11:31', '127.0.0.1', 0, '2013-05-17 10:53:31', 'bd4564acb96d9ae04ea41ed794466c77e142faffdcce603ba0c51ad6c0be9b7c792e2600db9ded14173b3a224acee8304fdaff9662ef7896de61e9796e0c8e41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_client`
--

CREATE TABLE `user_client` (
  `userid` int(11) unsigned NOT NULL,
  `client` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`client`),
  KEY `client` (`client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_facility`
--

CREATE TABLE `user_facility` (
  `userid` int(11) unsigned NOT NULL,
  `facility` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`,`facility`),
  KEY `facility` (`facility`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_task_client`
--

CREATE TABLE `user_task_client` (
  `client_id` int(11) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `task` varchar(64) NOT NULL,
  PRIMARY KEY (`client_id`,`user_id`,`task`),
  KEY `fk_user_id` (`user_id`),
  KEY `fk_task` (`task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_task_facility`
--

CREATE TABLE `user_task_facility` (
  `facility_id` int(11) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `task` varchar(64) NOT NULL,
  PRIMARY KEY (`facility_id`,`user_id`,`task`),
  KEY `fk_user_id` (`user_id`),
  KEY `fk_task` (`task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AuthAssignment`
--
ALTER TABLE `AuthAssignment`
  ADD CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `AuthItemChild`
--
ALTER TABLE `AuthItemChild`
  ADD CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `noticeboard`
--
ALTER TABLE `noticeboard`
  ADD CONSTRAINT `noticeboard_ibfk_1` FOREIGN KEY (`submitted_by`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `noticeboard_ibfk_2` FOREIGN KEY (`approver`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Rights`
--
ALTER TABLE `Rights`
  ADD CONSTRAINT `Rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_client`
--
ALTER TABLE `user_client`
  ADD CONSTRAINT `user_client_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_client_ibfk_2` FOREIGN KEY (`client`) REFERENCES `client` (`client_id`);

--
-- Constraints for table `user_facility`
--
ALTER TABLE `user_facility`
  ADD CONSTRAINT `user_facility_ibfk_1` FOREIGN KEY (`facility`) REFERENCES `facility` (`facility_id`),
  ADD CONSTRAINT `user_facility_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `user` (`id`);

--
-- Constraints for table `user_task_client`
--
ALTER TABLE `user_task_client`
  ADD CONSTRAINT `user_task_client_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_task_client_ibfk_2` FOREIGN KEY (`task`) REFERENCES `AuthItem` (`name`);

--
-- Constraints for table `user_task_facility`
--
ALTER TABLE `user_task_facility`
  ADD CONSTRAINT `user_task_facility_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_task_facility_ibfk_2` FOREIGN KEY (`task`) REFERENCES `AuthItem` (`name`);
