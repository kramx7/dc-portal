<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL','http://localhost/dcportal/index-test.php/');
//define('TEST_BASE_URL','http://blogsio.com/index-test.php/');

/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class WebTestCase extends CWebTestCase
{
	/**
	 * Sets up before each test method runs.
	 * This mainly sets the base URL for the test application.
	 */
        protected $captureScreenshotOnFailure = TRUE;
        protected $screenshotPath = '/Applications/MAMP/htdocs/screenshots';
        protected $screenshotUrl = 'http://localhost/screenshots';
        //protected $screenshotPath = '/home1/blogsdig/public_html/blogsio/screenshots';
        //protected $screenshotUrl = 'http://blogsio.com/screenshots';
    
	protected function setUp()
	{
		parent::setUp();
		$this->setBrowserUrl(TEST_BASE_URL);
	}
        
        public function createUrl($route,$params=array()) {
 
            $url = explode('phpunit',Yii::app()->createUrl($route,$params));
            return $url[1];
            //$url = Yii::app()->createUrl($route,$params);
            //return $url;
        }
        
        public function login(){
            
            // login as superadmin or any desired user to perform the following test
            $username = 'kramx7@yahoo.com'; //provide user email
            $password = 'sa@1234'; // provide user password     

            $this->open('');
            // ensure the user is logged out
            if($this->isTextPresent('Logout'))
                    $this->clickAndWait('link=Logout (demo)');

            // test login process, including validation
            $this->clickAndWait('link=Login');
            $this->assertElementPresent('name=LoginForm[username]');
            $this->type('name=LoginForm[username]',$username); //provide user email
            $this->click("//input[@value='Login']");
            $this->waitForTextPresent('Password cannot be blank.');
            $this->type('name=LoginForm[password]',$password); // provide user password
            $this->clickAndWait("//input[@value='Login']");
            $this->assertTextNotPresent('Password cannot be blank.');
            $this->assertTextPresent('Logout');
        }
}
