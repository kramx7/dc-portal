<?php

class ReportTest extends WebTestCase {

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds' => 'AuthItemChild',
        'authitems' => 'AuthItem',
    );

    public function login($username = 'kramx7@yahoo.com', $password = 'sa@1234') {

        // login as superadmin by default or any desired user to perform the following test
        // default user account is for the superadmin
        // the username and password can be edited on the test/method that access the login method    
        // ecnrypted password: c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0

        $this->open('');
        // ensure the user is logged out
        if ($this->isTextPresent('Logout'))
            $this->clickAndWait('link=Logout');

        // test login process, including validation
        //$this->clickAndWait('link=Login');
        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', $username); //provide user email
        $this->click("//input[@value='Login']");
        $this->waitForTextPresent('Password cannot be blank.');
        $this->type('name=LoginForm[password]', $password); // provide user password
        $this->clickAndWait("//input[@value='Login']");
        $this->assertTextNotPresent('Password cannot be blank.');
        $this->assertTextPresent('Logout');
    }

    public function testPowerKw() {

        //test scenario: power kw graph, total kw
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('report/power/kW');

        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Power Graph");
        
        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=meter');

        // select meter
        $this->select("//select[@id='meter']", "Total");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=power-graph-image');

        //test scenario: power kw graph, specific meter
        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=meter');

        // select meter
        $this->select("//select[@id='meter']", "PDU_A2-2/2Mtr (Mon)");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=power-graph-image');
    }

    public function testPowerKwh() {

        //test scenario: power kw graph, total kw
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('report/power/kWh');

        // check page title is correct
        //$this->assertTitleEquals(Yii::app()->name." - Power Graph");
        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=meter');

        // select meter
        $this->select("//select[@id='meter']", "Total");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/03/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=power-graph-image');

        //test scenario: power kw graph, specific meter
        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=meter');

        // select meter
        $this->select("//select[@id='meter']", "PDU_A2-2/2Mtr (Mon)");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/03/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=power-graph-image');
    }

    public function testEnvironmentTemperature() {

        //test scenario: environment temperature graph
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('report/environment/Temperature');

        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=sensor');

        // select meter
        $this->select("//select[@id='sensor']", "Aisle B");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=environment-graph-image');
    }

    public function testEnvironmentHumidity() {

        //test scenario: environment humidity graph
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('report/environment/Humidity');

        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // check meter option is present
        $this->assertElementPresent('id=sensor');

        // select meter
        $this->select("//select[@id='sensor']", "Aisle D");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check submit button is present
        $this->assertElementPresent('id=submit');
        $this->clickAndWait("//button[@id='submit']");

        $this->assertTextNotPresent('There are no results for the specified input.');

        // check graph image is present
        $this->assertElementPresent('id=environment-graph-image');
    }

    public function testNGER() {

        //test scenario: nger
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('report/nger');

        // check facility option is present
        $this->assertElementPresent('id=facility-id');

        // select facility
        $this->select("//select[@id='facility-id']", "Noble Park");

        // wait data to refresh
        //$this->waitForVisible('id=done-loading');
        
        // check kwh data
        //$this->assertElementContainsText('id=kwh-data', '3072186.6');
        $this->assertElementNotContainsText('id=kwh-data','x');
        
        $this->assertElementNotContainsText('id=average-pue-data','x');
        
        $this->assertElementNotContainsText('id=total-kwh-data','x');
        
        $this->assertElementNotContainsText('id=carbon-data','x');
            
        // check meter option is present
        $this->assertElementPresent('id=year');

        // select meter
        $this->select("//select[@id='year']", "2012");
        
        $this->assertElementNotContainsText('id=kwh-data','x');
        
        $this->assertElementNotContainsText('id=average-pue-data','x');
        
        $this->assertElementNotContainsText('id=total-kwh-data','x');
        
        $this->assertElementNotContainsText('id=carbon-data','x');
    }

}
