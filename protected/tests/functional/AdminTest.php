<?php

class AdminTest extends WebTestCase {

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds' => 'AuthItemChild',
        'authitems' => 'AuthItem',
    );

    public function login($username = 'kramx7@yahoo.com', $password = 'sa@1234') {

        // login as superadmin by default or any desired user to perform the following test
        // default user account is for the superadmin
        // the username and password can be edited on the test/method that access the login method    
        // ecnrypted password: c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0

        $this->open('');
        // ensure the user is logged out
        if ($this->isTextPresent('Logout'))
            $this->clickAndWait('link=Logout');

        // test login process, including validation
        //$this->clickAndWait('link=Login');
        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', $username); //provide user email
        $this->click("//input[@value='Login']");
        $this->waitForTextPresent('Password cannot be blank.');
        $this->type('name=LoginForm[password]', $password); // provide user password
        $this->clickAndWait("//input[@value='Login']");
        $this->assertTextNotPresent('Password cannot be blank.');
        $this->assertTextPresent('Logout');
    }

    public function testProfile() {

        //test scenario: update user profile   
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('admin/profile');

        // check if the profile form header is present
        $this->assertTextPresent('Profile');
        $this->assertTextPresent('Contact Info');
        $this->assertTextPresent('Password');
        $this->assertTextPresent('Notification');

        // check if email address label is present
        $this->assertTextPresent('Email Address');

        // check if email address is present on the form
        $this->assertTextPresent('kramx7@yahoo.com');

        // assert values
        //$this->assertQuery('input#valid_from[value=""]', 0);
        // check if all fields to update in profile is present
        // fill up the form to check errors
        // check if first name field is present
        $this->assertElementPresent('name=User[first_name]');

        // provide first name to pass test
        $this->type('name=User[first_name]', 'Jan Mark');

        // check if last name field is present
        $this->assertElementPresent('name=User[last_name]');

        // provide last name to pass test
        $this->type('name=User[last_name]', 'Salarda');

        // check if office number field is present
        $this->assertElementPresent('name=User[officephone]');

        // provide office number, must be at least 7 chars to pass
        $this->type('name=User[officephone]', '12345678');

        // check if fax number field is present
        $this->assertElementPresent('name=User[officefax]');

        // provider fax number, must be at least 7 chars to pass
        $this->type('name=User[officefax]', '12345678');

        // check if mobile number field is present
        $this->assertElementPresent('name=User[mobilephone]');

        // provider mobile number, must be at least 7 chars to pass
        $this->type('name=User[mobilephone]', '12345678');

        // check if password field is present
        $this->assertElementPresent('name=User[new_password]');

        // provide valid 7 char password
        $this->type('name=User[new_password]', 'sa@1234');

        // check if password confirmation field is present
        $this->assertElementPresent('name=User[password_compare]');

        // confirm valid 7 char password
        $this->type('name=User[password_compare]', 'sa@1234');

        // check if button update is present
        $this->assertElementPresent('name=update');

        // click the button to update the profile
        $this->clickAndWait("//button[@name='update']");

        // check that there are no errors as expected after update
        $this->assertTextNotPresent('Please fix the following input errors');

        // check that the success message is present, check if its is on the same page
        $this->assertTextPresent('Successfully updated the profile.'); // means success
    }

    public function testCreateUser() {

        //test scenario: update user profile
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('admin/createuser');

        // check if the create new user label is present, this signifies the user is on the right page
        $this->assertTextPresent('Create New User');

        // check search user field is present
        $this->assertElementPresent('name=User[emailaddress]');

        // provide user to search
        $this->type('name=User[emailaddress]', 'kramx73@yahoo.com');

        // check if first name field is present
        $this->assertElementPresent('name=User[first_name]');

        // type in the first name value
        $this->type('name=User[first_name]', 'Jan Mark');

        // check if last name field is present
        $this->assertElementPresent('name=User[last_name]');

        // type in the first name value
        $this->type('name=User[last_name]', 'Salarda3');

        // check if mobile phone field is present
        $this->assertElementPresent('name=User[mobilephone]');

        // type in the mobile phone value
        $this->type('name=User[mobilephone]', '1234567');

        // check if office phone field is present
        $this->assertElementPresent('name=User[officephone]');

        // type in the office phone value
        $this->type('name=User[officephone]', '1234567');

        // check if valid from date field is present
        $this->assertElementPresent('name=User[valid_from]');
        //$this->assertQuery('input#from_date[@value=""]', 1);
        // check if valid to date field is present
        $this->assertElementPresent('name=User[valid_to]');

        // check if facility fields is present
        $this->assertElementPresent('name=User[_facility]');
        // select noble park
        $this->select("name=User[_facility]", "Noble Park");

        // check if customer field is present
        $this->assertElementPresent('name=User[_client]');

        // select customer
        $this->select("name=User[_client]", "University of Melbourne");

        // check assign operation field is present
        $this->assertElementPresent('id=assign-op');

        // set role value
        $this->select('name=User[_assign_op]', "Roles");

        // select customer admin from available roles
        $this->assertElementPresent('id=unassigned-item');
        $this->select("//select[@id='unassigned-item']", "Customer Administrator");

        // click add
        $this->assertElementPresent('id=user-add');
        $this->click("//button[@id='user-add']");

        // select operations
        $this->select('name=User[_assign_op]', "Operations");

        // select access facility
        $this->assertElementPresent('id=unassigned-item');
        $this->waitForVisible('id=done-loading');
        $this->select("//select[@id='unassigned-item']", "Access Facility");

        //click add
        $this->assertElementPresent('id=user-add');
        $this->click("//button[@id='user-add']");

        // check if button create is present
        $this->assertElementPresent("id=user-create");

        //click button create
        $this->clickAndWait("//button[@id='user-create']");

        // assert there are no errors
        $this->assertTextNotPresent('Please fix the following input error');

        // check if update is successfull
        //$this->assertTextPresent("Successfully created a new user");
    }

    public function testUser() {

        //test scenario: manage users
        // must login to manage users
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('admin/user');

        // check if the manage user header is present, this signifies the user is on the right page
        $this->assertTextPresent('Manage Users');

        // check search user field is present
        $this->assertElementPresent('name=UserUpdateForm[search_user]');

        // provide user to search
        $this->type('name=UserUpdateForm[search_user]', 'jan');

        // click the button to update dropdownlist of users
        $this->clickAndWait("//button[@name='update-data']");

        // check assign field is present
        $this->assertTextPresent('Assign');
        $this->assertElementPresent('name=UserUpdateForm[assignment_operation]');

        // check if user list is present
        $this->assertElementPresent('name=UserUpdateForm[selected_user]');

        // select one user
        $this->select('name=UserUpdateForm[selected_user]', 'Jan Mark Salardaz');

        // click the button to update dropdownlist of users
        $this->clickAndWait("//button[@name='update-data']");

        // check if assigned items is present
        $this->assertElementPresent("id=assigned-item");

        // check if assigned items is present
        $this->assertElementPresent("id=unassigned-item");

        // select one unassigned item
        //$this->select('id=unassigned-item','Customer Administrator');
        $this->select('id=unassigned-item', 'Facility Administrator');

        // click the button to add the item
        $this->click("//button[@name='user-add']");

        // check if button update is present
        $this->assertElementPresent("name=user-update");

        //click button update
        $this->click("//a[@name='user-update']");

        // check if confirmation button is present
        $this->assertElementPresent("id=confirm-update");

        // click the confirmation button and wait for result
        $this->clickAndWait("//button[@id='confirm-update']");

        // check if update is successfull
        $this->assertTextPresent("Successfully updated");
    }

    public function testCreateRole() {

        //test scenario: create new role
        // must login to view create new role form
        $this->login('kramx7@yahoo.com', 'sa@1234');
        $this->open('admin/createrole');

        // check if the create new role label is present, this signifies the user is on the right page
        $this->assertTextPresent('Create New Role');

        // check auth item name is present
        $this->assertElementPresent('name=AuthItem[name]');

        // provide user to search
        $this->type('name=AuthItem[name]', 'Role3');

        // check if first name field is present
        $this->assertElementPresent('id=assign-op');

        // type in the first name value
        $this->select("//select[@id='assign-op']", 'Add/Remove Roles');

        // check if last name field is present
        //$this->assertElementPresent('name=AuthItem[_inherited_items][]');
        $this->assertElementPresent('id=inherited-items');

        //$this->assertElementPresent('name=AuthItem[_available_items][]');
        $this->assertElementPresent('id=available-items');

        // select one user
        //$this->select('name=AuthItem[_available_items][]', 'Customer Administrator');
        $this->select("//select[@id='available-items']", "Customer Administrator");

        //click button add
        $this->click("//button[@id='role-add']");

        //$this->select('name=AuthItem[_available_items][]', 'Customer User');
        $this->select("//select[@id='available-items']", "Customer User");

        //click button add
        $this->click("//button[@id='role-add']");

        // check if button save is present
        $this->assertElementPresent("id=role-save");

        //click button save
        $this->clickAndWait("//button[@id='role-save']");

        // assert there are no errors
        $this->assertTextNotPresent('Name cannot be blank');
        $this->assertTextNotPresent('has already been taken');

        // assert there are no errors
        $this->assertTextNotPresent('Inherited Items cannot be blank.');
    }

    public function testRole() {

        //test scenario: manage roles
        // must login to view manage role form
        $this->login('kramx7@yahoo.com', 'sa@1234');
        $this->open('admin/role');

        // check if the manage roles label is present, this signifies the user is on the right page
        $this->assertTextPresent('Manage Roles');

        // check assign type is present, role, operations
        $this->assertElementPresent('id=assign-op');

        // select assign to role
        $this->select("//select[@id='assign-op']", 'Add/Remove Roles');

        // check if there are roles present to choose
        $this->assertElementPresent('id=selected-role');

        // select role1 to update
        $this->select("//select[@id='selected-role']", 'Customer User');

        // wait for the inherited and available roles to appear first
        $this->waitForVisible('id=done-loading');

        // check inherited list is present
        //$this->assertElementPresent('name=AuthItem[_inherited_items][]');
        $this->assertElementPresent('id=inherited-items');

        // check if available list is present
        //$this->assertElementPresent('name=AuthItem[_available_items][]');
        $this->assertElementPresent('id=available-items');

        //$this->select('name=AuthItem[_available_items][]', 'Fujitso User');
        $this->select("//select[@id='available-items']", 'Operation Staff');

        //click button add
        $this->click("//button[@id='role-add']");

        // check if button update is present
        $this->assertElementPresent("id=role-update");

        //click button update
        $this->clickAndWait("//button[@id='role-update']");


        // check if update is successfull
        $this->assertTextPresent("Successfully saved the role.");
    }

}
