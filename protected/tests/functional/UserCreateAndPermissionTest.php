<?php

class UserCreateAndPermissionTest extends WebTestCase {

    public $fixtures = array(
        'users' => 'User',
        'notices' => 'Noticeboard',
        'authassignments' => 'AuthAssignment',
        'authitemchilds' => 'AuthItemChild',
        'authitems' => 'AuthItem',
    );

    public function login($username = 'kramx7@yahoo.com', $password = 'sa@1234') {

        // login as superadmin by default or any desired user to perform the following test
        // default user account is for the superadmin
        // the username and password can be edited on the test/method that access the login method    
        // ecnrypted password: c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0

        $this->open('');
        // ensure the user is logged out
        if ($this->isTextPresent('Logout'))
            $this->clickAndWait('link=Logout');

        // test login process, including validation
        //$this->clickAndWait('link=Login');
        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', $username); //provide user email
        $this->click("//input[@value='Login']");
        $this->waitForTextPresent('Password cannot be blank.');
        $this->type('name=LoginForm[password]', $password); // provide user password
        $this->clickAndWait("//input[@value='Login']");
        $this->assertTextNotPresent('Password cannot be blank.');
        $this->assertTextPresent('Logout');
    }

    public function testCreateSuperAdminUser() {

        //test scenario: power kw graph, total kw
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('admin/createuser');

        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Createuser Admin");
        
        // check facility option is present
        $this->assertElementPresent('id=User_facility');

        // select facility
        $this->select("//select[@id='User_facility']", "Noble Park");

        // check facility option is present
        $this->assertElementPresent('id=User_client');

        // select facility
        $this->select("//select[@id='User_client']", "University of Melbourne");

        // check facility option is present
        $this->assertElementPresent('id=User_role');

        // select facility
        $this->select("//select[@id='User_role']", "Super Admin");

        // check meter option is present
        $this->assertElementPresent('id=User_emailaddress');

        // select meter
        $this->type("//input[@id='User_emailaddress']", "su@dcportal.com");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check meter option is present
        $this->assertElementPresent('id=User_first_name');

        // select meter
        $this->type("//input[@id='User_first_name']", "SUFirstname");

        // check meter option is present
        $this->assertElementPresent('id=User_last_name');

        // select meter
        $this->type("//input[@id='User_last_name']", "SULastname");

        // check meter option is present
        $this->assertElementPresent('id=User_mobilephone');

        // check meter option is present
        $this->assertElementPresent('id=User_officephone');

        // check meter option is present
        $this->assertElementPresent('id=User_officefax');

        // check meter option is present
        $this->assertElementPresent('id=User_remote_hands');


        // check submit button is present
        $this->assertElementPresent("//button[@name='save']");
        $this->clickAndWait("//button[@name='save']");


        $this->assertTextNotPresent('There are no results for the specified input.');


        // verify user, 3rd user expected after the defaul 2
        $user = User::model()->findByPk(3);

        $this->open('user/verify/'.$user->verification_code);
        
        $this->assertTextPresent('Account Verification');

        $this->assertElementPresent('name=User[password]');
        $this->type('name=User[password]','12345678');

        $this->assertElementPresent('name=User[password_compare]');
        $this->type('name=User[password_compare]','12345678');

        $this->assertElementPresent('name=User[alternateemailaddress]');
        $this->type('name=User[alternateemailaddress]','');

        $this->assertElementPresent('name=User[officephone]');
        $this->type('name=User[officephone]','');

        $this->assertElementPresent('name=User[officefax]');
        $this->type('name=User[officefax]','');

        $this->assertElementPresent('name=User[mobilephone]');
        $this->type('name=User[mobilephone]','');

        $this->clickAndWait("//button[@name='Active']");

        $this->assertTextNotPresent('Please fix the following input errors');

        $this->assertTextPresent('You have successfully verified and activated your account.'); // means success

        $this->login('su@dcportal.com', '12345678');

        $this->open('admin/user');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - User Admin");

        $this->open('report/power/kW');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Power Graph");

        $this->open('report/environment/Temperature');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Environment Graph");


        $this->open('report/nger');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - NGER Graph");

    }

    public function testCreateCustomerAdminUser() {

        //test scenario: power kw graph, total kw
        // must login to view profile
        $this->login('kramx7@yahoo.com', 'sa@1234');

        $this->open('admin/createuser');

        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Createuser Admin");
        
        // check facility option is present
        $this->assertElementPresent('id=User_facility');

        // select facility
        $this->select("//select[@id='User_facility']", "Noble Park");

        // check facility option is present
        $this->assertElementPresent('id=User_client');

        // select facility
        $this->select("//select[@id='User_client']", "University of Melbourne");

        // check facility option is present
        $this->assertElementPresent('id=User_role');

        // select facility
        $this->select("//select[@id='User_role']", "Customer Administrator");

        // check meter option is present
        $this->assertElementPresent('id=User_emailaddress');

        // select meter
        $this->type("//input[@id='User_emailaddress']", "su@dcportal.com");

        // check start date is present
        $this->assertElementPresent('id=from_date');

        // set start date
        $this->type("//input[@id='from_date']", "16/04/2013");

        // check end date is present
        $this->assertElementPresent('id=to_date');

        // set end date
        $this->type("//input[@id='to_date']", "16/04/2013");

        // check meter option is present
        $this->assertElementPresent('id=User_first_name');

        // select meter
        $this->type("//input[@id='User_first_name']", "SUFirstname");

        // check meter option is present
        $this->assertElementPresent('id=User_last_name');

        // select meter
        $this->type("//input[@id='User_last_name']", "SULastname");

        // check meter option is present
        $this->assertElementPresent('id=User_mobilephone');

        // check meter option is present
        $this->assertElementPresent('id=User_officephone');

        // check meter option is present
        $this->assertElementPresent('id=User_officefax');

        // check meter option is present
        $this->assertElementPresent('id=User_remote_hands');


        // check submit button is present
        $this->assertElementPresent("//button[@name='save']");
        $this->clickAndWait("//button[@name='save']");


        $this->assertTextNotPresent('There are no results for the specified input.');


        // verify user, 3rd user expected after the defaul 2
        $user = User::model()->findByPk(3);

        $this->open('user/verify/'.$user->verification_code);
        
        $this->assertTextPresent('Account Verification');

        $this->assertElementPresent('name=User[password]');
        $this->type('name=User[password]','12345678');

        $this->assertElementPresent('name=User[password_compare]');
        $this->type('name=User[password_compare]','12345678');

        $this->assertElementPresent('name=User[alternateemailaddress]');
        $this->type('name=User[alternateemailaddress]','');

        $this->assertElementPresent('name=User[officephone]');
        $this->type('name=User[officephone]','');

        $this->assertElementPresent('name=User[officefax]');
        $this->type('name=User[officefax]','');

        $this->assertElementPresent('name=User[mobilephone]');
        $this->type('name=User[mobilephone]','');

        $this->clickAndWait("//button[@name='Active']");

        $this->assertTextNotPresent('Please fix the following input errors');

        $this->assertTextPresent('You have successfully verified and activated your account.'); // means success

        $this->login('su@dcportal.com', '12345678');

        $this->open('admin/user');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - User Admin");

        $this->open('report/power/kW');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Power Graph");

        $this->open('report/environment/Temperature');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - Environment Graph");


        $this->open('report/nger');
        // check page title is correct
        $this->assertTitle(Yii::app()->name." - NGER Graph");

    }


}
