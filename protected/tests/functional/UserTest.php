<?php

class UserTest extends WebTestCase
{   
        
        var $testToPerform = array(
                //1=>'testCreateSuperAdmin',
                //'testUpdateSuperAdmin',
                //'testVerify',
                //'testCreateFacilityAdmin',
                //'testUpdateFacilityAdmin',
                //'testCreateCustomerAdmin',
                //'testUpdateCustomerAdmin',
                //'testCreateCustomerUser',
                //'testUpdateCustomerUser',
                //'testFacilityAdminCreateCustomerAdmin',
                //'testFacilityAdminUpdateCustomerAdmin',
                //'testFacilityAdminCreateCustomerUser',
                //'testFacilityAdminUpdateCustomerUser',
                //'testSuperAdminView',
                //'testFacilityAdminView',
                //'testCustomerAdminView',
                //'testCustomerUserView',
                
            );
        
        public function isIncluded($test){
            
            if(in_array($test, $this->testToPerform)){
                return true;
            }else{
                return false;
            }
            
        }


        public function login($username = 'kramx7@yahoo.com', $password = 'sa@123'){
            
            // login as superadmin or any desired user to perform the following test
            // default user account is for the superadmin
            // the username and password can be edited on the test/method that access the login method    
            // ecnrypted password: c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0
            
            $this->open('');
            // ensure the user is logged out
            if($this->isTextPresent('Logout'))
                    $this->clickAndWait('link=Logout (demo)');

            // test login process, including validation
            $this->clickAndWait('link=Login');
            $this->assertElementPresent('name=LoginForm[username]');
            $this->type('name=LoginForm[username]',$username); //provide user email
            $this->click("//input[@value='Login']");
            $this->waitForTextPresent('Password cannot be blank.');
            $this->type('name=LoginForm[password]',$password); // provide user password
            $this->clickAndWait("//input[@value='Login']");
            $this->assertTextNotPresent('Password cannot be blank.');
            $this->assertTextPresent('Logout');
        }
    
	public function testCreateSuperAdmin()
	{
            // test scenario: a superadmin user creating a superadmin user
            
            $this->login();

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','SuperAdmin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[facility]');
            $this->type('name=User[facility]',0); //0=>unlimited, 5 -> malaga, 8 -> noble park
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx71@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
       
        public function testUpdateSuperAdmin()
	{
            // test scenario: a superadmin user update a superadmin user
            
            
            $this->login();

            $user_id = 2;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','SuperAdmin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[facility]');
            $this->type('name=User[facility]',0); //0=>unlimited, 5 -> malaga, 8 -> noble park
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx71@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
      public function testVerify(){
            
            //test scenario: verify user acccount, use to activate account    
            
            $code = '38b9592c23dae2f34ed25d7e0e9dc5f47ff6b92779c9daa0f261b3acf8d6045a83bb601f1a6819c81fce367f5baf2f1c13cc0692f5382bd38b9bb3ce0f7320fc';

            $this->open('user/verify/'.$code);

            $this->assertTextPresent('Account Verification');

            $this->assertElementPresent('name=User[password]');
            $this->type('name=User[password]','12345678');

            $this->assertElementPresent('name=User[passCompare]');
            $this->type('name=User[passCompare]','12345678');

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->clickAndWait("//input[@name='Active']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('Please fill out the following form with your login credentials'); // means success
          
      }
      
      
      public function testCreateFacilityAdmin()
	{
            //test scenario: admin user creating a facility admin user
            
           
            $this->login();

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Fac_Admin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[facility]');
            $this->type('name=User[facility]',5); //0=>unlimited, 5 -> malaga, 8 -> noble park
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx72@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testUpdateFacilityAdmin()
	{
            // test scenario: a superadmin user update a fac admin user
            
            $this->login();

            $user_id = 3;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Fac_Admin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[facility]');
            $this->type('name=User[facility]',5); //0=>unlimited, 5 -> malaga, 8 -> noble park
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx72@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testCreateCustomerAdmin()
	{
            //test scenario: admin user creating a facility admin user
            
            $this->login();

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_Admin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx73@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testUpdateCustomerAdmin()
	{
            // test scenario: a superadmin user update a fac admin user
            
            $this->login();

            $user_id = 4;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_Admin');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx73@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testCreateCustomerUser()
	{
            //test scenario: admin user creating a facility admin user
          
            
            $this->login();

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_User');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx74@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testUpdateCustomerUser()
	{
            // test scenario: a superadmin user update a fac admin user
            
            $this->login();

            $user_id = 5;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_User');
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            $this->clickAndWait("//input[@name='step3']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx74@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        
        public function testFacilityAdminCreateCustomerAdmin()
	{
            //test scenario: facility admin creating a customer admin user
            
            
            $this->login('kramx72@yahoo.com','12345678');

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_Admin');
            
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx75@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testFacilityAdminUpdateCustomerAdmin()
	{
            // test scenario: a facility admin user update a customer admin user
            
            
            $this->login('kramx72@yahoo.com','12345678');

            $user_id = 4;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_Admin');
            
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx75@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testFacilityAdminCreateCustomerUser()
	{
            //test scenario: facility admin user creating a customer user
            
            $this->login('kramx72@yahoo.com','12345678');

            $this->open('user/create');


            $this->assertTextPresent('Create User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_User');
            
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');


            //test step 2
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx76@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testFacilityAdminUpdateCustomerUser()
	{
            // test scenario: a facility admin user update a customer user
            
            $this->login('kramx72@yahoo.com','12345678');

            $user_id = 5;

            $this->open('user/update/'.$user_id);

            $this->assertTextPresent('Update User');

            // test step 1
            $this->assertElementPresent('name=User[user_role]');
            $this->type('name=User[user_role]','Cust_User');
            
            $this->assertElementPresent('name=User[client]');
            $this->type('name=User[client]',215); //215 -> melbourne , 207-> monash, 211 -> rmit
            
            $this->clickAndWait("//input[@name='step2']");
            $this->assertTextNotPresent('Please fix the following input errors');

            //test step 3
            $this->assertElementPresent('name=User[emailaddress]');
            $this->type('name=User[emailaddress]','kramx76@yahoo.com'); 

            $this->assertElementPresent('name=User[alternateemailaddress]');
            $this->type('name=User[alternateemailaddress]','');

            $this->assertElementPresent('name=User[officephone]');
            $this->type('name=User[officephone]','');

            $this->assertElementPresent('name=User[officefax]');
            $this->type('name=User[officefax]','');

            $this->assertElementPresent('name=User[mobilephone]');
            $this->type('name=User[mobilephone]','');

            $this->assertElementPresent('name=display_status');

            $this->assertElementPresent('name=User[valid_from]');
            //$this->type('name=User[valid_from]',date('Y-m-d'));

            $this->assertElementPresent('name=User[valid_to]');
            //$this->type('name=User[valid_to]',date('Y-m-d'),strtotime('+ 1 month'));

            $this->clickAndWait("//input[@name='finish']");

            $this->assertTextNotPresent('Please fix the following input errors');

            $this->assertTextPresent('View User'); // means success
	}
        
        public function testSuperAdminView(){
            
            // test scenario: superadmin user viewing a user profile
            
             $this->login();
             
             $user_id = 2;
             $this->open('user/view/'.$user_id);
             $this->assertTextPresent('View User #'.$use_id);
        }
        
        public function testFacilityAdminView(){
            
            // test scenario: facility administrator viewing a user profile
            
            $this->login('kramx72@yahoo.com','12345678');
            
            $user_id = 3;
            $this->open('user/view/'.$user_id);
            $this->assertTextPresent('View User #'.$use_id);
        }
        
        public function testCustomerAdminView(){
            
            // test scenario: customer admin viewing a user profile
            
            $this->login('kramx73@yahoo.com','12345678');
            
            $user_id = 4;
            $this->open('user/view/'.$user_id);
            $this->assertTextPresent('View User #'.$use_id);
        }
        
        public function testCustomerUserView(){
            
            // test scenario: customer user viewing a user profile
            
            
            $this->login('kramx74@yahoo.com','12345678');
            
            $user_id = 5;
            $this->open('user/view/'.$user_id);
            $this->assertTextPresent('View User #'.$use_id);
        }
        
}
