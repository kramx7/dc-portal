<?php

class NoticeboardTest extends WebTestCase
{
	public function login(){
            
            // login as superadmin or any desired user to perform the following test
            $username = 'kramx7@yahoo.com'; //provide user email
            $password = 'sa@123'; // provide user password     

            $this->open('');
            // ensure the user is logged out
            if($this->isTextPresent('Logout'))
                    $this->clickAndWait('link=Logout (demo)');

            // test login process, including validation
            $this->clickAndWait('link=Login');
            $this->assertElementPresent('name=LoginForm[username]');
            $this->type('name=LoginForm[username]',$username); //provide user email
            $this->click("//input[@value='Login']");
            $this->waitForTextPresent('Password cannot be blank.');
            $this->type('name=LoginForm[password]',$password); // provide user password
            $this->clickAndWait("//input[@value='Login']");
            $this->assertTextNotPresent('Password cannot be blank.');
            $this->assertTextPresent('Logout');
        }
    
	public function testCreate()
	{
                $this->login();
                
                $this->open('noticeboard/create');
                
                $this->assertTextPresent('Create Announcement');
                
                $this->assertElementPresent('name=display_status');
               
                
		$this->assertElementPresent('name=Noticeboard[facility]');
                $this->type('name=Noticeboard[facility]',5); //5 -> malaga, 8 -> noble park
               
                $this->assertElementPresent('name=Noticeboard[priority]');
                $this->type('name=Noticeboard[priority]',1); 
                
                
                $this->assertElementPresent('name=Noticeboard[publish_from]');
                //$this->type('name=Noticeboard[publish_from]',date('Y-m-d'));
                
                $this->assertElementPresent('name=Noticeboard[publish_to]');
                //$this->type('name=Noticeboard[publish_to]',date('Y-m-d'),strtotime('+ 1 month'));
                
                $this->assertElementPresent('name=Noticeboard[title]');
                $this->type('name=Noticeboard[title]','title - '.time());
                
                $this->assertElementPresent('name=Noticeboard[short_message]');
                $this->type('name=Noticeboard[short_message]','short message');
                
                $this->assertElementPresent('name=Noticeboard[full_message]');
                $this->type('name=Noticeboard[full_message]','full message');
                
                $this->clickAndWait("//input[@name='draft']");
                //$this->clickAndWait("//input[@name='publish']");
                
                $this->assertTextNotPresent('Please fix the following input errors');
                
                $this->assertTextPresent('View Announcement'); // means success
	}
        
        
        public function testUpdate()
	{
                $this->login();
           
                $notice_id = 2;
                
                $this->open('noticeboard/update/'.$notice_id);
                
                $this->assertTextPresent('Update Announcement');
                
                $this->assertElementPresent('name=display_status');
               
                
		$this->assertElementPresent('name=Noticeboard[facility]');
                $this->type('name=Noticeboard[facility]',5); //5 -> malaga, 8 -> noble park
               
                $this->assertElementPresent('name=Noticeboard[priority]');
                $this->type('name=Noticeboard[priority]',1); 
                
                
                $this->assertElementPresent('name=Noticeboard[publish_from]');
                //$this->type('name=Noticeboard[publish_from]',date('Y-m-d'));
                
                $this->assertElementPresent('name=Noticeboard[publish_to]');
                //$this->type('name=Noticeboard[publish_to]',date('Y-m-d'),strtotime('+ 1 month'));
                
                $this->assertElementPresent('name=Noticeboard[title]');
                $this->type('name=Noticeboard[title]','title - '.time());
                
                $this->assertElementPresent('name=Noticeboard[short_message]');
                $this->type('name=Noticeboard[short_message]','short message');
                
                $this->assertElementPresent('name=Noticeboard[full_message]');
                $this->type('name=Noticeboard[full_message]','full message');
                
                $this->clickAndWait("//input[@name='publish']");
                
                $this->assertTextPresent('View Announcement'); // means success
	}
        
}
