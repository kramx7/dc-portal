<?php

return CMap::mergeArray(
                require(dirname(__FILE__) . '/main.php'), array(
            'components' => array(
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                ),
                /* uncomment the following to provide test database connection
                  'db'=>array(
                  'connectionString'=>'DSN for test database',
                  ),
                 */
                'db' => array(
                    /*
                      'connectionString' => 'mysql:host=localhost;dbname=blogsdig_dcportal_test',
                      'emulatePrepare' => true,
                      'username' => 'blogsdig_dbadmin',
                      'password' => '}K4cm%3PSDkW',
                      'charset' => 'utf8',
                      */
                    
                    'connectionString' => 'mysql:host=localhost;dbname=dcportal_test',
                    'emulatePrepare' => true,
                    'username' => 'root',
                    'password' => 'root',
                    'charset' => 'utf8',
                     
                    
                ),
                'urlManager' => array(
                    'showScriptName' => true,
                    'urlFormat' => 'path',
                    'rules' => array(
                        'site/index' => 'site/index',
                        'user/verify/<code:\w+>' => 'user/verify',
                        '<controller:\w+>/<id:\d+>' => '<controller>/view',
                        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    ),
                ),
            ),
                )
);
