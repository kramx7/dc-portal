<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Fujitsu Data Center Portal',
    'defaultController' => 'site/index',
    // preloading 'log' component
    'preload' => array('log', 'bootstrap'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.simpleWorkflow.*', // Import simpleWorkflow extension
        'ext.yii-mail.YiiMailMessage',
    //'application.vendors.jpgraph.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => date('jY'),
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstrap.gii'
            )
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            //'allowAutoLogin'=>true, // do not allow cookie based authentication
            'authTimeout' => (60 * 70), // 70 minutes, for session time out,
        ),
        'session' => array (
            'autoStart'=>true,
            'sessionName' => 'fujitsu-portal',
            'cookieMode' => 'only',
            'savePath' => ROOT_DIR.'/protected/sessions',
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'sessionTableName' => 'session',
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        //'class' => 'application.modules.srbac.components.SDbAuthManager',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'showScriptName' => true,
            'urlFormat' => 'path',
            'rules' => array(
                'site/index' => 'site/index',
                'user/verify/<code:\w+>' => 'user/verify',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<code:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        /* 'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ), */
        // uncomment the following to use a MySQL database
        'db' => array(
             /*
              'connectionString' => 'mysql:host=localhost;dbname=blogsdig_dcportal',
              //'connectionString' => 'mysql:host=blogsdigger.com;dbname=blogsdig_dcportal',
              'emulatePrepare' => true,
              'username' => 'blogsdig_dbadmin',
              'password' => '}K4cm%3PSDkW',
              'charset' => 'utf8',
            */
            'connectionString' => 'mysql:host=localhost;dbname=dcportal',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'tablePrefix' => '',
             
        ),
        'fixture' => array(
            'class' => 'system.test.CDbFixtureManager',
        ),
        'swSource' => array(
            'class' => 'application.extensions.simpleWorkflow.SWPhpWorkflowSource',
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
            //'host' => 'smtp.gmail.com',
            //'username' => 'XXXX@gmail.com',
            //'password' => 'XXXX',
            //'port' => '465',
            //'encryption'=>'tls',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*array(
                'class'=>'CWebLogRoute',
                ),*/
            ),
        ),
        
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'kramx7z@gmail.com',
        'encryptionKey' => hash('sha512', '0987612345'),
        //'domain' => 'blogsio.com',
        //'completeUrl' => 'http://blogsio.com',
        'never_date' => '2035-12-31',
        'site_name' => 'Fujitsu',
        'SuperAdminCode' => 'SuperAdmin',
        'AppAdminCode'=> 'ApplicationAdmin',
        'FacAdminCode' => 'FacilityAdmin',
        'CustAdminCode' => 'CustomerAdmin',
        'CustUserCode' => 'CustomerUser',
        'BizruleExclusion'=>array('SuperAdmin','ApplicationAdmin'),
        //'RootDirectory' => dirname(Yii::app()->getBasePath()),
        'TimeLimit' => 70,
        'ClientNotRequired' =>  '"SuperAdmin", "ApplicationAdmin", "FacilityAdmin", "FujitsuUser", "ImplementationStaff", "OperationStaff", "SecurityStaff" ',
    ),
);