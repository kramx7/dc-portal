<?php

/**
 * This is the model class for table "AuthItem".
 *
 * The followings are the available columns in table 'AuthItem':
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 *
 * The followings are the available model relations:
 * @property AuthAssignment[] $authAssignments
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren1
 */
class AuthItem extends CActiveRecord {

    public $permission_list = array(
        'ManagePortalLogin'=>array('field'=>'portallogin_permission','desc'=>'Portal Login','group'=>'PortalLoginGroup'),
        'ManageConfiguration'=>array('field'=>'portalconfiguration_permission','desc'=>'Portal Configuration','group'=>'PortalConfigurationGroup'),
        'ManageCustomer'=>array('field'=>'customers_permission','desc'=>'Customers','group'=>'CustomerGroup'),
        'ManageFacility'=>array('field'=>'facilities_permission','desc'=>'Facilities','group'=>'FacilityGroup'),
        'ManageRole'=>array('field'=>'roles_permission','desc'=>'Roles','group'=>'RoleGroup'),
        'ManageUser'=>array('field'=>'users_permission','desc'=>'Users','group'=>'UserGroup'),
        'ManageProximityCard'=>array('field'=>'proximitycard_permission','desc'=>'Proximity Card','group'=>'ProximityCardGroup'),
        'ManageEMF'=>array('field'=>'emf_permission','desc'=>'EMF','group'=>'EMFGroup'),
        'ManageDCAccess'=>array('field'=>'dcaccess_permission','desc'=>'DC Access','group'=>'DCAccessGroup'),
        'ManageTelcoAccess'=>array('field'=>'telcoaccess_permission','desc'=>'Telco Access','group'=>'TelcoAccessGroup'),
        'ManageRemoteHands'=>array('field'=>'remotehands_permission','desc'=>'Remote Hands','group'=>'RemoteHandsGroup'),
        'ManagePost'=>array('field'=>'bulletinboard_permission','desc'=>'Bulletin Board','group'=>'BulletinBoardGroup'),
        'ManageLibraryContent'=>array('field'=>'library_permission','desc'=>'Library','group'=>'LibraryGroup'),
        'ManageReport'=>array('field'=>'report_permission','desc'=>'Report','group'=>'ReportGroup')
        );
    public $roles = array();
    public $allowed_permissions = array();
    public $assigned_permissions = array();
    public $permission_options = array();
    public $temp_permissions = array();
    public $portallogin_permission;
    public $portalconfiguration_permission;
    public $customers_permission;
    public $facilities_permission;
    public $roles_permission;
    public $users_permission;
    public $proximitycard_permission;
    public $emf_permission;
    public $dcaccess_permission;
    public $telcoaccess_permission;
    public $remotehands_permission;
    public $bulletinboard_permission;
    public $library_permission;
    public $report_permission;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AuthItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'AuthItem';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('name', 'required', 'on' => 'admin-create'),
            array('type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 64),
            array('name', 'unique'),
            array('description, bizrule, data', 'safe'),
            array('allowed_permissions','validateTempPermissions'),
            array('type', 'default', 'value' => 2, 'on' => 'admin-create'),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('name, type, description, bizrule, data', 'safe', 'on' => 'search'),
            array('name, type, description, allowed_permissions, portallogin_permission, portalconfiguration_permission, customers_permission, facilities_permission, roles_permission,
                users_permission, proximitycard_permission, emf_permission, dcaccess_permission, telcoaccess_permission, remotehands_permission,
                bulletinboard_permission, library_permission, report_permission','safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'authAssignments' => array(self::HAS_MANY, 'AuthAssignment', 'itemname'),
            'authItemChildren' => array(self::HAS_MANY, 'AuthItemChild', 'parent'),
            'authItemChildren1' => array(self::HAS_MANY, 'AuthItemChild', 'child'),
                //'users' => array(self::HAS_MANY, 'User', 'user_role')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
            'allowed_permissions'=>'Permissions',
            'portallogin_permission' => "Portal Login",
            'portalconfiguration_permission'=>'Portal Configuration',
            'customers_permission' => "Customers",
            'facilities_permission'=>'Facilities',
            'users_permission' => "Users",
            'proximitycard_permission' => "Proximity Card",
            'emf_permission' => "EMF",
            'dcaccess_permission' => "DC Access",
            'telcoaccess_permission' => "Telco/carrier Access",
            'remotehands_permission' => "Remote Hands",
            'bulletinboard_permission' => "Bulletin Board",
            'library_permission' => "Library",
            'report_permission' => "Report",
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('bizrule', $this->bizrule, true);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {

        // set default description, this happend for admin-create scenario only, 
        // because the user does not provide description
        if ($this->scenario == 'admin-create') {
            // set description to the name specified by the user, ucwords capitalize the first letter
            
            $this->name = ucwords($this->name);
            $this->description = 'Custom Role - '.$this->name;
        }

        return parent::beforeSave();
    }

    public function afterSave() {

        if ($this->scenario == 'admin-create') {
            $this->assignTemporaryPermissions();
        }

        return parent::afterSave();
    }

    public function scopes() {
        return array(
            'userRoleForList' => array(
                'select' => "name, description",
                'condition' => "type = 2",
                'order' => ''
            ),
            'facAdminRoleForList' => array(
                'select' => "name, description",
                'condition' => "name = 'Cust_Admin' OR name = 'Cust_User'",
                'order' => ''
            ),
            'userOperationForList' => array(
                'select' => "name, description",
                'condition' => "type = 0",
                'order' => ''
            ),
        );
    }

// end function scope

    public function getAllRoles() {

        $results = AuthItem::model()->findAllByAttributes(array('type' => 2));

        if (empty($results)) {
            $roles = array();
        } else {
            $roles = array();
            foreach ($results as $row) {
                $roles[$row->name] = $row->description;
            }
        }

        return $roles;
    }

    public function getAllOperations() {

        $results = AuthItem::model()->findAllByAttributes(array('type' => 0));
        //$results = AuthItem::model()->findAll('type = 0 OR type = 1');

        if (empty($results)) {
            $operations = array();
        } else {
            $operations = array();
            foreach ($results as $row) {
                $operations[$row->name] = $row->description;
            }
        }

        return $operations;
    }

    public function getChildItem($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "'";
        $childs = $cmd->query();

        if (empty($childs)) {
            $childs = array();
        }

        return $childs;
    }

    public function getChildRoles($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "' AND i.type = 2";
        $cmd->order = "i.precedence asc";
        $roles = $cmd->query();

        if (empty($roles)) {
            $roles = array();
        }

        return $roles;
    }

    public function getChildOperations($parent) {

        $cmd = Yii::app()->db->createCommand();
        $cmd->select = "i.name, i.type, i.description, c.parent, c.child";
        $cmd->from('AuthItem as i');
        $cmd->join('AuthItemChild as c', 'c.child=i.name');
        $cmd->where = "c.parent = '" . $parent . "' AND (i.type = 0 OR i.type = 1)";
        $operations = $cmd->query();

        if (empty($operations)) {
            $operations = array();
        }
        return $operations;
    }


    // -------------------------------------------------------------------------------------------------
    // start of code that is tested and used codes
    // set assigned permissions base on role,
    // and set permission choices
   
    public function setAssignedPermissions($role)
    {   
        // get assigned permissions
        $items = $this->getAuthItemChildren($role);
        $permissions = array();

        foreach($items as $code => $description){
            // set individual permissions
            if (array_key_exists($code, $this->permission_list)) {
                $permissions[$code] = $description;
            }
        }            

        $this->assigned_permissions = $permissions;

        $this->setPermissionOptions();
    }    

    // set permission options for choices
    // permission_list is define above
    public function setPermissionOptions()
    {
        
        foreach($this->permission_list as $code => $detail){
                // set individual permissions
                if (array_key_exists($code, $this->assigned_permissions)) {
                    // assign the child items to the corresponding member variables of the given permissions
                   $this->permission_options[$detail['group']] = $this->getAuthItemChildren($detail['group']);
                }   
         }
    }

    // get the child items of a given auth item, role/task/operation
    public function getAuthItemChildren($itemname) {

        $items = array();
        $auth = Yii::app()->authManager;

        $inherited_items = $auth->getItemChildren($itemname);

        foreach ($inherited_items as $item) {
            $items[$item->name] = $item->description;
        }

        return $items;
    }

    // save permission and options to sessions temporarily
    // while the user is still assigning permissions
    public function saveTempPermission($permission, $options){

        $temp_permissions = Yii::app()->session->itemAt('temp_permissions'); // gets value

        if($temp_permissions == null){
            $temp_permissions = array();
        }

        $temp_permissions[$permission] = $options;

        Yii::app()->session->add('temp_permissions', $temp_permissions);
        $this->temp_permissions = $temp_permissions;
    }

    //validate if session temp_permissions has content
    public function validateTempPermissions($value='')
    {
        $temp_permissions = Yii::app()->session->itemAt('temp_permissions'); // gets value

        if($temp_permissions == null){
            $this->addError('allowed_permissions', 'There are no permissions assigned.');
        }
    }

    // the temporary model var for permission to be used on dropdown inline edit
    public function setTemporaryPermissions()
    {
        $temp_permissions = Yii::app()->session->itemAt('temp_permissions'); // gets value

        if($temp_permissions == null){
            $temp_permissions = array();
        }

        // loop thru the 
        foreach($this->permission_list as $permission => $detail){

            if (array_key_exists($permission, $temp_permissions)){

                $field = $detail['field'];
                $temp_value = $temp_permissions[$permission];
                // for checklist inline edit
                if(is_array($temp_value)){
                    $value = implode(',', $temp_value);
                }else{
                    // for select inline edit
                    $value = $temp_value;
                }   

                $this->$field = $value;
            }
        }

    }

    // save permissions set in create role dropdown inline edit and stored on temp_permission sessions
    // into the database table authitemchild
    public function assignTemporaryPermissions(){

        $auth = Yii::app()->authManager;

        $temp_permissions = Yii::app()->session->itemAt('temp_permissions'); // gets value

        foreach($temp_permissions as $permission => $permission_options){
            //$auth->addItemChild($this->name, $permission);

            if(is_array($permission_options)){
                foreach($permission_options as $new_permission){
                    $auth->addItemChild($this->name, $new_permission);
                }
            }else{
               $auth->addItemChild($this->name, $permission_options);
            }
        }    
    }

    public function setAllRoles()
    {
        
        $results = AuthItem::model()->findAllByAttributes(array('type'=>2));
        $roles = array();

        if(!empty($results)){

            foreach($results as $role){
                $role->setPermissionItems($role->name);
                $roles[] = $role;
            }

            $this->roles = $roles;
        }

    }

    public function getPermissionOptions($type)
    {
        if(array_key_exists($type, $this->permission_options)){
            return $this->permission_options[$type];
        }else{
            return array();
        }
    }

    public function updatePermissions($role, $permission_type, $permissions){

        $auth = Yii::app()->authManager;

        foreach($permissions as $val){
            // item is not yet assigned as child, set the permission item as child
            if(!$auth->hasItemChild($role, $val)){
                $auth->addItemChild($role, $val);
            }
        }    
    }

    public function setPermissionItems($role)
    {   
         $auth = Yii::app()->authManager;

        // set allowed permission items
        foreach($this->permission_list as $permission => $detail){
            $field = $detail['field'];
            $group = $detail['group'];

            $group_child_items = $this->getAuthItemChildren($group);

            //var_dump($group_child_items);

            if($auth->hasItemChild($role, $group)){
                // set all group items as permission, 
                // required by inline edit to separate default value for checklist into comma
                $this->$field = implode(',',array_keys($group_child_items));
            }else{
                $items = array();
                foreach($group_child_items as $name => $desc){
                    if($auth->hasItemChild($role, $name)){
                        $items[] = $name;
                    }
                 $this->$field = implode(',',$items);
                }
            }

        }
    }

    // set inherited items
    public function setAuthInheritedItems($itemname, &$items, $type = null, $data = '') {

        $auth = Yii::app()->authManager;

        $inherited_items = $auth->getItemChildren($itemname);

        foreach ($inherited_items as $item) {

            if ($type !== null && $data !== '' && $item->type == $type && strpos($item->data, $data) !== false) {
                $items[$item->name] = $item->description; 
            } else if ($data ==='' && $type !== null && $item->type == $type) {
                $items[$item->name] = $item->description;
            } else if ($type === null && $data !== '' && strpos($item->data, $data) !== false) {
                $items[$item->name] = $item->description;
            } else if ($type === null && $data === '') {
                $items[$item->name] = $item->description;
            }

            $this->setAuthInheritedItems($item->name, $items, $type, $data);
        }
    }

}