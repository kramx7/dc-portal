<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class WorkItem extends CFormModel
{
	

	public $type;
	public $id;
	public $item;
	public $description;
	public $url;
	public $status;
	public $user_id;
	public $facilities;
	public $items = array();

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			array('username', 'email'),
                        //array('password','length','min'=>8),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'type'=>'Type',
			'id', 'ID',
			'description'=>'Description',
			'url'=>'URL',
			'status'=>'Status'
		);
	}

	public function setWorkItems()
	{
		$items = array();

		$notices = $this->getNoticeboardItems();
		$items = array_merge($items, $notices);

		$access = $this->getFacilityAccess();
		$items = array_merge($items, $access);
		
		$proximity = $this->getProximityCard();
		$items = array_merge($items, $proximity);
		
		$equipment = $this->getEquipmentMovement();
		$items = array_merge($items, $equipment);

		$this->items = $items;

	}

	public function getNoticeboardItems()
	{
		$requests = new Noticeboard;
        $requests->unsetAttributes();  // clear any default values

        $requests->creator_id = $this->user_id;
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $requests->facilities = $this->facilities;

        
        $params["post"] = $requests;
        $submit_permission = User::model()->isAuthorized('SubmitPost',$params);
        $approve_permission = User::model()->isAuthorized('ApprovePost',$params);

        $requests->submit_permission = $submit_permission;
        $requests->approve_permission = $approve_permission;

        $notices = $requests->getWorkItems();

        return $notices;

	}

	public function getFacilityAccess()
	{
		$requests = new AccessRequest;
        $requests->unsetAttributes();  // clear any default values

        $requests->creator_id = $this->user_id;
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $requests->facilities = $this->facilities;
        
        $params["request"] = $requests;
        $submit_permission = User::model()->isAuthorized('SubmitDCAccess',$params);
        $approve_permission = User::model()->isAuthorized('ApproveDCAccess',$params);

        $requests->submit_permission = $submit_permission;
        $requests->approve_permission = $approve_permission;

        $access = $requests->getWorkItems();

        return $access;

	}

	public function getProximityCard()
	{
		$requests = new ProximityCardRequest;
        $requests->unsetAttributes();  // clear any default values

        $requests->creator_id = $this->user_id;
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $requests->facilities = $this->facilities;
        
        $params["request"] = $requests;
        $submit_permission = User::model()->isAuthorized('SubmitProximityCard',$params);
        $approve_permission = User::model()->isAuthorized('ApproveProximityCard',$params);

        $requests->submit_permission = $submit_permission;
        $requests->approve_permission = $approve_permission;

        $access = $requests->getWorkItems();

        return $access;

	}

	public function getEquipmentMovement()
	{
		$requests = new EquipmentMovement;
        $requests->unsetAttributes();  // clear any default values

        $requests->creator_id = $this->user_id;
        // set search user to filter results to facility for facility administrator, customer admin and customer user
        $requests->facilities = $this->facilities;
        
        $params["request"] = $requests;
        $submit_permission = User::model()->isAuthorized('SubmitEMF',$params);
        $approve_permission = User::model()->isAuthorized('ApproveEMF',$params);

        $requests->submit_permission = $submit_permission;
        $requests->approve_permission = $approve_permission;

        $access = $requests->getWorkItems();

        return $access;

	}

	
}
