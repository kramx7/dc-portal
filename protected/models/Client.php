<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $record_id
 * @property integer $client_id
 * @property string $client_name
 * @property string $modified_time
 */
class Client extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Client the static model class
     */
    public $facility_id = null;
    public $facilities = array();
    public $temp_facilities = array();
    public $clients = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'client';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_id, client_name', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('client_name', 'length', 'max' => 50),
            array('modified_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, client_id, client_name, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'facility' => array(self::MANY_MANY, 'Client','client_facility(client_id, facility_id)')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'client_name' => 'Customer Name',
            'modified_time' => 'Modified Time',
            'facility_id' => 'Facility'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('client_name', $this->client_name, true);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function scopes() {

        $scopes = array(
            'clientForList' => array(
                'select' => 'id, client_name',
                'order' => 'client_name ASC'
            )
        );

        if ($this->facility_id != null) {
            $scopes['clientForList'] = array(
                'select' => 'client.id as client_id, client.client_name',
                'condition' => "client_facility.facility_id = " . $this->facility_id,
                'alias' => 'client',
                'join' => 'LEFT JOIN client_facility on client.id = client_facility.client_id',
                'order' => 'client.client_name ASC',
            );
        }

        return $scopes;
    }

    function beforeSave() {

        if($this->isNewRecord ){

            $client = Client::model()->find(array('order'=>'id desc','limit'=>'1'));

            if(!empty($client)){
                // add one to last id, since id is not autoincrement
                $this->id = $client->id + 1;
            }
        }

        return parent::beforeSave();
    } 

    function afterSave() {

        if($this->isNewRecord ){

            $client_facility= new ClientFacility;
            $client_facility->client_id = $this->id;
            $client_facility->facility_id = $this->facility_id;
            $client_facility->save();
        }

        return parent::afterSave();
    }   

    public function getClients() {

        $criteria = new CDbCriteria;
        //$criteria->with = array('itemname0');
        //$criteria->condition = " t.userid = ".$user_id." AND itemname0.type = 2";
        
        if(empty($this->facility_id)){
            //$clients = Client::model()->findAll();
            $clients = array();
        }else{
            $clients = Client::model()->with('facility')->findAll(
                array('condition' => 'facility_id=:facility_id', 'params' => array(':facility_id' => $this->facility_id))
                );
        }

        if (empty($clients)) {
            $clients = array();
        }

        return $clients;
    }

    public function getClientById($client_id) {

        $client = Client::model()->findByAttributes(array('id' => $client_id));

        if (!empty($client)) {
            return $client;
        } else {
            return null;
        }
    }

    public function setFacilities($user_id)
    {
        //$this->facilities = Facility::model()->getFacilities();
        //$this->facilities = UserFacility::model()->getFacilities($user_id);
        $this->facilities = UserTaskFacility::model()->getuserFacilities($user_id);
    }

    public function setTempFacilities()
    {   
        foreach($this->facilities as $code => $name){// $row){
           // $this->temp_facilities[$row->id] = $row->facility_name;
            $this->temp_facilities[$code] = $name;
        }
    }

    public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

    

}