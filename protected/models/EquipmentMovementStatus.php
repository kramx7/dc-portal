<?php

/**
 * This is the model class for table "equipment_movement_status".
 *
 * The followings are the available columns in table 'equipment_movement_status':
 * @property integer $status
 * @property string $status_description
 *
 * The followings are the available model relations:
 * @property EquipmentMovement[] $equipmentMovements
 */
class EquipmentMovementStatus extends CActiveRecord
{
	const STATUS_NEW = 0;
	const STATUS_DRAFT = 1;
	const STATUS_SUBMITTED = 2;
	const STATUS_APPROVED = 3;
	const STATUS_CORRECTION = 4;
	const STATUS_RECEIVED = 5;
	const STATUS_COMPLETED = 6;
	const STATUS_DELETED = 255;


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EquipmentMovementStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'equipment_movement_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, status_description', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('status_description', 'length', 'max'=>48),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('status, status_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'equipmentMovements' => array(self::HAS_MANY, 'EquipmentMovement', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'status' => 'Status',
			'status_description' => 'Status Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('status',$this->status);
		$criteria->compare('status_description',$this->status_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getStatusDescription($status_id)
	{
		$result = EquipmentMovementStatus::model()->findByPk($status_id);

		if(empty($result)){
			return '';
		}else{
			return $result->status_description;
		}
	}
}