<?php

/**
 * This is the model class for table "access_request_entry_type".
 *
 * The followings are the available columns in table 'access_request_entry_type':
 * @property integer $entry_type
 * @property string $entry_description
 *
 * The followings are the available model relations:
 * @property AccessRequestEntry[] $accessRequestEntries
 */
class AccessRequestEntryType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccessRequestEntryType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_request_entry_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entry_type, entry_description', 'required'),
			array('entry_type', 'numerical', 'integerOnly'=>true),
			array('entry_description', 'length', 'max'=>48),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('entry_type, entry_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessRequestEntries' => array(self::HAS_MANY, 'AccessRequestEntry', 'entry_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entry_type' => 'Entry Type',
			'entry_description' => 'Entry Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entry_type',$this->entry_type);
		$criteria->compare('entry_description',$this->entry_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}