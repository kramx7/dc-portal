<?php

/**
 * SystemConfigurationForm class.
 * SystemConfigurationForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class SystemConfigurationForm extends CFormModel
{
	public $file = 'system-configuration.ini';
	public $default_priority;
	public $priority_list = array();
	public $locked_limit;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
           	array('default_priority, locked_limit','numerical','integerOnly'=>true,'min'=>0),
			// password needs to be authenticated
			array('default_priority, locked_limit', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'default_priority'=>'Default BB Post Priority',
			'locked_limit', 'Login Failed Attempt Limit',
		);
	}

	
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function read()
	{
		if ( !$settings = parse_ini_file($this->file, TRUE) ) 
        throw new exception('Unable to open ' . $this->file);
    	
	   	$this->attributes = $settings['configuration'];

	   	// set options
	   	$this->priority_list = NoticeboardMessagePriority::model()->getPriorityList();

	}

	public function write()
	{
		$res = array();
	    $res[] = '[configuration]';
	    $res[] = 'default_priority='.$this->default_priority;
	    $res[] = 'locked_limit='.$this->locked_limit;

	    file_put_contents($this->file, implode("\r\n", $res));
	}
}
