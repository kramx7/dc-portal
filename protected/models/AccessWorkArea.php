<?php

/**
 * This is the model class for table "access_work_area".
 *
 * The followings are the available columns in table 'access_work_area':
 * @property integer $area_type
 * @property string $area_description
 *
 * The followings are the available model relations:
 * @property AccessRequest[] $accessRequests
 */
class AccessWorkArea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccessWorkArea the static model class
	 */
	
	const AREA_DATAHALL = 0;
    const AREA_SITETOUR = 1;
    const AREA_MEETINGROOM = 2;
    const AREA_NODE = 3;
    const AREA_MDF = 4;
    const AREA_CLIENTAREA = 5;
    const AREA_PLANT = 6;
    const AREA_OTHER = 7; 

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_work_area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('area_type, area_description', 'required'),
			array('area_type', 'numerical', 'integerOnly'=>true),
			array('area_description', 'length', 'max'=>48),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('area_type, area_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessRequests' => array(self::HAS_MANY, 'AccessRequest', 'workarea'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'area_type' => 'Area Type',
			'area_description' => 'Area Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('area_type',$this->area_type);
		$criteria->compare('area_description',$this->area_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getWorkAreas()
	{
		$result = AccessWorkArea::model()->findAll();

		if(empty($result)){
			return array();
		}else{
			$workareas = array();
			foreach($result as $row){
				$workareas[$row->area_type] = $row->area_description;
			}
			return $workareas;
		}

	}
}