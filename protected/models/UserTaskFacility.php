<?php

/**
 * This is the model class for table "user_task_facility".
 *
 * The followings are the available columns in table 'user_task_facility':
 * @property string $facility_id
 * @property string $user_id
 * @property string $task
 *
 * The followings are the available model relations:
 * @property User $user
 * @property AuthItem $task0
 */
class UserTaskFacility extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserTaskFacility the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_task_facility';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_id, task', 'required'),
            array('facility_id', 'length', 'max' => 11),
            array('user_id', 'length', 'max' => 10),
            array('task', 'length', 'max' => 64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('facility_id, user_id, task', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'task0' => array(self::BELONGS_TO, 'AuthItem', 'task'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'facility_id' => 'Facility',
            'user_id' => 'User',
            'task' => 'Task',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('facility_id', $this->facility_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('task', $this->task, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getAssignFacilitiesByUserIdAndTask($user_id, $task = '', $override_task = '', $allTask = 'AssociateAllFacility') {

        if (User::model()->isAuthorized($allTask) ){

            $results = Facility::model()->findAll();

            if (empty($results)) {
                $facilities = array();
            } else {
                $facilities = array();
                foreach ($results as $row) {
                    $facilities[$row->id] = $row->facility_name;
                }
            }
        } else {

            $cmd = Yii::app()->db->createCommand();
            $cmd->select = "f2.id as facility_id, f2.facility_name";
            $cmd->from('user_task_facility as f1');
            $cmd->join('facility as f2', 'f1.facility_id=f2.id');
            $cmd->where = "f1.userid = '" . $user_id . "'";
            //$cmd->addCondition('f1.userid',$user_id);
            if (!empty($task)) {
              $cmd->where = "f1.user_id = " . $user_id."  AND (f1.task = '" . $task . "' OR f1.task = '".$override_task."')";
              //$cmd->addCondition('f1.task',$task);
            }else{
                $cmd->where = "f1.user_id = " . $user_id;
            }
           

            $_facilities = $cmd->query();

            if (empty($_facilities)) {
                $facilities = array(); // to avoid error on foreach
            } else {
                $facilities = array();
                foreach ($_facilities as $row) {
                    $facilities[$row['facility_id']] = $row['facility_name'];
                }
            }
        }

        return $facilities;
    }

    function addUserTasks($user_id, $facility_id, $tasks) {

        if (!is_array($tasks) || count($tasks) <= 0 || $facility_id == 0) {
            return;
        }

        foreach ($tasks as $code => $desc) {

            if (is_numeric($code)) {
                //task array that no complete code and 
                $code = $desc;
            }

            if ($code == 'temp' || empty($code)) {
                continue;
            }
            
            if(!$this->isUserTaskAssigned($user_id, $facility_id, $code)){

                $newtask = new UserTaskFacility;
                $newtask->facility_id = $facility_id;
                $newtask->user_id = $user_id;
                $newtask->task = $code;
                $newtask->save();
            }
        }
    }

    function addUserTask($user_id, $facility_id, $task) {

        if($facility_id == 0){
            return;
        }

        if(!$this->isUserTaskAssigned($user_id, $facility_id, $task)){

            $newtask = new UserTaskFacility;
            $newtask->facility_id = $facility_id;
            $newtask->user_id = $user_id;
            $newtask->task = $task;
            $newtask->save();
        }
    }

    function deleteUserTaskByUserIdAndFacilityIdAndTask($user_id, $facility_id, $task) {

        UserTaskFacility::model()->deleteAllByAttributes(array('user_id' => $user_id, 'facility_id' => $facility_id, 'task' => $task));
    }

    function deleteUserTaskByUserIdAndFacilityId($user_id, $facility_id) {

        UserTaskFacility::model()->deleteAllByAttributes(array('user_id' => $user_id, 'facility_id' => $facility_id));
    }

    function deleteUserTaskByUserId($user_id) {

        UserTaskFacility::model()->deleteAllByAttributes(array('user_id' => $user_id));
    }

    function isUserTaskAssigned($user_id, $facility_id, $task) {
        
        $result = UserTaskFacility::model()->findByAttributes(array('user_id' => $user_id, 'facility_id' => $facility_id, 'task' => $task));

        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    public function getPrimaryFacility($user_id){
        
         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "f2.id as facility_id, f2.facility_name";
         $cmd->from('user_task_facility as f1');
         $cmd->join('facility as f2', 'f1.facility_id=f2.id');
         $cmd->where = "f1.user_id = '".$user_id."' AND f1.task = 'AssociateFacility' ";
         $result = $cmd->query();
        
        if(empty($result)){
            return 0;
        }else{    
            foreach($result as $row){
                return $row['facility_id'];
            }
        }
    }

    public function getUserFacilities($user_id){
        
         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "f2.id as facility_id, f2.facility_name";
         $cmd->from('user_task_facility as f1');
         $cmd->join('facility as f2', 'f1.facility_id=f2.id');
         $cmd->where = "f1.user_id = '".$user_id."' AND f1.task = 'AssociateFacility' ";
         $cmd->group = "f1.facility_id";
         $result = $cmd->query();
        
        if(empty($result)){
            $facilities = array();// to avoid error on foreach
        }else{
            $facilities = array();    
            foreach($result as $row){
                $facilities[$row['facility_id']] = $row['facility_name'];
            }
        }
        
        return $facilities;
    }

}

//704cc99c11a6f7bbc949246c453418a3405df2ab8d848e617f6731b69eafe9ee3ca25f2b38d7593ec586c92660f9af4fc90debd419cdec8ef77e82b03d005106