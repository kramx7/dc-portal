<?php

/**
 * This is the model class for table "proximity_card_request".
 *
 * The followings are the available columns in table 'proximity_card_request':
 * @property string $id
 * @property string $facility_id
 * @property string $client_id
 * @property string $user_id
 * @property integer $status
 * @property string $comments
 * @property string $submitted
 * @property string $submitted_by
 * @property string $approved
 * @property string $approved_by
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Client $client
 * @property Facility $facility
 * @property User $submittedBy
 * @property User $approvedBy
 * @property ProximityCardRequestStatus $status0
 */
class ProximityCardRequest extends CActiveRecord
{
	public $creator_id;
	public $facilities;
	public $validity_months = array(1=>'January','February','March','April','May','June','July','August','September','October','November','December');
	public $types = array(1=>'Add','Delete','Change');
	public $approve_permission;
    public $submit_permission;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProximityCardRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proximity_card_request';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('facility_id, client_id, user_id, validity, type, company, area', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('facility_id, client_id, user_id, submitted_by, approved_by', 'length', 'max'=>10),
			array('comments', 'length', 'max'=>512),
			array('submitted, approved', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, facility_id, client_id, user_id, status, comments, submitted, submitted_by, approved, approved_by,
				validity, type, company, area', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'facility' => array(self::BELONGS_TO, 'Facility', 'facility_id'),
			'submittedBy' => array(self::BELONGS_TO, 'User', 'submitted_by'),
			'approvedBy' => array(self::BELONGS_TO, 'User', 'approved_by'),
			'status0' => array(self::BELONGS_TO, 'ProximityCardRequestStatus', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'facility_id' => 'Facility',
			'client_id' => 'Client',
			'user_id' => 'User',
			'status' => 'Status',
			'comments' => 'Comments',
			'submitted' => 'Submitted',
			'submitted_by' => 'Submitted By',
			'approved' => 'Approved',
			'approved_by' => 'Approved By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('facility_id',$this->facility_id,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('submitted',$this->submitted,true);
		$criteria->compare('submitted_by',$this->submitted_by,true);
		$criteria->compare('approved',$this->approved,true);
		$criteria->compare('approved_by',$this->approved_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function beforeSave()
    {
        if($this->status == ProximityCardRequestStatus::STATUS_SUBMITTED){
        	
        	$this->submitted = date('Y-m-d H:i:s');

        	if(User::model()->isAuthorized('ApproveProximityCard',array('request'=>new ProximityCardRequest)) ){
				$this->status = ProximityCardRequestStatus::STATUS_APPROVED;
            }
        }

        if($this->status == ProximityCardRequestStatus::STATUS_APPROVED){
			$this->approved_by = $this->submitted_by;
			$this->approved = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
     }

     public function getWorkItems() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        
        if(count($this->facilities)>0){
            $facility_ids = array_keys($this->facilities);
            $criteria->addInCondition('facility_id',$facility_ids);
        }

        // limit base on approve and submit permission
        if($this->approve_permission){
            // if user can approve
            $criteria->addInCondition('status', array(ProximityCardRequestStatus::STATUS_DRAFT,ProximityCardRequestStatus::STATUS_SUBMITTED, ProximityCardRequestStatus::STATUS_APPROVED));
        }else{
            $criteria->compare('submitted_by', $this->creator_id);
            $criteria->addInCondition('status', array(ProximityCardRequestStatus::STATUS_DRAFT, ProximityCardRequestStatus::STATUS_CORRECTION, ProximityCardRequestStatus::STATUS_APPROVED));
        }


        $results = ProximityCardRequest::model()->findAll($criteria);
        $items = array();
        
        foreach($results as $row){
            $workitem = new WorkItem;
            $workitem->id = $row->id;
            $workitem->type = 'Proximity Card';
            $workitem->item = 'Proximity Card';//$row->company;
            $workitem->description = $row->comments;
            $workitem->status = ProximityCardRequestStatus::model()->getStatusDescription($row->status);
            $workitem->url = '/request/updateproximity/'.$row->id;
            $items[] = $workitem;
        }    

        return $items;
    }

     public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }   	


}