<?php

/**
 * This is the model class for table "noticeboard_message_priority".
 *
 * The followings are the available columns in table 'noticeboard_message_priority':
 * @property integer $message_priority
 * @property string $message_priority_description
 */
class NoticeboardMessagePriority extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NoticeboardMessagePriority the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'noticeboard_message_priority';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message_priority, message_priority_description', 'required'),
			array('message_priority', 'numerical', 'integerOnly'=>true),
			array('message_priority_description', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('message_priority, message_priority_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message_priority' => 'Message Priority',
			'message_priority_description' => 'Message Priority Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message_priority',$this->message_priority);
		$criteria->compare('message_priority_description',$this->message_priority_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function scopes() {
		return array(
				'PriorityList' => array(
					'select' => 'message_priority, message_priority_description',
					'order' => 'message_priority ASC'
				)
		);
	}

	public function getPriorityList()
	{
		$records = NoticeboardMessagePriority::model()->findAll();
		$priorities = array();

		foreach($records as $row){
			$priorities[$row->message_priority] = $row->message_priority_description;
		}

		return $priorities;
	}
}