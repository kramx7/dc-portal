<?php

/**
 * This is the model class for table "user_client".
 *
 * The followings are the available columns in table 'user_client':
 * @property string $userid
 * @property integer $client
 */
class UserClient extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserClient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('client', 'numerical', 'integerOnly'=>true),
			array('userid', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, client', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user'=>array(self::BELONGS_TO, 'User', 'userid'),
                    'client'=>array(self::BELONGS_TO, 'Client', 'client')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'client' => 'Client',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('client',$this->client);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function getClients($user_id){

         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "c2.id as client_id, c2.client_name";
         $cmd->from('user_client as c1');
         $cmd->join('client as c2', 'c1.client=c2.id');
         $cmd->where("c1.userid = '".$user_id."'");
         $result = $cmd->query();
        
        if(empty($result)){	
            $clients = array();// to avoid error on foreach
        }else{
            $clients = array();  
            foreach($result as $row){
                $clients[$row['client_id']] = $row['client_name'];
            }
        }

        return $clients;
    }
    
    public function addClient($user_id, $clients){
        
        if($clients == null || count($clients) <= 0){
            return;
        }
        
        foreach($clients as $client_id){
            $client = new UserClient;
            $client->userid = $user_id;
            $client->client = $client_id;
            $client->save();
        }
    }
    
    public function deleteClients($user_id){
        
        $criteria = new CDbCriteria();
        $criteria->condition = 'userid=:userid';
        $criteria->params = array(':userid'=>$user_id);
        $model = UserClient::model()->deleteAll($criteria);
    }

    public function getClient($user_id){

         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "c2.id as client_id, c2.client_name";
         $cmd->from('user_client as c1');
         $cmd->join('client as c2', 'c1.client=c2.id');
         $cmd->where("c1.userid = '".$user_id."'");
         $result = $cmd->query();
        
        if(empty($result)){	
            return 0;
        }else{
            foreach($result as $row){
                return $row['client_id'];
            }
        }
    }
}