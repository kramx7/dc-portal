<?php

/**
 * This is the model class for table "user_facility".
 *
 * The followings are the available columns in table 'user_facility':
 * @property string $userid
 * @property integer $facility
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Facility $facility0
 */
class UserFacility extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserFacility the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_facility';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('facility', 'numerical', 'integerOnly'=>true),
			array('userid', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, facility', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userid'),
			'facility1' => array(self::BELONGS_TO, 'Facility', 'facility'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'User Id',
			'facility' => 'Facility',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('facility',$this->facility);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function getFacilities($user_id){
        
         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "f2.id as facility_id, f2.facility_name";
         $cmd->from('user_facility as f1');
         $cmd->join('facility as f2', 'f1.facility=f2.id');
         $cmd->where = "f1.userid = '".$user_id."'";
         $cmd->group = "f1.facility";
         $result = $cmd->query();
        
        if(empty($result)){
            $facilities = array();// to avoid error on foreach
        }else{
            $facilities = array();    
            foreach($result as $row){
                $facilities[$row['facility_id']] = $row['facility_name'];
            }
        }
        
        return $facilities;
    }
    
    public function addFacility($user_id, $facilities){
        
        if($facilities == null || count($facilities) <= 0){
            return;
        }
        
        foreach($facilities as $facility_id){
            $facility = new UserFacility;
            $facility->userid = $user_id;
            $facility->facility = $facility_id;
            $facility->save();
        }
    }
    
    public function deleteFacilities($user_id){
        
        $criteria = new CDbCriteria();
        $criteria->condition = 'userid=:userid';
        $criteria->params = array(':userid'=>$user_id);
        $model = UserFacility::model()->deleteAll($criteria);
    }

    public function getFacility($user_id){
        
         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "f2.id as facility_id, f2.facility_name";
         $cmd->from('user_facility as f1');
         $cmd->join('facility as f2', 'f1.facility=f2.id');
         $cmd->where = "f1.userid = '".$user_id."'";
         $result = $cmd->query();
        
        if(empty($result)){
            return 0;
        }else{    
            foreach($result as $row){
                return $row['facility_id'];
            }
        }
    }

}