<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserUpdateForm extends CFormModel {

    public $search_user = null;
    public $assignment_operation = null;
    public $selected_user = null;
    public $users = array();
    public $action = null;
    public $assigned_item = array();
    public $unassigned_item = array();
    public $assigned_items = array();
    public $unassigned_items = array();
    public $unassigned_masterlist = array();
    public $assignment_list = array('Roles' => 'Roles', 'Operations' => 'Operations', 'Customers' => 'Customers', 'Facilities' => 'Facilities');
    public $_facility;
    public $_facility_options = array();
    public $_client;
    public $_client_options = array();

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('search_user', 'required', 'on' => 'search'),
            array('search_user', 'length', 'min' => 3),
            array('assignment_operation', 'required', 'on' => 'assign, update', 'message' => 'Please select an assignment type.'),
            array('selected_user', 'required', 'on' => 'assign, update', 'message' => 'Please select a user.'),
            //array('sel_ass_item','required','on'=>'update','message'=>'Please select items to assign.'),
            array(
                'search_user, assignment_operation, selected_user, users,
                            action, assigned_items, unassigned_items, _facility, _client', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'search_user' => 'Search User',
            'assignment_operation' => 'Assignment Operation',
            'selected_user', 'Selected User',
            'users' => 'User List',
            'assigned_items' => 'Assigned Items',
            'action' => 'Action',
            'unassigned_items' => 'Unassigned ',
            '_facility' => 'Facility',
            '_client' => 'Customer'
        );
    }

    public function searchUsers() {

        // do not search when search string is invalid
        if (strlen($this->search_user) < 3) {
            $this->users = array();
            return;
        }

        $items = null;

        $users = User::model()->searchUsers($this->search_user);

        // recreate array for uniform key and value
        foreach ($users as $key => $user) {
            $fullname = $user->firstname . ' ' . $user->lastname;

            if (trim($fullname) == '') {
                $fullname = $user->emailaddress;
            }

            $items[$user->id] = $fullname;
        }

        if ($items == null) {
            $items = array();
        }

        $this->users = $items;
    }

    public function setAssignedItems() {

        $assigned_items = array();

        if ($this->selected_user != null && $this->assignment_operation != null) {

            switch ($this->assignment_operation) {
                case 'Roles':
                    // get assigned roles
                    $roles = AuthAssignment::model()->getRoles($this->selected_user);

                    // recreate array for uniform key and value
                    foreach ($roles as $key => $item) {
                        $assigned_items[$item->itemname0->name] = $item->itemname0->description;
                    }
                    break;
                case 'Operations':
                    // get all operations inherited of the user creator, and directly assigned opeations on authassignment
                    $operations = AuthAssignment::model()->getInheritedAndDirectlyAssignedOperations($this->selected_user);

                    foreach ($operations as $key => $item) {
                        $assigned_items[$key] = $item;
                    }
                    break;
            }
        }


        $this->assigned_items = $assigned_items;
    }

    public function setUnassignedItems($creator_id) {

        $unassigned_items = array();
        $masterlist = array();

        if ($this->selected_user != null && $this->assignment_operation != null) {

            switch ($this->assignment_operation) {
                case 'Roles':
                    // get roles that can be assigned, inherited roles of the creator
                    $inherited_roles = AuthAssignment::model()->getInheritedRoles($creator_id);

                    foreach ($inherited_roles as $role => $desc) {
                        if (!in_array($desc, $this->assigned_items)) {
                            $unassigned_items[$role] = $desc;
                        }
                        $masterlist[$role] = $desc;
                    }

                    break;
                case 'Operations':
                    // get all operations inherited of the user creator, and directly assigned opeations on authassignment
                    $operations = AuthAssignment::model()->getInheritedAndDirectlyAssignedOperations($creator_id);

                    // sort the operations alphabetically
                    if (is_array($operations)) {
                        ksort($operations); // retain the keys/index when sorting
                    }

                    foreach ($operations as $op => $desc) {
                        if (!in_array($op, $this->assigned_items)) {
                            $unassigned_items[$op] = $desc;
                        }
                        $masterlist[$op] = $desc;
                    }
                    break;
            }
        }


        $this->unassigned_items = $unassigned_items;
        $this->unassigned_masterlist = $masterlist;
    }

    private function isItemPresent($search, $items, $field) {

        foreach ($items as $key => $item) {
            if ($search == $item->{$field}) {
                return true;
            }
        }

        return false;
    }

    public static function getAssignmentList() {
        return $this->$assignment_list;
    }

    public function updateUser() {

        // only update the user when action is update and validation did not result to errors
        if ($this->scenario != 'update' || $this->hasErrors()) {
            return;
        }

        switch ($this->assignment_operation) {
            case 'Roles':
                AuthAssignment::model()->deleteUserAssignment($this->selected_user);
                AuthAssignment::model()->addUserAssignment($this->selected_user, $this->assigned_items);
                break;
            case 'Operations':
                AuthAssignment::model()->deleteUserAssignment($this->selected_user);
                AuthAssignment::model()->addUserAssignment($this->selected_user, $this->assigned_items);

                // save assign operations to user_task_facility
                if ($this->_facility == '0') {
                    // when associate to all facilities is choosen
                    // add entry for every assigned facility
                    foreach ($this->_facility_options as $id => $desc) {
                        if ($id != 0) {
                            UserTaskFacility::model()->deleteUserTaskByUserIdAndFacilityId($this->selected_user, $id);
                            UserTaskFacility::model()->addUserTask($this->selected_user, $id, $this->assigned_items);
                        }
                    }
                } elseif ($this->_facility != '') {
                    UserTaskFacility::model()->deleteUserTaskByUserIdAndFacilityId($this->selected_user, $this->_facility);
                    UserTaskFacility::model()->addUserTask($this->selected_user, $this->_facility, $this->assigned_items);
                }else{
                    UserTaskFacility::model()->deleteUserTaskByUserId($this->selected_user);
                }

                // save assign operations to user_task_client
                if ($this->_client == '0') {
                    // when associate to all customers is choosen
                    // add entry for every assigned customer
                    foreach ($this->_client_options as $id => $desc) {
                        if ($id != 0) {
                            UserTaskClient::model()->deleteUserTaskByUserIdAndClientId($this->selected_user, $id);
                            UserTaskClient::model()->addUserTask($this->selected_user, $id, $this->assigned_items);
                        }
                    }
                } elseif ($this->_client != '') {
                    UserTaskClient::model()->deleteUserTaskByUserIdAndClientId($this->selected_user, $this->_client);
                    UserTaskClient::model()->addUserTask($this->selected_user, $this->_client, $this->assigned_items);
                }else{
                    UserTaskClient::model()->deleteUserTaskByUserId($this->selected_user);
                }
                break;
        }

        Yii::app()->user->setFlash('info', 'Successfully updated the assign ' . $this->assignment_operation . '.');

        // set assign items
        $this->scenario = 'assign';
        $this->setAssignedItems();
    }

}
