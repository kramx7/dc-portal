<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class GraphForm extends CFormModel {

    public $type = '';
    public $facility_id = 0;
    public $facilities = array();
    public $default_facility = 0;
    public $client_id = 0;
    public $clients = array();
    public $meter = '';
    public $default_meter = 0;
    public $meters = array();
    public $target_id = 0;
    public $sensor = 0;
    public $sensors = array();
    public $from_date = '';
    public $to_date = '';
    public $status = '';
    public $date_format = '';
    public $year = 0;
    public $years = array();
    public $sum_kwh = 0;
    public $average_pue = 0;
    public $sum_total_kwh = 0;
    public $sum_carbon = 0;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('from_date, to_date', 'required'),
            array('type', 'length', 'min' => 3),
            array('meter', 'numerical', 'integerOnly' => true),
            array('from_date, to_date', 'length', 'min' => 10),
            array('facility_id, type, meter, sensor, year, from_date, to_date', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'type' => 'Type',
            'meter', 'Meter',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
        );
    }

    public function setDatesToTimestamp() {

        if ($this->from_date != '') {
            //$temp_date = DateTime::createFromFormat('d/m/Y', $this->from_date)->format('Y-m-d 00:00:00');
            //$this->from_date = strtotime($temp_date);
            $this->from_date = CDateTimeParser::parse($this->from_date, 'dd/MM/yyyy');
        } else {
            if ($this->type == 'kW') {
                $this->from_date = mktime(0, 0, 0, date("n"), date("d"), date("Y"));
            } elseif ($this->type == 'kWh') {
                $this->from_date = mktime(0, 0, 0, date("n") - 1, date("d"), date("Y"));
            } else {
                $this->from_date = mktime(0, 0, 0, date("n"), date("d"), date("Y"));
            }
        }

        if ($this->to_date != '') {
            //$temp_date = DateTime::createFromFormat('d/m/Y', $this->to_date)->format('Y-m-d 23:59:59');
            //$this->to_date = strtotime($temp_date);
            $this->to_date = CDateTimeParser::parse($this->to_date, 'dd/MM/yyyy');
            $this->to_date += ((24*60*60) - 1); // minus 1 for up to 59 seconds
        } else {
            $this->to_date = mktime(23, 59, 59, date('n'), date('d'), date('Y'));
        }

        $this->date_format = 'timestamp';
    }

    public function setDatesToFormat($format = 'd/m/Y') {

        if ($this->date_format == 'timestamp') {
            $this->from_date = date($format, $this->from_date);
            $this->to_date = date($format, $this->to_date);
        }

        $this->date_format = 'date';
    }

    public function isUserInFacility($task) {

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }
    
        return $command->execute() >= 1 ? true : false;
    }

    public function isUserInClient($task) {
        
        return true;
        
        if ($this->client_id == 0) {
            // if no client id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_client WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_client WHERE client_id=:client_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":client_id", $this->client_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

    public function computeNGER($powers, $pues, $carbons) {

        $sum_it_kwh = 0;
        $avg_pue = 0;
        $sum_total_kwh = 0;
        $sum_co2 = 0;

        foreach ($powers as $row) {

            $sum_kwh = $row['kwh'];
            $pue = $pues[$row['day']];
            $efscope2a = $carbons[$row['day']];

            $total_kwh = $sum_kwh * $pue;
            $co2 = ($total_kwh * $efscope2a) / 1000;

            $sum_it_kwh = $sum_it_kwh + $sum_kwh;
            $sum_total_kwh = $sum_total_kwh + $total_kwh;
            $sum_co2 = $sum_co2 + $co2;
        }

        if ($sum_total_kwh > 0) {
            $avg_pue = $sum_total_kwh / $sum_it_kwh;
        } else {
            $avg_pue = 0;
        }
        $this->sum_kwh = $sum_it_kwh;
        $this->average_pue = $avg_pue;
        $this->sum_total_kwh = $sum_total_kwh;
        $this->sum_carbon = $sum_co2;
    }

}
