<?php

/**
 * This is the model class for table "user_task_client".
 *
 * The followings are the available columns in table 'user_task_client':
 * @property string $client_id
 * @property string $user_id
 * @property string $task
 *
 * The followings are the available model relations:
 * @property User $user
 * @property AuthItem $task0
 */
class UserTaskClient extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserTaskClient the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_task_client';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('client_id, task', 'required'),
            array('client_id', 'length', 'max' => 11),
            array('user_id', 'length', 'max' => 10),
            array('task', 'length', 'max' => 64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('client_id, user_id, task', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'task0' => array(self::BELONGS_TO, 'AuthItem', 'task'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'client_id' => 'Client',
            'user_id' => 'User',
            'task' => 'Task',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('client_id', $this->client_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('task', $this->task, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getAssignClientsByUserIdAndTask($user_id, $task = '', $alltask = 'AssociateAllCustomer', $authCheck = true) {

        if (User::model()->isAuthorized($alltask) || $authCheck == false) {
            
            $results = Client::model()->findAll();

            if (empty($results)) {
                $clients = array();
            } else {
                $clients = array();
                foreach ($results as $row) {
                    $clients[$row->id] = $row->client_name;
                }
            }
        } else {
            
            $cmd = Yii::app()->db->createCommand();
            $cmd->select = "c2.id as client_id, c2.client_name";
            $cmd->from('user_task_client as c1');
            $cmd->join('client as c2', 'c1.client_id=c2.id');
            $cmd->where = "c1.user_id = '" . $user_id . "'";
            if (!empty($task)) {
                $cmd->andWhere("c1.task = '" . $task . "'");
            }
            $_clients = $cmd->query();

            if (empty($_clients)) {
                $clients = array(); // to avoid error on foreach
            } else {
                $clients = array();
                foreach ($_clients as $row) {
                    $clients[$row['client_id']] = $row['client_name'];
                }
            }
        }

        return $clients;
    }

    function addUserTasks($user_id, $client_id, $tasks) {

        if ($tasks == null || count($tasks) <= 0) {
            return;
        }

        foreach ($tasks as $code) {
            $task = new UserTaskClient;
            $task->client_id = $client_id;
            $task->user_id = $user_id;
            $task->task = $code;
            $task->save();
        }
    }

    function addUserTask($user_id, $client_id, $task) {

        $newtask = new UserTaskClient;
        $newtask->client_id = $client_id;
        $newtask->user_id = $user_id;
        $newtask->task = $task;
        $newtask->save();
    }

    function deleteUserTaskByUserIdAndClientId($user_id, $client_id) {

        UserTaskClient::model()->deleteAllByAttributes(array('user_id' => $user_id, 'client_id' => $client_id));
    }

    function deleteUserTaskByUserId($user_id) {

        UserTaskClient::model()->deleteAllByAttributes(array('user_id' => $user_id));
    }
    
    function getAssignedPrimaryClient($user_id){

        $clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($user_id, 'AssociateCustomer');
        
        if(count($clients)>0){
            $keys = array_keys($clients);
            $client_id = $keys[0];
            return $client_id;
        }else{
            return 0;
        }
        
    }

    public function getPrimaryClient($user_id){

         $cmd = Yii::app()->db->createCommand();
         $cmd->select = "c2.id as client_id, c2.client_name";
         $cmd->from('user_task_client as c1');
         $cmd->join('client as c2', 'c1.client_id=c2.id');
         $cmd->where("c1.user_id = '".$user_id."' AND c1.task = 'AssociateCustomer' ");
         $result = $cmd->query();
        
        if(empty($result)){ 
            return 0;
        }else{
            foreach($result as $row){
                return $row['client_id'];
            }
        }
    }

}