<?php

/**
 * This is the model class for table "equipment_movement".
 *
 * The followings are the available columns in table 'equipment_movement':
 * @property string $id
 * @property string $facility_id
 * @property string $client_id
 * @property integer $status
 * @property string $submitted
 * @property string $submitted_by
 * @property string $approved
 * @property string $approved_by
 * @property integer $in_or_out
 * @property string $receivedfrom
 * @property integer $numberofboxes
 * @property string $requestedstagingtime
 * @property string $clientreferencenumber
 * @property string $comments
 * @property string $notes
 *
 * The followings are the available model relations:
 * @property User $submittedBy
 * @property User $approvedBy
 * @property Client $client
 * @property Facility $facility
 * @property EquipmentMovementStatus $status0
 * @property EquipmentMovementEntry[] $equipmentMovementEntries
 */
class EquipmentMovement extends CActiveRecord
{
	public $facilities = array();
	public $client_name;
	public $locations = array('Location1','Location2','Location3');
	public $in_or_out_list = array(0=>'In',1=>'Out');
	public $areas = array(1=>'Data Hall','Rack','Cage ID');
	public $delivery_date;
	public $delivery_time;
	public $entries = array();
	public $creator_id;
	public $approve_permission;
	public $submit_permission;
	public $last_item_number = 0;
	public $client_user = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EquipmentMovement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'equipment_movement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//delivery_date, delivery_time,
			array('facility_id, client_id, receivedfrom,  numberofboxes', 'required'),
			array('status, in_or_out, numberofboxes', 'numerical', 'integerOnly'=>true),
			array('facility_id, client_id, submitted_by, approved_by', 'length', 'max'=>10),
			array('receivedfrom, comments', 'length', 'max'=>255),
			array('clientreferencenumber', 'length', 'max'=>48),
			array('notes', 'length', 'max'=>4096),
			array('submitted, approved, requestedstagingtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, facility_id, client_id, receivedfrom, delivery_date, delivery_time, numberofboxes,
			 status, submitted, submitted_by, approved, approved_by, in_or_out, requestedstagingtime, clientreferencenumber, comments, notes', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'submittedBy' => array(self::BELONGS_TO, 'User', 'submitted_by'),
			'approvedBy' => array(self::BELONGS_TO, 'User', 'approved_by'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'facility' => array(self::BELONGS_TO, 'Facility', 'facility_id'),
			'status0' => array(self::BELONGS_TO, 'EquipmentMovementStatus', 'status'),
			'equipmentMovementEntries' => array(self::HAS_MANY, 'EquipmentMovementEntry', 'request_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'facility_id' => 'Facility',
			'client_id' => 'Client',
			'status' => 'Status',
			'submitted' => 'Submitted',
			'submitted_by' => 'Submitted By',
			'approved' => 'Approved',
			'approved_by' => 'Approved By',
			'in_or_out' => 'In Or Out',
			'receivedfrom' => 'Received From',
			'numberofboxes' => 'Number Of Boxes',
			'requestedstagingtime' => 'Requested Staging Time',
			'clientreferencenumber' => 'Clientreferencenumber',
			'comments' => 'Comments',
			'notes' => 'Notes',
		);
	}

	public function beforeSave()
	{
		$this->approved_by = NULL;	

		if($this->status == EquipmentMovementStatus::STATUS_SUBMITTED){

			$this->submitted = date('Y-m-d H:i:s');

            if(User::model()->isAuthorized('ApproveEMF', array('request' => new EquipmentMovement)) ){
            	$this->status = EquipmentMovementStatus::STATUS_APPROVED;
            }
        }

        // set approved_by and approved date, when approved
        if($this->status == EquipmentMovementStatus::STATUS_APPROVED){
        	if($this->isNewRecord){
        		$this->approved_by = $this->submitted_by;	
        	}else{
        		$this->approved_by = $this->creator_id;
        	}
			
			$this->approved = date('Y-m-d H:i:s');
        }

        // set date to y-m-d format before saving, because the calendar ui is in d/m/y format
        $date1 = CDateTimeParser::parse($this->requestedstagingtime, 'dd/MM/yyyy');
        $this->requestedstagingtime = date('Y-m-d H:i:s',$date1);	

		return parent::beforeSave();
	}

	public function afterSave()
	{
		$request_id = $this->id;
		
		if($this->isNewRecord){
			// save temporary items from sessions
			// get session data
			$this->setTempEntries();

			foreach($this->entries as $row){
				$entry = new EquipmentMovementEntry;
				$entry->request_id = $request_id;
				$entry->item_number = $row['item_number'];
				$entry->in_or_out = $row['in_or_out'];
				$entry->description = $row['description'];
				$entry->makemodel = $row['makemodel'];
				$entry->location = $row['location'];
				$entry->hostname = $row['hostname'];
				$entry->serialnumber = $row['serialnumber'];
				$entry->staged_by = $this->submitted_by;
				$entry->staged = date('Y-m-d H:i:s');
				$entry->save(false);
			}	
		}
				

		return parent::afterSave();
	}

	public function afterFind()
	{
		// set date to d/m/y format, after query from table
        $this->requestedstagingtime = date('d/m/Y',strtotime($this->requestedstagingtime));

        return parent::afterFind();	
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('facility_id',$this->facility_id,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('submitted',$this->submitted,true);
		$criteria->compare('submitted_by',$this->submitted_by,true);
		$criteria->compare('approved',$this->approved,true);
		$criteria->compare('approved_by',$this->approved_by,true);
		$criteria->compare('in_or_out',$this->in_or_out);
		$criteria->compare('receivedfrom',$this->receivedfrom,true);
		$criteria->compare('numberofboxes',$this->numberofboxes);
		$criteria->compare('requestedstagingtime',$this->requestedstagingtime,true);
		$criteria->compare('clientreferencenumber',$this->clientreferencenumber,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function saveTempEntries($data)
	{
		$this->entries = Yii::app()->session->itemAt('temp_entries'); // gets value

        if($this->entries == null){
            $this->entries = array();
        }

        $this->entries[] = $data;

        Yii::app()->session->add('temp_entries', $this->entries);
	}

	public function deleteTempEntries($item_number)
	{
		$this->entries = Yii::app()->session->itemAt('temp_entries'); // gets value

        if($this->entries == null){
            $this->entries = array();
        }else{
        	foreach($this->entries as $i => $row){
        		if($item_number == $row['item_number']){
        			unset($this->entries[$i]);
        		}
        	}
        	Yii::app()->session->add('temp_entries', $this->entries);
        }
	}

	public function setTempEntries()
	{
		$this->entries = Yii::app()->session->itemAt('temp_entries'); // gets value

        if($this->entries == null){
            $this->entries = array();
        }else{
        	foreach($this->entries as $row){
        		$this->last_item_number = $row['item_number'];
        	}
        }
	}

	public function saveEntries($data)
	{
		$request_id = $this->id;

		$entry = new EquipmentMovementEntry;
		$entry->request_id = $request_id;
		$entry->item_number = $data['item_number'];
		$entry->in_or_out = $data['in_or_out'];
		$entry->description = $data['description'];
		$entry->makemodel = $data['makemodel'];
		$entry->location = $data['location'];
		$entry->hostname = $data['hostname'];
		$entry->serialnumber = $data['serialnumber'];
		$entry->staged_by = $this->submitted_by;
		$entry->staged = date('Y-m-d H:i:s');
		$entry->save(false);
	}

	public function deleteEntry($item_number)
	{	
		EquipmentMovementEntry::model()->deleteAllByAttributes(array('item_number'=>$item_number, 'request_id'=>$this->id));
	}

	public function setEntries()
	{
		$this->entries = array();
		$result = EquipmentMovementEntry::model()->findAllByAttributes(array('request_id'=>$this->id));

		if(!empty($result)){
			foreach($result as $row){
				$entry = array(
					'type'=>$row->type,
					'item_number'=>$row->item_number,
					'in_or_out'=>$row->in_or_out,
					'description'=>$row->description,
					'makemodel'=>$row->makemodel,
					'location'=>$row->location,
					'hostname'=>$row->hostname,
					'serialnumber'=>$row->serialnumber,
					);

				$this->entries[] = $entry;
				$this->last_item_number = $row->item_number;
			}
		}
	}



	public function getWorkItems() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        
        if(count($this->facilities)>0){
            $facility_ids = array_keys($this->facilities);
            $criteria->addInCondition('facility_id',$facility_ids);
        }

        // limit base on approve and submit permission
        if($this->approve_permission){
            // if user can approve
            $criteria->addInCondition('status', array(EquipmentMovementStatus::STATUS_DRAFT,EquipmentMovementStatus::STATUS_SUBMITTED, EquipmentMovementStatus::STATUS_APPROVED));
        }else{
            $criteria->compare('submitted_by', $this->creator_id);
            $criteria->addInCondition('status', array(EquipmentMovementStatus::STATUS_DRAFT, EquipmentMovementStatus::STATUS_CORRECTION, EquipmentMovementStatus::STATUS_APPROVED));
        }


        $results = EquipmentMovement::model()->findAll($criteria);
        $items = array();
        
        foreach($results as $row){
            $workitem = new WorkItem;
            $workitem->id = $row->id;
            $workitem->type = 'Equipment Movement';
            $workitem->item = 'Equipment Movement';
            $workitem->description = $row->comments;
            $workitem->status = EquipmentMovementStatus::model()->getStatusDescription($row->status);
            $workitem->url = '/request/updateequipment/'.$row->id;
            $items[] = $workitem;
        }    

        return $items;
    }

    public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }   
}