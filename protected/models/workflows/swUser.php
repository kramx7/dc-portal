<?php
	return array(
		'initial' => 'New',
		'node' => array(
			array('id'=>'New', 'transition'=>'Active, Deleted'),
			array('id'=>'Active', 'transition'=>'Inactive, Deleted'),
			array('id'=>'Inactive', 'transition'=>'Active, Deleted'),
			array('id'=>'Deleted', 'transition'=>'Active')
		)
	);
?>
