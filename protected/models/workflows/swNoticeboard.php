<?php
	return array(
		'initial' => 'Draft',
		'node' => array(
			//array('id'=>'Draft', 'transition'=>'Submitted',),
			//array('id'=>'Submitted', 'transition'=>'Published'),
			array('id'=>'Draft', 'transition'=>'Submitted, Approval, Published'),
			array('id'=>'Submitted','transition'=>'Approval, Correction, Published'),
			array('id'=>'Approval','transition'=>'Correction, Published'),
			array('id'=>'Correction','transition'=>'Draft, Submitted, Approval, Published'),
			array('id'=>'Published')
		)
	);
?>	
