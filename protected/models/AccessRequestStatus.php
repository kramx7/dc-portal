<?php

/**
 * This is the model class for table "access_request_status".
 *
 * The followings are the available columns in table 'access_request_status':
 * @property integer $status
 * @property string $status_description
 *
 * The followings are the available model relations:
 * @property AccessRequest[] $accessRequests
 */
class AccessRequestStatus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccessRequestStatus the static model class
	 */
	
	const STATUS_NEW = 0;
	const STATUS_DRAFT = 1;
	const STATUS_SUBMITTED = 2;
	const STATUS_CUSTOMER_APPROVED = 3;
	const STATUS_CORRECTION = 4;
	const STATUS_APPROVAL = 5;
	const STATUS_APPROVED = 6;
	const STATUS_REJECTED = 7;
	const STATUS_DELETED = 255;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_request_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, status_description', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('status_description', 'length', 'max'=>48),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('status, status_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessRequests' => array(self::HAS_MANY, 'AccessRequest', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'status' => 'Status',
			'status_description' => 'Status Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('status',$this->status);
		$criteria->compare('status_description',$this->status_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getStatusList()
	{
		$result = AccessRequestStatus::model()->findAll();

		if(empty($result)){
			return array();
		}else{
			$status = array();
			foreach($result as $row){
				$status[$row->status] = $row->status_description;
			}
			return $status;
		}
	}

	public function getNextStatus($current_id = -1)
	{
		$next_id = $current_id + 1;
		$result = AccessRequestStatus::model()->findByPk($next_id);

		if(empty($result)){
			return -1;
		}else{
			return $result->status;
		}
	}

	public function getStatusDescription($status_id)
	{
		$result = AccessRequestStatus::model()->findByPk($status_id);

		if(empty($result)){
			return '';
		}else{
			return $result->status_description;
		}
	}
}