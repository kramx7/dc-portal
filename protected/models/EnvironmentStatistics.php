<?php

/**
 * This is the model class for table "environment_statistics".
 *
 * The followings are the available columns in table 'environment_statistics':
 * @property integer $record_id
 * @property integer $sensor_id
 * @property string $timestamp
 * @property string $abshumidity
 * @property string $dewpoint
 * @property string $humidity
 * @property string $temperature
 */
class EnvironmentStatistics extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EnvironmentStatistics the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'environment_statistics';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sensor_id, timestamp', 'required'),
            array('sensor_id', 'numerical', 'integerOnly' => true),
            array('abshumidity, dewpoint, humidity, temperature', 'length', 'max' => 8),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, sensor_id, timestamp, abshumidity, dewpoint, humidity, temperature', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'record_id' => 'Record',
            'sensor_id' => 'Sensor',
            'timestamp' => 'Timestamp',
            'abshumidity' => 'Abshumidity',
            'dewpoint' => 'Dewpoint',
            'humidity' => 'Humidity',
            'temperature' => 'Temperature',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('record_id', $this->record_id);
        $criteria->compare('sensor_id', $this->sensor_id);
        $criteria->compare('timestamp', $this->timestamp, true);
        $criteria->compare('abshumidity', $this->abshumidity, true);
        $criteria->compare('dewpoint', $this->dewpoint, true);
        $criteria->compare('humidity', $this->humidity, true);
        $criteria->compare('temperature', $this->temperature, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getTemperatureBySensorIdAndDates($sensor_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, temperature
                FROM environment_statistics
                WHERE ((sensor_id = :sensor_id)
                AND ( Timestamp BETWEEN :from_date AND :to_date))
                GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
                ORDER BY UNIX_TIMESTAMP( environment_statistics.Timestamp ) ASC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':sensor_id', $sensor_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['temperature']);
            }
        }

        return $statistics;
    }

    public function getHumidityBySensorIdAndDates($sensor_id, $from, $to) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, humidity
                FROM environment_statistics
                WHERE ((sensor_id = :sensor_id)
                AND ( Timestamp BETWEEN :from_date AND :to_date))
                GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
                ORDER BY UNIX_TIMESTAMP( environment_statistics.Timestamp ) ASC";

        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':sensor_id', $sensor_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['humidity']);
            }
        }

        return $statistics;
    }

    public function getTemperatureBySensorIdAndDatesAndClients($sensor_id, $from, $to, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, temperature
                FROM environment_statistics
                JOIN client_sensor ON client_sensor.sensor_id = environment_statistics.sensor_id
                WHERE ((client_sensor.sensor_id = :sensor_id)
                AND ( Timestamp BETWEEN :from_date AND :to_date))
                AND client_sensor.client_id IN(:clients)
                GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
                ORDER BY UNIX_TIMESTAMP( environment_statistics.Timestamp ) ASC";
        
        $clients_value = implode(',', array_keys($clients));
                
        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':sensor_id', $sensor_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['temperature']);
            }
        }

        return $statistics;
    }

    public function getHumidityBySensorIdAndDatesAndClients($sensor_id, $from, $to, $clients) {

        $from_date = date('Y-m-d G:i:s', $from);
        $to_date = date('Y-m-d G:i:s', $to);

        $query = "SELECT UNIX_TIMESTAMP(Timestamp) as tmp, humidity
                FROM environment_statistics
                JOIN client_sensor ON client_sensor.sensor_id = environment_statistics.sensor_id
                WHERE ((client_sensor.sensor_id = :sensor_id)
                AND ( Timestamp BETWEEN :from_date AND :to_date))
                AND client_sensor.client_id IN(:clients)
                GROUP BY floor(UNIX_TIMESTAMP(Timestamp)/300)
                ORDER BY UNIX_TIMESTAMP( environment_statistics.Timestamp ) ASC";
        
        $clients_value = implode(',', array_keys($clients));
                
        $cmd = Yii::app()->db->createCommand($query);
        $cmd->bindParam(':sensor_id', $sensor_id);
        $cmd->bindParam(':from_date', $from_date);
        $cmd->bindParam(':to_date', $to_date);
        $cmd->bindParam(':clients',$clients_value);
        $result = $cmd->query();

        $statistics = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $statistics[] = array(0 => $row['tmp'], $row['humidity']);
            }
        }

        return $statistics;
    }

}