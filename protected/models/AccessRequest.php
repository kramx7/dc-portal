<?php

/**
 * This is the model class for table "access_request".
 *
 * The followings are the available columns in table 'access_request':
 * @property string $id
 * @property string $facility_id
 * @property string $client_id
 * @property integer $status
 * @property string $submitted
 * @property string $submitted_by
 * @property string $approved
 * @property string $approved_by
 * @property integer $workarea
 * @property string $clientspecificarea
 * @property string $supervisionrequired
 * @property string $supervised_by
 * @property string $reasonforaccess
 * @property string $ordernumber
 * @property string $serviceid
 * @property string $carrier
 * @property string $cclocation
 * @property string $servicetype
 * @property string $imps_approved
 * @property string $imps_approved_by
 * @property string $notes
 *
 * The followings are the available model relations:
 * @property User $submittedBy
 * @property User $approvedBy
 * @property User $impsApprovedBy
 * @property Client $client
 * @property Facility $facility
 * @property AccessRequestStatus $status0
 * @property AccessWorkArea $workarea0
 * @property AccessRequestEntry[] $accessRequestEntries
 */
class AccessRequest extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AccessRequest the static model class
     */
    public $facilities = array();
    public $expected_start_date;
    public $expected_start_time;
    public $expected_end_date;
    public $expected_end_time;
    public $company;
    public $workareas = array();
    public $service_types = array('ADSL', 'Fibre', 'PSTN');
    public $approve_permission;
    public $submit_permission;
    public $creator_id;
    public $prev_status;
    public $client_user= false;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'access_request';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('submitted_by, client_id, facility_id, workarea, 
                carrier, servicetype, ordernumber, cclocation, serviceid', 'required'),
            array('submitted_by, client_id, facility_id, workarea, clientspecificarea, ordernumber', 'numerical', 'integerOnly' => true),
            array('facility_id, client_id, submitted_by, approved_by, imps_approved_by', 'length', 'max' => 10),
            array('clientspecificarea, supervised_by, reasonforaccess, ordernumber, serviceid, carrier, cclocation, servicetype', 'length', 'max' => 255),
            array('supervisionrequired', 'boolean'),
            array('workarea','checkConditionalRequiredField'),
            array('notes', 'length', 'max' => 4096),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, facility_id, client_id, status, submitted, submitted_by, approved, approved_by, workarea, clientspecificarea, supervisionrequired, 
				supervised_by, reasonforaccess, ordernumber, serviceid, carrier, cclocation, servicetype, imps_approved, imps_approved_by, notes,
                expected_start_date, expected_start_time, expected_end_date, expected_end_time ', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'submittedBy' => array(self::BELONGS_TO, 'User', 'submitted_by'),
            'approvedBy' => array(self::BELONGS_TO, 'User', 'approved_by'),
            'impsApprovedBy' => array(self::BELONGS_TO, 'User', 'imps_approved_by'),
            'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
            'facility' => array(self::BELONGS_TO, 'Facility', 'facility_id'),
            'status0' => array(self::BELONGS_TO, 'AccessRequestStatus', 'status'),
            'workarea0' => array(self::BELONGS_TO, 'AccessWorkArea', 'workarea'),
            'accessRequestEntries' => array(self::HAS_MANY, 'AccessRequestEntry', 'request_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'facility_id' => 'Facility',
            'client_id' => 'Client',
            'status' => 'Status',
            'submitted' => 'Submitted',
            'submitted_by' => 'Submitted By',
            'approved' => 'Approved',
            'approved_by' => 'Approved By',
            'workarea' => 'Work Area',
            'clientspecificarea' => 'Client Specific Area',
            'supervisionrequired' => 'Supervision Required',
            'supervised_by' => 'Supervised By',
            'reasonforaccess' => 'Reason of Access',
            'ordernumber' => 'Order Number',
            'serviceid' => 'Service ID',
            'carrier' => 'Carrier/Telco',
            'cclocation' => 'Cross Connect',
            'servicetype' => 'Service Type',
            'imps_approved' => 'Imps Approved',
            'imps_approved_by' => 'Imps Approved By',
            'notes' => 'Notes',
            'expected_start_date' => '',
            'expected_start_time' => '',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('facility_id', $this->facility_id, true);
        $criteria->compare('client_id', $this->client_id, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('submitted', $this->submitted, true);
        $criteria->compare('submitted_by', $this->submitted_by, true);
        $criteria->compare('approved', $this->approved, true);
        $criteria->compare('approved_by', $this->approved_by, true);
        $criteria->compare('workarea', $this->workarea);
        $criteria->compare('clientspecificarea', $this->clientspecificarea, true);
        $criteria->compare('supervisionrequired', $this->supervisionrequired, true);
        $criteria->compare('supervised_by', $this->supervised_by, true);
        $criteria->compare('reasonforaccess', $this->reasonforaccess, true);
        $criteria->compare('ordernumber', $this->ordernumber, true);
        $criteria->compare('serviceid', $this->serviceid, true);
        $criteria->compare('carrier', $this->carrier, true);
        $criteria->compare('cclocation', $this->cclocation, true);
        $criteria->compare('servicetype', $this->servicetype, true);
        $criteria->compare('imps_approved', $this->imps_approved, true);
        $criteria->compare('imps_approved_by', $this->imps_approved_by, true);
        $criteria->compare('notes', $this->notes, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function afterConstruct()
    {
        return parent::afterConstruct();
    }

    public function beforeValidate() {
        
        if ($this->scenario == 'insert') {
            // get the users associated client
            $this->client_id = UserTaskClient::model()->getAssignedPrimaryClient($this->submitted_by);

            // set submitted time
            $this->submitted = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        $this->clientspecificarea = NULL; // this is temporary until this is resolve by the client

        if($this->status == AccessRequestStatus::STATUS_SUBMITTED){

            if(User::model()->isAuthorized('ApproveDCAccess',array('request'=>new AccessRequest)) ){

                switch($this->workarea){
                    case AccessWorkArea::AREA_PLANT:
                            $this->status = AccessRequestStatus::STATUS_APPROVAL;
                            break;
                    case AccessWorkArea::AREA_MDF:
                            $this->status = AccessRequestStatus::STATUS_APPROVAL;  
                            break;
                    case AccessWorkArea::AREA_NODE:  
                            $this->status = AccessRequestStatus::STATUS_APPROVAL;
                            break;
                    case AccessWorkArea::AREA_SITETOUR:
                            $this->status = AccessRequestStatus::STATUS_APPROVAL;
                            break;                
                    default: 
                        break;
                }
            }
        }

        // set access_start_time and access_end_time
        $date1 = CDateTimeParser::parse($this->expected_start_date, 'dd/MM/yyyy');
        $date2 = CDateTimeParser::parse($this->expected_end_date, 'dd/MM/yyyy');

        $this->access_start_time = date('Y-m-d',$date1).' '.date('H:m',strtotime($this->expected_start_time));
        $this->access_end_time = date('Y-m-d',$date2).' '.date('H:m',strtotime($this->expected_end_time));

        return parent::beforeSave();
    }

    public function afterFind()
    {

        // set expected date and time
        $this->expected_start_date = date('d/m/Y',strtotime($this->access_start_time));
        $this->expected_start_time = date('h:m A',strtotime($this->access_start_time));
        $this->expected_end_date = date('d/m/Y',strtotime($this->access_end_time));
        $this->expected_end_time = date('h:m A',strtotime($this->access_end_time));

        return parent::afterFind();
    }

    public function checkConditionalRequiredField(){


        $supervised = false;
        $description = false;
        $carrier = false;
        $cclocation = false;

        // check supervised_by, supervisionrequired, description of work, requirements, comments field
        switch($this->workarea){
            case AccessWorkArea::AREA_MEETINGROOM:
                $supervised = true;
                break;
            case AccessWorkArea::AREA_SITETOUR:
                $supervised = true;
                break;
            case AccessWorkArea::AREA_NODE:
                $carrier = true;
                $cclocation = true;
                break;           
            case AccessWorkArea::AREA_MDF:
                $carrier = true;
                $cclocation = true;
                break;
            case AccessWorkArea::AREA_OTHER:
                $description = true;
                break;         
            default:
                break;
        }

        if($supervised && (empty($this->supervised_by) || empty($this->supervisionrequired)) ) {
            $this->addError('supervisionrequired', 'Require Supervision field is required.');
            $this->addError('supervised_by', 'Supervised By field is required.');
        }

        if($description && empty($this->reasonforaccess)){
            $this->addError('reasonforaccess', 'Description of Work, Requirements, Comments field is required.');
        }

        if($carrier && empty($this->carrier)){
            $this->addError('carrier', 'Carrier Telco field is required.');
        }

        if($cclocation && empty($this->cclocation)){
            $this->addError('cclocation', 'Cross Connect field is required.');
        }

        if($this->supervisionrequired != false && empty($this->supervised_by)){
            $this->addError('supervised_by', 'Supervised By field is required.');
        }


    }

    public function getWorkItems() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        
        if(count($this->facilities)>0){
            $facility_ids = array_keys($this->facilities);
            $criteria->addInCondition('facility_id',$facility_ids);
        }

        // limit base on approve and submit permission
        if($this->approve_permission){
            // if user can approve
            $criteria->addInCondition('status', array(AccessRequestStatus::STATUS_DRAFT, AccessRequestStatus::STATUS_SUBMITTED ,AccessRequestStatus::STATUS_APPROVAL, AccessRequestStatus::STATUS_APPROVED));
        }else{
            $criteria->compare('submitted_by', $this->creator_id);
            $criteria->addInCondition('status', array(AccessRequestStatus::STATUS_DRAFT, AccessRequestStatus::STATUS_CORRECTION, AccessRequestStatus::STATUS_APPROVED));
        }


        $results = AccessRequest::model()->findAll($criteria);
        $items = array();
        
        foreach($results as $row){
            $workitem = new WorkItem;
            $workitem->id = $row->id;
            $workitem->type = 'Facility Access';
            $workitem->item = 'Facility Access';//$row->carrier;
            $workitem->description = $row->reasonforaccess;
            $workitem->status = AccessRequestStatus::model()->getStatusDescription($row->status);
            $workitem->url = '/request/updatefacilityaccess/'.$row->id;
            $items[] = $workitem;
        }    

        return $items;
    }

    public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

}