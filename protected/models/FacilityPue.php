<?php

/**
 * This is the model class for table "facility_pue".
 *
 * The followings are the available columns in table 'facility_pue':
 * @property integer $record_id
 * @property integer $facility_id
 * @property string $start_timestamp
 * @property string $end_timestamp
 * @property string $pue_value
 * @property string $modified_time
 */
class FacilityPue extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FacilityPue the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'facility_pue';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('facility_id, start_timestamp, end_timestamp, pue_value', 'required'),
            array('facility_id', 'numerical', 'integerOnly' => true),
            array('pue_value', 'length', 'max' => 5),
            array('modified_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('record_id, facility_id, start_timestamp, end_timestamp, pue_value, modified_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'facility_id' => 'Facility',
            'start_timestamp' => 'Start Timestamp',
            'end_timestamp' => 'End Timestamp',
            'pue_value' => 'Pue Value',
            'modified_time' => 'Modified Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('facility_id', $this->facility_id);
        $criteria->compare('start_timestamp', $this->start_timestamp, true);
        $criteria->compare('end_timestamp', $this->end_timestamp, true);
        $criteria->compare('pue_value', $this->pue_value, true);
        $criteria->compare('modified_time', $this->modified_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getPueByPowerDatesAndFacilityId($powers, $facility_id) {

        $pues = array();

        foreach ($powers as $p) {
            
            $query = "SELECT pue_value FROM facility_pue 
            where :day between DATE(start_timestamp) and DATE(end_timestamp) 
            and facility_id = :facility_id";

            $cmd = Yii::app()->db->createCommand($query);
            $cmd->bindParam(':day', $p['day']);
            $cmd->bindParam(':facility_id', $facility_id);
            $result = $cmd->query();
            
            if (!empty($result)) {
                foreach ($result as $row) {
                    $pues[$p['day']] = $row['pue_value'];
                }
            }else{
                $pues[$p['day']] = 0;
            }
        }

        return $pues;
    }

}