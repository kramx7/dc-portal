<?php

/**
 * This is the model class for table "equipment_movement_entry".
 *
 * The followings are the available columns in table 'equipment_movement_entry':
 * @property string $id
 * @property string $request_id
 * @property integer $item_number
 * @property string $description
 * @property string $makemodel
 * @property string $location
 * @property string $hostname
 * @property string $staged
 * @property string $staged_by
 * @property string $serialnumber
 *
 * The followings are the available model relations:
 * @property EquipmentMovement $request
 * @property User $stagedBy
 */
class EquipmentMovementEntry extends CActiveRecord
{
	public $type;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EquipmentMovementEntry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'equipment_movement_entry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_number, type, description, makemodel, location, serialnumber, hostname', 'required'),
			array('item_number', 'numerical', 'integerOnly'=>true),
			array('request_id, staged_by', 'length', 'max'=>10),
			array('description, makemodel, location, hostname, serialnumber', 'length', 'max'=>50),
			array('staged', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, request_id, item_number, description, makemodel, location, hostname, staged, staged_by, serialnumber', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'request' => array(self::BELONGS_TO, 'EquipmentMovement', 'request_id'),
			'stagedBy' => array(self::BELONGS_TO, 'User', 'staged_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'request_id' => 'Request',
			'item_number' => 'Item Number',
			'description' => 'Description',
			'makemodel' => 'Makemodel',
			'location' => 'Location',
			'hostname' => 'Hostname',
			'staged' => 'Staged',
			'staged_by' => 'Staged By',
			'serialnumber' => 'Serialnumber',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('request_id',$this->request_id,true);
		$criteria->compare('item_number',$this->item_number);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('makemodel',$this->makemodel,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('hostname',$this->hostname,true);
		$criteria->compare('staged',$this->staged,true);
		$criteria->compare('staged_by',$this->staged_by,true);
		$criteria->compare('serialnumber',$this->serialnumber,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}