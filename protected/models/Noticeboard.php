<?php

/**
 * This is the model class for table "noticeboard".
 *
 * The followings are the available columns in table 'noticeboard':
 * @property string $id
 * @property string $facility
 * @property integer $message_status
 * @property string $thread_id
 * @property string $priority
 * @property string $publish_from
 * @property string $publish_to
 * @property integer $sticky
 * @property string $title
 * @property string $short_message
 * @property string $full_message
 * @property string $submitted_by
 * @property string $submitted_time
 * @property string $approver
 * @property string $approved_time
 *
 * The followings are the available model relations:
 * @property User $approver0
 * @property NoticeboardMessageStatus $messageStatus
 * @property User $submittedBy
 */
class Noticeboard extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Noticeboard the static model class
     */
    
    public $facilities = array();
    public $priorities = array();
    public $default_priority;
    public $temp_publish_from = ''; // temporary holder of the string word immediately, used on beforeValidate and afterValidate
    public $temp_publish_to = ''; // temporary holder of the string word never, used on beforeValidate and afterValidate
    public $creator_id=0;
    public $sticky = false;
    public $read_permission = false;
    public $submit_permission = false;
    public $approve_permission = false;
    public $emailaddress = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'noticeboard';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, message_status, facility_id, priority, publish_from, publish_to, , short_message, full_message', 'required'),
            array('title', 'length', 'max' => 80),
            array('title', 'filter', 'filter' => 'strip_tags'),
            array('facility_id, priority, submitted_by, approved_by', 'numerical', 'integerOnly' => true),
            array('facility_id','validateFacilityAuthorization'),
            array('short_message', 'length', 'max' => 256),
            array('short_message', 'filter', 'filter' => 'strip_tags'),
            array('full_message', 'length', 'max' => 10240),
            //array('full_message','filter', 'filter' => 'strip_tags'),
            array('publish_from, publish_to', 'date', 'format' => 'yyyy-MM-dd'),
            array('submitted, approved', 'date', 'format' => 'yyyy-MM-dd H:m:s'),
            array('publish_from,  publish_to', 'checkPublishDateRange'),
            array('submitted', 'default', 'value' => new CDbExpression('NOW()'), 'on' => 'insert'),
            array('sticky', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            /* array('id, facility, message_status, thread_id, priority, publish_from, publish_to, sticky, title, short_message, full_message, submitted_by, submitted_time, approver, approved_time', 'safe', 'on'=>'search'), */
            array('id, message_status, priority, publish_from, publish_to, title, short_message, full_message, submitted_by, submitted, approved_by, approved', 'safe', 'on' => 'search')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'approver1' => array(self::BELONGS_TO, 'User', 'approved_by'), // it has the number 1 append because there is already a field approver on the table noticeboard
            'submittedBy' => array(self::BELONGS_TO, 'User', 'submitted_by'),
            'facility1' => array(self::BELONGS_TO, 'Facility', 'facility_id'), // it has the number 1 append because there is already a field facility on table noticeboard
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'facility_id' => 'Facility',
            'message_status' => 'Message Status',
            'priority' => 'Priority',
            'publish_from' => 'Publish From',
            'publish_to' => 'Publish To',
            'sticky' => 'Sticky',
            'title' => 'Title',
            'short_message' => 'Short Message',
            'full_message' => 'Full Message',
            'submitted_by' => 'Submitted By',
            'submitted' => 'Submitted Time',
            'approved_by' => 'Approver',
            'approved' => 'Approved Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($status='') {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        /*
        $criteria->compare('id', $this->id, true);
        $criteria->compare('facility_id', $this->facility_id, true);
        $criteria->compare('thread_id',$this->thread_id,true);
        $criteria->compare('priority', $this->priority, true);
        $criteria->compare('publish_from', $this->publish_from, true);
        $criteria->compare('publish_to', $this->publish_to, true);
        $criteria->compare('sticky', $this->sticky);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('short_message', $this->short_message, true);
        $criteria->compare('full_message', $this->full_message, true);
        $criteria->compare('submitted_by', $this->submitted_by, true);
        $criteria->compare('submitted', $this->submitted, true);
        $criteria->compare('approved_by', $this->approved_by, true);
        $criteria->compare('approved', $this->approved, true);
        */
       // limit result to assigned facility of the login user, this is set on 
        // Controller:Noticebard,Action:Search
        //filter resutls to assign facility
        
        if(count($this->facilities)>0){
            $facility_ids = array_keys($this->facilities);
            $criteria->addInCondition('facility_id',$facility_ids);
        }

        if($status != ''){
            // specific status is search, like published post only
            $criteria->compare('message_status', $status);
        }else{
            // limit base on approve and submit permission
            if($this->approve_permission){
                // if user can approve
                $criteria->addInCondition('message_status', array(NoticeboardMessageStatus::STATUS_DRAFT,NoticeboardMessageStatus::STATUS_SUBMITTED));
            }else{
                $criteria->compare('submitted_by', $this->creator_id);
                $criteria->addInCondition('message_status', array(NoticeboardMessageStatus::STATUS_DRAFT, NoticeboardMessageStatus::STATUS_CORRECTION));
            }
        }

        $criteria->addCondition('DATE(NOW()) BETWEEN DATE(publish_from) AND DATE(publish_to)');
        $criteria->order = "sticky ASC, priority ASC";

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getWorkItems() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        
        if(count($this->facilities)>0){
            $facility_ids = array_keys($this->facilities);
            $criteria->addInCondition('facility_id',$facility_ids);
        }

        // limit base on approve and submit permission
        if($this->approve_permission){
            // if user can approve
            $criteria->addInCondition('message_status', array(NoticeboardMessageStatus::STATUS_DRAFT,NoticeboardMessageStatus::STATUS_SUBMITTED));
        }else{
            $criteria->compare('submitted_by', $this->creator_id);
            $criteria->addInCondition('message_status', array(NoticeboardMessageStatus::STATUS_DRAFT, NoticeboardMessageStatus::STATUS_CORRECTION));
        }

        $criteria->addCondition('DATE(NOW()) BETWEEN DATE(publish_from) AND DATE(publish_to)');
        $criteria->order = "sticky ASC, priority ASC";


        $results = Noticeboard::model()->findAll($criteria);
        $items = array();
        
        foreach($results as $row){
            $workitem = new WorkItem;
            $workitem->id = $row->id;
            $workitem->type = 'Notice';
            $workitem->item = 'Bulletin Board Post';//$row->title;
            $workitem->description = $row->short_message;
            $workitem->status = NoticeboardMessageStatus::model()->getStatusDescription($row->message_status);
            $workitem->url = '/noticeboard/update/'.$row->id;
            $items[] = $workitem;
        }    

        return $items;
    }
   

    function afterConstruct() {

        // set the default publish from and to dates to string immediately and never as requested
        if ($this->publish_from == null) {
            $this->publish_from = 'immediately';
        }

        if ($this->publish_to == null) {
            $this->publish_to = 'never';
        }

        $this->setPriorities();

        return parent::afterConstruct();
    }

    function afterFind() {

        // format publish from and to dates to since data from database is formated in y-m-d m:i
        // but the views only need the y-m-d format
        if ($this->publish_from == '0000-00-00 00:00:00') {
            $this->publish_from = '';
        }

        if ($this->publish_to == '0000-00-00 00:00:00') {
            $this->publish_to = '';
        }

        if ($this->publish_from != '') {
            $this->publish_from = date('Y-m-d', strtotime($this->publish_from));
        }

        if ($this->publish_to != '') {
            $this->publish_to = date('Y-m-d', strtotime($this->publish_to));
        }

        // show the never string for to publish to date if it matches the never data on config main file
        if ($this->publish_to == date('Y-m-d', strtotime(Yii::app()->params['never_date']))) {
            $this->temp_publish_to = $this->publish_to;
            $this->publish_to = 'never';
        }

        $this->setPriorities();

        return parent::afterFind();
    }

    function beforeValidate() {

        // need to change the value of publish dates from and to valid format to avoid 
        // validation errors, it will stored temporarily to its corresponding to temp variable
        if ($this->publish_from == 'immediately') {
            $this->temp_publish_from = $this->publish_from;
            $this->publish_from = date('Y-m-d');
        }else{
            $this->temp_publish_from = $this->publish_from;
            if($this->scenario != 'approval'){
                $this->publish_from = CDateTimeParser::parse($this->publish_from, 'dd/MM/yyyy');
                $this->publish_from = date('Y-m-d',$this->publish_from); 
            }
        }

        if ($this->publish_to == 'never') {
            $this->temp_publish_to = $this->publish_to;
            $this->publish_to = Yii::app()->params['never_date'];
        }else{
            $this->temp_publish_to = $this->publish_to; 
            $this->publish_to = CDateTimeParser::parse($this->publish_to, 'dd/MM/yyyy');
            $this->publish_to = date('Y-m-d',$this->publish_to);
        }

        return parent::beforeValidate();
    }

    function afterValidate() {

       
        // if there publish dates from and to have values like dates or string immediately and never 
        // return the original value from temp var
        // this is executed when there are errors on the validation process
        if ($this->temp_publish_from != '') {
            $this->publish_from = $this->temp_publish_from;
        }

        if ($this->temp_publish_to != '') {
            $this->publish_to = $this->temp_publish_to;
        }

        return parent::afterValidate();
    }

    public function beforeSave() {

        if ($this->isNewRecord) {
            $this->submitted = new CDbExpression('NOW()');
        }

        if ($this->publish_from == 'immediately') {
            $this->publish_from = date('Y-m-d');
        }else if ($this->publish_from != '') {
            //$this->publish_from = date('Y-m-d', strtotime($this->publish_from));
            if($this->scenario != 'approval'){
                $this->publish_from = CDateTimeParser::parse($this->publish_from, 'dd/MM/yyyy');
                $this->publish_from = date('Y-m-d',$this->publish_from);
            }
        }

        if ($this->publish_to == 'never') {
            $this->publish_to = Yii::app()->params['never_date'];
        }else if ($this->publish_to != '') {
            //$this->publish_to = date('Y-m-d', strtotime($this->publish_to));
            $this->publish_to = CDateTimeParser::parse($this->publish_to, 'dd/MM/yyyy');
            $this->publish_to = date('Y-m-d',$this->publish_to);
        }

        // set status
        $this->setStatus();

        return parent::beforeSave();
    }

    public function afterSave() {

        try
        {
  
            $message = new YiiMailMessage;
            $message->view = 'post-notification'; // view file in /protected/views/mail/

            $name = Yii::app()->name;
            $subject = $name . ' - Post Notification';
            $message->getHeaders()->get('Subject')->setValue($subject);

            //userModel is passed to the view
            $message->setBody(array('model' => $this), 'text/html');


            $message->addTo($user->emailaddress);
            $message->from = Yii::app()->params['adminEmail'];


            Yii::app()->mail->send($message);

            return true;

        }catch(Exception $e){
            return false;
        }

        return parent::afterSave();
    }

    public function checkPublishDateRange() {

        // check publish dates; check whether publish from/start date is less than the publish to/end date
        if (strtotime($this->publish_from) > strtotime($this->publish_to)) {
            $this->addError('publish_from', 'Publish From date should be earlier than the Publish To date.');
        }
    }

    public function validateFacilityAuthorization() {

        // check publish dates; check whether publish from/start date is less than the publish to/end date
        if ($this->facility_id !=0 && (!$this->isUserInFacility('SubmitPost') && !$this->isUserInFacility('ApprovePost'))) {
            $this->addError('facility_id', 'The user does not have authorization to post for the selected facility.');
        }
    }

    public function published() {

        $this->updateByPk($this->id, array('message_status' => NoticeboardMessageStatus::STATUS_APPROVED));
        $this->message_status = NoticeboardMessageStatus::STATUS_APPROVED;
    }

    public function getFormatedSubmittedTime($format = 'd/m/Y') {


        return date($format, strtotime($this->submitted));
    }

    public function getFacilityName() {

        $facility = Facility::model()->findByAttributes(array('id' => $this->facility_id));

        if ($facility != null) {
            return $facility->facility_name;
        } else {
            return '';
        }
    }

    public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($this->facility_id == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility_id, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

    public function setStatus()
    {
        switch($this->message_status){
            case NoticeboardMessageStatus::STATUS_SUBMITTED:
                // check if user can approve, then set status to approve
                if($this->isUserInFacility('ApprovePost')){
                    $this->message_status = NoticeboardMessageStatus::STATUS_APPROVED;
                }
                break;
            default: break;
        }

         // set the approver to the user id of login user,
        if ($this->message_status == NoticeboardMessageStatus::STATUS_APPROVED) {
            // if notice is approved
            $this->approved_by = $this->creator_id;
            $this->approved = new CDbExpression('NOW()');
        }
    }

    public function setPriorities()
    {
        $priorities = NoticeboardMessagePriority::model()->getPriorityList();
        $this->priorities = $priorities;

        $configuration = new SystemConfigurationForm;
        $configuration->read();
        $this->default_priority = $configuration->default_priority;

    }

    public function getStatusDescription()
    {
        $status = NoticeboardMessageStatus::model()->getStatusDescription($this->message_status);
        return $status;
    }


}