<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $login
 * @property string $password
 * @property string $emailaddress
 * @property string $alternateemailaddress
 * @property string $officephone
 * @property string $officefax
 * @property string $mobilephone
 * @property string $client
 * @property string $facility
 * @property string $user_status
 * @property string $valid_from
 * @property string $valid_to
 * @property string $last_login
 * @property string $last_login_from
 * @property integer $modified_by
 * @property string $modified_time
 *
 * The followings are the available model relations:
 * @property Client $client0
 * @property Facility $facility0
 * @property UserAuthority[] $userAuthorities
 * @property UserRole $userRole
 * @property UserRole[] $userRoles
 */
class User extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    /*
    const STATUS_NEW = 'swUser/New';
    const STATUS_ACTIVE = 'swUser/Active';
    const STATUS_INACTIVE = 'swUser/Inactive';
    const STATUS_DELETED = 'swUser/Deleted';
    */
    const STATUS_NEW = 0;
    const STATUS_UNCONFIRMED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_INACTIVE = 3;
    const STATUS_LOCKED = 4;
    const STATUS_DEACTIVATED = 5;
    const STATUS_DELETED = 255;


    public $creator_id;
    public $fullname;
    public $role;
    public $roles = array();
    public $primary_role = '';
    public $operations = array();
    public $tasks = array();
    public $notification; // temporary for notification
    public $facility;
    public $facilities = array();
    public $client;
    public $clients = array();
    public $search_user;
    public $users;
    private $_identity;
    public $old_password;
    public $temp_oldpassword;
    public $new_password;
    public $password_compare;
    public $temp_valid_from = '';
    public $temp_valid_to = '';
    // for permissions
    public $permissions = array();
    public $portallogin_permission;
    public $portallogin_permissions = array();
    public $portalconfiguration_permission;
    public $portalconfiguration_permissions = array();
    public $customers_permission;
    public $customers_permissions = array();
    public $facility_permission;
    public $facility_permissions = array();
    public $role_permission;
    public $role_permissions = array();
    public $users_permission;
    public $users_permissions = array();
    public $proximitycard_permission;
    public $proximitycard_permissions = array();
    public $emf_permission;
    public $emf_permissions = array();
    public $dcaccess_permission;
    public $dcaccess_permissions = array();
    public $telcoaccess_permission;
    public $telcoaccess_permissions = array();
    public $remotehands_permission;
    public $remotehands_permissions = array();
    public $bulletinboard_permission;
    public $bulletinboard_permissions = array();
    public $library_permission;
    public $library_permissions = array();
    public $report_permission;
    public $report_permissions = array();

    public $deleted_users = false;
    public $roles_to_manage = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('facility, role, emailaddress, first_name, last_name, 
                valid_from, valid_to', 'required', 'on' => 'admin-create, admin-update'),
            array('emailaddress','required','on'=>'request-reset'),
            array('new_password, password_compare','required','on'=>'reset-password-request'),
            array('facility, client', 'numerical', 'integerOnly' => true, 'min' => 0),
            array('role', 'length', 'min' => 2),
            array('client','checkRequiredClient','on'=>'admin-create, admin-update'),
            array('first_name, last_name', 'required', 'on' => 'profile, profile-pass, verification'),
            array('emailaddress', 'email'),
            //array('emailaddress', 'unique', 'message' => 'User {value} already exists in the system.','on'=>'insert, update, admin-create, profile, profile-pass, verification'),
            array('emailaddress', 'checkExistingUser','on'=>'insert, update, admin-create, profile, profile-pass, verification'),
            array('emailaddress','checkActiveStatus','on'=>'request-reset'),
            array('first_name, last_name', 'length', 'min' => 2),
            array('officephone, officefax, mobilephone', 'length', 'min' => 7, 'max' => 20),
            array('remote_hands', 'length', 'min' => 8, 'max' => 20),
            array('user_status', 'default', 'value' => self::STATUS_NEW, 'on' => 'insert, admin-create'),
            array('new_password, password_compare', 'required', 'on' => 'verification'),
            array('new_password', 'compare', 'compareAttribute' => 'password_compare', 'on' => 'verification, reset-password-request'),
            array('old_password', 'required', 'on' => 'profile-pass'),
            array('old_password', 'compare', 'compareAttribute' => 'password', 'on' => 'profile-pass', 'message' => 'Old password did not match on the system record.'),
            array('new_password', 'compare', 'compareAttribute' => 'password_compare', 'on' => 'profile-pass, verification'),
            array('password', 'authenticate', 'on' => 'login'),
            array('password, new_password, password_compare', 'length', 'min' => 7),
            array('last_login_from', 'length', 'max' => 48),
            array('valid_from, valid_to', 'date', 'format' => 'yyyy-MM-dd', 'on' => 'insert, update, admin-create, admin-update'),
            array('last_login, modified_time', 'date', 'format' => 'yyyy-MM-dd H:m:s'),
            array('valid_from, valid_to', 'checkPublishDateRange', 'on' => 'insert, update, admin-create, admin-update'),
            array('modified_time', 'default', 'value' => new CDbExpression('NOW()'), 'on' => 'insert, admin-create, admin-update'),
            array('modified_by', 'default', 'value' => Yii::app()->user->id, 'on' => 'update, admin-update'),
            array('modified_time', 'default', 'value' => new CDbExpression('NOW()'), 'on' => 'update, admin-update'),
            //array('verified', 'default', 'value' => 1, 'on' => 'verification'),
            //array('notify_noticeboard, notify_workitem, notify_request_update','numerical', 'integerOnly' => true, 'min' => 0,'max'=>1),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, emailaddress, alternateemailaddress, first_name, last_name, fullname, 
                officephone, officefax, mobilephone, remote_hands, valid_from, valid_to, 
                user_status, role, facility, client, search_user, 
                portallogin_permission, portalconfiguration_permission customers_permission, facility_permission, role_permission, users_permission, roles_permission, proximitycard_permission, emf_permission,
                dcaccess_permission, telcoaccess_permission, remotehands_permission, bulletinboard_permission, library_permission, 
                report_permission, notification, deleted_users', 'safe',),
            array('password, old_password, new_password, password_compare, notify_noticeboard, notify_workitem, notify_request_update', 'safe', 'on' => 'verification, profile, profile-pass'),
        );
        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'noticeboards' => array(self::HAS_MANY, 'Noticeboard', 'approver'),
            'noticeboards1' => array(self::HAS_MANY, 'Noticeboard', 'submitted_by'),
            'facility1' => array(self::BELONGS_TO, 'Facility', 'facility'),
            'client1' => array(self::BELONGS_TO, 'Client', 'client'),
            'authRole' => array(self::BELONGS_TO, 'AuthItem', 'user_role'),
        );

        //return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            //'login' => 'Username',
            'emailaddress' => 'Email Address',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Surname',
            'old_password'=>'Old Password',
            'password_compare' => 'Confirm Password',
            'alternateemailaddress' => 'Alternate Email Address',
            'officephone' => 'Office Phone',
            'officefax' => 'Fax Phone',
            'mobilephone' => 'Mobile Phone',
            'remote_hands' => 'Remote Hands',
            'valid_from' => 'Valid From',
            'valid_to' => 'Valid To',
            'last_login' => 'Last Login',
            'last_login_from' => 'Last Login From',
            'modified_by' => 'Modified By',
            'modified_time' => 'Modified Time',
            'user_status' => 'Status',
            'role' => 'Role',
            'client' => 'Client',
            'facility' => 'Facility',
            'verification_code' => 'Verification Code',
            'verified' => 'Verified Status',
            'notification' => 'Notification',
            'portallogin_permission' => "Portal Login",
            'customers_permission' => "Customers",
            'users_permission' => "Users",
            'proximitycard_permission' => "Proximity Card",
            'emf_permission' => "EMF",
            'dcaccess_permission' => "DC Access",
            'telcoaccess_permission' => "Telco/carrier Access",
            'remotehands_permission' => "Remote Hands",
            'bulletinboard_permission' => "Bulletin Board",
            'library_permission' => "Library",
            'report_permission' => "Report",
            'deleted_users'=>'Display deleted users'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('emailaddress', $this->emailaddress, true);
        $criteria->compare('alternateemailaddress', $this->alternateemailaddress, true);
        $criteria->compare('officephone', $this->officephone, true);
        $criteria->compare('officefax', $this->officefax, true);
        $criteria->compare('mobilephone', $this->mobilephone, true);
        $criteria->compare('client', $this->client, true);
        $criteria->compare('facility', $this->facility, true);
        $criteria->compare('user_status', $this->user_status, true);
        $criteria->compare('user_role', $this->user_role, true);
        $criteria->compare('valid_from', $this->valid_from, true);
        $criteria->compare('valid_to', $this->valid_to, true);
        $criteria->compare('last_login', $this->last_login, true);
        $criteria->compare('last_login_from', $this->last_login_from, true);
        $criteria->compare('modified_by', $this->modified_by);
        $criteria->compare('modified_time', $this->modified_time, true);

        if ($this->_search_user_role != 'SuperAdmin') {
            //to limit results to assigned facility if user is not the super admin
            //$criteria->compare('facility', $this->_search_user_facility);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    function afterConstruct() {

        // set default valid date from and to string immediately and never
        // this is only if its null, or for the first form load, as required by the user
        if ($this->valid_from == '') {
            $this->valid_from = 'now';
        }

        if ($this->valid_to == '') {
            $this->valid_to = 'never';
        }

        return parent::afterConstruct();
    }

    function afterFind() {

        // format the valid dates from and to to y-m-d, because data stored on the table is formated with hours and minutes
        if ($this->valid_from == '0000-00-00 00:00:00') {
            $this->valid_from = '';
        }

        if ($this->valid_to == '0000-00-00 00:00:00') {
            $this->valid_to = '';
        }

        if ($this->valid_from != '') {
            $this->valid_from = date('Y-m-d', strtotime($this->valid_from));
        }

        if ($this->valid_to != '') {
            $this->valid_to = date('Y-m-d', strtotime($this->valid_to));
        }

        if ($this->valid_to == date('Y-m-d', strtotime(Yii::app()->params['never_date']))) {
            $this->temp_valid_to = $this->valid_to;
            $this->valid_to = 'never';
        }

        $this->facility = UserTaskFacility::model()->getPrimaryFacility($this->id);
        $this->client = UserTaskClient::model()->getPrimaryClient($this->id);
        $this->role = $this->getPrimaryRole($this->id);

        return parent::afterFind();
    }

    function beforeValidate() {

        // need to change the value of publish dates from and to valid format to avoid 
        // validation errors, it will stored temporarily to its corresponding to temp variable
        if ($this->valid_from == 'now') {
            $this->temp_valid_from = $this->valid_from;
            $this->valid_from = date('Y-m-d');
        }else{
            $this->temp_valid_from = $this->valid_from;
            $this->valid_from = CDateTimeParser::parse($this->valid_from, 'dd/MM/yyyy');
            $this->valid_from = date('Y-m-d',$this->valid_from); 
        }

        if ($this->valid_to == 'never') {
            $this->temp_valid_to = $this->valid_to;
            $this->valid_to = Yii::app()->params['never_date'];
        }else{
            $this->temp_valid_to = $this->valid_to; 
            $this->valid_to = CDateTimeParser::parse($this->valid_to, 'dd/MM/yyyy');
            $this->valid_to = date('Y-m-d',$this->valid_to);
        }

        if($this->old_password != ''){
            $this->temp_oldpassword = $this->old_password;
            $this->old_password = $this->encrypt($this->old_password); 
        }

        return parent::beforeValidate();
    }

    function afterValidate() {


        // if there publish dates from and to have values like dates or string immediately and never 
        // return the original value from temp var
        // this is executed when there are errors on the validation process
        if ($this->temp_valid_from != '') {
            $this->valid_from = $this->temp_valid_from;
        }

        if ($this->temp_valid_to != '') {
            $this->valid_to = $this->temp_valid_to;
        }

        if($this->old_password != ''){
            $this->old_password = $this->temp_oldpassword; 
        }

        return parent::afterValidate();
    }

    function beforeSave() {

        // set default status to new
        if ($this->user_status == '' && $this->scenario == 'admin-create') {
            $this->user_status = self::STATUS_NEW;
        }


        //encrypt the password supplied by the user before saving to the database
        if ($this->scenario == 'verification' || $this->scenario == 'reset-password-request')
            $this->password = $this->encrypt($this->new_password);

        if ($this->new_password != '' && $this->scenario == 'profile-pass') {
            $this->password = $this->encrypt($this->new_password);
        }

        // generate verification code if the user is still, not activated
        // on insert and update
        if (($this->user_status == self::STATUS_NEW || $this->user_status == '') && $this->scenario != 'verification') {
            $this->verification_code = $this->encrypt(date('A:s:Y:i:m:h:d:l:F'));
        }

        if ($this->valid_from == 'now') {
            $this->valid_from = date('Y-m-d');
        }else if ($this->valid_from != '') {
            //$this->valid_from = date('Y-m-d', strtotime($this->valid_from));
            $this->valid_from = CDateTimeParser::parse($this->valid_from, 'dd/MM/yyyy');
            $this->valid_from = date('Y-m-d',$this->valid_from);
        }

        if ($this->valid_to == 'never') {
            $this->valid_to = Yii::app()->params['never_date'];
        }else if ($this->valid_to != '') {
            //$this->valid_to = date('Y-m-d', strtotime($this->valid_to));
            $this->valid_to = CDateTimeParser::parse($this->valid_to, 'dd/MM/yyyy');
            $this->valid_to = date('Y-m-d',$this->valid_to);
        }

        $this->modified_time = new CDbExpression('NOW()');
        
        return parent::beforeSave();
    }

    function afterSave() {

        if ($this->scenario == 'admin-create' || $this->scenario == 'admin-update') {
            // set user role
            $this->setUserRole();

            // set user facility on table user_facility
            $this->setUserFacility();

            // set user client on table user_client
            $this->setUserClient();

            // set default user permissions, facility needs to be set first
            $this->setDefaultPermissions($this->id, $this->facility);
        }
        

        if($this->user_status == self::STATUS_DELETED){
            $this->removeAllUserPermissions();
        }


        return parent::afterSave();
    }

    public function encrypt($value) {
        //create a static salt key string to be added for encrypting the user password
        $salt = Yii::app()->params['encryptionKey'];
        return hash('sha512', $value . $salt);
    }

    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->emailaddress, $this->password);
            if (!$this->_identity->authenticate())
                $this->addError('password', 'Incorrect username or password.');
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->emailaddress, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }

    function checkPublishDateRange() {
        // check publish dates; check whether publish from/start date is less than the publish to/end date
        if (strtotime($this->valid_from) > strtotime($this->valid_to)) {
            $this->addError('valid_from', 'Valid From date should be earlier than the Valid To date.');
        }
    }

    function checkActiveStatus() {

        $user = User::model()->findByAttributes(array('emailaddress'=>$this->emailaddress));

        if($user == null){
            $this->addError('emailaddress', 'Email address is not found.');
        }else if ($user->user_status != self::STATUS_ACTIVE) {
            $this->addError('emailaddress', 'Account is not active. You are not allowed to reset the password.');
        }
    }

    function checkExistingUser() {

        $user = User::model()->findByAttributes(array('emailaddress'=>$this->emailaddress));

        if($user != null){

            $yes_link = CHtml::link('Yes', Yii::app()->getController()->createUrl('/admin/updateuser/'.$user->id.'?activate=yes'));
            $no_link = CHtml::link('No', Yii::app()->getController()->createUrl('/admin/createuser'));

            if($user->user_status == self::STATUS_ACTIVE){
                $this->addError('emailaddress','User already exists in the system. <br>Would you like to edit the existing user? '.$yes_link.' '.$no_link);
            }else{
                $this->addError('emailaddress','User already exists in the system. <br>Would you like to activate and edit the existing user? '.$yes_link.' '.$no_link);
            }
        }
    }

    public function checkRequiredClient()
    {
        $haystack = Yii::app()->params['ClientNotRequired'];
        $needle = (empty($this->role))?'xxx':$this->role;

        if (strpos($haystack, $needle) === false && $this->client === '') {
            $this->addError('client', 'Client is required.');
        }else{
            $this->client = '';
        }
    }

    public function setLoginDetails() {
        // set login information, ip address and time
        $this->updateByPk($this->id, array('last_login_from' => (php_sapi_name() != 'cli') ? $_SERVER['REMOTE_ADDR'] : '', 
            'last_login' => new CDbExpression('NOW()'),
            'last_login_status'=>0) 
        );
    }

    public function checkUserAccess($operation, $params = array(), $override_operation = '') {

        if (!Yii::app()->user->checkAccess($operation, $params)) {
             if (Yii::app()->user->checkAccess($override_operation, $params)) { return;}
            //throw new CHttpException(403, 'You are not allowed to do this.');
            Yii::app()->user->setFlash('auth', 'You are not allowed to access the page.');
            Yii::app()->getController()->redirect(Yii::app()->getController()->createUrl('site/page?view=error'));
        }
    }

    public function isAuthorized($operation, $params = array(), $override_operation = '') {

        if (Yii::app()->user->checkAccess($operation, $params)) {
            return true;
        } else if($override_operation != '' && Yii::app()->user->checkAccess($override_operation, $params)){
            return true;
        }else{

            return false;
        }
    }


    public function setUserRole() {

        if ($this->role != '') {
            AuthAssignment::model()->addUserRole($this->id, $this->role);
        }
    }

    public function setUserFacility() {

        if (!empty($this->facility)) {

            // delete all previously assigned facilities
            //UserFacility::model()->deleteAllByAttributes(array('userid' => $this->id));
            // double check if facility is already assigned
            /*$facility = UserFacility::model()->findByAttributes(array('userid' => $this->id, 'facility' => $this->facility));

            if ($facility == null) {
                $facility = new UserFacility;
                $facility->userid = $this->id;
                $facility->facility = $this->facility;
                $facility->save(false);
            }*/

            // delete all previously associated facility
            UserTaskFacility::model()->deleteAllByAttributes(array('user_id' => $this->id, 'task'=>'AssociateFacility') );
            // associate facility
            UserTaskFacility::model()->addUserTask($this->id, $this->facility, 'AssociateFacility');
            
        } else if ($this->facility === 0) {

            if (count($this->facilities) <= 0 || !is_array($this->facilities)) {
                return;
            }

            // delete all previously associated facility
            UserTaskFacility::model()->deleteAllByAttributes(array('user_id' => $this->id,'task'=>'AssociateFacility') );

            // associate all facilities
            foreach ($this->facilities as $facility_id => $facility_name) {
                if ($facility_id != 0) {
                    // add record to user_facility folder
                    /*
                    $facility = new UserFacility;
                    $facility->userid = $this->id;
                    $facility->facility = $facility_id;
                    $facility->save(false);*/
                    
                    // add record to user_task_facility
                    UserTaskFacility::model()->addUserTask($this->id, $facility_id, 'AssociateFacility');
                }
            }
        }
    }

    public function setUserClient() {

        if (!empty($this->client)) {

            // delete previous assigned clients
            //UserClient::model()->deleteAllByAttributes(array('userid' => $this->id));
            // check previously assigned clients
            /*$client = UserClient::model()->findByAttributes(array('userid' => $this->id, 'client' => $this->client));

            if ($client == null) {
                $client = new UserClient;
                $client->userid = $this->id;
                $client->client = $this->client;
                $client->save(false);
            }*/

            // delete all previosuly associated clients
            UserTaskClient::model()->deleteAllByAttributes(array('user_id'=>$this->id, 'task'=>'AssociateCustomer'));
            // associate client
            UserTaskClient::model()->addUserTask($this->id, $this->client, 'AssociateCustomer');

        } else{

            if (count($this->clients) <= 0 || !is_array($this->clients)) {
                return;
            }

            // get assigned facilities
            $facilities = UserTaskFacility::model()->getAssignFacilitiesByUserIdAndTask($this->id, 'AssociateFacility', 'AssociateAllFacility');

            // delete all previosuly associated clients
            UserTaskClient::model()->deleteAllByAttributes(array('user_id'=>$this->id,'task'=>'AssociateCustomer'));

            // associate all clients
            //foreach ($this->clients as $client_id => $client_name) {
            foreach($facilities as $facility_id => $facility_name){

                //get clients attach to the facility
                $clients = ClientFacility::model()->getClientDetailByFacilityId($facility_id);

                foreach ($clients as $client_id => $client_name) {
                    if ($client_id != 0) {
                        // add record user_client
                        /*
                        $client = new UserClient;
                        $client->userid = $this->id;
                        $client->client = $client_id;
                        $client->save(false);
                        */
                        // add record to user_task_client
                        UserTaskClient::model()->addUserTask($this->id, $client_id, 'AssociateCustomer');
                    }
                }
            }
        }
    }

    public function searchUsers() {

        $criteria = new CDbCriteria;
        $criteria->alias = 'u';
        $criteria->select = "u.id, u.emailaddress, CONCAT(u.first_name, ' ', u.last_name) AS  fullname, u.user_status, a.ItemName as role";
        $criteria->mergeWith(array(
            'join'=>"LEFT JOIN AuthAssignment AS a ON a.userid = u.id
                LEFT JOIN user_task_facility as f on f.user_id = u.id AND f.task = 'AssociateFacility'
                LEFT JOIN user_task_client as c on c.user_id = u.id AND c.task = 'AssociateCustomer' ",
        ));
        

        $status = '<>'.self::STATUS_DELETED;
        $criteria->compare('f.facility_id', $this->facility);
        $criteria->compare('emailaddress', $this->search_user, true);
        $criteria->compare('first_name', $this->search_user, true);
        $criteria->compare('last_name', $this->search_user, true);
        $criteria->compare('user_status', $status);
        
        if(strpos(Yii::app()->params['ClientNotRequired'], $this->primary_role) !== false){

        }else if(count($this->clients) > 0){
            $clients = array_keys($this->clients);
            $criteria->addInCondition('c.client_id', $clients);
        }

        $criteria->group = "u.id";

        $results = User::model()->findAll($criteria);

        $users = array();

        foreach($results as $row){
            //$usermodel = new User();
            //set default attributes
            //$usermodel->attributes = $row;
                    
            $usermodel = User::model()->findByPk($row->id);
            $usermodel->fullname = $row->fullname;
            $usermodel->facility = $this->facility;
            //set permissions
            $usermodel->setAssignedPermissions($usermodel->id);
            $primary_role = $usermodel->getPrimaryRole($usermodel->id);

            if(array_key_exists($primary_role, $this->roles_to_manage)){
                $users[] = $usermodel;
            }
        }

        // set users to member var
        $this->users = $users;
    }

    public function _setAssignOptions($authCheck = true) {

        //$assign_options = array('Roles' => 'Roles', 'Operations' => 'Operations', 'Customers' => 'Customers', 'Facilities' => 'Facilities');
        $assign_options = array();
        // check authorization if user can assign roles
        if ($this->isAuthorized('AssignRole') || $authCheck == false) {
            $assign_options['Roles'] = 'Roles';
        }

        if ($this->isAuthorized('AssignOperation') || $authCheck == false) {
            $assign_options['Operations'] = 'Operations';
        }

        if ($this->isAuthorized('AssignCustomer') || $authCheck == false) {
            //$assign_options['Customers'] = 'Customers';
        }

        if ($this->isAuthorized('AssignFacility') || $authCheck == false) {
            //$assign_options['Facilities'] = 'Facilities';
        }

        $this->_assign_options = $assign_options;

        return $assign_options;
    }

    public function sendVerficationEmail($user) {

        if ($user->user_status == User::STATUS_ACTIVE) {
            return true;
        }
        /*
          $emailaddress = $user->emailaddress;
          $name= Yii::app()->name;
          $subject= $name.' - Account Verification';
          $headers="From: ".$name." ".Yii::app()->params['adminEmail']."\r\n".
          "MIME-Version: 1.0\r\n".
          'Content-type: text/html; charset=iso-8859-1' . "\r\n";


          $body = $this->renderPartial('verify-email', array('model'=>$user), true);

          mail($emailaddress,$subject,$body,$headers);
         * 
         */
        try
        {
  
            $message = new YiiMailMessage;
            $message->view = 'verify-email'; // view file in /protected/views/mail/

            $name = Yii::app()->name;
            $subject = $name . ' - Account Verification';
            //$subject = $message->getHeaders()->get('Subject');
            $message->getHeaders()->get('Subject')->setValue($subject);

            //userModel is passed to the view
            $message->setBody(array('model' => $user), 'text/html');


            $message->addTo($user->emailaddress);
            $message->from = Yii::app()->params['adminEmail'];


            Yii::app()->mail->send($message);

        }catch(Exception $e){
        
        }

        return true;
    }

    public function associateUserToFacility($facility_id, $user_id, $task) {
        $sql = "INSERT INTO user_task_facility (facility_id, user_id,
            task) VALUES (:facility_id, :user_id, :task)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":facility_id", $facility_id, PDO::PARAM_INT);
        $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
        $command->bindValue(":task", $task, PDO::PARAM_STR);
        return $command->execute() == 1 ? true : false;
    }

    public function associateUserToClient($client_id, $user_id, $task) {
        $sql = "INSERT INTO user_task_client (client_id, user_id,
            task) VALUES (:client_id, :user_id, :task)";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":client_id", $client_id, PDO::PARAM_INT);
        $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
        $command->bindValue(":task", $task, PDO::PARAM_STR);
        return $command->execute() == 1 ? true : false;
    }

    public function removeUserFromFacility($facility_id, $user_id, $task) {
        $sql = "DELETE FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":facility_id", $facility_id, PDO::PARAM_INT);
        $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
        $command->bindValue(":task", $task, PDO::PARAM_STR);
        return $command->execute() == 1 ? true : false;
    }

    public function removeUserFromClient($client_id, $user_id, $task) {
        $sql = "DELETE FROM user_task_client WHERE client_id=:client_id AND user_id=:user_id AND task=:task";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":client_id", $client_id, PDO::PARAM_INT);
        $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
        $command->bindValue(":task", $task, PDO::PARAM_STR);
        return $command->execute() == 1 ? true : false;
    }

    public function isUserInFacility($task) {

        $user_id = Yii::app()->user->getId();

        if ($user_id == 0 || $user_id == null) {
            $user_id = $this->id;
        }

        if ($this->facility == 0) {
            // if no facility id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_facility WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_facility WHERE facility_id=:facility_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":facility_id", $this->facility, PDO::PARAM_INT);
            $command->bindValue(":user_id", $user_id, PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

    public function isUserInClient($task) {

        return true;

        if ($this->client == 0) {
            // if no client id is set/choosen, just check if the user has the task assigned
            $sql = "SELECT task FROM user_task_client WHERE user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        } else {
            $sql = "SELECT task FROM user_task_client WHERE client_id=:client_id AND user_id=:user_id AND task=:task";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":client_id", $this->client, PDO::PARAM_INT);
            $command->bindValue(":user_id", Yii::app()->user->getId(), PDO::PARAM_INT);
            $command->bindValue(":task", $task, PDO::PARAM_STR);
        }

        return $command->execute() >= 1 ? true : false;
    }

    public function isUserHasRole($user_code) {

        if (count($this->roles) <= 0) {
            // set roles for  login user
            $user_id = Yii::app()->user->id;
            $this->setRoles($user_id);
        }

        if (in_array($user_code, $this->roles)) {
            return true;
        } else {
            return false;
        }
    }

    public function isProfileOwner() {

        if ($this->id == Yii::app()->user->id) {
            return true;
        } else {
            return false;
        }
    }

    public function getStatusList() {

        /*return array(self::STATUS_NEW, self::STATUS_UNCONFIRMED, self::STATUS_ACTIVE, self::STATUS_INACTIVE,
            self::STATUS_LOCKED, self::STATUS_DEACTIVATED, self::STATUS_DELETED);
            */
         $status = UserStatus::model()->getStatusList();
         return $status;   
    }
    /*
    
     */
    
    public function getPrimaryRole($user_id = 0)
    {
        if($user_id == 0){
            $user_id = $this->id;
        }

        // used yii authmanager
        $auth = Yii::app()->authManager;
        $roles = $auth->getRoles($user_id);

        if(empty($roles)){
            return false;
        }else{
            foreach($roles as $role){
                // get the first role assign
                return $role->name;
            }
        }

    }

    public function setRoles($user_id) {

        // initialize authitem to get the inherited roles
        $authItem = new AuthItem;

        // used yii authmanager
        $auth = Yii::app()->authManager;
        $roles = $auth->getRoles($user_id);
        $this->roles = array();

        foreach ($roles as $role) { 
            $this->roles[$role->name] = $role->description;
            // get inherited roles
            $authItem->setAuthInheritedItems($role->name, $this->roles, 2);
        }//end first foreach
    }

    public function setInheritedPermissions($user_id = 0) {

        if ($user_id == 0) {
            // set the models id
            $user_id = $this->id;
        }

        $primary_role = $this->getPrimaryRole($user_id);

        // set direclty assigned permissions
        $authItem = new AuthItem;
        $authItem->setAssignedPermissions($primary_role);

        $this->permissions = $authItem->assigned_permissions;
        
        $this->portallogin_permissions = $authItem->getPermissionOptions('PortalLoginGroup');
        $this->portalconfiguration_permissions = $authItem->getPermissionOptions('PortalConfigurationGroup');    
        $this->customers_permissions = $authItem->getPermissionOptions('CustomerGroup');
        $this->facility_permissions = $authItem->getPermissionOptions('FacilityGroup');
        $this->role_permissions = $authItem->getPermissionOptions('RoleGroup');
        $this->users_permissions = $authItem->getPermissionOptions('UserGroup');
        $this->proximitycard_permissions = $authItem->getPermissionOptions('ProximityCardGroup');
        $this->emf_permissions = $authItem->getPermissionOptions('EMFGroup');
        $this->dcaccess_permissions = $authItem->getPermissionOptions('DCAccessGroup');
        $this->telcoaccess_permissions = $authItem->getPermissionOptions('TelcoAccessGroup');
        $this->remotehands_permissions = $authItem->getPermissionOptions('RemoteHandsGroup');
        $this->bulletinboard_permissions = $authItem->getPermissionOptions('BulletinBoardGroup');
        $this->library_permissions = $authItem->getPermissionOptions('LibraryGroup');
        $this->report_permissions = $authItem->getPermissionOptions('ReportGroup');

    }


    public function setAssignedPermissions($user_id = 0) {

        if ($user_id == 0) {
            // set the models id
            $user_id = $this->id;
        }

        $primary_role = $this->getPrimaryRole($user_id);

        // set direclty assigned permissions
        $authItem = new AuthItem;
        $authItem->setPermissionItems($primary_role);


        // set assigned portal permissions    
        $temp_permissions = $authItem->getAuthItemChildren('PortalLoginGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->portallogin_permission = $permissions;

        //set customer permissions
        $temp_permissions = $authItem->getAuthItemChildren('CustomerGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->customers_permission = $permissions;

        //set facility permissions
        $temp_permissions = $authItem->getAuthItemChildren('FacilityGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->facility_permission = $permissions;

        // set role group
        $temp_permissions = $authItem->getAuthItemChildren('RoleGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->role_permission = $permissions;

        //set user permissions
        $temp_permissions = $authItem->getAuthItemChildren('UserGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->users_permission = $permissions;

        //set proximity card permissions
        $temp_permissions = $authItem->getAuthItemChildren('ProximityCardGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->proximitycard_permission = $permissions;

        //set emf permissions
        $temp_permissions = $authItem->getAuthItemChildren('EMFGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->emf_permission = $permissions;

        //set dc access permissions
        $temp_permissions = $authItem->getAuthItemChildren('DCAccessGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->dcaccess_permission = $permissions;

        //set telco access permissions
        $temp_permissions = $authItem->getAuthItemChildren('TelcoAccessGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->telcoaccess_permission = $permissions;

        //set remote hands permissions
        $temp_permissions = $authItem->getAuthItemChildren('RemoteHandsGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->remotehands_permission = $permissions;

        //set bulletin board permissions
        $temp_permissions = $authItem->getAuthItemChildren('BulletinBoardGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->bulletinboard_permission = $permissions;

        //set library permissions
        $temp_permissions = $authItem->getAuthItemChildren('LibraryGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->library_permission = $permissions;

        //set report permissions
        $temp_permissions = $authItem->getAuthItemChildren('ReportGroup');
        $permissions = $this->getAssignedTask($user_id, $this->facility, $temp_permissions);
        $this->report_permission = $permissions;
    }

    public function getAssignedTask($user_id, $facility_id, $tasks) {

        $assigned_task = array();

        foreach ($tasks as $code => $desc) {
            if (UserTaskFacility::model()->isUserTaskAssigned($user_id, $facility_id, $code)) {
                $assigned_task[] = $code;
            }
        }

        $assigned_task = implode(',', $assigned_task);

        return $assigned_task;
    }

    public function updateUserPermissions($field, $permissions) {

        switch ($field) {
            case 'role':
                AuthAssignment::model()->addUserRole($this->id, $permissions);
                break;
            case 'user_status':
                $this->updateUserStatus($permissions);
                break;
            case 'portallogin_permission':
                $this->updateUserTasks($this->id, $this->facility, 'PortalLoginGroup', $permissions);
                break;
            case 'portalconfiguration_permission':
                $this->updateUserTasks($this->id, $this->facility, 'PortalConfigurationGroup', $permissions);
                break;    
            case 'customers_permission':
                $this->updateUserTasks($this->id, $this->facility, 'CustomerGroup', $permissions);
                break;
            case 'facility_permission':
                $this->updateUserTasks($this->id, $this->facility, 'FacilityGroup', $permissions);
                break;
            case 'role_permission':
                $this->updateUserTasks($this->id, $this->facility, 'RoleGroup', $permissions);
                break;       
            case 'users_permission':
                $this->updateUserTasks($this->id, $this->facility, 'UserGroup', $permissions);
                break;
            case 'proximitycard_permission':
                $this->updateUserTasks($this->id, $this->facility, 'ProximityCardGroup', $permissions);
                break;
            case 'emf_permission':
                $this->updateUserTasks($this->id, $this->facility, 'EMFGroup', $permissions);
                break;
            case 'dcaccess_permission':
                $this->updateUserTasks($this->id, $this->facility, 'DCAccessGroup', $permissions);
                break;
            case 'telcoaccess_permission':
                $this->updateUserTasks($this->id, $this->facility, 'TelcoAccessGroup', $permissions);
                break;
            case 'remotehands_permission':
                $this->updateUserTasks($this->id, $this->facility, 'RemoteHandsGroup', $permissions);
                break;
            case 'bulletinboard_permission':
                $this->updateUserTasks($this->id, $this->facility, 'BulletinBoardGroup', $permissions);
                break;
            case 'library_permission':
                $this->updateUserTasks($this->id, $this->facility, 'LibraryGroup', $permissions);
                break;
            case 'report_permission':
                $this->updateUserTasks($this->id, $this->facility, 'ReportGroup', $permissions);
                break;
        }
    }

    public function updateUserTasks($user_id, $facility_id, $main_task, $new_tasks) {

        $authItem = new AuthItem;

        // get previous assigned task    
        $previous_tasks = $authItem->getAuthItemChildren($main_task);

        if (count($previous_tasks) > 0) {
            $previous_tasks = array_keys($previous_tasks);
        } else {
            $previous_tasks = array();
        }

        if($facility_id == 0){

            foreach($this->facilities as $facility_id => $facility_name){
                foreach ($previous_tasks as $task) {
                    UserTaskFacility::model()->deleteUserTaskByUserIdAndFacilityIdAndTask($user_id, $facility_id, $task);
                }

                if (is_array($new_tasks)) {
                    UserTaskFacility::model()->addUserTasks($user_id, $facility_id, $new_tasks);
                } else if ($new_tasks != '' && !empty($new_tasks) && $new_tasks != 'temp') {
                    UserTaskFacility::model()->addUserTask($user_id, $facility_id, $new_tasks);
                }
            }

        }else{

            foreach ($previous_tasks as $task) {
                UserTaskFacility::model()->deleteUserTaskByUserIdAndFacilityIdAndTask($user_id, $facility_id, $task);
            }

            if (is_array($new_tasks)) {
                UserTaskFacility::model()->addUserTasks($user_id, $facility_id, $new_tasks);
            } else if ($new_tasks != '' && !empty($new_tasks) && $new_tasks != 'temp') {
                UserTaskFacility::model()->addUserTask($user_id, $facility_id, $new_tasks);
            }
        }
    }

    public function updateUserTaskItems($user_id, $facility_id, $new_tasks) {

        if($facility_id == 0){
            foreach($this->facilities as $facility_id => $facility_name){
                if (is_array($new_tasks)) {
                    UserTaskFacility::model()->addUserTasks($user_id, $facility_id, $new_tasks);
                } else if ($new_tasks != '' && !empty($new_tasks) && $new_tasks != 'temp') {
                    UserTaskFacility::model()->addUserTask($user_id, $facility_id, $new_tasks);
                }
            }    
        }else{     
            if (is_array($new_tasks)) {
                UserTaskFacility::model()->addUserTasks($user_id, $facility_id, $new_tasks);
            } else if ($new_tasks != '' && !empty($new_tasks) && $new_tasks != 'temp') {
                UserTaskFacility::model()->addUserTask($user_id, $facility_id, $new_tasks);
            }
        }
    }

    public function setDefaultPermissions($user_id, $facility_id) {
        // set inherited permissions
        //$this->setInheritedPermissions($user_id);
        
        // set direclty assigned permissions
        $authItem = new AuthItem;
        $authItem->setPermissionItems($this->role);

        // set id and facility field for updatePermissions field 
        $this->id = $user_id;
        $this->facility = $facility_id;

        // get all the permissions inside group when the user has permission to the task
        // and save it to user_task tables
        if (array_key_exists('ManagePortalLogin', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('portallogin_permission', $authItem->portallogin_permissions);
        }

        if (array_key_exists('ManageConfiguration', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('portalconfiguration_permission', $authItem->portalconfiguration_permissions);
        }

        if (array_key_exists('ManageCustomer', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('customers_permission', $authItem->customers_permissions);
        }

        if (array_key_exists('ManageFacility', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('facility_permission', $authItem->facility_permissions);
        }

        if (array_key_exists('ManageRole', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('role_permission', $authItem->role_permissions);
        }

        if (array_key_exists('ManageUser', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('users_permission', $authItem->users_permissions);
        }

        if (array_key_exists('ManageProximityCard', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('proximitycard_permission', $authItem->proximitycard_permissions);
        }

        if (array_key_exists('ManageEMF', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('emf_permission', $authItem->emf_permissions);
        }

        if (array_key_exists('ManageDCAccess', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('dcaccess_permission', $authItem->dcaccess_permissions);
        }

        if (array_key_exists('ManageTelcoAccess', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('telcoaccess_permission', $authItem->telcoaccess_permissions);
        }

        if (array_key_exists('ManageRemoteHands', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('remotehands_permission', $authItem->remotehands_permissions);
        }

        if (array_key_exists('ManagePost', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('bulletinboard_permission', $authItem->bulletinboard_permissions);
        }

        if (array_key_exists('ManageLibrary', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('library_permission', $authItem->library_permissions);
        }

        if (array_key_exists('ManageReport', $authItem->assigned_permissions)) {
            $this->updateUserPermissions('report_permission', $authItem->report_permissions);
        }

        // set individual permission items, directly assigned to role on authitemchild
        $permission_items = $authItem->getAuthItemChildren($this->role);
        $this->updateUserTaskItems($this->id, $this->facility, $permission_items);
        
    }


    public function getUserClients($user_id = 0) {

        if ($user_id == 0) {
            $user_id = $this->id;
        }
        
        //$this->clients = UserClient::model()->getClients($user_id);
        $this->clients = UserTaskClient::model()->getAssignClientsByUserIdAndTask($user_id, 'AssociateCustomer');	
        
        return $this->clients;
    }
    
    public function printStringFromArrayValues($str, $arr){
	    
	    $keys = explode(',', $str);
	    $new_str = '';
	    
	    if(count($keys) <= 0){
		    return;
	    }
	    
	    foreach($keys as $key){
	    	if(array_key_exists($key, $arr)){
		    	$new_str .= $arr[$key].", ";
		    }
	    }
	    
	    $new_str = rtrim($new_str, ', ');
	    
	    return $new_str;
    }

    public function removeAllUserPermissions()
    {
        //remove all permissions
        $authitem = new AuthItem;
        $permissions = $authitem->permission_list;
        foreach($permissions as $code => $detail){
            $this->updateUserTasks($this->id, $this->facility, $detail['group'], '');
        } 
    }

    public function updateUserStatus($new_status)
    {
        $this->user_status = $new_status;
        $this->scenario = 'disable';
        $this->save();
    }
    
    public function checkLockedStatus($email_address)
    {   
        // get user detail
        $user = User::model()->findByAttributes(array('emailaddress'=>$email_address));
        // read configuration
        $configuration = new SystemConfigurationForm;
        $configuration->read();

        if($user != null){
            if($user->last_login_status + 1 < $configuration->locked_limit){
                User::model()->updateByPk($user->id, array('last_login_status' => $user->last_login_status + 1));
            }else{
                User::model()->updateByPk($user->id, array('last_login_status'=>$user->last_login_status + 1,'user_status'=>self::STATUS_LOCKED));
            }
        }

    }

    public function sendRequestResetNotification($user)
    {
        try
        {
            $message = new YiiMailMessage;
            $message->view = 'request-reset-email'; // view file in /protected/views/mail/

            $name = Yii::app()->name;
            $subject = $name . ' - Request Reset Password Notification';
            //$subject = $message->getHeaders()->get('Subject');
            $message->getHeaders()->get('Subject')->setValue($subject);

            //userModel is passed to the view
            $message->setBody(array('model' => $user), 'text/html');


            $message->addTo($user->emailaddress);
            $message->from = Yii::app()->params['adminEmail'];


            Yii::app()->mail->send($message);

        }catch(Exception $e){
        
        }
    }

    public function sendResetPasswordNotification($user)
    {
        try
        {
            $message = new YiiMailMessage;
            $message->view = 'reset-password-email'; // view file in /protected/views/mail/

            $name = Yii::app()->name;
            $subject = $name . ' - Reset Password Notification';
            //$subject = $message->getHeaders()->get('Subject');
            $message->getHeaders()->get('Subject')->setValue($subject);

            //userModel is passed to the view
            $message->setBody(array('model' => $user), 'text/html');


            $message->addTo($user->emailaddress);
            $message->from = Yii::app()->params['adminEmail'];


            Yii::app()->mail->send($message);

        }catch(Exception $e){
        
        }
    }

    public function setRolesToManage($primary_role)
    {
        $roles = array();
        //$roles[$primary_role] = $primary_role;
        
        AuthItem::model()->setAuthInheritedItems($primary_role, $roles, 2);

        $this->roles_to_manage = $roles;
    }

}