<?php

//ini_set("memory_limit", "2048M");
//ini_set("max_execution_time", "0");

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
Yii::import('application.vendors.jpgraph.*');

class GraphComponent extends CComponent {

    public $directory = '';
    public $filename = '';
    public $title = '';
    public $subtitle = '';
    public $width = 800;
    public $height = 600;
    public $xdata = array();
    public $ydata = array();
    public $num_of_results = 0;
    public $num_of_yaxis = 0;
    public $from_date;
    public $to_date;
    public $date_format = 'timestamp';
    public $type;
    public $target_id = 0;

    public function setDateLine2Graph($display = false, $filename = 'images/dateline2.png') {

        require_once('jpgraph.php');
        require_once('jpgraph_line.php');

        function DateTimeFormat($aVal) {
            return Date('Y-m-d G:i', $aVal);
        }

        // Some (random) data

        $ydata = array(170, 150, 180, 140, 170, 150, 180, 160, 190);
        $ydata2 = array(340, 340, 340, 340, 340, 340, 340, 340, 340);
        $ynum = count($ydata);

        $xdata = array(
            strtotime('2013-05-23 01:41'),
            strtotime('2013-05-23 03:21'),
            strtotime('2013-05-23 05:06'),
            strtotime('2013-05-23 06:46'),
            strtotime('2013-05-23 08:36'),
            strtotime('2013-05-23 10:15'),
            strtotime('2013-05-23 11:56'),
            strtotime('2013-05-23 13:36'),
            strtotime('2013-05-23 15:16')
        );

        $xnum = count($xdata);

        // Size of the overall graph
        $width = $this->width;
        $height = $this->height;

        // Create the graph and set a scale.
        // These two calls are always required
        $graph = new Graph($width, $height);

        $graph->SetMargin(100, 100, 50, 120);

        $graph->title->Set($this->title);
        $graph->subtitle->Set($this->subtitle);

        $graph->SetAlphaBlending();

        //$graph->SetScale('intlin');
        // Setup a manual x-scale (We leave the sentinels for the
        // Y-axis at 0 which will then autoscale the Y-axis.)
        // We could also use autoscaling for the x-axis but then it
        // probably will start a little bit earlier than the first value
        // to make the first value an even number as it sees the timestamp
        // as an normal integer value.
        $graph->SetScale("intlin", 0, 450, $xdata[0], $xdata[$xnum - 1]);

        $graph->xaxis->SetLabelFormatCallback('DateTimeFormat');
        $graph->xaxis->SetLabelAngle(45);

        $graph->yaxis->title->Set('KW');
        $graph->yaxis->title->SetMargin(30);

        //$graph->xaxis->title->Set('Time');
        $graph->xaxis->SetTitle('Time', 'center');
        $graph->xaxis->title->SetMargin(80);

        $graph->xaxis->title->SetPos('center', 'bottom');

        // Create the linear plot
        $lineplot = new LinePlot($ydata, $xdata);

        $lineplot->SetColor("blue");


        // Add the plot to the graph
        $graph->Add($lineplot);

        $lineplot2 = new LinePlot($ydata2, $xdata);
        $lineplot2->SetColor("red");
        // Add the plot to the graph
        $graph->Add($lineplot2);

        $lineplot->SetLegend('Load');
        $lineplot2->SetLegend('Capacity');

        $graph->legend->SetAbsPos(10, 10, 'right', 'top');
        $graph->legend->SetLayout(LEGEND_VERT);
        $graph->legend->SetLineWeight(2);
        $graph->legend->SetShadow();

        // Display the graph
        if ($display) {
            $graph->Stroke();
        } else {
            $graph->Stroke($filename);
            $this->filename = $filename;
        }
    }

    public function setData($data, $from, $to, $type, $target_id = 0) {

        $this->type = $type;
        $this->from_date = $from;
        $this->to_date = $to;
        $this->target_id = $target_id;

        // set ydata and xdata var

        $this->num_of_results = count($data);

        if ($this->num_of_results > 0) {
            $num_of_fields = count($data[0]);
        } else {
            $num_of_fields = 0;
        }

        $this->num_of_yaxis = $num_of_fields - 1;


        for ($i = 0; $i < $this->num_of_yaxis; $i++) {
            $this->ydata[$i] = array();
        }

        for ($i = 0; $i < $this->num_of_results; $i++) {
            $row = $data[$i];
            array_push($this->xdata, $row[0]);
            for ($j = 0; $j < $this->num_of_yaxis; $j++) {
                array_push($this->ydata[$j], $row[$j + 1]);
            }
        }

        $timezone = new DateTimeZone("Australia/Sydney");
        $offset = $timezone->getOffset(new DateTime("now")); // Offset in seconds

        $xtemp = 0;

        for ($i = 0; $i < $this->num_of_results; $i++) {
            $xtemp = $this->xdata[$i] + $offset;
            $this->xdata[$i] = $xtemp;
        }
    }

    public function setTitle($title) {

        $from_date = date('d/m/Y G:i:s', $this->from_date);
        $to_date = date('d/m/Y G:i:s', $this->to_date);

        if (($this->type == "kW") || ($this->type == "kWh")) {
            $type = ($this->type == "kWh")?"kW-h":$this->type;
            if ($this->target_id) {
                $this->title = $type . " for " . $title . "\n" . $from_date . " to " . $to_date;
            } else {
                $this->title = $type . " Total\n" . $from_date . " to " . $to_date;
            }
        } else {
            $this->title = $this->type . " for " . $title . "\n" . $from_date . " to " . $to_date;
        }
    }

    public function setSubTitle($client_name, $facility_name) {

        $this->subtitle = $facility_name . " \n " . $client_name;
    }

    public function plotGraph() {

        require_once('jpgraph.php');
        require_once('jpgraph_line.php');
        require_once('jpgraph_date.php');

        // inner function for array walk callback
        if(!function_exists('formatDate')) {

            function formatDate(&$aVal) {
                $aVal = date('d-m-Y H:i', $aVal);
            }

        }

        $xmin = min($this->xdata);
        $xmax = max($this->xdata);
        $this->from_date = date('d/m/Y-H:i:s', $xmin);
        $this->to_date = date('d/m/Y-H:i:s', $xmax);

        array_walk($this->xdata, 'formatDate');

        $ymax = 0;
        for ($i = 0; $i < $this->num_of_yaxis; $i++) {
            if ($ymax < max($this->ydata[$i])) {
                $ymax = max($this->ydata[$i]);
            }
        }

        if ($ymax == 0) {
            $ymax = 1;
            for ($i = 0; $i < $this->num_of_yaxis; $i++) {
                for ($j = 0; $j < $this->num_of_results; $j++) {
                    $this->ydata[$i][$j] = 0.003;
                }
            }
        } else {
            $ymax = $ymax * 1.1;
        }

        $date = date("YmdGis");
        $this->filename = 'images/jpgraph/jpgraph' . $date . '.png';

        if ($this->directory == '') {
            $this->directory = dirname(Yii::app()->getBasePath()) . '/' . $this->filename;
        } else {
            $this->directory .= $this->filename;
        }

        $graph = new Graph($this->width, $this->height);
        $graph->title->Set($this->title);
        $graph->subtitle->Set($this->subtitle);
        $graph->SetScale('datlin');

        $graph->SetMarginColor('green@0.95');

        $graph->SetMargin(100, 20, 70, 200);

        $graph->xaxis->SetTickSide(SIDE_BOTTOM);
        $graph->yaxis->SetTickSide(SIDE_LEFT);

        $graph->yaxis->scale->SetAutoMin(0);

        $lineplots = array();

        for ($i = 0; $i < $this->num_of_yaxis; $i++) {
            $lineplots[$i] = new LinePlot($this->ydata[$i]);
            $graph->Add($lineplots[$i]);
        }

        for ($i = 0; $i < $this->num_of_yaxis; $i++) {
            if ($this->num_of_yaxis > 1) {
                if ($i == 0) {
                    $lineplots[$i]->SetBarCenter();
                    $lineplots[$i]->SetColor("blue");
                    $lineplots[$i]->SetLegend("Load");
                }
                if ($i == 1) {
                    $lineplots[$i]->SetBarCenter();
                    $lineplots[$i]->SetColor("red");
                    $lineplots[$i]->SetLegend("Capacity");
                }
            }

            $lineplots[$i]->SetFillFromYMin();
            $lineplots[$i]->SetStyle("solid");
        }

        $graph->legend->SetReverse();

        $graph->xaxis->SetTickLabels($this->xdata);

        $graph->xaxis->SetTextLabelInterval(2);

        $graph->xaxis->SetLabelAngle(45);
        $graph->xaxis->HideFirstTickLabel();
        $graph->xaxis->HideLastTickLabel();
        $graph->xaxis->SetTitle('Date', 'middle');
        $graph->xaxis->title->SetMargin(70);
        $graph->yaxis->scale->SetAutoMax($ymax);
        $graph->yaxis->title->Set($this->type);
        $graph->yaxis->title->SetMargin(30);
        $graph->yaxis->HideZeroLabel();
        $graph->legend->SetShadow('gray@0.4', 5);
        $graph->legend->SetPos(0, 0, 'right', 'right');
        $graph->legend->SetColumns(1);
        
        //echo $this->directory;
        
        $graph->Stroke($this->directory);

        $this->deleteOldGraphs();
    }

    public function deleteOldGraphs() {


        foreach (glob(Yii::app()->params['RootDirectory'] . 'images/jpgraph/*') as $filename) {
            $file_age = time() - filemtime($filename);
            if ($file_age > Yii::app()->params['TimeLimit']) {
                unlink($filename);
            }
        }
    }

}