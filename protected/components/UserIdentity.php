<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private $_id;

    const ERROR_NOT_ACTIVE = 3;
    const ERROR_NO_PORTAL_LOGIN = 4;
    const ERROR_LOCKED = 5;

    public $_encrypt = true;

    public function authenticate() {
        //get the user detail from the user table using the login username		
        $user = User::model()->findByAttributes(array('emailaddress' => $this->username));


        if ($user == null)//no record found on username
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($user->user_status == User::STATUS_LOCKED)
            $this->errorCode = self::ERROR_LOCKED;
        elseif ($user->user_status != User::STATUS_ACTIVE)
            $this->errorCode = self::ERROR_NOT_ACTIVE; // set error if user status is not active, new, deleted, inactive
        elseif ($user->password !== $user->encrypt($this->password) && $this->_encrypt == true) //user supplied password does not match on record
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        elseif ($user->password !== $this->password && $this->_encrypt == false) //user supplied password does not match on record, special override for test case
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {

            $this->_id = $user->id;
            
            if ($user->isUserInFacility('PortalLogin') == false) {
                $this->errorCode = self::ERROR_NO_PORTAL_LOGIN;
            } else {
                $this->errorCode = self::ERROR_NONE;

                //set user's name for header menu
                if ($user->first_name != '') {
                    // if first name is set, use it as user's name, else use the email address as name
                    // previous user's do not require the first name and last name
                    $this->setState('name', $user->first_name . ' ' . $user->last_name);
                }
                // set login details like ip address and date
                $user->setLoginDetails(); // update login details like remote ip, and date
            }
            
        }

        if(!$this->errorCode == false){
            User::model()->checkLockedStatus($this->username);
        }

        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

    public static function isUserExluded() {

        // used yii authmanager
        $auth = Yii::app()->authManager;
        $roles = $auth->getRoles(Yii::app()->user->id);

        foreach ($roles as $role) {

            if (in_array($role->name, Yii::app()->params['BizruleExclusion'])) {
                return true;
            }
        }

        return false;
    }

}