drop table if exists `AuthAssignment`;
drop table if exists `AuthItemChild`;
drop table if exists `AuthItem`;

create table `AuthItem`
(
   `name`                 varchar(64) not null,
   `type`                 integer not null,
   `description`          text,
   `bizrule`              text,
   `data`                 text,
   primary key (`name`)
) engine InnoDB;

create table `AuthItemChild`
(
   `parent`               varchar(64) not null,
   `child`                varchar(64) not null,
   primary key (`parent`,`child`),
   foreign key (`parent`) references `AuthItem` (`name`) on delete cascade on update cascade,
   foreign key (`child`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;

create table `AuthAssignment`
(
   `itemname`             varchar(64) not null,
   `userid`               varchar(64) not null,
   `bizrule`              text,
   `data`                 text,
   primary key (`itemname`,`userid`),
   foreign key (`itemname`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;



CREATE TABLE IF NOT EXISTS `client` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `client_name` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4;

INSERT INTO `client` (`record_id`, `client_id`, `client_name`, `modified_time`) VALUES
(1, 215, 'University of Melbourne', NULL),
(2, 207, 'Monash University', NULL),
(3, 211, 'RMIT', NULL);


CREATE TABLE IF NOT EXISTS `facility` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) NOT NULL,
  `facility_name` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `facility_tz` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `facility_id` (`facility_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `facility` (`record_id`, `facility_id`, `facility_name`, `facility_tz`, `state`, `modified_time`) VALUES
(1, 5, 'Noble Park', '', 'VIC', NULL),
(2, 8, 'Malaga', '', 'WA', NULL);


CREATE TABLE IF NOT EXISTS `client_facility` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `Facility_id` int(11) NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

INSERT INTO `client_facility` (`record_id`, `client_id`, `Facility_id`, `modified_time`) VALUES
(1, 215, 5, NULL),
(2, 207, 5, NULL),
(3, 211, 5, NULL);


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` char(128) NOT NULL,
  `password` char(128) NOT NULL,
  `emailaddress` char(128) DEFAULT NULL,
  `alternateemailaddress` char(128) DEFAULT NULL,
  `officephone` char(20) DEFAULT NULL,
  `officefax` char(20) DEFAULT NULL,
  `mobilephone` char(20) DEFAULT NULL,
  `client` int(11) DEFAULT NULL,
  `facility` int(11) DEFAULT NULL,
  `user_status` int(10) unsigned NOT NULL DEFAULT '0',
  `user_role` varchar(64) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_from` char(48) DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client` (`client`),
  KEY `facility` (`facility`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

INSERT INTO `user` (`id`, `login`, `password`, `emailaddress`, `alternateemailaddress`, `officephone`, `officefax`, `mobilephone`, `client`, `facility`, `user_status`, `user_role`, `valid_from`, `valid_to`, `last_login`, `last_login_from`, `modified_by`, `modified_time`) VALUES
(1, 'superadmin', 'c8c495bd676082ea74899a5afa000758bf29a43be6beb9dde93e6be24a2156a3b298f2388935e51eef5a65e6029bbbe17859d959cde58da85bd54537ed1e5cf0', 'kramx7@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'SuperAdmin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-04-17 22:40:02', '180.190.163.31', 0, NULL),
(2, 'cust1', '5856174e4d7ecd0f019077583541c012f0b5f99f5894b1aff64d1a7bdc122cce955beeda50a573a9e3d1a408af855444680b0fb28736448c06bc3f13cdb3036a', 'cust@gmail.com', '', '', '', '', NULL, NULL, 0, 'Cust_User', '2013-04-17 00:00:00', '2013-04-30 00:00:00', '2013-04-17 22:41:32', '180.190.163.31', 0, '2013-04-17 00:30:56'),
(3, 'unimelb', 'd0abd5cf2013c7a5eda680d1a6bb63b80a822f70a24ee47a2a4cdcf58ca16bb08347f643deadc005930ed678c3011339e5d1bb47d2d12b19250a2017ee2346bb', 'test@melb.uni.edu.au', '', '', '', '', NULL, NULL, 0, 'Fac_Admin', '2013-04-25 00:00:00', '2013-04-30 00:00:00', '2013-04-17 07:12:24', '110.142.217.231', 0, '2013-04-17 07:11:53');

ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`client`) REFERENCES `client` (`client_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`facility`) REFERENCES `facility` (`facility_id`) ON DELETE CASCADE;
  
  
CREATE TABLE `user`(
`id` Int UNSIGNED NOT NULL AUTO_INCREMENT,
`login` Char(128) NOT NULL,
`password` Char(128) NOT NULL,
`emailaddress` Char(128),
`alternateemailaddress` Char(128),
`officephone` Char(20),
`officefax` Char(20),
`mobilephone` Char(20),
`client` int(11) UNSIGNED,
`facility` int(11) UNSIGNED,
`user_status` Int UNSIGNED NOT NULL DEFAULT 0,
`user_role` Int UNSIGNED NOT NULL DEFAULT 0,
`valid_from` Datetime NOT NULL,
`valid_to` Datetime NOT NULL,
`last_login` Datetime,
`last_login_from` Char(48),
`modified_by` Int NOT NULL,
`modified_time` Datetime,
PRIMARY KEY (`id`),
FOREIGN KEY (`client`) REFERENCES `client` (`client_id`) ON DELETE CASCADE,
FOREIGN KEY (`facility`) REFERENCES `facility` (`facility_id`) ON DELETE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 0;

CREATE TABLE `user_status`(
`status_id` Int NOT NULL,
`status_description` Char(20) NOT NULL,
PRIMARY KEY (`status_id`)
) ENGINE = InnoDB;

INSERT INTO `user_status`(`status_id`,`status_description`)
VALUES(0,'New'),
(1,'Unconfirmed'),
(2,'Active'),
(3,'Inactive'),
(255,'Deleted');

CREATE TABLE `role` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`name` char(30) DEFAULT NULL,
`description` char(200), 
`code` char(30),
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

INSERT INTO `role`(`name`,`description`,`code`)
VALUES('Superadmin','Holds all privileges including system configuration.','SuperAdmin'),
('Application Administrator','Can create, alter, and delete all objects such as Facilities, Customers, and users.  Can delegate this role.','App_Admin'),
('Facility Administrator','Can create, alter, and delete all objects such as customers and users for a specific facility.  Can delegate this role.','Fac_Admin'),
('Customer Administrator','Can create, alter, and delete all objects for a specific customer.  Can delegate this role.','Cust_Admin'),
('Customer User','Read-only plus permissions as delegated by Customer Administrator.','Cust_User'),
('Operations staff','As defined. Restricted to the facility to which the user is assigned.','Op_Staff'),
('Security staff','As defined. Restricted to the facility to which the user is assigned.','Sec_Staff'),
('Implementations staff','As defined. Restricted to the facility to which the user is assigned.','Imp_Staff'),
('Internal Employee','Permissions as delegated by Facility Administrator or assignment of a staff role.','Int_Emp');


CREATE TABLE `noticeboard`(
`id` Int UNSIGNED NOT NULL AUTO_INCREMENT,
`facility` Int(11) UNSIGNED NOT NULL,
`message_status` tinyint NOT NULL DEFAULT 1,
`thread_id` Int UNSIGNED NOT NULL,
`priority` Int UNSIGNED,
`publish_from` Datetime NOT NULL,
`publish_to` Datetime NOT NULL,
`sticky` Tinyint UNSIGNED,
`title` Varchar(80),
`short_message` Varchar(256),
`full_message` Varchar(10240),
`submitted_by` Int UNSIGNED,
`submitted_time` Datetime,
`approver` Int UNSIGNED,
`approved_time` Datetime,
PRIMARY KEY (id)
) ENGINE = InnoDB AUTO_INCREMENT = 0;

CREATE INDEX ix_noticeboard_1 ON `noticeboard` (`facility`);
CREATE INDEX ix_noticeboard_2 ON `noticeboard` (`publish_from`);
CREATE INDEX ix_noticeboard_3 ON `noticeboard` (`publish_to`);
CREATE INDEX ix_noticeboard_4 ON `noticeboard` (`thread_id`);
CREATE INDEX ix_noticeboard_5 ON `noticeboard` (`message_status`);

ALTER TABLE `noticeboard` ADD FOREIGN KEY (`submitted_by`) REFERENCES `user`(`id`) ON DELETE CASCADE;
ALTER TABLE `noticeboard` ADD FOREIGN KEY (`approver`) REFERENCES `user`(`id`) ON DELETE CASCADE;

CREATE TABLE `noticeboard_message_status`(
`message_status` tinyint NOT NULL DEFAULT 1,
`message_status_description` varchar(48) NOT NULL
) ENGINE = InnoDB;

ALTER TABLE `noticeboard_message_status` ADD PRIMARY KEY (`message_status`);

ALTER TABLE `noticeboard` ADD FOREIGN KEY (`message_status`) REFERENCES `noticeboard_message_status`(`message_status`) ON DELETE CASCADE;

INSERT INTO `noticeboard_message_status`(`message_status`,`message_status_description`)
VALUES(0,'New'),
(1,'Draft'),
(2,'Submitted'),
(3,'Approved'),
(4,'Requires Correction'),
(255,'Deleted');

